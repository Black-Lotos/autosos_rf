<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route = load_system_route();
$route['404_override']          = 'static_page/error_pages';
$route['static_page/(.*)']      = "static_page/$0";
$route['administration']        = 'administration/administration';
$route['administration/(.*)'] 	= "administration/$1";
$route['default_controller'] 	= "home";
//$route['page/(.*)'] = "/page/index/$0";
//$route['page(/:any)'] = "page/index/$0"; 
//$route['page/(.*)'] 			= 'home/$1';
///$route['admin/index'] 		= "admin/index";
///$route['admin/logout'] 		= "admin/logout";
///$route['admin/login'] 		= "admin/login";
///$route['admin/content'] 		= "admin/content";
///$route['admin/users']         = "admin/users";

//$route['admin/users/list_users']         = "admin/users/list_users";
///$route['admin/content/(.*)'] 	= "adm/$1";
//$route['admin/users/(.*)'] 	= "adm/$1";
///$route['admin/(.*)'] 			= "adm/$1";

///$route['register'] 				= 'auth/register';
///$route['login'] 				= 'auth/login';
///
