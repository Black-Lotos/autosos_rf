<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Entry_workshop extends MY_Controller {
    //
    public function index() {   
        $inData = array(); $inBlocks = array(); $inKeysCreate=array();
        $inData = $this->includeUp('site_menu',TRUE);
        // установка заголовка
        $inBlocks['header_page'] = getVariable('header_workshop');
        // установка сообщения если необходимо
        $inBlocks['top_message_page'] = getVariable('top_message_workshop');
        // заполняем контент
        $this->load->model('Workshop_model');
        //заполняем марки
		
        $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
        //заполняем модели          
		 
        $this->Model_model->setOrder($this->Model_model->getSelfName(),'ASC');
        //$this->Model_model->Debug();
        $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));   
									                         
            
        $inKeysCreate['form'] = $this->Workshop_model->getForm($inKeysCreate);
        $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
        //echo "<pre>"; var_dump($inBlocks['content']); die();
        // установка футера если необходимозаголовка
        $inBlocks['footer_page'] = getVariable('footer_workshop');
        $inData['page']['content']['right'] = $this->twig->render('blocks/blocks-build-page.twig', array('data'=>$inBlocks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    protected function save() {
        $inData = array(); $inBlocks = array(); $inKeysCreate=array(); $inModel=array();
        $outValidate = true;
        $inData = $this->includeUp('site_menu',TRUE);
        // установка заголовка
        $inBlocks['header_page'] = getVariable('header_workshop');
        // установка сообщения если необходимо
        $inBlocks['top_message_page'] = getVariable('top_message_workshop');
        // установка футера если необходимозаголовка
        $inBlocks['footer_page'] = getVariable('footer_workshop');
        // заполняем контент
        $this->load->model('Workshop_model');
        if ($_POST) {
            //var_dump($_POST);
            $inKeysCreate = $_POST;
            //заполняем марки
            $inActiveCar = $inKeysCreate[$this->Auto_model->getCategoryId()];
            //var_dump($inActiveCar); die();
            $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
								
            $inKeysCreate[$this->Auto_model->getCategoryId()] = setActiveItem($inKeysCreate[$this->Auto_model->getCategoryId()], $inActiveCar);                        
            // модель машины
            $inActiveModel = $inKeysCreate['model_id'];
			$this->Model_model->setOrder('model_name','ASC');
            $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'car_id' => $inActiveCar,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));
									
            $inKeysCreate['model_id'] = setActiveItem($inKeysCreate['model_id'], $inActiveModel);
            // Проверка на валидность полей ввода
            if(filter_input(INPUT_POST,'workshop_phone')) {
                if(!valid_phone(filter_input(INPUT_POST,'workshop_phone'))) {
                    setMessage('Неверно введен телефон.','error');
                    $outValidate = false;
                };
            };
            // Валидность капчи
            //var_dump(getCaptchaValidate()); die();
            if (strtolower(getCaptchaValidate())!=strtolower(filter_input(INPUT_POST,'edtCaptcha'))) {
                $outValidate = false;
                setMessage('Неверно введена каптча.','error');
            } else {
                unset($_POST['edtCaptcha']);
            }
            //var_dump($_POST); die();
            if($outValidate) {
                $inKeysCreate = array();
                               
                if ($this->Workshop_model->save($_POST)) {
                    setMessage('Вы записаны.');
                    $message = $this->Workshop_model->formEmail($_POST,"Обращение на запись в мастерскую");
                    $message = iconv('utf-8', 'windows-1251',$message);
                    $to = getVariable('email_send');
                    $from = getVariable('email_administrator');
                    $subject = iconv('utf-8', 'windows-1251', "Запись в мастерскую.");
                    _email($to, $from, $subject, $message);
                    //die();
                } else setMessage('Ошибка записи на изготовление ключей');
                $inKeysCreate["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
                // устанавливаем по умолчанию
                //марка
                $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
                $inActiveCar = $inKeysCreate[$this->Auto_model->getCategoryId()][0]['value'];
                //модель 
		$this->Model_model->setOrder($this->Model_model->getSelfName(),'ASC');
                $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'car_id' => $inActiveCar,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));
                //
                $inKeysCreate['model_id'] = ($inKeysCreate['model_id'])?array_merge($this->Model_model->getNoItem(),$inModel):$this->Model_model->getNoItem(7);
                // автомобиль
                $inKeysCreate['form'] = $this->Workshop_model->getForm($inKeysCreate);
                $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
            } else {
                $inKeysCreate["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
                $inKeysCreate['form'] = $this->Workshop_model->getForm($inKeysCreate);
                $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
            }
            
            $inData['page']['content']['right'] = $this->twig->render('blocks/blocks-build-page.twig', array('data'=>$inBlocks));
            
        };
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}

