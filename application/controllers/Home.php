<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home extends MY_Controller {
    //
    public function index() {
        
        $inData = array();
        $inData = $this->includeUp('site_menu',true);
        $this->load_page('main_info',$inData);
        $inCar = $this->Auto_model->loadCategory(array($this->Auto_model->getCategoryStatus()=>1,'fields'=>array('car_id as value','car_name as title','images_id','car_url as url')));
        $this->Auto_model->setCategoryOrder(array());
        $inMaps = '<center><p>&nbsp;</p><p><script type="text/javascript" charset="utf-8" src="//api-maps.yandex.ru/services/constructor/1.0/js/?sid=GzYg9ACoAotha5cr803VKvRyHlToFfcN&width=600&height=450"></script></p></center>';
        $inCar = $this->twig->render('avto/list-car-start-up.twig', array('data'=>$inCar));
        $inData['page']['content']['right'] = $inData['page']['content']['right'].$inCar.$inMaps;
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
}

