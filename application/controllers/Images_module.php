<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Images_module extends MY_Controller {
    function __construct() { parent::__construct(); }
    public function index() {   
        echo $this->inImgPath;
    }
     //
    public function ajax_upload_clean() {
        $inRes = array();
        if (filter_input(INPUT_POST, 'images_id') && filter_input(INPUT_POST, 'process')=='images-upload-clean') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $_POST['images_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $this->load->model("Images_model");
            $inData = $this->Images_model->loadById($inId);
            
            if (unlink($this->inImgPath . $inData['images_name'])) {
                
                if ($this->Images_model->deleteById($inId)) {
                    $inRes["rec-no"] = '0';
                    $inRes["rec-name"] = $this->inImgPath . $inData['images_name'];
                    $inRes["rec-message"] = jdecoder("Удалены все входжения");
                }
            } else {
                $inRes["rec-no"] = $inId;
                $inRes["rec-name"] = $this->inImgPath . $inData['images_name'];
                $inRes["rec-message"] = jdecoder("Невозможно удалить");
            }
            echo json_encode($inRes);
        }
    }
    public function ajax_upload() {
        $inFile     = $_FILES['img_file'];
        $inExt      = pathinfo($inFile['name']); $inExt = $inExt['extension'];
        $outName    = md5(time()).".$inExt";
        
        if (move_uploaded_file($inFile["tmp_name"], $this->inImgPath . $outName)){
            
            $inData = array(
                'images_created'=>date('Y-m-d H:i:s',time()),
                'images_folder'=>$this->inImgPath,
                'images_name'=>$outName,
                'images_type'=>T_IMAGE,
                'images_status'=>1);
            $this->load->model("Images_model");
            
            $outRes = $this->Images_model->save($inData);
            //$this->Picture_model->writeWm($this->inImgPath . $outName);
            $outRes['rec-name'] = $this->inImgPath . $outName;
            //$this->Picture_model->writeWm($outFName);
            //$this->Picture_model->save($inDecode);
            //$this->Picture_model->writeWm();
        }else {
            $outRes = array('rec-error'=>'No copy');
        }
        echo json_encode($outRes);
    }
    public function ajax_upload_video() {
        $inFile     = $_FILES['img_file'];
        $inExt      = pathinfo($inFile['name']); $inExt = $inExt['extension'];
        $outName    = 'videos_'.md5(time()).".$inExt";
        if (move_uploaded_file($inFile["tmp_name"], $this->inVideoPath . $outName)){
            $inData = array(
                'images_created'=>date('Y-m-d H:i:s',time()),
                'images_folder'=>$this->inVideoPath,
                'images_name'=>$outName,
                'images_type'=>T_IMAGE,
                'images_status'=>1);
                
            //$this->load->model("Images_model");
            //$outRes = $this->Images_model->save($inData);
            $outRes['rec-name'] = $this->inVideoPath . $outName;
        }else {
            $outRes = array('rec-error'=>'No copy');
        }
        echo json_encode($outRes);
    }
    function getImages($aImageId, $aImageWidth, $aImgeHeight, $aSpectRatio = true) {
        //var_dump($aImageId, $aImageWidth, $aImgeHeight, $aSpectRatio); die();
        $this->load->model('Images_model');
        $this->Images_model->resizePicture((int)$aImageId, (int)$aImageWidth, (int)$aImgeHeight, (bool)$aSpectRatio);
    }
    //
    function getPicture($aImageId, $aImageWidth, $aImgeHeight, $aSpectRatio = true) {
        //var_dump($aImageId, $aImageWidth, $aImgeHeight, $aSpectRatio); die();
        $this->load->model('Picture_model');
        $this->Picture_model->resizePicture((int)$aImageId, (int)$aImageWidth, (int)$aImgeHeight, (bool)$aSpectRatio);
    }
    //
    function getWM($aImageId, $aImageWidth, $aImgeHeight, $aSpectRatio = true) {
        //var_dump($aImageId, $aImageWidth, $aImgeHeight, $aSpectRatio); die();
        $this->load->model('Picture_model');
        $this->Picture_model->resizeWM($aImageId, (int)$aImageWidth, (int)$aImgeHeight, (bool)$aSpectRatio);
    }
}
