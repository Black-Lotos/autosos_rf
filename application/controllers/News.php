<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends MY_Controller {
    //
    public function index() {   
        $inData = $this->includeUp('site_menu',true);
        $this->inOutRecord = getVariable('news_count_by_page');
        $this->News_model->setCountRecord(0);
        //var_dump($this->uri->segment(1));
        //
        $inData['page']['property']['description'] = 'Новости сайта автосос.рф';
        // вставляем мета keywords со всех записей собираем
        $inKeywords = $this->News_model->load(array('news_status'=>1,'fields'=>array("meta_keywords")),false);
        $outKeywords = '';
        if (!empty($inKeywords)) {
            foreach ($inKeywords as $key => $value) {
                if(!empty($value['meta_keywords'])) {
                    $outKeywords .= $value['meta_keywords'] . ", ";
                }
            }
        }
        $outKeywords = (!empty($outKeywords))?preg_replace('#,$#', '',trim($outKeywords)):'';
        $inData['page']['property']['keywords'] = $outKeywords;
        // конец мета keywords
        $inNews['title'] = "Все новости";
        $inNews['data'] = $this->News_model->load(array('news_status'=>1,'fields'=>array("news_id as value","news_title as title",'images_id','news_text as note','news_creates as date')),false,$this->inPage,$this->inOutRecord);
        $inBloks = array();
        $inBloks[] = $this->twig->render('news/news-list-all.twig',$inNews);
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function CategoryDetail() {
        $inData = $this->includeUp('site_menu',true);
        $this->News_model->setOrder('news_creates');
        $this->News_model->setCountRecord(0);
        $inArg = func_get_args();
        $inCategory = isset($inArg[0])?$inArg[0]:1;
        //$inData['pieses']['listNews'] = $this->News_model->getOutput(OUTPUT_LIST);
        //$this->News_model->debug();
        //$this->News_model->Debug();
        //$this->News_model->Debug();
        $inData['pieses']['listNews'] = (empty($inCategory))?
                $this->News_model->getOutput(OUTPUT_LIST,array('news_status'=>1,'language_id'=>$this->inLang),false,$this->inPage):
                $this->News_model->getOutput(OUTPUT_LIST,array('news_status'=>1,'language_id'=>$this->inLang,'nc_id'=>$inCategory),false,$this->inPage);
                /*$this->News_model->load(array('news_status'=>1,'language_id'=>$this->inLang),false,$this->inPage):
                $this->News_model->load(array('news_status'=>1,'language_id'=>$this->inLang,'nc_id'=>$inCategory),false,$this->inPage);*/
        //
        if ($inData['pieses']['listNews']['data']) {
            foreach ($inData['pieses']['listNews']['data'] as $outKey => $outData) {
                $outData['news_count'] = $this->Comments->CountByNewsId($outData['news_id']);
                $inCategory = $this->News_model->loadCategoryById($outData['nc_id']);
                $outData['nc_title'] = $inCategory['nc_title'];
                $inData['pieses']['listNews']['data'][$outKey] = $outData;
            }
        };
        //$inData = $this->includeUp(array('SET-NEWS-CATEGORY'=>$inCategory));
        //$inData['rss'] = @"/rss/NewsCategoryDetail/{$inCategory}";
        $inData['page']['property']['description'] = 'Новости сайта автосос.рф';
        if($inCategory) {
            // вставляем мета keywords по категории
            $inKeywords = $this->News_model->load(array('news_status'=>1,'nc_id'=>$inCategory,'fields'=>array("meta_keywords")),false);
            $outKeywords = '';
            foreach ($inKeywords as $key => $value) {
                if(!empty($value['meta_keywords'])) {
                    $outKeywords .= $value['meta_keywords'] . ", ";
                }
            }
            $outKeywords = (!empty($outKeywords))?preg_replace('#,$#', '',trim($outKeywords)):'';
            $inData['page']['property']['keywords'] = $outKeywords;
            // конец мета keywords
        }
        $inData['sub_page'] = $this->twig->render('news/news-out-list.twig', $inData);
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function detail($aParam=null) {
        //ArgCheck($aParam);
        $inData = $this->includeUp('site_menu',true);
        $inData['page']['property']['description'] = 'Новости сайта автосос.рф';
        $inAvto = $this->News_model->loadById($aParam,
                array('fields'=>array("news_id as value","images_id","news_title as title","news_text as text","cpicture_id as collection",'meta_description','meta_keywords'))
        );
        if($inAvto) {
            $inData['page']['property']['keywords'] = $inAvto['meta_keywords'];
            $inData['page']['property']['description'] = $inAvto['meta_description'];
        }
        if($inAvto['collection']>0) {
            $inAvto['collection'] = $this->Picture_model->load(array('cpicture_id'=>$inAvto['collection'],
                'fields'=>array("picture_title as title","picture_file as picture","picture_id as value")));
        }
        //echo "<pre>"; var_dump($inAvto); die();
        $inBloks = array();
        $inBloks[] = $this->twig->render('avto/avto-detail.twig',array("data"=>$inAvto));
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    public function save_comments() {
        if (!empty($_POST)) {
            $inCheckComments = $this->CheckComments();
            //var_dump($inCheckComments);
            //echo "<pre>"; var_dump($_POST); die();
		//var_dump($inCheckComments, $_POST);
            unset($_POST['edtCaptcha']);
            if ($inCheckComments) {
                $this->Comments->save($_POST);
        	
	    }
	    Goto_Page("/news/detail/{$_POST['news_id']}");    	
        }
    }
    protected function goSearch() {
        $inStrSearch = filter_input(INPUT_GET, 'inSearch');
        $inData = $this->includeUp('site_menu',true);
        $this->inOutRecord = getVariable('news_count_by_page');
        $this->News_model->setCountRecord(0);
        //
        $inData['page']['property']['description'] = 'Новости сайта автосос.рф';
        // вставляем мета keywords со всех записей собираем
        $inKeywords = $this->News_model->load(array('news_status'=>1,'fields'=>array("meta_keywords")),false);
        $outKeywords = '';
        if (!empty($inKeywords)) {
            foreach ($inKeywords as $key => $value) {
                if(!empty($value['meta_keywords'])) {
                    $outKeywords .= $value['meta_keywords'] . ", ";
                }
            }
        }
        $outKeywords = (!empty($outKeywords))?preg_replace('#,$#', '',trim($outKeywords)):'';
        $inData['page']['property']['keywords'] = $outKeywords;
        // конец мета keywords
        $inNews['title'] = "Результат поиска";
        //$this->News_model->Debug();
        $inNews['data'] = $this->News_model->load(array('news_status'=>1,'news_text'=>"like '%{$inStrSearch}%'",'fields'=>array("news_id as value","news_title as title",'images_id','news_text as note','news_creates as date')),false,$this->inPage,$this->inOutRecord);
        $inBloks = array();
        $inBloks[] = $this->twig->render('news/news-list-all.twig',$inNews);
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    
}