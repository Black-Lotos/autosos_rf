<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page_car extends MY_Controller {
    //
    public function index() {   
        $inData = array();
        $inData = $this->includeUp('site_menu',true);
        //$this->Language_model->load(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')));
        //$this->load_page('main_info',$inData);
        
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function load($aParam) {
        //ArgCheck($aParam);
        $outKeywords = '';
	$this->Model_model->setOrder("model_name",'ASC');
        $inData = $this->includeUp('site_menu',true);
        $inCar = $this->Model_model->loadCategoryById($aParam,
                array("fields"=>array('car_name as title','images_id','car_description as note','car_id as value','meta_title','meta_keywords','meta_description')));
        //$this->Model_model->debug();
        //var_dump($inCar); die();
        $inModel = $this->Model_model->load(
            array("{$this->Model_model->getCategoryId()}"=>$inCar['value'],
                    'fields'=>array("model_id as value","images_id","model_name as title","model_description as text","car_id as owner",'meta_keywords','meta_description','meta_title')),
            false        
        );
        $inData['page']['property']['title'] = (!empty($inCar['meta_title']))?$inCar['meta_title']:'';
        $inData['page']['property']['keywords'] = (!empty($inCar['meta_keywords']))?$inCar['meta_keywords']:'';
        $inData['page']['property']['description'] = (!empty($inCar['meta_description']))?$inCar['meta_description']:'';
        $inBloks = array();
        // Данные для марок автомобилей
        if($inCar===false || ($inCar && $inModel===false)) {
            $dataCar=array('title'=>'Выбранная марка автомобилей пуста','text'=>'','value'=>'','images_id'=>$inCar['images_id']);
        } else  {
                    $dataCar=array('title'=>$inCar['title'],'text'=>$inCar['note'],'value'=>$inCar['value'],'images_id'=>$inCar['images_id']);
                }
        // Данные для автомобилей
        $dataModel = $inModel;        
        if($inModel===false) {
            $dataModel=array(array('title'=>'Моделей не найдено','text'=>'','value'=>'','images_id'=>$inCar['images_id']));
        } else  {
                    //
                    $dataModel=$inModel;
                    //var_dump($inModel);
                    foreach ($dataModel as $outKey => $outData) {
                        $this->Auto_model->setOrder("avto_name",'ASC');
                        $inAvto = $this->Auto_model->load(array(
                            "{$this->Auto_model->getStatus()}"=>1,
                            "{$this->Auto_model->getCategoryId()}"=>$outData['owner'],
                            "model_id"=>$outData['value'],
                            "fields"=>array("avto_id as value","images_id","avto_name as title","avto_description as text","avto_year as year","avto_url as url",
                                'meta_keywords','meta_description')
                        ),false);
                        if($inAvto) {
                            // собираем все меты по машинкам
                            foreach ($inAvto as $key => $value) {
                                if(!empty($value['meta_keywords'])) {
                                    $outKeywords .= $value['meta_keywords'] . ", ";
                                }
                            }
                            // конец сбора мета по машинкам
                            $outData['sub_tree'] = $inAvto;
                        }
                        $dataModel[$outKey] = $outData;
                    }
                    $outKeywords = (!empty($outKeywords))?preg_replace('#,$#', '',trim($outKeywords)):'';
                    //$inData['page']['property']['keywords'] = $inModel['meta_keywords'];
                }        
        // Формируем представление для марок автомобилей
        //echo "<pre>"; var_dump(array('data'=>$dataModel)); die();
        $inBloks[] = $this->twig->render('blocks/block-car-start-up.twig',array('data'=>$dataCar));
        // Собираем подчиненные автомобили
        //echo "<pre>"; var_dump(array('data'=>$dataModel)); die();
        $inBloks[] = $this->twig->render('blocks/block-model-start-up.twig',array('data'=>$dataModel));
        // загоняем в общий вывод всех блоков
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function detail_top_keys($aParam,$aKeysType=0) {
        //ArgCheck($aParam); 
        //ArgCheck($aKeysType);
        $inData = $this->includeUp('site_menu',true);
        //$this->Auto_model->Debug();
        $inAvto = $this->Auto_model->loadCategoryById($aParam,
                array("fields"=>array('car_name as title','images_id','car_description as note','car_id as value','meta_title','meta_keywords','meta_description')));
        if($inAvto) {
            $inData['page']['property']['title'] = (!empty($inAvto['meta_title']))?$inAvto['meta_title']:'';
            $inData['page']['property']['keywords'] = $inAvto['meta_keywords'];
            $inData['page']['property']['description'] = $inAvto['meta_description'];
        }
        //$inAvto['sub_tree']
        $inAvto['property']['keys_type'] = true;
        if($aKeysType==0) {
            $inKeys = $this->Keys_model->load(array(
                'car_id'=>$aParam,
                'keys_status'=>1,
                'keys_top'=>1,
                ));
        } else {
            $inKeys = $this->Keys_model->load(array(
            'car_id'=>$aParam,
            'keys_status'=>1,
            'keys_top'=>1,
            'keys_type'=>$aKeysType));
        }
        if($inKeys) {
            foreach($inKeys as $inKeyKey=>$inKeyData) {
                $inKeyData['keys_mhz'] = $this->Frequency_model->loadById($inKeyData['keys_mhz'],array('fields'=>array('mhz_name as title','mhz_id as value')));
                $inData['page']['property']['title']        .= " ".$inKeyData['meta_title'];
                $inData['page']['property']['keywords']     .= ",".$inKeyData['meta_keywords'];
                $inData['page']['property']['description']  .= " ".$inKeyData['meta_description'];
                $inKeys[$inKeyKey] = $inKeyData;
            }
        }
        $inAvto['sub_tree'] = $inKeys;
        //echo "<pre>"; var_dump($inKeys); die();
        $inAvto['title'] = getVariable('top_caption_key')." ".$inAvto['title'];
        
        $inBloks = array();
        $inBloks[] = $this->twig->render('avto/avto-detail.twig',array("data"=>$inAvto));
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
        //echo "<pre>"; var_dump($inKeys);
    }
    //
    public function detail_avto($aParam) {
        $inData = $this->includeUp('site_menu',true);
        $inAvto = $this->Auto_model->loadById($aParam,
            array('fields'=>array(
                "avto_id as value","images_id","avto_name as title","avto_year as year","avto_description as text","avto_url as url","model_id","car_id",
                "cpicture_id as collection",'meta_title','meta_keywords','meta_description')
            )
        );
        //echo "<pre>"; var_dump($inAvto); die();
        if($inAvto) {
            $inData['page']['property']['title'] = (!empty($inAvto['meta_title']))?$inAvto['meta_title']:'';
            $inData['page']['property']['keywords'] = $inAvto['meta_keywords'];
            $inData['page']['property']['description'] = $inAvto['meta_description'];
        }
        //$this->Picture_model->Debug();
        $inAvto['collection'] = $this->Picture_model->load(array('cpicture_id'=>$inAvto['collection'],
            'fields'=>array("picture_title as title","picture_file as picture","picture_id as value")));
        //echo "<pre>"; var_dump($inAvto['car_id'],$inAvto['model_id'],$inAvto['value']); die();
        //$this->Keys_model->Debug();
        $inKeys = $this->Keys_model->load(array(
            'car_id'=>$inAvto['car_id'],
            'model_id'=>$inAvto['model_id'],
            'avto_id'=>$inAvto['value'],
            'keys_status'=>1
        ));
        if($inKeys) {
            foreach($inKeys as $inKeyKey=>$inKeyData) {
                $inKeyData['keys_mhz'] = $this->Frequency_model->loadById($inKeyData['keys_mhz'],array('fields'=>array('mhz_name as title','mhz_id as value')));
                $inKeys[$inKeyKey] = $inKeyData;
            }
        }
        
        $inAvto['sub_tree'] = $inKeys;
        
        $inBloks = array();
        $inBloks[] = $this->twig->render('avto/avto-detail.twig',array("data"=>$inAvto));
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}

