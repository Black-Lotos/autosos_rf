<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page_keycreate extends MY_Controller {
    //
    public function index() {   
        $inData = array(); $inBlocks = array(); $inKeysCreate=array();
        $inData = $this->includeUp('site_menu',TRUE);
        // установка заголовка
        $inBlocks['header_page'] = getVariable('header_keycreate');
        // установка сообщения если необходимо
        $inBlocks['top_message_page'] = getVariable('top_message_keycreate');
        // заполняем контент
        $this->load->model('Keyscreate_model');
        //заполняем марки
        $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
        //заполняем модели        
        $this->Model_model->setOrder($this->Model_model->getSelfName(),'ASC');                            
        $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));                            
                
        $inKeysCreate['form'] = $this->Keyscreate_model->getForm($inKeysCreate);
        $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
        //echo "<pre>"; var_dump($inBlocks['content']); die();
        // установка футера если необходимозаголовка
        $inBlocks['footer_page'] = getVariable('footer_keycreate');
        $inData['page']['content']['right'] = $this->twig->render('blocks/blocks-build-page.twig', array('data'=>$inBlocks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    protected function save() {
        $inData = array(); $inBlocks = array(); $inKeysCreate=array();
        $outValidate = true;
        $inData = $this->includeUp('site_menu',TRUE);
        // установка заголовка
        $inBlocks['header_page'] = getVariable('header_keycreate');
        // установка сообщения если необходимо
        $inBlocks['top_message_page'] = getVariable('top_message_keycreate');
        // установка футера если необходимозаголовка
        $inBlocks['footer_page'] = getVariable('footer_keycreate');
        // заполняем контент
        $this->load->model('Keyscreate_model');
        if ($_POST) {
            //var_dump($_POST);
            $inKeysCreate = $_POST;
            //заполняем марки
            $inActiveCar = $inKeysCreate[$this->Auto_model->getCategoryId()];
            //var_dump($inActiveCar); die();
            $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
            $inKeysCreate[$this->Auto_model->getCategoryId()] = setActiveItem($inKeysCreate[$this->Auto_model->getCategoryId()], $inActiveCar);                        
            // модель машины
            $inActiveModel = $inKeysCreate['model_id'];
            $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'car_id' => $inActiveCar,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));
            $inKeysCreate['model_id'] = setActiveItem($inKeysCreate['model_id'], $inActiveModel);
            // Проверка на валидность полей ввода
            if(filter_input(INPUT_POST,'keys_create_phone')) {
                if(!valid_phone(filter_input(INPUT_POST,'keys_create_phone'))) {
                    setMessage('Неверно введен телефон.','error');
                    $outValidate = false;
                };
            }
            // Валидность капчи
            //var_dump(getCaptchaValidate()); die();
            
            if (strtolower(getCaptchaValidate())!=strtolower(filter_input(INPUT_POST,'edtCaptcha'))) {
                $outValidate = false;
                setMessage('Неверно введена каптча.','error');
            } else {
                unset($_POST['edtCaptcha']);
            }
            if($outValidate) {
                $inKeysCreate = array();
                               
                if ($this->Keyscreate_model->save($_POST)) {
                setMessage('Вы записаны.');
                $message = $this->Keyscreate_model->formEmail($_POST,"Обращение на запись по подбору ключей");
                //var_dump($message); die();
                $message = iconv('utf-8', 'windows-1251',$message);
                $to = getVariable('email_send');
                $from = getVariable('email_administrator');
                $subject = iconv('utf-8', 'windows-1251', "Запись на подбор ключей.");
                _email($to, $from, $subject, $message);
                } else setMessage('Ошибка записи на изготовление ключей');
                $inKeysCreate["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
                // устанавливаем по умолчанию
                //марка
                $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
                $inActiveCar = $inKeysCreate[$this->Auto_model->getCategoryId()][0]['value'];
                //модель 
                $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'car_id' => $inActiveCar,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));
                //
                $inKeysCreate['model_id'] = ($inKeysCreate['model_id'])?array_merge($this->Model_model->getNoItem(),$inKeysCreate['model_id']):$this->Model_model->getNoItem();
                // автомобиль
                $inKeysCreate['form'] = $this->Keyscreate_model->getForm($inKeysCreate);
                $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
            } else {
                $inKeysCreate["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
                $inKeysCreate['form'] = $this->Keyscreate_model->getForm($inKeysCreate);
                $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
            }
            
            $inData['page']['content']['right'] = $this->twig->render('blocks/blocks-build-page.twig', array('data'=>$inBlocks));
            
        };
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}

