<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page_keys extends MY_Controller {
    //
    public function index() {   
        $inData = array();
        $inData = $this->includeUp('site_menu',true);
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function load($aParam) {
        //ArgCheck($aParam);
        $inData = $this->includeUp('site_menu',true);
        $inCar = $this->Model_model->loadCategoryById($aParam,array("fields"=>array('car_name as title','images_id','car_description as note','car_id as value')));
        $inModel = $this->Model_model->load(
            array("{$this->Model_model->getCategoryId()}"=>$inCar['value'],'fields'=>array("model_id as value","images_id","model_name as title","model_description as text","car_id as owner")),
            false        
        );
        //
        $inBloks = array();
        // Данные для марок автомобилей
        if($inCar===false || ($inCar && $inModel===false)) {
            $dataCar=array('title'=>'Выбранная марка автомобилей пуста','text'=>'','value'=>'','images_id'=>$inCar['images_id']);
        } else  {
                    $dataCar=array('title'=>$inCar['title'],'text'=>$inCar['note'],'value'=>$inCar['value'],'images_id'=>$inCar['images_id']);
                }
        // Данные для автомобилей
        if($inModel===false) {
            $dataModel=array('title'=>'Моделей не найдено','text'=>'','value'=>'','images_id'=>$inCar['images_id']);
        } else  {
                    $dataModel=$inModel;
                    foreach ($dataModel as $outKey => $outData) {
                        $inAvto = $this->Auto_model->load(array(
                            "{$this->Auto_model->getCategoryId()}"=>$outData['owner'],
                            "model_id"=>$outData['value'],
                            "fields"=>array("avto_id as value","images_id","avto_name as title","avto_description as text","avto_year as year","avto_url as url")
                        ),false);
                        if($inAvto) {
                            $outData['sub_tree'] = $inAvto;
                        }
                        $dataModel[$outKey] = $outData;
                    }
                }        
        // Формируем представление для марок автомобилей
        $inBloks[] = $this->twig->render('blocks/block-car-start-up.twig',array('data'=>$dataCar));
        // Собираем подчиненные автомобили
        $inBloks[] = $this->twig->render('blocks/block-model-start-up.twig',array('data'=>$dataModel));
        // загоняем в общий вывод всех блоков
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function detail($aParam) {
        //ArgCheck($aParam);
        $inData = $this->includeUp('site_menu',true);
        $inKeys = $this->Keys_model->loadById($aParam);
        if($inKeys) {
            $inData['page']['property']['title']        = $inKeys['meta_title'];
            $inData['page']['property']['keywords']     = $inKeys['meta_keywords'];
            $inData['page']['property']['description']  = $inKeys['meta_description'];
        }
        $inKeys['keys_sting'] =  $this->Sting_model->loadById($inKeys['keys_sting'],array('fields'=>array('sting_name as title','sting_id as value')));
        $inKeys['keys_sting'] = (!$inKeys['keys_sting'])?array('title'=>false,'value'=>0):$inKeys['keys_sting'];
        $inKeys['keys_mhz'] =  $this->Frequency_model->loadById($inKeys['keys_mhz'],array('fields'=>array('mhz_name as title','mhz_id as value')));
        $inKeys['keys_mhz'] = (!$inKeys['keys_mhz'])?array('title'=>false,'value'=>0):$inKeys['keys_mhz'];
        $inBloks = array();
        $inBloks[] = $this->twig->render('avto/keys-detail.twig',array("data"=>$inKeys));
        $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
}

