<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page_price extends MY_Controller {
    protected function afterInclude($aData = array()) {
        $this->load->model('Invoice_model');
        return $aData;
    }
    //
    public function index() {   
        $inData = array();
        $inData = $this->includeUp('site_menu',true);
        $inBlocks = array();
        $inBlocks['header_page'] = getVariable('header_price');
        $inBlocks['top_message_page'] = getVariable('top_message_price');
        $inPrice = $this->Invoice_model->loadTree(
            array(
                'tree'=>array($this->Invoice_model->getCategoryStatus()=>1,
                'fields'=>array(
                    "{$this->Invoice_model->getCategoryId()} as value",
                    "{$this->Invoice_model->getCategoryName()} as title",)),
                'item'=>array($this->Invoice_model->getStatus()=>1,
                'fields'=>array(
                    "{$this->Invoice_model->getSelfId()} as value",
                    "{$this->Invoice_model->getSelfName()} as title",
                    "invoice_description","invoice_price"        
                )),
            )
        );
        //echo "<pre>"; var_dump($inPrice); die();
        $inBlocks['content'] = $this->twig->render('price/blocks-build-price.twig', array('data'=>$inPrice));
        $inBlocks['footer_page'] = getVariable('footer_price');
        $inData['page']['content']['right'] = $this->twig->render('blocks/blocks-build-page.twig', array('data'=>$inBlocks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
}

