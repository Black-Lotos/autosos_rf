<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page_workshop extends MY_Controller {
    //
    public function index() {   
        $inData = array(); $inBlocks = array(); $inKeysCreate=array();
        $inData = $this->includeUp('site_menu',TRUE);
        // установка заголовка
        $inBlocks['header_page'] = getVariable('header_price');
        // установка сообщения если необходимо
        $inBlocks['top_message_page'] = getVariable('top_message_price');
        // заполняем контент
        $this->load->model('Workshop_model');
        //заполняем марки
        $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
        
		
		
		
		//заполняем модели                            
        $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));                            
                
        $inKeysCreate['form'] = $this->Workshop_model->getForm($inKeysCreate);
        $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
        //echo "<pre>"; var_dump($inBlocks['content']); die();
        // установка футера если необходимозаголовка
        $inBlocks['footer_page'] = getVariable('footer_price');
        $inData['page']['content']['right'] = $this->twig->render('blocks/blocks-build-page.twig', array('data'=>$inBlocks));
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    protected function save() {
        $inData = array(); $inBlocks = array(); $inKeysCreate=array();
        $outValidate = true;
        $inData = $this->includeUp('site_menu',TRUE);
        // установка заголовка
        $inBlocks['header_page'] = getVariable('header_price');
        // установка сообщения если необходимо
        $inBlocks['top_message_page'] = getVariable('top_message_price');
        // установка футера если необходимозаголовка
        $inBlocks['footer_page'] = getVariable('footer_price');
        // заполняем контент
        $this->load->model('Workshop_model');
        if ($_POST) {
            //var_dump($_POST);
            $inKeysCreate = $_POST;
            //заполняем марки
            $inActiveCar = $inKeysCreate[$this->Auto_model->getCategoryId()];
            //var_dump($inActiveCar); die();
            $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
            $inKeysCreate[$this->Auto_model->getCategoryId()] = setActiveItem($inKeysCreate[$this->Auto_model->getCategoryId()], $inActiveCar);                        
            // модель машины
            $inActiveModel = $inKeysCreate['model_id'];
            $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'car_id' => $inActiveCar,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));
            $inKeysCreate['model_id'] = setActiveItem($inKeysCreate['model_id'], $inActiveModel);
            // сам автомобиль
            $inActiveAvto = $inKeysCreate['avto_id'];
            $inKeysCreate['avto_id'] = $this->Auto_model->load(
                array(
                    'avto_status'=>1,
                    'car_id' => $inActiveCar,
                    'model_id' => $inActiveModel,
                    "fields" => array(
                        "avto_name as title",
                        "avto_id as value"
                    )
            ),false);
            $inKeysCreate['avto_id'] = setActiveItem($inKeysCreate['avto_id'], $inActiveAvto);
            // Проверка на валидность полей ввода
            if(!valid_phone(filter_input(INPUT_POST,'keys_create_phone'))) {
                setMessage('Неверно введен телефон.','error');
                $outValidate = false;
            };
            if($outValidate) {
                $inKeysCreate = array();
                               
                if ($this->Workshop_model->save($_POST)) {
                    setMessage('Вы записаны.');
                    $message = "Обращение на запись в мастерскую";
                    _email(getVariable('email_send'), "автосос.рф", "Запись в мастерскую.", $message);
                //die();
                } else setMessage('Ошибка записи на изготовление ключей');
                $inKeysCreate["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
                // устанавливаем по умолчанию
                //марка
                $inKeysCreate[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
                $inActiveCar = $inKeysCreate[$this->Auto_model->getCategoryId()][0]['value'];
                //модель 
                $inKeysCreate['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'car_id' => $inActiveCar,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));
                //
                $inKeysCreate['model_id'] = ($inKeysCreate['model_id'])?array_merge($this->Model_model->getNoItem(),$inModel):$this->Model_model->getNoItem();
                // автомобиль
                $inKeysCreate['form'] = $this->Workshop_model->getForm($inKeysCreate);
                $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
            } else {
                $inKeysCreate["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
                $inKeysCreate['form'] = $this->Workshop_model->getForm($inKeysCreate);
                $inBlocks['content'] = $this->twig->render("administration/common/form-system.twig", $inKeysCreate);
            }
            
            $inData['page']['content']['right'] = $this->twig->render('blocks/blocks-build-page.twig', array('data'=>$inBlocks));
            
        };
        //echo $this->twig->render('site-master-page.twig', $inData);
    }
}

