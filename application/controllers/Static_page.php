<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Static_page extends MY_Controller {
    //
    public function index($aPage='page_404') {   
        $inAlias = $aPage;
        $inData = array();
        $inData = $this->includeUp('site_menu');
        //$this->Static_model->Debug();
        $inPage = $this->Static_model->loadByAlias($inAlias,array('language_id'=>$this->inLang),true);
        
        $inData['page']['property']['title'] = (!empty($inPage->meta_title))?$inPage->meta_title:'';
        $inData['page']['property']['keywords'] = (!empty($inPage->meta_keywords))?$inPage->meta_keywords:'';
        $inData['page']['property']['description'] = (!empty($inPage->meta_description))?$inPage->meta_description:'';
        if (!$inPage) {
            $inPage = $this->Static_model->loadByAlias("page_404",array('language_id'=>$this->inLang),true);
        }
        if ($inAlias=='page_404') {
            setMessage($inPage->sp_text, 'error');
            $inData['sub_page'] = getMessage('error',true,'error');
        } else {
            $inData['page']['content']['right'] = $inPage->sp_text;
        }
        echo $this->twig->render('site-master-page.twig',$inData);
    }
    //
    public function error_pages() {
        $aPage='page_404';
        if(isset($_SERVER['REQUEST_URI'])) {
            
            // так бежим по редиректам
            // 1 шаг проверяем статические страницы
            $inRedirectPage = $this->Static_model->load(array('redirect_url'=>preg_replace("#/#","",$_SERVER['REQUEST_URI'])),false);
            if($inRedirectPage) { // если есть перепрыгиваем в пределах сайта на страницу
                $inRedirectPage = $inRedirectPage[0];
                
                Goto_Page("/static_page/{$inRedirectPage['sp_name']}/");
            }
            // 2 шаг проверяем автомобили страницы
            $inCar = $this->Model_model->loadCategory(array('redirect_url'=>preg_replace("#/#","",$_SERVER['REQUEST_URI'])),false);
            //var_dump($inCar); die("tyt");
            if($inCar) { // если есть перепрыгиваем в пределах сайта на страницу
                $inCar = $inCar[0];
                //var_dump($inRedirectPage); die();
                Goto_Page("/page_car/load/{$inCar['car_id']}/");
            }
        }
        $inAlias = $aPage;
        $inData = array();
        $inData = $this->includeUp('site_menu');
        //$this->Static_model->Debug();
        $inPage = $this->Static_model->loadByAlias($inAlias,array('language_id'=>$this->inLang),true);
        
        $inData['page']['property']['title'] = (!empty($inPage->meta_title))?$inPage->meta_title:'';
        $inData['page']['property']['keywords'] = (!empty($inPage->meta_keywords))?$inPage->meta_keywords:'';
        $inData['page']['property']['description'] = (!empty($inPage->meta_description))?$inPage->meta_description:'';
        if (!$inPage) {
            $inPage = $this->Static_model->loadByAlias("page_404",array('language_id'=>$this->inLang),true);
        }
        if ($inAlias=='page_404') {
            setMessage($inPage->sp_text, 'error');
            $inData['sub_page'] = getMessage('error',true,'error');
        } else {
            $inData['page']['content']['right'] = $inPage->sp_text;
        }
        echo $this->twig->render('site-master-page.twig',$inData);
    }
}