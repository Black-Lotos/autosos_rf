<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_avto extends MY_Controller {
    protected function afterInclude($aData = array()) {
        $this->load->model('Auto_model');
        $this->load->model('Model_model');
        return $aData;
    }
    //
    public function index() {   
    
    }
    public function delete_keys($aParam=null) {
        if (filter_input(INPUT_POST,'item_id') && filter_input(INPUT_POST,'process')=='delete-keys') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'item_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Keys_model->loadById($inId);
            $inDataDel = $this->Keys_model->DeleteById($inId);
            setMessage("Ключ «{$inData['keys_name']}» удален.", 'status');
        }else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
    }
    public function top_keys_change($aParam=null) {
        if (filter_input(INPUT_POST,'item_id') && filter_input(INPUT_POST,'process')=='top-keys-change') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'item_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Keys_model->loadById($inId);
            $inData['keys_top'] = ($inData['keys_top']==1)?0:1;
            $this->Keys_model->save($inData);
        }
    }
    public function edit_keys($aParam=null) {
        if (filter_input(INPUT_POST,'item_id') && filter_input(INPUT_POST,'process')=='edit-keys') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'item_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Keys_model->loadById($inId);
            $inAvto  = $this->Auto_model->loadById($inData['avto_id'],
            array(
                $this->Auto_model->getSelfId()=>$aParam,
                $this->Auto_model->getStatus()=>1,
                'fields'=>array("{$this->Auto_model->getSelfId()} as value","{$this->Auto_model->getSelfName()} as title","model_id","car_id")),false);
            $inActiveAvto =  $inAvto['value'];
            $inActiveModel = $inAvto["model_id"];
            $inActiveCar = $inAvto["car_id"];
            $inData[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
            $inData[$this->Auto_model->getCategoryId()] = setCategoryActiveItem($inData[$this->Auto_model->getCategoryId()], $inActiveCar);
            $inData['model_id'] = $this->Model_model->load(
                    array(
                        'car_id'=>$inActiveCar,
                        'model_status'=>1,
                        'fields'=>array("model_name as title",
                                        "model_id as value")));  
            $inData['model_id'] = setCategoryActiveItem($inData['model_id'], $inActiveModel);
            //
            $inData['avto_id'] = $this->Auto_model->load(
                    array(
                        'car_id'=>$inActiveCar,
                        'model_id'=>$inActiveModel,
                        'avto_status'=>1,
                        'fields'=>array("avto_name as title",
                                        "avto_id as value")),false);  
            $inData['avto_id'] = setCategoryActiveItem($inData['avto_id'], $inActiveAvto);
            // тип ключа
            $inActiveType = (empty($inData['keys_type'])?1:$inData['keys_type']);
            $inData['keys_type'] = array(
                array('title'=>'С кнопками','value'=>1),
                array('title'=>'Без кнопок','value'=>2),
                array('title'=>'Корпус','value'=>3),
            );
            $inData['keys_type'] = setCategoryActiveItem($inData['keys_type'], $inActiveType);
            //
            //echo "<pre>"; var_dump($inData); die();
            // жало ключа
            $inActiveSting = $inData['keys_sting'];
            $inData['keys_sting'] = $this->Sting_model->load(array('fields'=>array("sting_name as title","sting_id as value")));
            $inData['keys_sting'] = setCategoryActiveItem($inData['keys_sting'], $inActiveSting);
            // частота
            $inActiveMhz = $inData['keys_mhz'];
            $inData['keys_mhz'] = $this->Frequency_model->load(array('fields'=>array("mhz_name as title","mhz_id as value")));
            $inData['keys_mhz'] = setCategoryActiveItem($inData['keys_mhz'], $inActiveMhz);
            //
            //echo "<pre>"; var_dump($inData); die();
            $inData['form'] = $this->Keys_model->getForm($inData);
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
        
    }
    public function load_keys($aParam=null) {
        $inData['page']['setup_page'] = $this->Keys_model->getOutput();
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = ceil($this->Keys_model->loadCountRecord(array(),true)/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_avto';
        //$this->Keys_model->isDebug();
        $inData['page']['data'] = $this->Keys_model->loadTree(
            array(
                'tree'=>array($this->Keys_model->getCategoryId()=>$aParam,
                    'order'=>array('order_fields'=>array($this->Keys_model->getCategoryName()),'order_type'=>'ASC'),
                    'fields'=>array("images_id","{$this->Keys_model->getCategoryId()} as value","{$this->Keys_model->getCategoryName()} as title","{$this->Keys_model->getCategoryStatus()} as status")),
                'item'=>array(
                    'order'=>array('order_fields'=>$this->Keys_model->getSelfName(),'order_type'=>'ASC'),
                    'fields'=>array("{$this->Keys_model->getCategoryId()} as owner","{$this->Keys_model->getSelfId()} as value","{$this->Keys_model->getSelfName()} as title", "{$this->Keys_model->getStatus()} as status","keys_top")), 
            ),true,$this->inPage,$this->inOutRecord
        );
        //var_dump($inData['page']['data']); die();
        if(!empty($inData['page']['data'])) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                if(!empty($outData['sub_tree'])) {
                    foreach($outData['sub_tree'] as $outSKey=>$outSData) {
                        $outSChecked = ($outSData['status']==1)?'checked':'un-checked';
                        $outMain = ($outSData['keys_top']==1)?'checked':'un-checked';
                        $outSData['action']      =  "<a href='#' id='ref-keys-edit-{$outSData['value']}' class='action-base action-edit'></a>";
                        $outSData['action']      .= "<a href='#' id='ref-keys-delete-{$outSData['value']}' class='action-base action-delete'></a>";
                        $outSData['action']      .= "<a href='#' id='ref-keys-checked-{$outSData['value']}' class='action-base action-{$outSChecked}'></a>";
                        $outSData['action']      .= "<a href='#' id='ref-keys-top-{$outSData['value']}' class='action-base action-main-{$outMain}' title='Топ ключ (включить/отключить)'></a>";
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                    /*
                     foreach($outData as $outSKey=>$outSData) {
                        $outSChecked = ($outSData['status']==1)?'checked':'un-checked';
                        $outSData['action']      = "<a href='#' id='ref-keys-edit-{$outSData['value']}' class='action-base action-edit'></a>";
                        $outSData['action']      .= "<a href='#' id='ref-keys-delete-{$outSData['value']}' class='action-base action-delete'></a>";
                        $outSData['action']      .= "<a href='#' id='ref-keys-checked-{$outSData['value']}' class='action-base action-{$outSChecked}'></a>";
                        $outData[$outSKey] = $outSData;
                    }
                     * 
                     */
                }
                $inData['page']['data'][$outKey] = $outData;
            }   
        }
        //echo "<pre>";                var_dump($inData['page']['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);        
    }
    public function add_keys($aParam) {
        //var_dump($aParam);
        $inData = array();
        $inAvto  = $this->Auto_model->loadById($aParam,
            array(
                $this->Auto_model->getSelfId()=>$aParam,
                $this->Auto_model->getStatus()=>1,
                'fields'=>array("{$this->Auto_model->getSelfId()} as value","{$this->Auto_model->getSelfName()} as title","model_id","car_id")),false);
        $inActiveAvto =  $inAvto['value'];
        $inActiveModel = $inAvto["model_id"];
        $inActiveCar = $inAvto["car_id"];
        $inData[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
        $inData[$this->Auto_model->getCategoryId()] = setCategoryActiveItem($inData[$this->Auto_model->getCategoryId()], $inActiveCar);
        $inData['model_id'] = $this->Model_model->load(
                array(
                    'car_id'=>$inActiveCar,
                    'model_status'=>1,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));  
        $inData['model_id'] = setCategoryActiveItem($inData['model_id'], $inActiveModel);
        //
        $inData['avto_id'] = $this->Auto_model->load(
                array(
                    'car_id'=>$inActiveCar,
                    'model_id'=>$inActiveModel,
                    'avto_status'=>1,
                    'fields'=>array("avto_name as title",
                                    "avto_id as value")),false);  
        $inData['avto_id'] = setCategoryActiveItem($inData['avto_id'], $inActiveAvto);
        // тип ключа
        $inActiveType = (empty($inData['keys_type'])?1:$inData['keys_type']);
        $inData['keys_type'] = array(
            array('title'=>'С кнопками','value'=>1),
            array('title'=>'Без кнопок','value'=>2),
            array('title'=>'Корпус','value'=>3),
        );
        $inData['keys_type'] = setCategoryActiveItem($inData['keys_type'], $inActiveType);
        // жало ключа
        //echo "<pre>"; var_dump($inData);
        //$inActiveSting = $inData['keys_sting'];
        $inData['keys_sting'] = $this->Sting_model->load(array('fields'=>array("sting_name as title","sting_id as value")));
        //$inData['keys_sting'] = setCategoryActiveItem($inData['keys_sting'], $inActiveSting);
        // Частота
        //$inActiveMhz = $inData['keys_mhz'];
        $inData['keys_mhz'] = array_merge(array(array('title'=>'-частота не указана-','value'=>0)),
            $this->Frequency_model->load(array('fields'=>array("mhz_name as title","mhz_id as value"))));
        //$inData['keys_mhz'] = setCategoryActiveItem($inData['keys_mhz'], $inActiveSting);
        // чипованый
        $inData['form'] = $this->Keys_model->getForm($inData);
        //echo "<pre>"; var_dump($inData); die();
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function load($aParam=null) {
        $this->load->model('Auto_model');
        $inData['page']['setup_page'] = $this->Auto_model->getOutput();
        //$inData['page']['active'] = $this->inPage;
        //$this->Auto_model->Debug();
        //$inData['page']['count'] = ceil($this->Auto_model->loadCountRecord(array(),true)/$this->inOutRecord);
        //$inData['page']['link_run'] = 'administration/section/admin_avto';
        //var_dump($inData['page']['count']);
        //$this->Auto_model->Debug();
        //$this->Model_model->Debug();
        $inData['page']['data'] = $this->Model_model->loadTree(
            array(
                'tree'=>array(
                    'order'=>array('order_fields'=>array($this->Model_model->getCategoryName()),'order_type'=>'ASC'),
                    'fields'=>array("images_id","{$this->Model_model->getCategoryId()} as value","{$this->Model_model->getCategoryName()} as title","{$this->Model_model->getCategoryStatus()} as status")),
            ),false,$this->inPage,$this->inOutRecord
        );
        //echo "<pre>"; var_dump($inData['page']['data'],$this->inPage,$this->inOutRecord); die();
        if($inData['page']['data']) {
            foreach ($inData['page']['data'] as $carKey => $carData) {
                // опции редактирования
                $outChecked = ($carData['status']==1)?'checked':'un-checked';
                $carData['action']      = "<a href='#' id='ref-{$this->Model_model->getCategoryPrefix()}-edit-{$carData['value']}' class='action-base action-edit'></a>";
                if(empty($carData['sub_tree']))
                    $carData['action'] .= "<a href='#' id='ref-{$this->Model_model->getCategoryPrefix()}-delete-{$carData['value']}' class='action-base action-delete'></a>";
                $carData['action']     .= "<a href='#' id='ref-{$this->Model_model->getCategoryPrefix()}-checked-{$carData['value']}' class='action-base action-{$outChecked}'></a>";
                // конец опции редактирования
                if (!empty($carData['sub_tree'])) {
                    foreach ($carData['sub_tree'] as $modelKey => $modelData) {
                        // опции редактирования
                        $outSChecked = ($modelData['model_status']==1)?'checked':'un-checked';
                        $carData['sub_tree'][$modelKey]['action'] = "<a href='#' id='ref-{$this->Model_model->getPrefix()}-edit-{$modelData['value']}' class='action-base action-edit'></a>";
                        if(empty($modelData['sub_tree']))
                            $carData['sub_tree'][$modelKey]['action'] .= "<a href='#' id='ref-{$this->Model_model->getPrefix()}-delete-{$modelData['value']}' class='action-base action-delete'></a>";
                        $carData['sub_tree'][$modelKey]['action']     .= "<a href='#' id='ref-{$this->Model_model->getPrefix()}-checked-{$modelData['value']}' class='action-base action-{$outChecked}'></a>";    
                        // конец опции редактирования
                        $this->Auto_model->setOrder($this->Auto_model->getSelfName());
                        $inAvto = $this->Auto_model->load(
                            array(
                                $this->Auto_model->getCategoryId()=>$carData['value'],
                                'model_id'=>$modelData['value'],
                                'fields'=>array("{$this->Auto_model->getSelfId()} as value","{$this->Auto_model->getSelfName()} as title", "{$this->Auto_model->getStatus()} as status")),
                                false
                        );
                        //
                        if(!empty($inAvto)){
                            foreach ($inAvto as $inKeyKeys=>$inKeysData) {
                                $outSSChecked = ($inKeysData['status']==1)?'checked':'un-checked';
                                $inKeysData['action']    = "<a href='#' id='ref-{$this->Auto_model->getPrefix()}-edit-{$inKeysData['value']}' class='action-base action-edit'></a>";
                                $inKeysData['action']    .= "<a href='#' id='ref-{$this->Auto_model->getPrefix()}-delete-{$inKeysData['value']}' class='action-base action-delete'></a>";
                                $inKeysData['action']    .= "<a href='#' id='ref-{$this->Auto_model->getPrefix()}-checked-{$inKeysData['value']}' class='action-base action-{$outSSChecked}'></a>";    
                                $this->Keys_model->setOrder($this->Keys_model->getSelfName());
                                $inKeysData['sub_tree'] = $this->Keys_model->load(
                                array(
                                    $this->Keys_model->getCategoryId()=>$inKeysData['value'],
                                    "fields"=>array(
                                        "{$this->Keys_model->getSelfId()} as value",
                                        "{$this->Keys_model->getSelfName()} as title",
                                        "{$this->Keys_model->getStatus()} as status", 
                                        "keys_mhz",
                                    ),
                                ),
                                false,$this->inPage,$this->inOutRecord
                                );
                                $inKeysData['sub_tree']['child']="/administration/section/admin_keys/{$inKeysData['value']}";
                                $inKeysData['sub_tree']['caption']='Ключи для автомобиля';
                                $inAvto[$inKeyKeys] = $inKeysData;
                            }        
                            //        
                            $carData['sub_tree'][$modelKey]['sub_tree'] = $inAvto;        
                        }
                    }
                //    echo "<pre>"; var_dump($carData['sub_tree']);
                }
                $inData['page']['data'][$carKey] = $carData;
            }
        }            
        /*if($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                if (!empty($outData['sub_tree'])) {
                    foreach($outData['sub_tree'] as $outSKey=>$outSData) {
                        $this->Auto_model->setOrder($this->Auto_model->getSelfName());
                        $inAvto = $this->Auto_model->load(
                            array(
                                $this->Auto_model->getCategoryId()=>$outSData['owner'],
                                'model_id'=>$outSData['value'],
                                'fields'=>array("{$this->Auto_model->getSelfId()} as value","{$this->Auto_model->getSelfName()} as title", "{$this->Auto_model->getStatus()} as status")),
                                false,$this->inPage,$this->inOutRecord
                            );
                        if($inAvto) {
                            foreach ($inAvto as $inKeyKeys=>$inKeysData) {
                                $this->Keys_model->setOrder($this->Keys_model->getSelfName());
                                $inKeysData['sub_tree'] = $this->Keys_model->load(
                                array(
                                    $this->Keys_model->getCategoryId()=>$inKeysData['value'],
                                    "fields"=>array(
                                        "{$this->Keys_model->getSelfId()} as value",
                                        "{$this->Keys_model->getSelfName()} as title",
                                        "{$this->Keys_model->getStatus()} as status", 
                                        "keys_mhz",
                                    ),
                                ),
                                false,$this->inPage,$this->inOutRecord
                                );
                                $inKeysData['sub_tree']['child']="/administration/section/admin_keys/{$inKeysData['value']}";
                                $inKeysData['sub_tree']['caption']='Ключи для автомобиля';
                                $inAvto[$inKeyKeys] = $inKeysData;
                            }
                            //echo "<pre>"; var_dump($inAvto); die();
                            //$inAvto
                            $outSData['sub_tree'] = $inAvto;
                        }
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                    //
                }
                $inData['page']['data'][$outKey] = $outData;
            }
            
        }        
        //die();
        //
        //        loadLanguage($this->inPage);
        if($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData['status']==1)?'checked':'un-checked';
                $outData['action']      = "<a href='#' id='ref-{$this->Model_model->getCategoryPrefix()}-edit-{$outData['value']}' class='action-base action-edit'></a>";
                if(empty($outData['sub_tree']))
                    $outData['action'] .= "<a href='#' id='ref-{$this->Model_model->getCategoryPrefix()}-delete-{$outData['value']}' class='action-base action-delete'></a>";
                $outData['action']     .= "<a href='#' id='ref-{$this->Model_model->getCategoryPrefix()}-checked-{$outData['value']}' class='action-base action-{$outChecked}'></a>";
                // выбор моделей
                if(!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey=>$outSData) {
                        $outSChecked = ($outSData['status']==1)?'checked':'un-checked';
                        $outSData['action'] = "<a href='#' id='ref-{$this->Model_model->getPrefix()}-edit-{$outSData['value']}' class='action-base action-edit'></a>";
                        if(empty($outSData['sub_tree']))
                            $outSData['action'] .= "<a href='#' id='ref-{$this->Model_model->getPrefix()}-delete-{$outSData['value']}' class='action-base action-delete'></a>";
                        $outSData['action']     .= "<a href='#' id='ref-{$this->Model_model->getPrefix()}-checked-{$outSData['value']}' class='action-base action-{$outChecked}'></a>";    
                        if(!empty($outSData['sub_tree'])) {
                            foreach ($outSData['sub_tree'] as $outSSKey=>$outSSData) {
                                $outSSChecked = ($outSSData['status']==1)?'checked':'un-checked';
                                $outSSData['action']    = "<a href='#' id='ref-{$this->Auto_model->getPrefix()}-edit-{$outSSData['value']}' class='action-base action-edit'></a>";
                                $outSSData['action']    .= "<a href='#' id='ref-{$this->Auto_model->getPrefix()}-delete-{$outSSData['value']}' class='action-base action-delete'></a>";
                                $outSSData['action']    .= "<a href='#' id='ref-{$this->Auto_model->getPrefix()}-checked-{$outSSData['value']}' class='action-base action-{$outSSChecked}'></a>";    
                                $outSData['sub_tree'][$outSSKey] = $outSSData;
                            }
                        }
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }
                $inData['page']['data'][$outKey] = $outData;
            }
        }
         * 
         */
        //echo "<pre>"; var_dump($inData['page']['data'],$this->inPage,$this->inOutRecord); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add() {
        $this->load->model('Auto_model');
        $inData = array();
        $this->Auto_model->setCategoryOrder(array($this->Auto_model->getCategoryName()));
        $inData[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
        $this->Model_model->setOrder('model_name');                            
        $inData['model_id'] = $this->Model_model->load(
                array(
                    'model_status'=>1,
                    'fields'=>array("model_name as title",
                                    "model_id as value")));                            
        $inData['model_id'] = array_merge(array(array('title'=>'- без модели -','value'=>'0')),$inData['model_id']);
        $inData['form'] = $this->Auto_model->getForm($inData);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function add_model() {
        $this->load->model('Auto_model');
        $inData = array();
        $this->Auto_model->setCategoryOrder(array($this->Auto_model->getCategoryName()));
        $inData[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(
                array(
                    $this->Auto_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Auto_model->getCategoryName()} as title",
                                    "{$this->Auto_model->getCategoryId()} as value")));
        $inData['form'] = $this->Auto_model->getForm($inData,PROCESS_MODEL_ADD);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function add_section() {
        $this->load->model('Auto_model');
        $inData = array();
        $inData['form'] = $this->Auto_model->getForm($inData,PROCESS_SECTION_ADD);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    
    public function edit() {
        $this->load->model('Auto_model');
        if (filter_input(INPUT_POST,'item_id') && filter_input(INPUT_POST,'process')=='edit-item') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'item_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Auto_model->loadById($inMcId);
            // устанаввливаем модель
            $inActiveModel = $inData[$this->Model_model->getSelfId()];
            $this->Model_model->setOrder('model_name');                            
            $inModel = $this->Model_model->load(array(
                "{$this->Model_model->getStatus()}"=>1,
                "{$this->Model_model->getCategoryId()}"=>$inData[$this->Model_model->getCategoryId()],
                "model_id"=>$inData[$this->Model_model->getSelfId()],
                'fields'=>array("{$this->Model_model->getSelfName()} as title","{$this->Model_model->getSelfId()} as value")
            ),false);
            $inModel = ($inModel)?array_merge(array(array('title'=>'- без модели -','value'=>'0')),$inModel):array(array('title'=>'- без модели -','value'=>0));
            //echo "<pre>"; var_dump($inModel); die();    
            $inData[$this->Model_model->getSelfId()] = setCategoryActiveItem($inModel, $inActiveModel);    
            // устанавливаем марку
            $inActive = $inData[$this->Auto_model->getCategoryId()];
            $inData[$this->Auto_model->getCategoryId()] = $this->Auto_model->loadCategory(array("{$this->Auto_model->getCategoryStatus()}"=>1,
                'fields'=>array("{$this->Auto_model->getCategoryName()} as title","{$this->Auto_model->getCategoryId()} as value")));
            $inData[$this->Auto_model->getCategoryId()] = setCategoryActiveItem($inData[$this->Auto_model->getCategoryId()], $inActive);
            //коллекция изображений
            $inActive = $inData['cpicture_id'];
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inData['cpicture_id'] = array(array('title'=>'- без коллекции -','value'=>'0'));
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inGetPicCat = $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')));
            $inData['cpicture_id'] = array_merge($inData['cpicture_id'],($inGetPicCat)?$inGetPicCat:array()                
            );
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inData['cpicture_id'] = setCategoryActiveItem($inData['cpicture_id'], $inActive);
            // передаем данные для формирования формы
            //echo "<pre>"; var_dump($inData); die();
            $inData['form'] = $this->Auto_model->getForm($inData);
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_model() {
        $this->load->model('Model_model');
        if (filter_input(INPUT_POST,'item') && filter_input(INPUT_POST,'process')=='edit-model') {
            //die("TYT");
            $inDataItem = filter_input(INPUT_POST,'item'); $inDataProcess = filter_input(INPUT_POST,'process');
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $inDataItem, $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Model_model->loadById($inMcId);
            $inActive = $inData[$this->Auto_model->getCategoryId()];
            $this->Model_model->setCategoryOrder(array($this->Model_model->getCategoryName()));
            $inData[$this->Model_model->getCategoryId()] = $this->Model_model->loadCategory(
                array(
                    $this->Model_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Model_model->getCategoryName()} as title",
                                    "{$this->Model_model->getCategoryId()} as value")));
            $inData[$this->Model_model->getCategoryId()] = setCategoryActiveItem($inData[$this->Model_model->getCategoryId()], $inActive);                        
            //var_dump($inData); die();
            $inData['form'] = $this->Auto_model->getForm($inData,PROCESS_MODEL_ADD);
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function delete_model() {
        $this->load->model('Model_model');
        if (filter_input(INPUT_POST,'item') && filter_input(INPUT_POST,'process')=='delete-model') {
            $inDataItem = filter_input(INPUT_POST,'item'); $inDataProcess = filter_input(INPUT_POST,'process');
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $inDataItem, $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Model_model->deleteById($inMcId);
        }
    }
    //
    public function edit_section() {
        $this->load->model('Auto_model');
        if (filter_input(INPUT_POST,'category_id') && filter_input(INPUT_POST,'process')=='edit-category') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'category_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Auto_model->loadCategoryById($inMcId);
            $inMenu = null;
            $inData['form'] = $this->Auto_model->getForm($inData,PROCESS_SECTION_ADD);
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function delete_section() {
        $this->load->model('Auto_model');
        if (filter_input(INPUT_POST,'category_id') && filter_input(INPUT_POST,'process')=='delete-category') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'category_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Auto_model->deleteCategoryById($inMcId);
        }
    }
    //
    public function delete() {
        $this->load->model('Auto_model');
        if (filter_input(INPUT_POST,'item_id') && filter_input(INPUT_POST,'process')=='delete-item') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'item_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Auto_model->deleteById($inMcId);
        }
    }
    //
    public function save() {   
        $this->load->model('Auto_model');
        if (!empty($_POST)) {
            $inDecode=$_POST;
            $inDecode['avto_status'] = (!empty($inDecode['avto_status']))?1:0;
            //if($inDecode['avto_status']=='on') { $inDecode['avto_status']=1; }
            //echo "<pre>"; var_dump($inDecode); die();
            //$this->Auto_model->debug();
            if ($this->Auto_model->save($inDecode)) {
                setMessage('Автомобиль записан.');
            }else setMessage('Ошибка записи автомобиля.','error');
            //echo $this->inUriString; 
            //die($this->inUriString);
            //Goto_Page("/administration/content");
            Goto_Page("/administration/section/admin_avto");
        }
    }
    public function save_model() {   
        $this->load->model('Model_model');
        if (!empty($_POST)) {
            $inDecode=$_POST;
            //if($inDecode['model_status']=='on') { $inDecode['model_status']=1; }
            $inDecode['model_status'] = (!empty($inDecode['model_status']))?1:0;
            //echo "<pre>"; var_dump($inDecode); die();
            //$this->Auto_model->debug();
            if ($this->Model_model->save($inDecode)) {
                setMessage('Модель записана.');
            }else setMessage('Ошибка записи модели.','error');
            //echo $this->inUriString; 
            //die($this->inUriString);
            //Goto_Page("/administration/content");
            Goto_Page("/administration/section/admin_avto");
        }
    }
    public function save_section() {   
        $this->load->model('Auto_model');
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if($inDecode['car_status']=='on') { $inDecode['car_status']=1; }
            //echo "<pre>"; var_dump($inDecode); die();
            //$this->Auto_model->Debug();
            if ($this->Auto_model->save_category($inDecode)) {
                setMessage('Марка автомобиля записанна.');
            }else setMessage('Ошибка записи марки автомобиля.','error');
            //echo $this->inUriString; 
            //die($this->inUriString);
            Goto_Page("/administration/section/admin_avto");
        }
    }
    public function save_keys() {   
        if (!empty($_POST)) {
            $inDecode=$_POST;
            $inDecode['keys_status'] = (!empty($inDecode['keys_status']))?1:0;
            $inDecode['keys_instock'] = (!empty($inDecode['keys_instock']))?1:0;
            $inDecode['keys_price'] = (empty($inDecode['keys_price']))?0:$inDecode['keys_price'];
            $inWmPut = (!empty($inDecode['keys_wm']))?1:0;
            //var_dump($inDecode); die();
            $inDecode['keys_mhz'] = ($inDecode["keys_mhz"]?$inDecode["keys_mhz"]:'-');
            unset($inDecode['keys_wm']);
            $inAvto = $this->Auto_model->loadById($inDecode['avto_id']);
            $inDecode['model_id'] = $inAvto['model_id'];
            $inDecode['car_id'] = $inAvto['car_id'];
            $inDecode['images_id'];
            $this->load->model('Images_model');
            $inImg = $this->Images_model->loadById($inDecode['images_id']);
            $inDecode['meta_title'] = (!empty($inDecode['meta_title']))?$inDecode['meta_title']:$inDecode['keys_name'];
            $inDecode['meta_keywords'] = (!empty($inDecode['meta_keywords']))?$inDecode['meta_keywords']:$inDecode['keys_name'];
            $inDecode['meta_description'] = (!empty($inDecode['meta_description']))?$inDecode['meta_description']:$inDecode['keys_name'];
            if ($this->Keys_model->save($inDecode)) {
                if ($inWmPut==1) {
                    $this->Picture_model->writeWm($this->inImgPath.$inImg['images_name']);
                }
                setMessage('Ключ автомобиля записан.');
            }else setMessage('Ошибка записи ключа автомобиля.','error');
            Goto_Page("/administration/section/admin_keys/{$inDecode['avto_id']}");
        }
    }
    public function model_change_item($aParam=null) {
        if(filter_input(INPUT_POST, 'process')&&filter_input(INPUT_POST, 'item')) {
            //$this->Model_model->debug();
            $this->Model_model->setOrder($this->Model_model->getSelfName(),'ASC');
            $inModel = $this->Model_model->load(array(
                $this->Model_model->getCategoryId()=>filter_input(INPUT_POST, 'item'),
                'fields'=>array("{$this->Model_model->getSelfName()} as title",
                                "{$this->Model_model->getSelfId()} as value")
            ),false);
            //var_dump($this->Model_model->getNoItem());
            $inModel = ($inModel)?array_merge($this->Model_model->getNoItem(),$inModel):$this->Model_model->getNoItem();
            echo jdecoder(json_encode($inModel));
        }
    }
    public function avto_change_item($aParam=null) {
        //var_dump($_POST);
        if(filter_input(INPUT_POST, 'process')&&filter_input(INPUT_POST, 'item')) {
            //$this->Auto_model->debug();
            $inAvto = $this->Auto_model->load(array(
                "model_id"=>filter_input(INPUT_POST, 'item'),
                'fields'=>array("{$this->Auto_model->getSelfName()} as title",
                                "{$this->Auto_model->getSelfId()} as value")
            ),false);
            //var_dump($inAvto);
            //$inAvto = ($inModel)?array_merge($this->Auto_model->getNoItem(),$inModel):$this->Auto_model->getNoItem();
            echo jdecoder(json_encode($inAvto));
        }
    }
    public function change_status($aParam=null) {
        if(filter_input(INPUT_POST, 'process')) {
            $inProcess = filter_input(INPUT_POST, 'process');
            switch ($inProcess) {
                case 'change-status-avto':
                    $inAvto = $this->Auto_model->load(array('fields'=>array("{$this->Auto_model->getSelfId()} as value","{$this->Auto_model->getStatus()} as status")
                    ),false);
                    $inAvto = $inAvto[0];
                    $inNewStatus = ($inAvto['status']==0)?1:0;
                    $inAvto[$this->Auto_model->getSelfId()] = $inAvto['value']; unset($inAvto['value']);
                    $inAvto[$this->Auto_model->getStatus()] = $inNewStatus; unset($inAvto['status']);
                    $this->Auto_model->save($inAvto);
                    echo jdecoder(json_encode(array('new-status'=>$inNewStatus)));
                    break;
            }
        }
    }
}

