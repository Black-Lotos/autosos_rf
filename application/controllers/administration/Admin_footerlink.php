<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_footerlink extends MY_Controller {    
    //
    
    //
    public function index() {   
    }
    //
    public function load() {
        $inArg = func_get_args();
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inProces = empty($inArg)?null:$inArg[0];
        
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Footerlink_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/content/admin_footerlink';
        //
        $inData['page']['data'] = $this->Footerlink_model->loadTree(
                array(
                    'tree'=>array('fields'=>array('cfooterlink_status','cfooterlink_id as value', 'cfooterlink_title as title')),
                    'item'=>array('fields'=>array('footerlink_status','footerlink_id as value', 'footerlink_title as title'))    
                ),false,$this->inPage,$this->inOutRecord
        );
        
        foreach ($inData['page']['data'] as $outKey => $outData) {
            $outName = "ref-сfooterlink-status-{$outData['value']}";                        
            $outChecked = ($outData['cfooterlink_status']==1)?'checked':'un-checked';
            $outData['action'] =    "<a href='#' id='ref-сfooterlink-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                    "<a href='#' id='ref-сfooterlink-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                    "<a href='#' id='ref-сfooterlink-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"        
                                    ;
            if (!empty($outData['sub_tree'])) {
                foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                    $outChecked = ($outSData['footerlink_status']==1)?'checked':'un-checked';
                    $outSData['action'] =    "<a href='#' id='ref-footerlink-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-footerlink-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-footerlink-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"
                                            ;
                    $outData['sub_tree'][$outSKey] = $outSData;
                }
            }
            $inData['page']['data'][$outKey] = $outData;
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        //$inCategory = $this->Footerlink_model->loadCategory(array('cfooterlink_status'=>1,'fields'=>array('cfooterlink_title as title','cfooterlink_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Footerlink_model->getForm(
                $inData,
                PROCESS_FOOTERLINK_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        //$inActive = $inData['cfooterlink_id'];
        $inData['cfooterlink_id'] = $this->Footerlink_model->loadCategory(array('cfooterlink_status'=>1,'fields'=>array('cfooterlink_title as title','cfooterlink_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Footerlink_model->getForm(
            $inData
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        if (!empty($_POST['footerlink_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['footerlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Footerlink_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inActive = $inData['cfooterlink_id'];
            $inData['cfooterlink_id'] = $this->Footerlink_model->loadCategory(array('cfooterlink_status'=>1,'fields'=>array('cfooterlink_title as title','cfooterlink_id as value')));
            $inData['cfooterlink_id'] = setCategoryActiveItem($inData['cfooterlink_id'], $inActive);
            //
            setLanguage($inData,true);
            //echo "<pre>"; var_dump($inData); die();
            $inData['form'] = $this->Footerlink_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (!empty($_POST['cfooterlink_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cfooterlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Footerlink_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            //
            setLanguage($inData,true);
            $inData['form'] = $this->Footerlink_model->getForm(
                $inData,
                PROCESS_FOOTERLINK_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['footerlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inFooterlink = $this->Footerlink_model->loadById($inId);
                    $inFooterlink['footerlink_status'] = ($inFooterlink['footerlink_status']==0)?1:0;
                    $this->Footerlink_model->save($inFooterlink);
                    echo $inFooterlink['footerlink_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cfooterlink_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCFooterlink = $this->Footerlink_model->loadCategoryById($inId);
                    $inCFooterlink['cfooterlink_status'] = ($inCFooterlink['cfooterlink_status']==0)?1:0;
                    $this->Footerlink_model->save_category($inCFooterlink);
                    echo $inCFooterlink['cfooterlink_status'];
                    break;
                case 'change-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['footerlink_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inFooterlink = $this->Footerlink_model->loadById($inId);
                    $inFooterlink['footerlink_main'] = ($inFooterlink['footerlink_main']==0)?1:0;
                    $this->Footerlink_model->save($inFooterlink);
                    echo $inFooterlink['footerlink_main'];
                    break;
            }
        }
        
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['footerlink_status'])&&$inDecode['footerlink_status']=='on') {
                $inDecode['footerlink_status']=1;
            }
            if(!empty($inDecode['footerlink_top'])&&$inDecode['footerlink_top']=='on') {
                $inDecode['footerlink_top']=1;
            }
            if(!empty($inDecode['footerlink_main'])&&$inDecode['footerlink_main']=='on') {
                $inDecode['footerlink_main']=1;
            }
            
            /*if ($_FILES['images_id']['size']>0) {
                
                $inFName = $_FILES['images_id']['tmp_name'];
                $inExt = pathinfo($_FILES['images_id']['name']); $inExt = $inExt['extension'];
                $inGenName = 'footerlink_'.md5(time()).".$inExt";
                $outFName = 'uploads/'.$inGenName;
                $inDecode['images_id'] = $inGenName;
                
                $outResultMove = move_uploaded_file($inFName, $outFName);
                if($outResultMove) {
                    $writeImages = array(
                        'images_created'=>date('Y-m-d H:i:s',time()),
                        'images_folder'=>'uploads/',
                        'images_name'=>$inGenName,
                        'images_type'=>T_IMAGE);
                    $this->load->model('Images_model','images');
                    $inDecode['images_id'] = $this->images->save($writeImages);
                    $inDecode['images_id'] = $inDecode['images_id']['rec-no'];
                    //$inDecode['images_id'] = $inDecode['images_id']['rec_no'];
                }
            } else {unset($inDecode['images_id']);}*/
            
            $this->Footerlink_model->save($inDecode);
            Goto_Page('/administration/content/admin_footerlink');
        }
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['cfooterlink_status'])&&$inDecode['cfooterlink_status']=='on') {
                $inDecode['cfooterlink_status']=1;
            }
            echo $this->Footerlink_model->save_category($inDecode);
        }
        Goto_Page('/administration/content/admin_footerlink');
    }
    //
    public function delete() {
        $outMatches = array();
        if (filter_input(INPUT_POST,'footerlink_id',FILTER_SANITIZE_SPECIAL_CHARS)&&
                (filter_input(INPUT_POST,'process',FILTER_SANITIZE_SPECIAL_CHARS)=='delete-footerlink')) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'footerlink_id',FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
            $this->Footerlink_model->DeleteById($inId);
        }
    }
    //
    public function delete_section() {
        $outMatches = array();
        if (filter_input(INPUT_POST,'footerlink_id',FILTER_SANITIZE_SPECIAL_CHARS)&&
                (filter_input(INPUT_POST,'process',FILTER_SANITIZE_SPECIAL_CHARS)=='delete-footerlink')) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'footerlink_id',FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
            //проверяем на дочерние пункты
            $inChild = $this->Footerlink_model->load(array($this->Footerlink_model->getCategoryId()=>$inId),false);
            if (!$inChild) {
                setMessage('Группа ссылок удалена!', 'status');
                $inData = $this->Footerlink_model->DeleteCategoryById($inId);
            } else {
                setMessage('Группу ссылок удалить не возможно. Есть дочерние элементы!', 'error');
            }
        }
    }
    //
    public function loadForSite() {
        //echo "test";
        $inData['output'] = $this->Footerlink_model->getOutput();
        $inData['data'] = $this->Footerlink_model->load();
        $outResult = $inData;
    }
}
