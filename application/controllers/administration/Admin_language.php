<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_language extends MY_Controller {
    protected function afterInclude($aData = array()) {
        $this->load->model('Language_model');
        return $aData;
    }
    //
    public function index() {   
    
    }
    public function load() {
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Users_model->loadCountPage($this->inOutRecord);
        $inData['page']['data'] = $this->Language_model->load(array('fields'=>array('lan_status','language_id as value', 'lan_title as title')),false,$this->inPage,$this->inOutRecord);
        //        loadLanguage($this->inPage);
        foreach ($inData['page']['data'] as $outKey => $outData) {
            $outChecked = ($outData['lan_status']==1)?'checked':'un-checked';
            $outData['action'] =    "<a href='#' id='ref-lan-edit-{$outData['value']}' class='action-base action-edit'></a>".
                                    //"<a href='#' id='ref-lan-delete-{$outData['value']}' class='action-base action-delete'></a>".
                                    "<a href='#' id='ref-lan-checked-{$outData['value']}' class='action-base action-{$outChecked}'></a>";
            $inData['page']['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inData['form'] = $this->Language_model->getForm($inData);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        //var_dump($_POST);
        if (!empty($_POST['item_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['item_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Language_model->load(array('language_id'=>$inMcId));
            //var_dump($inData); die();
            $inData['form'] = $this->Language_model->getForm($inData);
            //var_dump($inData); die();
            //var_dump($this->twig->render("administration/common/form-system.twig", $inData));
        }
    }
    //
    public function change_status() {
        
    }
    //
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=array();
            foreach($_POST['data_form'] as $outKey=>$outData) {
                if (!empty($outData['value']))
                    $inDecode[$outData['name']]=$outData['value'];
            }
            //$inDecode['owner_id'] = $this->Menu_model->
            //echo "<pre>"; var_dump($inArg,$inDecode); 
            if($inDecode['lan_status']=='on') {
                $inDecode['lan_status']=1;
            }
            //echo "<pre>"; var_dump($inDecode); die();
            echo $this->Language_model->save($inDecode);
            //echo $this->inUriString; 
            //die($this->inUriString);
            //Goto_Page("/administration/content");
            
        }
    }
}

