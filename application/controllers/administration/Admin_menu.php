<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_menu extends MY_Controller {
    //
    public function index() {   
    }
    //
    public function load($aParam=null) {   
        $inData = array();
        //var_dump($this->inPage); die();
        $inSelectMenu = !empty($aParam)?"/$aParam/":"";
        $incAction = (!empty($aParam))?'menu':"mc";
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Menu_model->loadCategoryCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/content/admin_menu';
        $inData['page']['setup_page'] = $this->Menu_model->getOutput();
        if(!$inSelectMenu) {
            $inData['page']['data'] = $this->Menu_model->loadCategory(array('fields'=>array('mc_status as status','mc_title as title','menu_category_id as value')));
        } else {
            //var_dump($this->Menu_model->getCategoryId()); die();
            $inData['page']['data'] = $this->Menu_model->loadTree(
                array(
                    'tree'=>array($this->Menu_model->getCategoryId()=>$aParam,'fields'=>array('mc_status as status','mc_title as title','menu_category_id as value')),
                    'item'=>array($this->Menu_model->getCategoryId()=>$aParam,'owner_id'=>0,'fields'=>array("{$this->Menu_model->getSelfId()} as value","{$this->Menu_model->getSelfName()} as title", "{$this->Menu_model->getStatus()} as status"))    
                ),false,$this->inPage,$this->inOutRecord);
        }
        //echo "<pre>"; var_dump($inData['page']['data']); die();
        if ($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData['status']==1)?'checked':'un-checked';
                if (empty($aParam))
                $outData['action'] =   
                    "<a href='/administration/content/admin_menu/{$outData['value']}/?page={$this->inPage}' id='ref-menu-child-{$outData['value']}' class='action-base action-child' title='Перейти к пунктам'></a>";    
                else $outData['action']='';           
                $outData['action'] .=
                    "<a href='#' id='ref-{$incAction}-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>";
                    if(empty($outData['sub_tree'])) {
                        $outData['action'] .= "<a href='#' id='ref-{$incAction}-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>";
                    };
                    $outData['action'] .= "<a href='#' id='ref-{$incAction}-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                    if (!empty($outData['sub_tree'])) {
                        foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                            $outChecked = ($outSData['status']==1)?'checked':'un-checked';
                            $outSData['action'] =   
                                "<a href='#' id='ref-menu-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                "<a href='#' id='ref-menu-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                "<a href='#' id='ref-menu-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                            $outData['sub_tree'][$outSKey] = $outSData;
                        }
                    }
                $inData['page']['data'][$outKey] = $outData;                        
            }
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add($aParam=null) {   
        $inData = array();
        //$inActive = $inData['menu_category_id'];
        $inActive = !empty($aParam)?$aParam:0;
        $inData['menu_category_id'] = $this->Menu_model->loadCategory(array('fields'=>array('mc_title as title','menu_category_id as value')));
        $inData['menu_category_id'] = setCategoryActiveItem($inData['menu_category_id'], $inActive);
        //родитель в пределах меню
        //$inActiveOwner = $inData['owner_id'];
        //var_dump($inData['owner_id']);
        //$this->Menu_model->debug();
        $inData['owner_id'] = $inResult = $this->Menu_model->load(array('menu_category_id'=>$inActive,'fields'=>array('mi_title_ru as title','menu_id as value','menu_id','owner_id','mi_url')),false,false);
        if (!$inResult) {
            $inData['owner_id'] = array(array('title'=>'- родитель отсутствует - ','value'=>'0'));
            $inData['owner_id'] = setCategoryActiveItem($inData['owner_id'], '0');
        } else {
            $inData['owner_id'] = array_merge(array(array('title'=>'- родитель отсутствует - ','value'=>'0')),$inData['owner_id']);
        }
        //var_dump($inData['owner_id']);
        //$inData['owner_id'] = setCategoryActiveItem($inData['owner_id'], $inActiveOwner);
        //$inParent = 
        $inData['language_id'] = $this->Language_model->load(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')));
        if ($aParam>0) {
            $inData['form'] = $this->Menu_model->getForm(
                    $inData
            );
        } else {
            $inData['form'] = $this->Menu_model->getForm(
                    $inData,
                    PROCESS_MENU_ADD_SECTION
            );
        }
        //var_dump($inData['form']); die();
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function edit_section() {   
        if (filter_input(INPUT_POST, 'menu_id')&&filter_input(INPUT_POST, 'process')=='menu-edit') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_id'), $outMatches))>0?(int)$outMatches[0]:0;
            $inData = $this->Menu_model->loadCategoryById($inMcId);
            $inData['language_id'] = $this->Language_model->load(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')));
            //echo "<pre>"; var_dump($inData['language_id']); die();
            $inData['form'] = $this->Menu_model->getForm(
                $inData,
                PROCESS_MENU_ADD_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function delete_section(){   
        if (filter_input(INPUT_POST, 'menu_id')&&filter_input(INPUT_POST, 'process')=='menu-delete') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //проверяем на дочерние пункты
            $inChild = $this->Menu_model->load(array($this->Menu_model->getCategoryId()=>$inMcId),false);
            if (!$inChild) {
                setMessage('Меню удалено!', 'status');
                $inData = $this->Menu_model->DeleteCategoryById($inMcId);
            } else {
                setMessage('Меню удалить не возможно. Есть дочерние элементы!', 'error');
            }
            //echo "<pre>"; var_dump($inChild); die();
            //$inData = $this->Menu_model->DeleteCategoryById($inMcId);
            //$inData = array();
        }
    }
    //
    public function edit() {   
        $outMatches = array();
        if (filter_input(INPUT_POST, 'menu_item_id')&&filter_input(INPUT_POST, 'process')=='menu-item-edit') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'), $outMatches))>0?(int)$outMatches[0]:0;
            $inData = $this->Menu_model->loadById($inMcId);
            //родительское меню
            $inActive = $inData['menu_category_id'];
            $inData['menu_category_id'] = $this->Menu_model->loadCategory(array('fields'=>array('mc_title as title','menu_category_id as value')));
            $inData['menu_category_id'] = setCategoryActiveItem($inData['menu_category_id'], $inActive);
            //родитель в пределах меню
            $inActiveOwner = $inData['owner_id'];
            $inData['owner_id'] = array_merge(array(array('title'=>'- родитель отсутствует - ','value'=>'0')),
            $this->Menu_model->load(array('menu_category_id'=>$inActive,'fields'=>array('mi_title_ru as title','menu_id as value','menu_id','owner_id','mi_url'))));
            $inData['owner_id'] = setCategoryActiveItem($inData['owner_id'], $inActiveOwner);
            //язык если нужен
            $inData['language_id'] = $this->Language_model->load(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')));
            //собираем селекты для вывода
            if (!empty($inRoot) && !empty($inCategory)) {
                $inData['owner_id'] = array_merge($inRoot,$inCategory);
            }
            $inData['form'] = $this->Menu_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function delete(){   
        if (filter_input(INPUT_POST, 'menu_item_id')&&filter_input(INPUT_POST, 'process')=='menu-item-delete') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'), $outMatches))>0?(int)$outMatches[0]:0;
            // проверяем на дочерние пункты
            $inChild = $this->Menu_model->load(array('owner_id'=>$inMcId),false);
            // 
            if (!$inChild) {
                setMessage('Пункт удален!', 'status');
                $inData = $this->Menu_model->DeleteById($inMcId);
            } else {
                setMessage('Пункт удалить не возможно. Есть дочерние элементы!', 'error');
            }
        }
    }
    //
    public function save() {   
        if (!empty($_POST)) {
            $inDecode=$_POST;
            // проверка статуса активен не активен и его установка
            $inDecode['mi_status'] = ($inDecode['mi_status']=='on')?1:0;
            $this->Menu_model->save($inDecode);
            Goto_Page("/administration/content/admin_menu/{$inDecode['menu_category_id']}");
        }
    }
    public function save_section() {   
        if (!empty($_POST)) {
            $inDecode=$_POST;
            // проверка статуса активен не активен и его установка
            $inDecode['mc_status'] = ($inDecode['mc_status']=='on')?1:0;
            $this->Menu_model->save_category($inDecode);
            Goto_Page("/administration/content/admin_menu/");
        }
    }
    public function change_weight() {
        if (filter_input(INPUT_POST, 'process')&&filter_input(INPUT_POST, 'menu_item_id')) {
            $inSplit = filter_input(INPUT_POST, 'process');
            $inPattern = "/(([A-Za-z]+)\-([0-9]+)$)/";
            $inProcAndId = preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'),$outMatches)>0?$outMatches[0]:false;
            $inParse = preg_split("/\-/", $inProcAndId);
            $outGo = 'DO-'.mb_strtoupper($inParse[0]);
            $outId = (int)$inParse[1];
            $inItem = $this->Menu_model->load(array('menu_id'=>$outId,'fields'=>array('menu_id','mi_weight','owner_id','menu_category_id','mi_title_ru')),true,false);
            $inItem = ($inItem)?$inItem[0]:false;
            if ($inItem) {
                $inItems = $this->Menu_model->load(array('owner_id'=>$inItem['owner_id'],'menu_category_id'=>$inItem['menu_category_id'],'fields'=>array('menu_id','mi_weight','owner_id','menu_category_id','mi_title_ru')),true,false);
            }
            
            switch ($outGo) {
                case 'DO-INC' :
                    $inItem['mi_weight'] =  (int)$inItem['mi_weight']+1;
                    $inStep = 1;
                    //var_dump($inItem); die();
                    break;
                case 'DO-DEC' :
                    $inItem['mi_weight'] =  (int)$inItem['mi_weight']-1;
                    $inStep = -1;
                    //var_dump($inItem); die();
                    break;
            }
            foreach ($inItems as $inKey => $inWeight) {
               if($inWeight['menu_id']==$inItem['menu_id'])  {
                   $inWeight = $inItem;
               }
               if ($inWeight['mi_weight']==$inItem['mi_weight']&&$inWeight['menu_id']!=$inItem['menu_id']) {
                   $inWeight['mi_weight'] = (int)$inWeight['mi_weight']+$inStep;
               }
               $inItems[$inKey] = $inWeight;
               $this->Menu_model->save($inWeight);
            }
            //echo "<pre>"; var_dump($inItems); die();
        };
    }
    public function ChangeStatus() {
        //var_dump($_POST); die();
        if ((filter_input(INPUT_POST, 'process')=='menu-item-check')&&filter_input(INPUT_POST, 'menu_item_id')) {
            $inPattern = '/([0-9]+)$/';
            $inItemId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //var_dump($inItemId); die();
            //$this->Menu_model->Debug();
            $outRes = $this->Menu_model->changeStatus($inItemId,'mi_status');
            echo $outRes;
        }
        if ((filter_input(INPUT_POST, 'process')=='menu-check')&&filter_input(INPUT_POST, 'menu_item_id')) {
            $inPattern = '/([0-9]+)$/';
            $inItemId = (preg_match($inPattern, filter_input(INPUT_POST, 'menu_item_id'), $outMatches))>0?(int)$outMatches[0]:0;
            //var_dump($inItemId); die();
            //$this->Menu_model->Debug();
            $outRes = $this->Menu_model->changeStatus($inItemId,'mc_status',true);
            echo $outRes;
        }
    }
}

