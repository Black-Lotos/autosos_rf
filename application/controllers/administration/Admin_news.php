<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_news extends MY_Controller {
    //
    public function index() {   
        
    }
    public function load() {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inArg = func_get_args();
        $inProces = empty($inArg)?null:$inArg[0];
        $inData['output'] = $this->News_model->getOutput($inProces);
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->News_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_news';
        //
        //echo "<pre>"; var_dump($inProces); die();
        $inData['page']['data'] = $this->News_model->loadTree(array(
            'tree'=>array('fields'=>array("{$this->News_model->getCategoryId()} as value, {$this->News_model->getCategoryName()} as title, {$this->News_model->getCategoryStatus()}")),
            'item'=>array('fields'=>array("{$this->News_model->getSelfId()} as value, {$this->News_model->getSelfName()} as title, {$this->News_model->getStatus()}, news_status_main")),        
                    ),true,$this->inPage,$this->inOutRecord);
        //echo "<pre>"; var_dump($inData['data']); die();
        if($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outName = "ref-snews-status-{$outData['value']}";                        
                $outChecked = ($outData['nc_status']==1)?'checked':'un-checked';
                //$outData['status'] = "<input name='{$outName}' type='checkbox' id='{$outName}' $outChecked />";
                //
                $outData['action'] =    "<a href='#' id='ref-snews-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать'></a>";
                    if (empty($outData['sub_tree']))    
                        $outData['action'] .= "<a href='#' id='ref-snews-delete-{$outData['value']}' class='action-base action-delete' title='Удалить'></a>";
                        $outData['action'] .= "<a href='#' id='ref-snews-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
                if (!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                        $outChecked = ($outSData['news_status']==1)?'checked':'un-checked';
                        //var_dump($outSData['news_status_main']); die();
                        $outMain = ($outSData['news_status_main']==1)?'checked':'un-checked';
                        $outSData['action'] =    "<a href='#' id='ref-news-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                                "<a href='#' id='ref-news-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                                "<a href='#' id='ref-news-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>"
                                                ;
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }
                $inData['page']['data'][$outKey] = $outData;
            }
        }
        //echo "<pre>"; var_dump($inData['data']); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inData['nc_id'] = $this->News_model->loadCategory(array('nc_status'=>1,'fields'=>array('nc_title as title','nc_id as value')));
        $inData['cpicture_id'][] = array('title'=>'- без коллекции -','value'=>'0');
        /*$inData['cpicture_id'] = array_merge($inData['cpicture_id'],                
        $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')))
                );*/
        //echo "<pre>"; var_dump($inData['cpicture_id']); die();
        setLanguage($inData);
        //var_dump($inData['cpicture_id']); die();
        $inData['form'] = $this->News_model->getForm(
            $inData
        );
        //var_dump($inData['form']); die();
        
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->News_model->loadCategory(array('nc_status'=>1,'fields'=>array('nc_title as title','nc_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->News_model->getForm(
                $inData,
                PROCESS_NEWS_SECTION_ADD
        );
        //var_dump($inData['form']); die();
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function edit() {
        if (!empty($_POST['news_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['news_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->News_model->loadById($inMcId);
            $inActive = $inData['nc_id'];
            $inData['nc_id'] = $this->News_model->loadCategory(array('nc_status'=>1,'fields'=>array('nc_title as title','nc_id as value')));
            $inData['nc_id'] = setCategoryActiveItem($inData['nc_id'], $inActive);
            //echo "<pre>"; var_dump($inData); die();
            $inMenu = null;
            $inCategory = $this->News_model->loadCategory(array('nc_status'=>1,'fields'=>array('nc_title as title','nc_id as value')));
            //коллекция изображений
            $inActive = $inData['cpicture_id'];
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inData['cpicture_id'] = array(array('title'=>'- без коллекции -','value'=>'0'));
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inData['cpicture_id'] = array_merge($inData['cpicture_id'],
                $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')))
                );
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            $inData['cpicture_id'] = setCategoryActiveItem($inData['cpicture_id'], $inActive);
            //echo "<pre>"; var_dump($inData['cpicture_id']); die();
            setLanguage($inData,true);
            $inData['form'] = $this->News_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function delete() {
        if (!empty($_POST['news_id'])&&$_POST['process']=='delete-news') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['news_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->News_model->DeleteById($inMcId);
        }
    }
    public function delete_category() {
        if (filter_input(INPUT_POST,'process')=='delete-snews' && filter_input(INPUT_POST,'nc_id')) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['nc_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->News_model->deleteCategoryById($inMcId);
        }
    }
        

    public function clear_image() {
        if (filter_input(INPUT_POST, 'news_id') && filter_input(INPUT_POST, 'process')=='clear-image-news') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['news_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->News_model->loadById($inMcId);
            $inData['images_id'] = null;
            //echo json_encode($inData); die();
            //$this->Debug();
            $inRes = $this->News_model->save($inData);
            $inRes["rec-no"] = '';
            $inRes["rec-name"] = "images/empty210x210.png";
            echo json_encode($inRes);
        }
    }
    public function edit_section() {
        if (!empty($_POST['nc_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['nc_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->News_model->loadCategoryById($inMcId);
            setLanguage($inData,true);
            $inData['form'] = $this->News_model->getForm(
                $inData,
                PROCESS_NEWS_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function save() {   
        
        if (!empty($_POST)) {
            $inDecode=$_POST; $outResultMove=true;
            if(!empty($inDecode['news_status'])&&$inDecode['news_status']=='on') {
                $inDecode['news_status']=1;
            }
            if(!empty($inDecode['news_status_top'])&&$inDecode['news_status_top']=='on') {
                $inDecode['news_status_top']=1;
            }
            if(!empty($inDecode['news_status_main'])&&$inDecode['news_status_main']=='on') {
                $inDecode['news_status_main']=1;
            }
            
            //$inDecode['images_id'] = 'uploads/empty.jpg';
            
            if ($_FILES['images_id']['size']>0) {
                $inFName = $_FILES['images_id']['tmp_name'];
                $inExt = pathinfo($_FILES['images_id']['name']); $inExt = $inExt['extension'];
                $inGenName = 'news_'.md5(time()).".$inExt";
                $outFName = 'uploads/'.$inGenName;
                $inDecode['images_id'] = $inGenName;
                $outResultMove = move_uploaded_file($inFName, $outFName);
                if($outResultMove) {
                    $writeImages = array(
                        'images_created'=>date('Y-m-d H:i:s',time()),
                        'images_folder'=>'uploads/',
                        'images_name'=>$inGenName,
                        'images_type'=>T_IMAGE);
                    $this->load->model('Images_model','images');
                    $inDecode['images_id'] = $this->images->save($writeImages);
                    $inDecode['images_id'] = $inDecode['images_id']['rec-no'];
                    //$inDecode['images_id'] = $inDecode['images_id']['rec_no'];
                } else {unset($inDecode['images_id']);}
                //die();
            }
            $inDecode['images_id'] = ($inDecode['images_id']==0)?null:$inDecode['images_id'];
            $this->News_model->save($inDecode);
            Goto_Page('/administration/section/admin_news');
        }
    }
    //
    public function change_status() {
        if (!empty($_POST)) {
            //var_dump(filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)); die();
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'news_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->News_model->loadById($inId);
                    $inBlogs['news_status'] = ($inBlogs['news_status']==0)?1:0;
                    $this->News_model->save($inBlogs);
                    echo $inBlogs['news_status'];
                    break;
                case 'change-status-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'news_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->News_model->loadById($inId);
                    $inBlogs['news_status_main'] = ($inBlogs['news_status_main']==0)?1:0;
                    $this->News_model->save($inBlogs);
                    echo $inBlogs['news_status_main'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'nc_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    //echo $inId; die();
                    $inCBlogs = $this->News_model->loadCategoryById($inId);
                    $inCBlogs['nc_status'] = ($inCBlogs['nc_status']==0)?1:0;
                    $this->News_model->save_category($inCBlogs);
                    echo $inCBlogs['nc_status'];
                    break;
            }
        }
    }
    //
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
        }
        if ($this->News_model->save_category($inDecode)) {
            setMessage('Категория новостей записана.');
        } else  {
                    setMessage('Ошибка записи раздела новостей.','error');
                };
        Goto_Page('/administration/section/admin_news');
    }
    //
}

