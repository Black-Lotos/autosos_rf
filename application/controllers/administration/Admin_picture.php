<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_picture extends MY_Controller {
    protected function afterInclude($aData = array()) {
        $this->load->model('Picture_model');
    }
    public function index() {   
        $this->load();
    }
    public function load($aProcess=null) {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['output'] = $this->Picture_model->getOutput();
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Picture_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/settings/admin_settings';  
        $inData['page']['data'] = $this->Picture_model->loadTree(array(
            'tree'=>array('fields'=>array("{$this->Picture_model->getCategoryId()} as value, {$this->Picture_model->getCategoryName()} as title, {$this->Picture_model->getCategoryStatus()}")),
            'item'=>array('fields'=>array("{$this->Picture_model->getSelfId()} as value, {$this->Picture_model->getSelfName()} as title, {$this->Picture_model->getStatus()}, picture_text as description, picture_id")),        
                    ),false,$this->inPage,$this->inOutRecord);
        if ($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData[$this->Picture_model->getCategoryStatus()]==1)?'checked':'un-checked';
                $outData['action'] =    "<a href='#' id='ref-spicture-edit-{$outData['value']}' class='action-base action-edit'></a>";
                if (empty($outData['sub_tree'])) {
                    $outData['action'] .= "<a href='#' id='ref-spicture-delete-{$outData['value']}' class='action-base action-delete'></a>";
                }
                $outData['action'] .= "<a href='#' id='ref-spicture-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                if (!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                        $outSData['title'] = "({$outSData['title']}) - {$outSData['description']}";
                        $outChecked = ($outSData[$this->Picture_model->getStatus()]==1)?'checked':'un-checked';
                        $outSData['action'] =   "<a href='#' id='ref-picture-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                                "<a href='#' id='ref-picture-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                                "<a href='#' id='ref-picture-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }
                $inData['page']['data'][$outKey] = $outData;
            }
        }
        //echo "<pre>"; var_dump($inData);
        echo $this->twig->render("administration/common/list-system-img.twig", $inData);
    }
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inData['cpicture_id'] = $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')));
        $inData['language_id'] = 1;
        //$inData['cpicture_id'] = setCategoryActiveItem($inData['cpicture_id'], $inActive);
        //setLanguage($inData);
        $inData['form'] = $this->Picture_model->getForm(
            $inData
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        $inCategory = $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')));
        $inData['language_id'] = 1;
        //setLanguage($inData);
        //var_dump($inData); die();
        $inData['form'] = $this->Picture_model->getForm(
                $inData,
                PROCESS_CONTENT_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function edit() {
        if (!empty($_POST['picture_id'])) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Picture_model->loadById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            $inActive = $inData['cpicture_id'];
            $inData['cpicture_id'] = $this->Picture_model->loadCategory(array('cpicture_status'=>1,'fields'=>array('cpicture_title as title','cpicture_id as value')));
            $inData['cpicture_id'] = setCategoryActiveItem($inData['cpicture_id'], $inActive);
            //setLanguage($inData,TRUE);
            $inData['language_id'] = 1;
            $inData['form'] = $this->Picture_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function edit_section() {
        if (filter_input(INPUT_POST,'cpicture_id') && filter_input(INPUT_POST,'process')=='edit-category') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'cpicture_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //echo "<pre>"; var_dump($inMcId); die();
            $inData = $this->Picture_model->loadCategoryById($inMcId);
            //echo "<pre>"; var_dump($inData); die();
            //setLanguage($inData,TRUE);
            $inData['language_id'] = 1;
            $inData['form'] = $this->Picture_model->getForm(
                $inData,
                PROCESS_CONTENT_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function picture_code() {
        $inProcess = filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS);
        if ($inProcess=='picture-code') {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $outObj = (object)$this->Picture_model->loadById($inId);
            echo $outObj->picture_code;
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Picture_model->loadById($inId);
                    $inBlogs['picture_status'] = ($inBlogs['picture_status']==0)?1:0;
                    $this->Picture_model->save($inBlogs);
                    echo $inBlogs['picture_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'cpicture_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inCBlogs = $this->Picture_model->loadCategoryById($inId);
                    //var_dump($inCBlogs);
                    $inCBlogs['cpicture_status'] = ($inCBlogs['cpicture_status']==0)?1:0;
                    $this->act_model->save_category($inCBlogs);
                    echo $inCBlogs['cpicture_status'];
                    break;
                case 'change-status-main':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inBlogs = $this->Picture_model->loadById($inId);
                    $inBlogs['picture_main'] = ($inBlogs['picture_main']==0)?1:0;
                    $this->Picture_model->save($inBlogs);
                    echo $inBlogs['picture_main'];
                    break;
            }
        }
        
    }
    public function save() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if(!empty($inDecode['picture_status'])&&$inDecode['picture_status']=='on') {
                $inDecode['picture_status']=1;
            }
            if(!empty($inDecode['picture_status_top'])&&$inDecode['picture_status_top']=='on') {
                $inDecode['picture_status_top']=1;
            }
            if(!empty($inDecode['picture_status_main'])&&$inDecode['picture_status_main']=='on') {
                $inDecode['picture_status_main']=1;
            }
            if (!empty($_FILES['picture_file']['name'])) {
                foreach($_FILES['picture_file']['name'] as $keyImg => $dataImg) {
                    $inFName = $_FILES['picture_file']['tmp_name'][$keyImg];
                    $inExt = pathinfo($_FILES['picture_file']['name'][$keyImg]); $inExt = $inExt['extension'];
                    $inGenName = 'pic_'.md5(time()).$keyImg.".$inExt";
                    $outFName = 'picture/'.$inGenName;
                    //var_dump($inFName); die();
                    $inDecode['picture_file'] = $inGenName;
                    move_uploaded_file($inFName, $outFName);
                    $this->Picture_model->writeWm($outFName);
                    $this->Picture_model->save($inDecode);
                }
            }
        }
        Goto_Page('/administration/section/admin_picture');
    }
    public function save_category() {   
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
        }
        //var_dump($inDecode); die();
        $this->Picture_model->save_category($inDecode);
        Goto_Page('/administration/section/admin_picture');
    }
    public function delete() {
        if (!empty($_POST['picture_id'])&&$_POST['process']=='delete-picture') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['picture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Picture_model->DeleteById($inMcId);
        }
    }
    public function delete_section() {
        
        if (!empty($_POST['cpicture_id'])&&$_POST['process']=='delete-category') {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['cpicture_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Picture_model->deleteCategoryById($inMcId);
        }
    }
}

