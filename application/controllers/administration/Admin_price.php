<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_price extends MY_Controller {
    protected function afterInclude($aData = array()) {
        $this->load->model('Invoice_model');
        return $aData;
    }
    //
    public function index() {   
    
    }
    public function load($aParam=null) {
        $this->load->model('Invoice_model');
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Invoice_model->loadCountPage($this->inOutRecord);
        $inData['page']['data'] = $this->Invoice_model->loadTree(
            array(
                'tree'=>array('fields'=>array(
                    "{$this->Invoice_model->getCategoryId()} as value",
                    "{$this->Invoice_model->getCategoryName()} as title",
                    "{$this->Invoice_model->getCategoryStatus()} as status")
                ),
                'item'=>array('fields'=>array(
                    "{$this->Invoice_model->getSelfId()} as value",
                    "{$this->Invoice_model->getSelfName()} as title",
                    "{$this->Invoice_model->getStatus()} as status")
                ),
            )
        );
        //echo "<pre>"; var_dump($inData['page']['data']); die();        
        //        loadLanguage($this->inPage);
        if($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData['status']==1)?'checked':'un-checked';
                $outData['action']  =  "<a href='#' id='ref-{$this->Invoice_model->getCategoryPrefix()}-edit-{$outData['value']}' class='action-base action-edit'></a>";
                $outData['action']  .= (empty($outData['sub_tree']))?
                    "<a href='#' id='ref-{$this->Invoice_model->getCategoryPrefix()}-delete-{$outData['value']}' class='action-base action-delete'></a>":"";
                $outData['action']  .="<a href='#' id='ref-{$this->Invoice_model->getCategoryPrefix()}-checked-{$outData['value']}' class='action-base action-{$outChecked}'></a>";
                if(!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                        $outSChecked = ($outSData['status']==1)?'checked':'un-checked';
                        $outSData['action'] =  "<a href='#' id='ref-{$this->Invoice_model->getPrefix()}-edit-{$outSData['value']}' class='action-base action-edit'></a>";
                        $outSData['action'] .= "<a href='#' id='ref-{$this->Invoice_model->getPrefix()}-delete-{$outSData['value']}' class='action-base action-delete'></a>";
                        $outSData['action'] .="<a href='#' id='ref-{$this->Invoice_model->getPrefix()}-checked-{$outSData['value']}' class='action-base action-{$outSChecked}'></a>";
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }
                $inData['page']['data'][$outKey] = $outData;
            }
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add() {
        $this->load->model('Invoice_model');
        $inData = array();
        $inData[$this->Invoice_model->getCategoryId()] = $this->Invoice_model->loadCategory(array(
            $this->Invoice_model->getCategoryStatus()=>1,
            'fields'=>array("{$this->Invoice_model->getCategoryName()} as title","{$this->Invoice_model->getCategoryId()} as value"))
        );
        $inData['form'] = $this->Invoice_model->getForm($inData);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function add_section() {
        $this->load->model('Invoice_model');
        $inData = array();
        $inData['form'] = $this->Invoice_model->getForm($inData,PROCESS_SECTION_ADD);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        $this->load->model('Invoice_model');
        if (filter_input(INPUT_POST, 'process')=='edit-item' && filter_input(INPUT_POST, 'item_id')) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'item_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Invoice_model->loadById($inId);
            $inActiveCategory = $inData[$this->Invoice_model->getCategoryId()];
            $inData[$this->Invoice_model->getCategoryId()] = $this->Invoice_model->loadCategory(array(
                $this->Invoice_model->getCategoryStatus()=>1,
                'fields'=>array("{$this->Invoice_model->getCategoryName()} as title","{$this->Invoice_model->getCategoryId()} as value"))
            );
            $inData[$this->Invoice_model->getCategoryId()] = setActiveItem($inData[$this->Invoice_model->getCategoryId()], $inActiveCategory);    
            //var_dump($inData); die();
            $inData['form'] = $this->Invoice_model->getForm($inData);
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function delete() {
        $this->load->model('Invoice_model');
        if (filter_input(INPUT_POST, 'process')=='delete-item' && filter_input(INPUT_POST, 'item_id')) {
            $inSufix = '';
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['item_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $this->Invoice_model->Debug();
            $inData = $this->Invoice_model->DeleteById($inMcId);
        }
    }
    
    public function edit_section() {
        $this->load->model('Invoice_model');
        if (!empty($_POST['language_id'])) {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, $_POST['language_id'], $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Invoice_model->loadLanguageById($inMcId);
            $inMenu = null;
            $inData['form'] = $this->Invoice_model->getForm($inData,PROCESS_SECTION_ADD);
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    //
    public function change_status() {
        $this->load->model('Invoice_model');
    }
    //
    public function save() {   
        $this->load->model('Invoice_model');
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if($inDecode[$this->Invoice_model->getStatus()]=='on') { $inDecode[$this->Invoice_model->getStatus()]=1; }
            //echo "<pre>"; var_dump($inDecode); die();
            if ($this->Invoice_model->save($inDecode)) {
                setMessage('Услуга записана.');
            } else { setMessage('Ошибка записи услуги.','error'); };
            Goto_Page("/administration/section/admin_price");
            
        }
    }
    public function save_section() {   
        $this->load->model('Invoice_model');
        $inArg = func_get_args();
        if (!empty($_POST)) {
            $inDecode=$_POST;
            if($inDecode[$this->Invoice_model->getCategoryStatus()]=='on') { $inDecode[$this->Invoice_model->getCategoryStatus()]=1; }
            //echo "<pre>"; var_dump($inDecode); die();
            if ($this->Invoice_model->save_category($inDecode)) {
                setMessage('Группа услуг записана.');
            } else { setMessage('Ошибка записи группы услуг.','error'); };
            Goto_Page("/administration/section/admin_price");
        }
    }
}

