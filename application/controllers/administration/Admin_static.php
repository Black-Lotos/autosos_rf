<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_static extends MY_Controller {
    //
    public function index() {   
    }
    //
    public function load() {
        //$this->act_model->Debug();
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Static_model->loadCountPage($this->inOutRecord);
        $inData['page']['data'] = $this->Static_model->loadTree();
        $inData['page']['setup_page'] = $this->Static_model->getOutput();
        //echo "<pre>"; var_dump($inData['page']['data']); //die();
        if (empty($inData['page']['data'])) {
            $outDefault = array('spc_title'=>'Системные страницы','spc_text'=>'Системные страницы','spc_status'=>1,'language_id'=>1);
            //$this->act_model->Debug();
            //$this->act_model->save_category($outDefault);
            $inData['page']['data'] = $this->Static_model->loadCategory(array('fields'=>array('spc_title as title, spc_id as value, spc_status')));
        }
        //var_dump($inList); die();
        //$inData['data'] = ($inList==false)?array("title"=>'Нет категорий страниц. Введите категорию страниц.',"url"=>'#'):$inList;
        if(!empty($inData['page']['data'])) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData['spc_status']==1)?'checked':'un-checked';
                $outData['action'] =    "<a href='#' id='ref-spc-edit-{$outData['value']}' class='action-base action-edit'></a>";
                if (empty($outData['sub_tree']))
                    $outData['action'] .= "<a href='#' id='ref-spc-delete-{$outData['value']}' class='action-base action-delete'></a>";
                $outData['action'] .= "<a href='#' id='ref-spc-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";        
                if (!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                        $outChecked = ($outSData['sp_status']==1)?'checked':'un-checked';
                        $outSData['action'] =   "<a href='#' id='ref-sp-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                            "<a href='#' id='ref-sp-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                            "<a href='#' id='ref-sp-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                                            //"<a href='#' id='ref-picture-main-{$outSData['value']}' class='action-base action-main-{$outMain}' title='Отключить/Включить'></a>".
                                            //"<a href='#' id='ref-picture-video-{$outSData['value']}' class='action-base action-video' title='Код встраивания'></a>"        
                                            ;
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }                        
                $inData['page']['data'][$outKey] = $outData;
            }
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    public function add() {
        $inData = array();
        $inData['spc_id'] = $this->Static_model->loadCategory(array('spc_status'=>1,'fields'=>array('spc_title as title','spc_id as value')));
        setLanguage($inData);
        $inData['form'] = $this->Static_model->getForm(
                $inData
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function add_section() {
        $inData = array();
        $inArg = func_get_args();
        //$inCategory = $this->act_model->loadCategory(array('cquestion_status'=>1,'fields'=>array('cquestion_title as title','cquestion_id as value')));
        setLanguage($inData);
        //die('tyta');
        $inData['form'] = $this->Static_model->getForm(
                $inData,
                PROCESS_STATIC_ADD_SECTION
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        
        if (filter_input(INPUT_POST,'sp_id') && filter_input(INPUT_POST,'process')=='edit-item') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'sp_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //$this->act_model->Debug();
            $inData = $this->Static_model->loadById($inMcId);
            //echo "$inMcId"; die();
            $inActive = $inData['spc_id'];
            $inData['spc_id'] = $this->Static_model->loadCategory(array('fields'=>array('spc_title as title','spc_id as value')));
            $inData['spc_id'] = setCategoryActiveItem($inData['spc_id'], $inActive);
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Static_model->getForm(
                $inData
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function delete() {
        if (filter_input(INPUT_POST,'sp_id') && filter_input(INPUT_POST,'process')=='delete-item') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'sp_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //$this->act_model->Debug();
            $inData = $this->Static_model->loadById($inMcId);
            $inDataDel = $this->Static_model->deleteById($inMcId);
            setMessage("Статическая страница «{$inData['sp_title']}» удалена.", 'status');
        }   else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
        
    }
    public function edit_section() {
        //$inData = $this->act_model->loadCategoryById(2);
        //var_dump(filter_input(INPUT_POST,'spc_id') && filter_input(INPUT_POST,'process')=='edit-category');
        if (filter_input(INPUT_POST,'spc_id') && filter_input(INPUT_POST,'process')=='edit-category') {
            $inPattern = '/([0-9]+)$/';
            $inMcId = (preg_match($inPattern, filter_input(INPUT_POST,'spc_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //$this->act_model->Debug();
            $inData = $this->Static_model->loadCategoryById($inMcId);
            setLanguage($inData,TRUE);
            $inData['form'] = $this->Static_model->getForm(
                $inData,
                PROCESS_STATIC_ADD_SECTION
            );
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'spc_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inData = $this->Static_model->loadById($inId);
                    $inData['sp_status'] = ($inData['sp_status']==0)?1:0;
                    $this->Static_model->save($inData);
                    echo $inData['sp_status'];
                    break;
                case 'change-status-category':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, filter_input(INPUT_POST, 'spc_id', FILTER_SANITIZE_SPECIAL_CHARS), $outMatches)>0)?(int)$outMatches[0]:0;
                    $inData = $this->Static_model->loadCategoryById($inId);
                    $inData['spc_status'] = ($inData['spc_status']==0)?1:0;
                    $this->Static_model->save_category($inData);
                    echo $inData['spc_status'];
                    break;
            }
        }
        
    }
    public function save() {   
        if(filter_input(INPUT_POST,'process')=='item-save') {
            $inData = $_POST; unset($inData['process']);
            $inData['sp_status'] = (!empty($inData['sp_status']))?1:0;
            //$inData['sp_status'] = $inStatus;
            //$this->Static_model->Debug();
            if ($this->Static_model->save($inData)) {
                setMessage('Страница записана');
            } else setMessage('Ошибка записи','error');
            //setMessage('Страница записана');
            Goto_Page('administration/section/admin_static');
        }
    }
    public function save_section() {   
        //var_dump($_POST); die("section-save");
        if(filter_input(INPUT_POST,'process')=='section-save') {
            $inData = $_POST; unset($inData['process']);
            $inData['spc_status'] = (!empty($inData['spc_status']))?1:0;
            //$this->Static_model->Debug();
            //echo $this->Static_model->save_category($inData);
            if ($this->Static_model->save_category($inData)) {
                setMessage('Категория записана');
            } else setMessage('Ошибка записи категории','error');
            Goto_Page('administration/section/admin_static');
        }
    }
}

