<?php
class Admin_variable extends MY_Controller {
    public function index() {   
    }
    //
    public function load() {
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData['output'] = $this->Variables_model->getOutput();
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Variables_model->loadCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/settings/admin_settings';  
        $inData['page']['data'] = $this->Variables_model->loadTree(array(
            'tree'=>array('fields'=>array("{$this->Variables_model->getCategoryId()} as value, {$this->Variables_model->getCategoryName()} as title, {$this->Variables_model->getCategoryStatus()}")),
            'item'=>array('fields'=>array("{$this->Variables_model->getSelfId()} as value, {$this->Variables_model->getSelfName()} as title, {$this->Variables_model->getStatus()}, variable_description as description")),        
                    ),false,$this->inPage,$this->inOutRecord);
        if ($inData['page']['data']) {
            foreach ($inData['page']['data'] as $outKey => $outData) {
                $outChecked = ($outData[$this->Variables_model->getCategoryStatus()]==1)?'checked':'un-checked';
                $outData['action'] =    "<a href='#' id='ref-ss-edit-{$outData['value']}' class='action-base action-edit'></a>";
                $outData['action'] .= "<a href='#' id='ref-ss-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                if (!empty($outData['sub_tree'])) {
                    foreach ($outData['sub_tree'] as $outSKey => $outSData) {
                        $outSData['title'] = "({$outSData['title']}) - {$outSData['description']}";
                        $outChecked = ($outSData[$this->Variables_model->getStatus()]==1)?'checked':'un-checked';
                        $outSData['action'] =   "<a href='#' id='ref-variable-edit-{$outSData['value']}' class='action-base action-edit' title='Редактировать'></a>".
                                                "<a href='#' id='ref-variable-delete-{$outSData['value']}' class='action-base action-delete' title='Удалить'></a>".
                                                "<a href='#' id='ref-variable-check-{$outSData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать'></a>";
                        $outData['sub_tree'][$outSKey] = $outSData;
                    }
                }
                $inData['page']['data'][$outKey] = $outData;
            }
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add() {
        $inData = array();
        $inData[$this->Variables_model->getCategoryId()] = 
            $this->Variables_model->loadCategory(
                array($this->Variables_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Variables_model->getCategoryName()} as title","{$this->Variables_model->getCategoryId()} as value")
                )
            );
        $inData['variable_type'] = VARIABLE_DIGIT;
        //setLanguage($inData);
        $inData['form'] = $this->Variables_model->getForm(
            $inData,VARIABLE_DIGIT
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function add_multi_variable() {
        $inData = array();
        $inData[$this->Variables_model->getCategoryId()] = 
            $this->Variables_model->loadCategory(
                array($this->Variables_model->getCategoryStatus()=>1,
                    'fields'=>array("{$this->Variables_model->getCategoryName()} as title","{$this->Variables_model->getCategoryId()} as value")
                )
            );
        $inData['variable_type'] = VARIABLE_STRING;            
        //setLanguage($inData);
        $inData['form'] = $this->Variables_model->getForm(
            $inData,VARIABLE_STRING
        );
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    //
    public function edit() {
        $inData = array();
        $inArg = func_get_args();
        //var_dump($this->Variables_model->getSelfId()); die();
        if ((filter_input(INPUT_POST, 'process')=='edit-variable') && (filter_input(INPUT_POST, $this->Variables_model->getSelfId()))) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,$this->Variables_model->getSelfId()), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Variables_model->loadById($inId);
            
            $inActive = $inData[$this->Variables_model->getCategoryId()];
            $inData[$this->Variables_model->getCategoryId()] = 
                $this->Variables_model->loadCategory(array('fields'=>array("{$this->Variables_model->getCategoryName()} as title","{$this->Variables_model->getCategoryId()} as value")));
            $inData[$this->Variables_model->getCategoryId()] = setCategoryActiveItem($inData[$this->Variables_model->getCategoryId()], $inActive);
            //var_dump($inData['variable_type']); die();
            $inProcess = $inData['variable_type'];
            $inData['form'] = $this->Variables_model->getForm($inData,$inProcess);
            //var_dump($inData['form']); die();
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        } else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
    }
    //
    public function delete_variable() {
        $inData = array();
        $inArg = func_get_args();
        //var_dump($this->Variables_model->getSelfId()); die();
        if ((filter_input(INPUT_POST, 'process')=='delete-variable') && (filter_input(INPUT_POST, $this->Variables_model->getSelfId()))) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,$this->Variables_model->getSelfId()), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Variables_model->loadById($inId);
            var_dump((bool)$inData['variable_readonly']);
            if ((bool)$inData['variable_readonly']) {
                setMessage("Переменная «{$inData['variable_name']}» не может быть удалена. Включен режим только редактирования.", 'error');
            } else {
                $this->Variables_model->deleteById($inId);
                setMessage("Переменная «{$inData['variable_name']}» удалена.", 'status');
            }
        } else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
        //Goto_Page("administration/settings/admin_settings");
    }
    //
    public function save_variable() {
        if (!empty($_POST)) {
            $inDecode = $_POST;
            $this->Variables_model->save($inDecode);
            setMessage('Переменная сохранена.','status');
        } else 
            setMessage("Операция не определена, либо отсутствует доступ.", 'error');
        Goto_Page("administration/settings/admin_variable");
    }
    public function add_section() {
        $inData = array();
        $inData['form'] = $this->Variables_model->getForm($inData,PROCESS_SECTION_ADD);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function save_section() {   
        if (!empty($_POST)) {
            $inDecode=$_POST;
            //if($inDecode[$this->Variables_model->getCategoryStatus()]=='on') { $inDecode[$this->Variables_model->getCategoryStatus()]=1; }
            //echo "<pre>"; var_dump($inDecode); die();
            //$this->Auto_model->Debug();
            if ($this->Variables_model->save_category($inDecode)) {
                setMessage('Группа переменных записана.');
            }else setMessage('Ошибка записи группы переменных.','error');
            //echo $this->inUriString; 
            //die($this->inUriString);
            Goto_Page("/administration/settings/admin_variable");
        }
    }
}