<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_workshops extends MY_Controller {
    //
    protected function startUp() {
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error');
            //echo $this->twig->render("administration/administration_empty.twig", $inData);
            die("TYT");
        }
        //$this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        
    }
    //
    protected function afterInclude($aData = array()) {
        $this->load->model('Language_model');
        $this->load->model('Footerlink_model');
        return $aData;
    }
    //
    public function index($aParam=null) {   
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['tabs'] = $this->Menu_model->getTabs($this->inPage,$aParam);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        Goto_Page("administration/admin_workshops/admin_keycreate/");
    }
    //
    public function admin_keycreate($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $this->load->model('Keyscreate_model');
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Keyscreate_model->loadCountRecord()/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/admin_workshops/admin_keycreate';
        $inData['page']['tabs'] = $this->Keyscreate_model->getTabs($aParam);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    public function admin_workshop($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $this->load->model('Workshop_model');
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Workshop_model->loadCountRecord()/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/admin_workshops/admin_workshop';
        $inData['page']['tabs'] = $this->Workshop_model->getTabs($aParam);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
}

