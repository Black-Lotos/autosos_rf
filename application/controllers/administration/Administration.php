<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administration extends MY_Controller {
    //
    protected function startUp() {
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error');
            //echo $this->twig->render("administration/administration_empty.twig", $inData);
            die("TYT");
        }
        //$this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        
    }
    //
    protected function includeUp($aMenuName,$aMenuRecursive=false) {
        $this->Menu_model->setRecursive($aMenuRecursive);
        $this->inMenu = $this->Menu_model->load(array('name'=>$aMenuName,'status'=>1));
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inData = array(
            'page' => array(
                'menu'=>$this->inMenu,
                'property'=>array(
                    'title'=>'Администрирование',
                    'copyright'=>"© 2015 <a href='".base_url()."'>автосос.рф</a>. Все права защищены"
                ),
                'content'=>array(
                    'left'=>"hkdhgfdkf",
                    'right'=>'sdfsdjflsfls'
                ),
                'user'=>array (
                    'status'=>'skhfdkjshfkjsh'
                )
            )
        );
        /*array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
        'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);*/
        $inData = $this->afterInclude($inData);
        return $inData;
    }
    //
    public function index() {   
        $inData = $this->includeUp('admin_menu');
        Goto_Page("/administration/content/admin_menu");
        echo $this->twig->render("admin-master-page.twig", $inData);
        
    }
    //
    public function login() {
        $inData = $this->includeUp();
        $inData['sub_page'] = "fffffff";
        echo "login";
    }
}

