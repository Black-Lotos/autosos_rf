<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends MY_Controller {
    //
    protected function startUp() {
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error');
            //echo $this->twig->render("administration/administration_empty.twig", $inData);
            die("TYT");
        }
        //$this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        
    }
    //
    protected function afterInclude($aData = array()) {
        $this->load->model('Language_model');
        $this->load->model('Footerlink_model');
        return $aData;
    }
    //
    public function index($aParam=null) {   
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['tabs'] = $this->Menu_model->getTabs($this->inPage,$aParam);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        Goto_Page("administration/content/admin_menu");
    }
    //
    public function admin_menu($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Menu_model->loadCountRecord()/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/content/admin_menu';
        $inData['page']['tabs'] = $this->Menu_model->getTabs($this->inPage,$aParam);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    //
    public function admin_language($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['tabs'] = $this->Language_model->getTabs();
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    //
    public function admin_footerlink($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['tabs'] = $this->Footerlink_model->getTabs();
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
}

