<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section extends MY_Controller {
    //
    protected function startUp() {
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error');
            //echo $this->twig->render("administration/administration_empty.twig", $inData);
            die("TYT");
        }
        //$this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        
    }
    //
    protected function afterInclude($aData = array()) {
        $this->load->model('Static_model');
        $this->load->model('Auto_model');
        $this->load->model('Invoice_model');
        return $aData;
    }
    //
    public function index($aParam=null) {   
        $inData = $this->includeUp('admin_menu',true);
        //$inSubData = array();
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['tabs'] = $this->Menu_model->getTabs($this->inPage,$aParam);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        //echo $this->twig->render("admin-master-page.twig", $inData);
        //
        Goto_Page("administration/section/admin_static");
    }
    //
    public function admin_picture($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        //$inSubData = array();
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Picture_model->loadCategoryCountPage($this->inOutRecord));
        $inData['page']['link_run'] = 'administration/section/admin_picture';
        $inData['page']['tabs'] = $this->Picture_model->getTabs($this->inPage);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    //
    public function admin_static($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Static_model->loadCategoryCountPage($this->inOutRecord));
        $inData['page']['link_run'] = 'administration/section/admin_static';
        $inData['page']['tabs'] = $this->Static_model->getTabs($this->inPage);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    public function admin_avto($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        //$this->Auto_model->Debug();
        $inData['page']['count'] = $this->Auto_model->loadCategoryCountPage($this->inOutRecord);
        //var_dump($inData['page']['count']); die();
        $inData['page']['link_run'] = 'administration/section/admin_avto';
        $inData['page']['tabs'] = $this->Auto_model->getTabs($this->inPage);
        $inData['page']['active'] = $this->inPage;
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    public function admin_keys($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Keys_model->loadCountRecord(array(),true)/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_keys';
        $inData['page']['tabs'] = $this->Keys_model->getTabs($aParam,$this->inPage);
        $inData['page']['active'] = $this->inPage;
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    public function admin_news($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = $this->News_model->loadCategoryCountPage($this->inOutRecord);
        $inData['page']['link_run'] = 'administration/section/admin_news';
        $inData['page']['tabs'] = $this->News_model->getTabs($this->inPage);
        $inData['page']['active'] = $this->inPage;
        $inData['page']['content']['left'] = "";
        //var_dump($inData['page']['tabs']); die();
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    public function admin_price($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Invoice_model->loadCategoryCountPage($this->inOutRecord));
        $inData['page']['link_run'] = 'administration/section/admin_price';
        $inData['page']['tabs'] = $this->Invoice_model->getTabs($this->inPage);
        $inData['page']['active'] = $this->inPage;
        $inData['page']['content']['left'] = "";
        //var_dump($inData['page']['tabs']); die();
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
}

