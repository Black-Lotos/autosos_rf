<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settings extends MY_Controller {
    //
    protected function startUp() {
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        if (!$this->Users_model->isLogin()) {
            CI_goto("/administration/users/login");
        }
        if ($this->Users_model->getCheckRoles($this->inUser,array('Администратор','Главный администратор'))) {
            $this->inPermition = $this->inUser->user_login;
        }
        else {
            $inData['message'] = getMessage('error');
            //echo $this->twig->render("administration/administration_empty.twig", $inData);
            die("TYT");
        }
        //$this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        
    }
    //
    protected function afterInclude($aData = array()) {
        $this->load->model('Roles_model');
        return $aData;
    }
    //
    public function index() {   
        $inData = $this->includeUp('admin_menu');
        Goto_Page("/administration/settings/admin_users");
        $inData['page']['content']['left'] = "";
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    //
    public function admin_users($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        //$inSubData = array();
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Users_model->loadCountRecord()/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/settings/admin_users';
        $inData['page']['tabs'] = $this->Users_model->getTabs($this->inPage);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    //
    public function admin_roles($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Roles_model->loadCountRecord()/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/settings/admin_roles';
        $inData['page']['tabs'] = $this->Roles_model->getTabs($this->inPage);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
    //
    public function admin_variable($aParam=null) {
        $inData = $this->includeUp('admin_menu',true);
        $inData["sub_page_message"] = getMessage('error', true,'error').getMessage('status', true,'status');
        $inData['page']['count'] = ceil($this->Variables_model->loadCountRecord()/$this->inOutRecord);
        $inData['page']['link_run'] = 'administration/settings/admin_variable';
        $inData['page']['tabs'] = $this->Variables_model->getTabs($this->inPage);
        $inData['page']['content']['left'] = "";
        $inData['page']['content']['right'] = $this->twig->render($inData['page']['tabs']['property']['template'],$inData);
        echo $this->twig->render("admin-master-page.twig", $inData);
    }
}

