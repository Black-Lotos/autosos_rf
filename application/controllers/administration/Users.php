<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {
    private $inMenu = null;
    private $inSite = null;
    private $inUriString = "";
    private $inUser;
    private $inPermition;
    private $inPage=1;
    private $inOutRecord = 26;
    public function _remap($aMethod=null){
        $inArg = func_get_args();
        $this->startUp();
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($inArg);
        } else {
            //CI_goto('/home/');
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    public function _output($output)
    {
        echo $output;  
    }
        //
     private function startUp() {
        $this->inMenu = $this->Menu_model->load(array('name'=>'admin_menu','status'=>1));
        $this->inUriString = "/".$this->uri->uri_string()."/";
        //$this->inSite = $this->Site_model->loadSettings(1); $this->inSite = $this->inSite[0];
        $this->inUser = $this->session->userdata('user');
        $this->inPermition = (($this->Users_model->get_permition($this->inUser)))?$this->inUser->user_login:'start';
        $this->load->model('Roles_model');
    }
    //
    public function login_new() {
        if ($_POST) {
            $this->config->set_item('sess_expiration', 60 * 60 * 24 * 10);
            if ($this->Users_model->login($_POST['user_login'],$_POST['user_pass'])) {
                
            };
            Goto_Page("/administration/");
        } else {
            $inData = array();
            $inArg = func_get_args();
            $inSufix = empty($inArg[0])?'':"-".$inArg[0];
            $inMenu = empty($inArg[1])?null:$inArg[1];
            $inData['form'] = $this->Users_model->getForm_New(PROCESS_LOGIN);
            //var_dump($inData);
            echo $this->twig->render("administration/users/users_login_new.twig", $inData);
        }
    }
    //
    public function login() {
        $inData = array();
        $inData['sub_page_message'] = getMessage('status',false,'status').getMessage('error',false,'error');
        //var_dump($_POST); 
        //sub_page_message
        if ($_POST) {
            //var_dump($_POST); 
            $this->config->set_item('sess_expiration', 60 * 60 * 24 * 10);
            if ($this->Users_model->login($_POST['user_login'],$_POST['user_pass'])) {
                setMessage('Вы успешно авторизовались.','status');
                Goto_Page("/administration/content/");
            } else {
                setMessage('Ошибка авторизации.','error');
                Goto_Page("/administration/");
            }
            
        } else {
            
            $inArg = func_get_args();
            $inSufix = empty($inArg[0])?'':"-".$inArg[0];
            $inMenu = empty($inArg[1])?null:$inArg[1];
            $inData['form'] = $this->Users_model->getForm(array(), PROCESS_LOGIN);
            //echo "<pre>";var_dump($inData['form']); die();
            $inData['sub_page'] = $this->twig->render("administration/common/form-system.twig", $inData);
            //echo "<pre>"; var_dump($inData); //die();
            //echo $this->twig->render("administration/common/form-system.twig", $inData);
            //echo "<pre>"; var_dump($inData);
            echo $this->twig->render("administration/admin-master-page.twig", $inData);
        }
    }
    //
    public function logOut() {
        $user = $this->session->userdata('user');
        $this->Users_model->logout($user->user_id);
        Goto_Page("/administration/");
    }
    //
    public function index() {   
        
    }
    public function load() {
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Users_model->loadCountPage($this->inOutRecord);
        //$this->Users_model->debug();
        $inData['page']['data'] = $this->Users_model->load(array('fields'=>array('user_id as value',"user_name as title", "user_email","user_active")),false,$this->inPage,$this->inOutRecord);
        //var_dump($inData['page']['data']); die();
        foreach ($inData['page']['data'] as $outKey => $outData) {
            $outChecked = ($outData['user_active']==1)?'checked':'un-checked';
            $outData['action'] = "<a id='ref-user-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать' href=''></a>".
            $outData['action'] = "<a id='ref-user-delete-{$outData['value']}' class='action-base action-delete' title='Удалить' href=''></a>".
            $outData['action'] = "<a id='ref-user-check-{$outData['value']}' class='action-base action-{$outChecked}' title='Активировать/Деактивировать' href=''></a>";
            $inData['page']['data'][$outKey] = $outData;
        }
        //echo "<pre>"; var_dump($inData); die();
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    public function add() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inData['form'] = $this->Users_model->getForm(array(),PROCESS_USER);
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit() {
        $inData = array();
        $inArg = func_get_args();
        //die("TYT");
        if ((filter_input(INPUT_POST, 'process')=='edit-user') && (filter_input(INPUT_POST, 'user_id'))) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'user_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Users_model->loadById($inId);
            $inData['form'] = $this->Users_model->getForm($inData,PROCESS_USER);
            //var_dump($inData['form']); die();
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        } else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
    }
    public function delete() {
        $inData = array();
        $inArg = func_get_args();
        //die("TYT");
        if ((filter_input(INPUT_POST, 'process')=='delete-user') && (filter_input(INPUT_POST, 'user_id'))) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'user_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Users_model->deleteById($inId);
            setMessage("Пользователь удален.", 'status');
            //$inData['form'] = $this->Users_model->getForm($inData,PROCESS_USER);
            //var_dump($inData['form']); die();
            //echo $this->twig->render("administration/common/form-system.twig", $inData);
        } else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
    }
    public function save() {
        $inValidInput = true;
        $inData['form']['form_data'] = ($_POST)?$_POST:array();
        //echo "<pre>"; var_dump($inData['form']['form_data']);
        //echo "<pre>"; var_dump($inData['form']['form_data']);
        if($_POST) {
            if (!$this->Users_model->valid_login($inData['form']['form_data']['user_login'])) {
                setMessage('Неверный логин', 'error'); $inValidInput = false;
            }
            if (empty($inData['form']['form_data']['user_id'])){
                if ($this->Users_model->valid_login_is_exists($inData['form']['form_data']['user_login'])) {
                    setMessage('Введенный логин занят', 'error'); $inValidInput = false;
                }
            }
            if (!$this->Users_model->valid_email($inData['form']['form_data']['user_email'])) {
                setMessage('Неверный e-mail', 'error'); $inValidInput = false;
            }
            if (!$this->Users_model->valid_name($inData['form']['form_data']['user_name'])) {
                setMessage('Неверное Ф.И.О.', 'error'); $inValidInput = false;
            }
            if (!$this->Users_model->valid_phone($inData['form']['form_data']['user_phone'])) {
                setMessage('Неверный телефон', 'error'); $inValidInput = false;
            }
            if(empty($inData['form']['form_data']['user_pass'])) {
                setMessage('Пароль не может быть пустым', 'error'); $inValidInput = false;
            }
            if ($inData['form']['form_data']['user_pass']!=$inData['form']['form_data']['user_pass_check']) {
                setMessage('Пароли не совпадают', 'error'); $inValidInput = false;
            }
            if (!$inValidInput) {
                $inData['sub_page_message'] = getMessage ('error',true,'error');
                $inData['form'] = $this->Users_model->getForm($inData['form']['form_data'],PROCESS_USER);
            } else {
                $inData['form']['form_data']['user_reg_date'] = date('Y-m-d');
                $inData['form']['form_data']['user_auth_key'] = 
                    md5(strtotime('+10 days', strtotime(date('Y-m-d'))));
                unset($inData['form']['form_data']['user_pass_check']);
                $inData['form']['form_data']['user_pass'] = md5($inData['form']['form_data']['user_pass']);
                unset($inData['form']['form_data']['btnSaveUserAjax']);
                $this->Users_model->save($inData['form']['form_data']);
                setMessage("Проведена регистрация!<br>На Ваш e-mail:({$inData['form']['form_data']['user_email']}) выслана ссылка активации.", 'status');
                $inSubject = iconv("utf-8", "windows-1251", 'Активация на сайте ""');
                $inAHref ="<a href='".base_url()."/site_module/activate/".$inData['form']['form_data']['user_auth_key']."'>Активация на сайте \"Допоможемо ТВ\"</a>";
                $inMessage = iconv("utf-8", "windows-1251",$inAHref);
                _email($inData['form']['form_data']['user_email'],'chiefwork@gmail.com',$inSubject, $inMessage);
                $inData['sub_page_message'] = getMessage ('status',true,"status");
                $inData['form'] = $this->Users_model->getForm(array(),PROCESS_USER);
            }
            //echo "<pre>"; var_dump($inData); die();
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        }
    }
    public function change_status() {
        if (!empty($_POST)) {
            switch (filter_input(INPUT_POST, 'process', FILTER_SANITIZE_SPECIAL_CHARS)) {
                case 'change-status':
                    $inPattern = '/([0-9]+)$/';
                    $inId = (preg_match($inPattern, $_POST['user_id'], $outMatches)>0)?(int)$outMatches[0]:0;
                    $inItem = $this->Users_model->loadById($inId);
                    $inItem[$this->Users_model->getStatus()] = ($inItem[$this->Users_model->getStatus()]==0)?1:0;
                    $this->Users_model->save($inItem);
                    setMessage("Статус пользователя изменен.", 'status');
                    echo $inItem[$this->Users_model->getStatus()];
                    break;
            }
        }
        
    }
    /*roles*/
    public function load_roles() {
        $inData['page']['active'] = $this->inPage;
        $inData['page']['count'] = $this->Roles_model->loadCountPage($this->inOutRecord);
        $inData['page']['data'] = $this->Roles_model->load(array('fields'=>array('role_id as value',"role_title as title")),false,$this->inPage,$this->inOutRecord);
        foreach ($inData['page']['data'] as $outKey => $outData) {
            $outData['action'] =    "<a id='ref-role-edit-{$outData['value']}' class='action-base action-edit' title='Редактировать' href=''></a>".
                                    "<a id='ref-role-delete-{$outData['value']}' class='action-base action-delete' title='Удалить' href=''></a>";
            $inData['page']['data'][$outKey] = $outData;
        }
        echo $this->twig->render("administration/common/list-system-tree.twig", $inData);
    }
    //
    public function add_roles() {
        $inData = array();
        $inArg = func_get_args();
        $inSufix = empty($inArg[0])?'':"-".$inArg[0];
        $inMenu = empty($inArg[1])?null:$inArg[1];
        $inData['form'] = $this->Roles_model->getForm();
        echo $this->twig->render("administration/common/form-system.twig", $inData);
    }
    public function edit_roles() {
        $inData = array();
        $inArg = func_get_args();
        if ((filter_input(INPUT_POST, 'process')=='edit-role') && (filter_input(INPUT_POST, 'role_id'))) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'role_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            $inData = $this->Roles_model->loadById($inId);
            $inData['form'] = $this->Roles_model->getForm($inData);
            echo $this->twig->render("administration/common/form-system.twig", $inData);
        } else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
    }
    public function delete_roles() {
        $inData = array();
        $inArg = func_get_args();
        if ((filter_input(INPUT_POST, 'process')=='delete-role') && (filter_input(INPUT_POST, 'role_id'))) {
            $inPattern = '/([0-9]+)$/';
            $inId = (preg_match($inPattern, filter_input(INPUT_POST,'role_id'), $outMatches)>0)?(int)$outMatches[0]:0;
            //$this->Roles_model->Debug();
            $inCheckData = $this->Roles_model->load(array($this->Roles_model->getSelfId()=>$inId));
            //var_dump(!$inCheckData);
            if ($inCheckData) {
                setMessage("Роль задействованна, удаление не возможно.", 'error');
            } else {
                $this->Roles_model->deleteById($inId);
                setMessage("Роль удалена", 'status');
            }
        } else setMessage("Операция не определена, либо отсутствует доступ.", 'error');
    }
    public function save_roles() {
        if (!empty($_POST)) {
            $this->Roles_model->save($_POST);
            setMessage("Роль сохранена", 'status');
        } else
            setMessage("Операция не определена, либо отсутствует доступ.", 'error');
        Goto_Page("administration/settings/admin_roles");
    }
    /*end roles*/
}

