<?php
class MY_Controller extends CI_Controller {
    private     $inMenu = null;
    private     $inSite = null;
    private     $inUriString = "";
    protected   $inUser;
    private     $inPermition;
    protected   $inPage=1;
    protected   $inLang=1;
    protected   $inLangName='';
    protected   $inFooterMenu=false;
    protected   $inModule;
    protected   $inOutRecord = 10;
    protected   $PropertySearch = "";
    protected   $inPicPath = "picture/";
    protected   $inImgPath = "uploads/";
    protected   $inVideoPath = "video/";
    protected   $inSiteMasterPage = 'site-master-page.twig';
    //
    function __construct() {
        parent::__construct();
    }
    //
    public function getImages($aImageId,$aImageWidth, $aImageHeight, $aSpectRatio=true){
    }
    // ремапинг вызова
    public function _remap($aMethod=null){
        $inArg = func_get_args();
         // class name
 // function name 
        //var_dump($inArg,$aMethod); die();
        $this->startUp(); 
        if (method_exists($this, $aMethod)) {
            echo call_user_func_array(array($this, $aMethod), $inArg[1]);
            //var_dump($arg);
        } else {
            $aMethod = 'index';
            echo call_user_func_array(array($this, $aMethod),$inArg[1]);
        }
    }
    // вывод в браузер
    public function _output($output) {
        echo $output;  
    }
    // чтение языка при необходимости
    protected function getLang() {
        return $this->inLang;
    }
    // расширение базового класса, вызова стартовой функцией
    protected function startUp() {
        $this->inUriString = "/".$this->uri->uri_string()."/";
        $this->load->library('Captcha');
        $this->inUser = $this->session->userdata('user');
        $inData = array(); 
        $this->inPermition = 'start';
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $this->inOutRecord = getVariable('output_admin_tree')?getVariable('output_admin_tree'):10;
    }
    //
    protected function includeUp($aMenuName,$aMenuRecursive=false) {
        $this->Menu_model->setRecursive($aMenuRecursive);
        //$this->Menu_model->Debug();
        $this->inMenu = $this->Menu_model->load(array('name'=>$aMenuName,'status'=>1));
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        //
        $inSegment = $this->uri->segment(1);
        $inSegment = empty($inSegment)?"home":$inSegment;
        $inTempMeta = $this->Menu_model->load(array('mi_url'=>"/$inSegment/",'status'=>1,"fields"=>array("meta_title as html_title","meta_keywords as html_keywords","meta_description as html_description")),false);
        $inTempMeta = $inTempMeta[0];
        $inHtmlTitle        = $inTempMeta['html_title'];
        $inHtmlKeywords     = $inTempMeta['html_keywords'];
        $inHtmlDescription  = $inTempMeta['html_description'];
        //
        //echo "<pre>"; var_dump($inTempMeta);
        //
        $inData = array(
            'page' => array(
                'menu'=>$this->inMenu,
                'search'=>0,
                'property'=>array(
                    'title'=>$inHtmlTitle,
                    'copyright'=>"© 2015 <a href='".base_url()."'>автосос.рф</a>. Все права защищены",
                    'description'=>$inHtmlDescription,
                    'keywords'=>$inHtmlKeywords
                ),
                'content'=>array(
                    'left'=>  $this->loadLeftContent(),
                    'right'=> $this->loadRightContent(),
                ),
                'user'=>array (
                    'status'=>'Пользователь'
                )
            )
        );
        /*array('title'=>'Главная страница','content'=>array('header'=>'Главная страница'),
        'menu'=>$this->inMenu,'site'=>$this->inSite,'user_status'=>$this->inPermition);*/
        $inData = $this->afterInclude($inData);
        return $inData;
    }
    // чтение пост вызова стартовой функции
    protected function afterInclude($aData=array()) {
        return $aData;
    }
    // описание функции если необходим поиск на сайте, переопределяется в дочерних классах
    protected function goSearch() {
        $inCntPage = 0; // переменная расчета количества выводимых записей
        $inData = array(); $inAvto=array();
        $inData = $this->includeUp('site_menu',true);
        $inData['page']['search'] = 1;
        //var_dump($inData['page']['search']);
        //$inTextPage = filter_input(INPUT_GET,'page');
        $this->inPage = isset($_GET['page'])?$_GET['page']:1;
        $inTextS = filter_input(INPUT_GET,'inSearch');
        if (empty($inTextS)) {
            setMessage('Данных для поиска не введено', 'error');
            $inData['page']['content']['right'] = getMessage('error', true,'error').getMessage('status', true,'status');
            $inData['page']['content']['right'] .= "<h1>Данных для поиска не введено.</h1><hr>";
        } else {
            $inBloks = array();
            // ключи
            //$this->Keys_model->Debug();
            $inKeys = $this->Keys_model->load(array('keys_status'=>1,'keys_name'=>"like '%{$inTextS}%'",
                    'fields'=>array("{$this->Keys_model->getSelfId()} as value","{$this->Keys_model->getSelfName()} as title","keys_description as note",'images_id')));
            //        
            $inCntPage += ($inKeys===false)?0:count($inKeys);                    
            $inKeys = $this->Keys_model->load(array('keys_status'=>1,'keys_name'=>"like '%{$inTextS}%'",
                    'fields'=>array("{$this->Keys_model->getSelfId()} as value","{$this->Keys_model->getSelfName()} as title","keys_description as note",'images_id')),
                    false,$this->inPage,  getVariable('output_search_record'));
            //        
            //$outKeys = ($inKeys===false)?array('title'=>'Ключи','data'=>array('value'=>0,'title'=>'Данных нет','note'=>'Данных нет')):$inKeys;            
            $outKeys = ($inKeys===false)?false:$inKeys;
            $inBloks[] = $this->twig->render('common/search/keys-list-search.twig',array("data"=>$outKeys));
            // новости
            //$this->News_model->Debug();
            $inNews = $this->News_model->load(
                array($this->News_model->getStatus()=>1,'news_text'=>"like '%{$inTextS}%'",
                    'fields'=>array("{$this->News_model->getSelfId()} as value", 
                        "{$this->News_model->getSelfName()} as title", 
                        "news_text as note")),false);
            $inCntPage += ($inNews===false)?0:count($inNews);            
            //$outNews =  ($inNews===false)?array('title'=>'Новости','data'=>array('value'=>0,'title'=>'Данных нет','note'=>'Данных нет')):$inNews;    
            $outNews =  ($inNews===false)?false:$inNews;
            $inBloks[] = $this->twig->render('news/news-list-all.twig',array('data'=>$outNews));
            // поиск автомобилей
            $inAvto = $this->Auto_model->load(
                array($this->Auto_model->getStatus()=>1,'avto_description'=>"like '%{$inTextS}%'",
                    'fields'=>array("{$this->Auto_model->getSelfId()} as value", 
                        "{$this->Auto_model->getSelfName()} as title", 
                        "avto_description as note",'images_id')),false);
            $inCntPage += ($inAvto===false)?0:count($inAvto);            
            //            
            $inAvto = $this->Auto_model->load(
                array($this->Auto_model->getStatus()=>1,'avto_description'=>"like '%{$inTextS}%'",
                    'fields'=>array("{$this->Auto_model->getSelfId()} as value", 
                        "{$this->Auto_model->getSelfName()} as title", 
                        "avto_description as note",'images_id')),false,$this->inPage,  getVariable('output_search_record'));
            //$outAvto =  ($inAvto===false)?array('title'=>'Автомобили','data'=>array('value'=>0,'title'=>'Данных нет','note'=>'Данных нет')):$inAvto;           
            $outAvto =  ($inAvto===false)?false:$inAvto;            
            $inBloks[] = $this->twig->render('avto/avto-list-all.twig',array('data'=>$outAvto));
            // добавляем навигацию
            $inPage = array();
            //$inBloks[] = $this->twig->render('blocks/block-search-navigation.twig',$inPage);
            $inPage = array(
                "property" => array(
                    "count"=>$inCntPage,
                    "current"=>$this->inPage,
                    "per_to_page"=>getVariable('output_search_record')?getVariable('output_search_record'):10,
                ),
                "url_link" =>"goSearch?inSearch={$inTextS}",
            );
            //echo "<pre>"; var_dump($inPage); die();
            $inData['page']['content']['right'] = $this->twig->render('blocks/block-build.twig',array("data"=>$inBloks,'page'=>$inPage));
        }
        echo $this->twig->render('site-master-page.twig', $inData);
    }
    //
    public function load_page($aPage='page_404',&$inData) {
        $inAlias = $aPage;
        $inPage = $this->Static_model->loadByAlias($inAlias,array('language_id'=>$this->inLang),true);
        if (!$inPage) {
            $inPage = $this->Static_model->loadByAlias("page_404",array('language_id'=>$this->inLang),true);
        }
        if ($inAlias=='page_404') {
            setMessage($inPage->sp_text, 'error');
            $inData['sub_page'] = getMessage('error',true,'error');
        } else {
            $inData['page']['content']['right'] = $inPage->sp_text;
        }
    }
    //
    protected function loadLeftContent() {
        $inParent01 = $this->Menu_model->loadCategory(array('mc_name'=>'page_service','fields'=>array('mc_status as status','mc_title as title','menu_category_id as value','status'=>1)));
        $inMenu01 = $inParent01[0]['value'];
        $inMenu01 = $this->Menu_model->loadTree(
                array(
                    'tree'=>array('status'=>1,'menu_category_id'=>$inMenu01,'fields'=>array('mc_status as status','mc_title as title','menu_category_id as value')),
                    'item'=>array('status'=>1,'menu_category_id'=>$inMenu01,'owner_id'=>0,'fields'=>array("{$this->Menu_model->getSelfId()} as value","{$this->Menu_model->getSelfName()} as title", "{$this->Menu_model->getStatus()} as status","mi_url as url"))    
                ),true,$this->inPage,$this->inOutRecord);
        $inParent01[0]['sub_tree'] = $inMenu01;
        //echo "<pre>"; var_dump($this->inOutRecord); 
        $inDataOut = $this->twig->render('blocks/block-01.twig', array('data'=>$inParent01));
        // поиск
        $inDataOut .= $this->twig->render('blocks/block-search.twig',array());
        // читаем марки авто и присоединяем
        $inCar = $this->Auto_model->getOutput();
        $this->Auto_model->setCategoryOrder(array("car_name"));
        $inCar['data'] = $this->Auto_model->loadCategory(array('fields'=>array('car_name as title','car_id as value','images_id','car_url as url')));
        //echo "<pre>"; var_dump($inCar); die();
        $inDataOut .= $this->twig->render('blocks/block-02.twig', $inCar);
        // Общее для новостей
        $inNews = $this->News_model->getOutput();
        
        $this->News_model->setCountRecord(getVariable("brief_output_to_main"));
        $inNews['title'] = "Новости";
        $inNews['data'] = $this->News_model->load(array('news_status_main'=>1,'language_id'=>$this->inLang,'fields'=>array("news_id as value","news_title as title",'images_id','news_text as note','news_creates as date')),false);
        //echo "<pre>"; var_dump($inNews); die();
        $inDataOut .= $this->twig->render('news/news-start-up.twig',$inNews);
        //
        $this->afterLoadLeftContent($inDataOut);
        return $inDataOut;
    }
    protected function afterLoadLeftContent(&$aData) {
        
    }
    protected function loadRightContent() {
        $inDataOut="";
        $this->afterLoadRightContent($inDataOut);
        return $inDataOut;
    }
    protected function afterLoadRightContent(&$aData) {
        $aData = "Правый контент системы";
    }
}
    
