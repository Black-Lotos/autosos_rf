<?php
/*
реализация расширения для универсальной работы с данными БД
 */
class MY_Model extends CI_Model {
    protected $inTblName            = '';
    protected $inTblCategoryName    = '';
    protected $inTblUnion           = '';
    protected $inCountRec           = 0;
    protected $inOrderFields        = '';
    protected $inDebug              = false;
    protected $inStatus             = ''; 
    protected $inStatusMain         = ''; 
    protected $inStatusTop          = '';
    protected $inAlias              = '';
    protected $inSelfId             = ''; 
    protected $inSelfName           = '';
    protected $inCategoryId         = '';
    protected $inCategoryName       = '';
    protected $inCategoryStatus     = '';
    protected $inCategoryOrderFields    = '';
    protected $inSufix              = '';
    protected $inPrefix             = '';
    protected $inCategorySufix      = '';
    protected $inCategoryPrefix     = '';
    protected $inRecursive          = true;
    protected $setObjectResult      = false;
    protected $inOrderType          = 'DESC';
    protected $inPicPath = "picture/";
    protected $inImgPath = "uploads/";
    protected $inVideoPath = "video/";
    protected $inWmPath = "images/wm/";
    protected $inTabs = 
        array(
            "property"  =>  array(),
            "data"      =>  array()
        );
    protected $inNoItem = array(array());
    function __construct() {
        parent::__construct();
        $this->StartUp();
    }
    // чтение защищенных атрибутов
    public function getNoItem(){return $this->inNoItem;}
    public function getTblName(){return $this->inTblName;}
    public function getTblCategoryName(){return $this->inTblCategoryName;}
    public function getTblUnion() {return $this->inTblUnion;}
    public function getCountRec(){return $this->inCountRec;}
    public function getOrderFields(){return $this->inOrderFields;}
    public function getStatus() {return $this->inStatus;}
    public function getStatusMain() {return $this->inStatusMain;}
    public function getStatusTop() {return $this->inStatusTop;}
    public function getAlias() {return $this->inAlias;}
    public function getSelfId() {return $this->inSelfId;}
    public function getSelfName() {return $this->inSelfName;}
    public function getCategoryId() {return $this->inCategoryId;}
    public function getCategoryName() {return $this->inCategoryName;}
    public function getCategoryStatus() {return $this->inCategoryStatus;}
    public function getSufix() {return $this->inSufix;}
    public function getPrefix() {return $this->inPrefix;}
    public function getCategorySufix() {return $this->inCategorySufix;}
    public function getCategoryPrefix() {return $this->inCategoryPrefix;}
    // установка типа сортировки, переопределяется при необходимости
    public function setOrderType() {
    }
    // установка типа сортировки, переопределяется при необходимости
    public function setCategoryOrder($aSOrder=array(),$aTOrder='ASC') {
        //var_dump(implode(',', $aSOrder));
        $this->inCategoryOrderFields = (!empty($aSOrder)&&is_array($aSOrder))?implode(',', $aSOrder):"";
        //var_dump($this->inCategoryOrderFields); die();
        $this->inOrderType = $aTOrder;
    }
    // вызов начальной инициализации
    protected function StartUp() {
        
    }
    // Проверка статуса Debbuging
    protected function isDebug($aData) { 
        if ($this->inDebug) { echo "<pre>"; var_dump($aData); die(); }  
        return;
    }
    // Триггер включения отключения debbuging
    public function Debug(){
        $this->inDebug = !$this->inDebug;
    }
    //
    public function loadCountRecord($aFilter=array(), $aUnion=false) {
        $inData = $this->load($aFilter, $aUnion);
        return (int)(count($inData));
    }
    public function loadCategoryCountPage($aRecordByPage) {
        $outResult = $this->loadCategory();
        $outResult = count($outResult);
        return ceil($outResult/$aRecordByPage);
        //(($outResult % $aRecordByPage)==0)?1:($outResult % $aRecordByPage);
    }
    // Выбор максимального элемента таблицы
    public function getMaxItem($aData, $aUnion=false) {
            $this->setObjectResult = true;
            if (is_array($aData)) {
                //$this->Debug();
                $aData['fields'] = array("max({$aData['fields'][0]}) as MaxRecNo");
                //$this->Debug();
                $inResult = $this->load($aData,false);
                $inResult = $inResult[0];
                //var_dump($inResult); die();
                return array('max_item'=>$inResult->MaxRecNo);
            } else return false;
        }
    // Установка режима рекурсивной организации
    public function setRecursive($aValue) {
        $this->inRecursive = $aValue;
    }
    // Установка количества выводимых записей, пейджинг
    public function setCountRecord($aValue) {
        $this->inCountRec = $aValue;
    }
    // Установка полей вывода
    public function setOrder($aValue,$aType='DESC') {
        $this->inOrderFields = $aValue;
        $this->inOrderType = $aType;
    }
    // Установка имени таблицы
    public function setTblName($aValue) {
        $this->inTblName = $aValue;
    }
    // Чтение данных из таблицы или представления с учетом формирования ффильтров вывода
    public function load($aFilter=array(), $aUnion=true, $aPage=0, $aCountRec=12) {
        //var_dump($aFilter); die();
        $outWhere = ''; $outFields = '*'; $inOrder='';
            if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
                $outFields = implode(',', $aFilter['fields']);
                unset($aFilter['fields']);
            }
            if (!empty($aFilter) &&  is_array($aFilter)) {
                foreach ($aFilter as $outKey=>$outData) {
                    if ($outKey=='status') {
                        $outWhere .= " and {$this->inStatus} = '$outData'";
                    } 
                    else {
                        $inPattern = "/like/";
                        if (preg_match($inPattern, $outData))
                            $outWhere .= " and  ({$outKey} {$outData})";
                        else $outWhere .= " and  {$outKey} = '{$outData}'";    
                        
                        };
                }
            }
            $inTable = (($aUnion)?$this->inTblUnion:$this->inTblName);
            $inOrder .= (!empty($this->inOrderFields))?"order by {$this->inOrderFields} {$this->inOrderType}":"";
            if ($this->inCountRec>0) {
                $inOrder .= " limit {$this->inCountRec}";
            } 
            /*if ($this->inCountRec>0) {
                $inOrder = "order by {$this->inOrderFields} {$this->inOrderType} limit {$this->inCountRec}";
            } */
            if ($aPage>0) {
                $inOrder = "order by {$this->inOrderFields} {$this->inOrderType} limit ".(($aPage-1)*$aCountRec).",$aCountRec";
            }
            $inSql = "SELECT {$outFields} FROM {$inTable} where 1 {$outWhere} {$inOrder}";
            $this->isDebug($inSql);
            $outData = (!$this->setObjectResult)?$this->db->query($inSql)->result_array():$this->db->query($inSql)->result();
            if (!empty($outData)) {
                return $outData;
            }
            return false;
    }
    // Поиск по ИД
    public function loadById($aId,$aFilter=array()) {
        $outWhere = ''; $outFields = '*';
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }
        if (!empty($aId)) {
            $outWhere .= " and {$this->inSelfId} = '" . $aId . "'";
        }
        $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere}";
        $this->isDebug($inSql);
        $outData = $this->db->query($inSql)->result_array();
        //echo "<pre>"; var_dump($outData); die();
        if (!empty($outData)) {
            return $outData[0];
        }
        return false;
    }
    // Поиск по символическому наименованию
    public function loadByAlias($aAlias,$aFilter=array(),$aObject=false) {
        $outWhere = ''; $outFields = '*';
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }
        if (!empty($aFilter) &&  is_array($aFilter)) {
            foreach ($aFilter as $outKey=>$outData) {
                if ($outKey=='status') {
                    $outWhere .= " and {$this->inStatus} = '{$outData}'";
                } else  {
                    $outWhere .= " and  {$outKey} = '{$outData}'";
                        }
            }
        }
        if (!empty($aAlias)) {
            $outWhere .= " and {$this->inAlias} = '" . $aAlias . "'";
        }
        $inSql = "SELECT {$outFields} FROM {$this->inTblName} where 1 {$outWhere}";
        $this->isDebug($inSql);
        $outData = (!$aObject)?$this->db->query($inSql)->result_array():$this->db->query($inSql)->result();
        //echo "<pre>"; var_dump($outData); die();
        if (!empty($outData)) {
            return $outData[0];
        }
        return false;
    }
    // Чтение категории
        public function loadCategory($aFilter=array(),$aObject=false,$aPage=0,$aCountRec=12) {
        $outWhere = ''; $outFields = '*'; $inOrder="";
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }
        if (!empty($aFilter) &&  is_array($aFilter)) {
            foreach ($aFilter as $outKey=>$outData) {
                if ($outKey=='status') {
                    $outWhere .= " and {$this->inCategoryStatus} = '{$outData}'";
                } 
                elseif($outKey=='name') {
                    $outWhere .= " and {$this->inCategoryName} = '{$outData}'";
                }else  {
                    $outWhere .= " and  {$outKey} = '{$outData}'";
                        }
            }
        }
        
        $inOrder .= (!empty($this->inCategoryOrderFields))?"order by {$this->inCategoryOrderFields} {$this->inOrderType}":"";
        //var_dump($inOrder); 
        if ($this->inCountRec>0) {
                $inOrder .= " limit {$this->inCountRec}";
            } 
        if ($aPage>0) {
            $inOrder .= " limit ".(($aPage-1)*$aCountRec).",$aCountRec";
        }
        $inSql = "SELECT {$outFields} FROM {$this->inTblCategoryName} where 1 {$outWhere} {$inOrder}";
        $this->isDebug($inSql);
        $outData = (!$aObject)?$this->db->query($inSql)->result_array():$this->db->query($inSql)->result();
        if (!empty($outData)) {
            return $outData;
        }
        return false;
    }
    // Выбюорка категории по ИД
    public function loadCategoryById($aId, $aFilter=array()) {
        $outWhere = '1'; $outFields = '*';
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }
        if (!empty($aId)) {
            $outWhere .= " and {$this->inCategoryId} = '" . $aId . "'";
        }
        $inSql = "SELECT {$outFields} FROM {$this->inTblCategoryName} where {$outWhere}";
        $this->isDebug($inSql);
        $outData = $this->db->query($inSql)->result_array();
        if (!empty($outData)) {
            return $outData[0];
        }
        return false;
    }
    // Загрузка дерева категория - элементы в разрезе пейджинга при необходимости
    public function loadTree($aFilter=array(), $aUnion=false, $aPage=0, $aCountRec=12,$aRender=false) {
        //var_dump($aUnion, $aPage, $aCountRec); echo "<br>";
        //var_dump($aFilter); die();
        $inTreeFilter = (!empty($aFilter['tree']))?$aFilter['tree']:
            array('fields'=>array("{$this->inCategoryId} as value, {$this->inCategoryName} as title, {$this->inCategoryStatus}"));
        $inItemFilter = (!empty($aFilter['item']))?$aFilter['item']:
            array('fields'=>array("{$this->inSelfId} as value, {$this->inSelfName} as title, {$this->inStatus}"));   
        if(!empty($inTreeFilter['order']['order_fields'])) {
            $this->setCategoryOrder($inTreeFilter['order']['order_fields'],$inTreeFilter['order']['order_type']);
            unset($inTreeFilter['order']);
        }    
        if(!empty($inItemFilter['order']['order_fields'])) {
            $this->setOrder($inItemFilter['order']['order_fields'],$inItemFilter['order']['order_type']);
            unset($inItemFilter['order']);
        }    
        //echo"<pre>"; var_dump($inTreeFilter, $inItemFilter); die();
        $inCategory = $this->loadCategory($inTreeFilter,false,$aPage,$aCountRec);
        if (is_array($inCategory)) {
            foreach ($inCategory as $keyCat=>$dataCat) {
                $inFilter = array_merge(
                    $inItemFilter,
                    array($this->inCategoryId=>$dataCat['value'])
                );
                //var_dump($aPage,$aCountRec);
                $this->setCountRecord(0);
                $inSubTree = $this->load($inFilter, false);
                
                if ($inSubTree) {
                    $dataCat['sub_tree'] = $inSubTree;
                    $inCategory[$keyCat] = $dataCat;
                } else {
                    /*$this->Debug();
                    $inSubTree = $this->load($inFilter, false, $aPage-1,$aCountRec);*/
                };
            }
        }
        //echo "<pre>"; var_dump($inCategory); die();
        return $inCategory;
    }
    // Загрузка дерева для поиска
    public function loadTreeSearch($aFilter=array(),$aUnion=false,$aPage=0) {
        $inCategory = $this->loadCategory(array($this->inCategoryStatus=>1,'fields'=>array("{$this->inCategoryId} as category, {$this->inCategoryName} as category_title")));
        //echo "<pre>"; var_dump($inCategory); die();
        foreach ($inCategory as $keyCat=>$dataCat) {
            //$this->Debug();
            $inFilter = array('fields'=>array("{$this->inSelfId} as item_id, {$this->inSelfName} as item_title"));
            if (!empty($aFilter)) {
                $inFilter = array_merge(
                        $aFilter,
                        array($this->inCategoryId=>$dataCat['category'],'language_id'=>$aFilter['language_id'],$this->inStatus=>1),
                        $inFilter
                        );
                //echo "<pre>"; var_dump($inFilter);
            }
            $this->setCountRecord(0);
            $inSubTree = $this->load($inFilter, false, $aPage);
            if ($inSubTree) {
                $dataCat['sub_tree'] = $inSubTree;
                $inCategory[$keyCat] = $dataCat;
            } else unset($inCategory[$keyCat]);
        }
        return $inCategory;
    }
    // Удаление записи
    public function delete($aData) {
        if (empty($aData)) {
            $outRes = $this->db->delete($this->inTblName, $aData); 
            return $outRes;
        }
    }
    // Удаление записи по ИД
    public function deleteById($aId) {
        $outWhere = ''; 
        if (!empty($aId)) {
            $outWhere .= "{$this->inSelfId} = '".$aId."'";
            $inSql = "delete FROM {$this->inTblName} where {$outWhere}";
            $outData = $this->db->query($inSql);
            if (!empty($outData)) {
                return $outData;
            }
        }
        return false;
    }
    // Удаление категории по ИД категории
    public function deleteCategoryById($aId) {
        $outWhere = ''; 
        if (!empty($aId)) {
            $outWhere .= "{$this->inCategoryId} = '".$aId."'";
            $inSql = "delete FROM {$this->inTblCategoryName} where {$outWhere}";
            $this->isDebug($inSql);
            $outData = $this->db->query($inSql);
            if (!empty($outData)) {
                return $outData;
            }
        }
        return false;
    }
    // Обсчет элементов в категории с учетом статуса активен
    public function CountByCategoryId($aId=null,$aFilter=array()) {
        if (empty($aId)) { return 0; }
        $outData= $this->load(array('status'=>1,"{$this->inCategoryId}"=>$aId));
        $outData = ($outData)?count($outData):0;
        return $outData;
    }
    // Изменение статуса
    public function changeStatus($aId, $aFieldName,$aCategory=FALSE) {
        
        $inData = ($aCategory)?$this->loadCategoryById($aId):$this->loadById($aId);
        if ($inData) {
            $inData[$aFieldName] = ($inData[$aFieldName]==1)?0:1;
            //var_dump($aId, $aFieldName,$aCategory);
            //$this->Debug();
            $outRes = ($aCategory)?$this->save_category($inData):$this->save($inData);
            return $outRes;
        }
        return FALSE;
    }
    // Чтение кол-ва страниц пейджинга
    public function loadCountPage($aRecordByPage,$aData=array()) {
        $outResult = $this->load($aData, true);
        $outResult = count($outResult);
        return ceil($outResult/$aRecordByPage);
    }
    // Сохранение записи
    public function save($aData) {
        $outRes = false;
        if (isset($aData['images_id'])) {
            $aData['images_id'] = (trim($aData['images_id'])=='')?null:trim($aData['images_id']);
        }
        
        if (empty($aData[$this->inSelfId])) {
            if (isset($aData[$this->inSelfId])) { unset($aData[$this->inSelfId]); }
            $inSql = "insert into {$this->inTblName}";
            $inField = array(); $inOutData = array();
            foreach ($aData as $inKey => $inData) {
                $inField[] = $inKey;
                $inOutData[] = "'".$inData."'";
            }
            $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
            $this->isDebug($inSql);
            $outRes = $this->db->insert($this->inTblName, $aData); 
            $outRes = $outRes?$this->db->insert_id():false;
            //return  array('rec-no'=>$outRes);
        } else  {
                    $this->db->where($this->inSelfId, $aData[$this->inSelfId]);
                    $outRes = $this->db->update($this->inTblName, $aData); 
                    $outRes = $aData[$this->inSelfId];
                    //echo "<pre>"; var_dump($aData); die("TYT");
                }
        return  array('rec-no'=>$outRes);       
    }
    // Сохранение каьтегории
    public function save_category($aData) {
        $outRes = false; $inField = array(); $inOutData = array();
        if (empty($aData[$this->inCategoryId])) {
            unset($aData[$this->inCategoryId]);
            $inSql = "insert into {$this->inTblCategoryName}";
            foreach ($aData as $inKey => $inData) {
                $inField[] = $inKey;
                $inOutData[] = "'".$inData."'";
            }
            $inSql .= "(".implode(',', $inField).") values (".implode(',', $inOutData).")";
            $this->isDebug($inSql);
            $outRes = $this->db->insert($this->inTblCategoryName, $aData); 
        } else  {
                    $inSql = "update {$this->inTblCategoryName} set ";
                    //die("TYT");
                    foreach ($aData as $inKey => $inData) {
                        $inField[] = $inKey;
                        $inOutData[] = "$inKey ='".$inData."'";
                    }
                    $inSql .= implode(',', $inOutData)." where({$this->inCategoryId}='{$aData[$this->inCategoryId]}')";
                    $this->isDebug($inSql);
                    //return array('rec-no'=>$inSql);
                    $this->db->where($this->inCategoryId, $aData[$this->inCategoryId]);
                    $outRes = $this->db->update($this->inTblCategoryName, $aData); 
                }
        return  array('rec-no'=>$outRes);       
    }
    // Чтение шаблона вывода
    public function getOutput($aProcess=null) {}
    // Чтение формы ввода данных
    public function getForm($aData=array(), $aProcess=null) {}
}
