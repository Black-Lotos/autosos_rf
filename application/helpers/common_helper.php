<?php
define('VARIABLE_DIGIT', 0501);
define('VARIABLE_STRING', 0502);
function month_to_text_rus($aMonth) {
  switch($aMonth):
	case 1: return "ЯНВ "; break;
	case 2: return "ФЕВ "; break;
	case 3: return "МАР "; break;
	case 4: return "АПР "; break;
	case 5: return "МАЙ "; break;
	case 6: return "ИЮН "; break;
	case 7: return "ИЮЛ "; break;
	case 8: return "АВГ "; break;
	case 9: return "СЕН "; break;
	case 10: return "ОКТ "; break;
	case 11: return "НОЯ "; break;
	case 12: return "ДЕК "; break;
	default: return "";break;
  endswitch;
}
function month_to_text_ukr($aMonth) {
  switch($aMonth):
	case 1: return "СІЧ "; break;
	case 2: return "ЛЮТ "; break;
	case 3: return "БЕР "; break;
	case 4: return "КВІ "; break;
	case 5: return "ТРА "; break;
	case 6: return "ЧЕР "; break;
	case 7: return "ЛИП "; break;
	case 8: return "СЕР "; break;
	case 9: return "ВЕР "; break;
	case 10: return "ЖОВ "; break;
	case 11: return "ЛИС "; break;
	case 12: return "ГРУ "; break;
	default: return "";break;
  endswitch;
}
function getDecodeDate($aDate,$aLan='ru') {
    $aDate = empty($aDate)?date('Y-m-d H:i:s',time()):$aDate;
    $inDate = preg_split('/\s/', $aDate);
    $inParse = preg_split('/-/', $inDate[0]);
    //echo "<pre>"; var_dump($aDate);
    $inParse = $inParse[2]." ".month_to_text_rus($inParse[1]).$inParse[0].", ".$inDate[1];
    //var_dump($inParse); die();
    return $inParse;
}
function jdecoder($json_str) {
    $cyr_chars = array (
    '\u0430' => 'а', '\u0410' => 'А', '\u0431' => 'б', '\u0411' => 'Б', '\u0432' => 'в', '\u0412' => 'В', '\u0433' => 'г', '\u0413' => 'Г', '\u0434' => 'д', '\u0414' => 'Д', '\u0435' => 'е', '\u0415' => 'Е',
    '\u0451' => 'ё', '\u0401' => 'Ё', '\u0436' => 'ж', '\u0416' => 'Ж', '\u0437' => 'з', '\u0417' => 'З', '\u0438' => 'и', '\u0418' => 'И', '\u0439' => 'й', '\u0419' => 'Й', '\u043a' => 'к', '\u041a' => 'К',
    '\u043b' => 'л', '\u041b' => 'Л', '\u043c' => 'м', '\u041c' => 'М', '\u043d' => 'н', '\u041d' => 'Н', '\u043e' => 'о', '\u041e' => 'О', '\u043f' => 'п', '\u041f' => 'П', '\u0440' => 'р', '\u0420' => 'Р',
    '\u0441' => 'с', '\u0421' => 'С', '\u0442' => 'т', '\u0422' => 'Т', '\u0443' => 'у', '\u0423' => 'У', '\u0444' => 'ф', '\u0424' => 'Ф', '\u0445' => 'х', '\u0425' => 'Х', '\u0446' => 'ц', '\u0426' => 'Ц',
    '\u0447' => 'ч', '\u0427' => 'Ч', '\u0448' => 'ш', '\u0428' => 'Ш', '\u0449' => 'щ', '\u0429' => 'Щ', '\u044a' => 'ъ', '\u042a' => 'Ъ', '\u044b' => 'ы', '\u042b' => 'Ы', '\u044c' => 'ь', '\u042c' => 'Ь',
    '\u044d' => 'э', '\u042d' => 'Э', '\u044e' => 'ю', '\u042e' => 'Ю', '\u044f' => 'я', '\u042f' => 'Я', '\r' => '', '\n' => '<br />', '\t' => '', '\u00ab'=>'«', '\u00bb'=>'»' );
	//var_dump($json_str);
	try {
            foreach ($cyr_chars as $key => $value) {
                    $json_str = str_replace($key, $value, $json_str);
            }
	}	
	catch (Exception $e) {   
                
        }
    return $json_str;
}
function CI_changeImageSize($aFile,$aWidth,$aHeight,$aQuality=75) {
    $outSave = ''; $outResult = "";
    
    if(!empty($aFile) && file_exists($aFile)) {
        $inInfo = pathinfo($aFile);
        //echo "<pre>"; var_dump($inInfo); die();
        switch (strtolower($inInfo['extension'])) {
            case 'jpg': $imSource   =   imagecreatefromjpeg($aFile);
                        $imResult   =   imagecreatetruecolor($aWidth,$aHeight);
                        if(imagesx($imSource)<$aWidth) {
                            imagecopyresampled($imResult, $imSource, 0,0,0,0,imagesx($imSource),imagesy($imSource),imagesx($imSource),imagesy($imSource));
                        } else {
                            imagecopyresampled($imResult, $imSource, 0,0,0,0,$aWidth,$aHeight,imagesx($imSource),imagesy($imSource));
                        }
                        $outSave = 'uploads/'."min_".$inInfo['basename'];
                        imagejpg($imResult,$outSave);
                        $outResult = $outSave;
                        //echo "$outSave";
                        break;    
            case 'png': $imSource   =   imagecreatefrompng($aFile);
                        
                        if(imagesx($imSource)<$aWidth) {
                            $imResult   =   imagecreatetruecolor(imagesx($imSource),imagesy($imSource));
                            imagecopyresampled($imResult, $imSource, 0,0,0,0,imagesx($imSource),imagesy($imSource),imagesx($imSource),imagesy($imSource));
                        } else {
                            $imResult   =   imagecreatetruecolor($aWidth,$aHeight);
                            imagecopyresampled($imResult, $imSource, 0,0,0,0,$aWidth,$aHeight,imagesx($imSource),imagesy($imSource));
                        }
                        $outSave = 'uploads/'."min_".$inInfo['basename'];
                        imagepng($imResult,$outSave);
                        //echo "$outSave";
                        $outResult = $outSave;
                        break;            
        }
    }
    return $outResult;
}
function setCategoryActiveItem($aArray,$aActive) {
    $resOut = array();
    if (is_array($aArray)) {
        foreach ($aArray as $inKey => $inValue) {
            $inValue['active'] = ($inValue['value']==$aActive)?'selected':'';
            $resOut[$inKey] = $inValue;
        }
    }
    return  $resOut;
}
function setActiveItem($aArray,$aActive) {
    $resOut = array();
    if (is_array($aArray)) {
        foreach ($aArray as $inKey => $inValue) {
            $inValue['active'] = ($inValue['value']==$aActive)?'selected':'';
            $resOut[$inKey] = $inValue;
        }
    }
    return  $resOut;
}
function _email($to, $from, $subject, $message) {
    $headers= "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=windows-1251\r\n";
    $headers .= "From: {$from}\r\n";
    return (mail($to, $subject, $message, $headers))?'send_success':'send_fail';
}
function getPicturesFromId($aImageId=0) {
    $CI = &get_instance();
    if ($aImageId==0)return "images/empty{$aDefaultSize}x{$aDefaultSize}.png";
    $CI->load->model('Picture_model');
    $inImages = $CI->Picture_model->loadById($aImageId);
    if (!$inImages) {
        return "images/empty{$aDefaultSize}x{$aDefaultSize}.png";
    }
    return $CI->Picture_model->inPicPath.$inImages['picture_file'];
}
function getImagesForElement($aImageId,$aDefaultSize=210) {
    $CI = &get_instance();
    if ($aImageId==0)return "images/empty{$aDefaultSize}x{$aDefaultSize}.png";
    $CI->load->model('Images_model');
    $inImages = $CI->Images_model->loadById($aImageId);
    if (!$inImages) {
        return "images/empty{$aDefaultSize}x{$aDefaultSize}.png";
    }
    return $inImages['images_folder'].$inImages['images_name'];
}
function getImagesIdForElement($aImageId,$aDefaultSize=210) {
    $CI = &get_instance();
    if ($aImageId==0)return false;
    $CI->load->model('Images_model');
    $inImages = $CI->Images_model->loadById($aImageId);
    if (!$inImages) {
        return false;
    }
    return "/Images_module/getImages/{$inImages['images_id']}/200/200/1";
}
function getImagesForElement01($aImageId,$aDefaultSize=210) {
    $CI = &get_instance();
    $CI->load->controller('Images_module');
    $CI->Images_module->getImage($aImageId,$aDefaultSize,$aDefaultSize);
}
function setLanguage(&$aData,$Process=false) {
    $CI = &get_instance();
    //var_dump($CI->Language_model->loadLanguage(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')))); die();
    if ($Process) {
        $inActive = $aData['language_id'];
        $aData['language_id'] = $CI->Language_model->load(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')));
        $aData['language_id'] = setCategoryActiveItem($aData['language_id'], $inActive);
    } else
        $aData['language_id'] = $CI->Language_model->load(array('lan_status'=>1,'fields'=>array('lan_title as title','language_id as value')));
}
function getCaptionInput($aValue) {
    $CI = &get_instance();
    return $CI->lang->line($aValue);
}
function getCaptchaValidate() {
    $CI = &get_instance();
    return $CI->session->userdata('captcha');;
}
function setMessage($aText = NULL, $aType = 'status', $aJson=false) {
    global $Containter_Messages;
    $CI = &get_instance();
    if (!isset($Containter_Messages))
        $Containter_Messages = $CI->session->userdata("messages");
    if (!empty($aText)) {
        $Containter_Messages[$aType][] = ($aJson)?jdecoder($aText):$aText;
    }
    $CI->session->set_userdata("messages", $Containter_Messages);
    return $Containter_Messages;
}
function getMessage($aType = NULL, $aClean = TRUE, $aCss = '') {
    global $Containter_Messages;
    $outResult = '';
    $CI = &get_instance();
    if (!isset($Containter_Messages))
        $Containter_Messages = $CI->session->userdata("messages");
    $inData = (empty($aType)) ? $Containter_Messages : ((!empty($Containter_Messages[$aType])) ? $Containter_Messages[$aType] : array());
    if (!empty($inData)) {
        $outResult = "<div id='output-system-success' class='messages $aCss'>";
        foreach ($inData as $fKey => $fData) {
            $outResult .= "<div class='{$aType}'>";
            if (!empty($fData)) {
                if (is_array($fData)) {
                    $outResult .= '<ul>';
                    foreach ($fData as $item) {
                        $outResult .= '<li>' . $item . '</li>';
                    }
                    $outResult .= '</ul>';
                } else $outResult .= $fData;
            }
            $outResult .= '</div>';
            if ($aClean) {
                unset($Containter_Messages[$aType][$fKey]);
            }
        }
        $outResult .= '</div>';
    }
    $CI->session->set_userdata("messages", $Containter_Messages);
    return $outResult;
}
function getVariable($aVariable) {
    $CI = &get_instance();
    if (!empty($aVariable)) {
        $CI->load->model('Variables_model');
        $inData = $CI->Variables_model->loadByAlias($aVariable,array('fields'=>array('variable_value as value','variable_type as type')),true);
        //var_dump($inData);
        if ($inData) {
            return $inData->value;
        }
        setMessage("Переменная $aVariable не определена.", 'error');
        return false;
    } else {
        setMessage("Переменная $aVariable не определена.", 'error');
        return false;
    }
}
function getCallback($aVariable) {
    $CI = &get_instance();
    if (!empty($aVariable)) {
        $CI->load->model('Variables_model');
        $inData = $CI->Variables_model->loadByAlias($aVariable,array('fields'=>array('variable_value as value', 'variable_callback as function_callback, variable_type as type')),true);
        if ($inData&&$inData->function_callback) {
            $inData->value = ($inData->type==VARIABLE_DIGIT)?(int)$inData->value:$inData->value;
            unset($inData->type);
            return $inData;
        }
        setMessage("Переменная $aVariable не определена.", 'error');
        return false;
    } else {
        setMessage("Переменная $aVariable не определена.", 'error');
        return false;
    }
}
function getVariableLanguage($aLanguage='ru') {
    $CI = &get_instance();
    $CI->load->model('Variables_model');
    $aLanguage = ($aLanguage=='ru')?'':'_'.$aLanguage;
    $inData = $CI->Variables_model->load(array('variable_type'=>VARIABLE_STRING,"fields"=>array('variable_name',"variable_value{$aLanguage}")),false);
    return $inData;
}
function getVariableLanguageByName($aVariable, $aLanguage='ru') {
    $CI = &get_instance();
    $CI->load->model('Variables_model');
    //$CI->Variables_model->Debug();
    $aLanguage = ($aLanguage=='ru')?'':'_'.$aLanguage;
    $inData = $CI->Variables_model->load(array('variable_name'=>$aVariable,'variable_type'=>VARIABLE_STRING,"fields"=>array('variable_name',"variable_value{$aLanguage}")),false);
    $inData = $inData[0]; 
    $inCss = (!empty($inData['variable_css']))?$inData['variable_css']:'';
    $inData = $inData["variable_value{$aLanguage}"];
    return $inData;
}
function clearHtmlTags($aValue) {
    $pattern = array ("'<script[^>]*?>.*?</script>'si", "'<[\/\!]*?[^<>]*?>'si",
                 "'([\r\n])[\s]+'", "'&(quot|#34);'i", "'&(amp|#38);'i",
                 "'&(lt|#60);'i", "'&(gt|#62);'i", "'&(nbsp|#160);'i",
                 "'&(iexcl|#161);'i", "'&(cent|#162);'i","'&(pound|#163);'i",
                 "'&(copy|#169);'i","'&#(\d+);'e");
 
    $replace = array ("","","\\1","\"","&","<",">"," ",chr(161),
                  chr(162),chr(163),chr(169),"chr(\\1)");
    return trim(preg_replace($pattern, $replace, $aValue));
}
function loadContentUrl($aUrl) {
    if( $curl = curl_init() ) {
        $outResult = file_get_contents($aUrl);
    } else {
        $outResult = file_get_contents($aUrl);
    }
    return $outResult;
}
function GoRedirect($aPage=array()) {
    echo "gud";
}
function valid_phone($aPhone = NULL) {
    $outResult = false;
    if (empty($aPhone)) return $outResult;
    $inPattern = '/^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/';
    if (preg_match($inPattern, $aPhone)) {return $outResult = true;}
    return $outResult;
}