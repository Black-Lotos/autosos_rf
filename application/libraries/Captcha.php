<?php
define('CAPTCHA_PARAM_MODULE', '_md');
define('CAPTCHA_MODULE_NAME',  'captcha');

define('CAPTCHA_PARAM_ACTION', '_ac');
define('CAPTCHA_ACTION_GET',   'get');
define('CAPTCHA_ACTION_RESET', 'reset');
define('CAPTCHA_PARAM_RESET',  '_rs');

define('CAPTCHA_SESSION_TAG',  'EAA152AB-3922-4254-AEDE-34F488473CE0');
define('CAPTCHA_SESSION_TAG_PARAMS',  'C38EA481-BF3D-4D56-8A50-2236B291C698');

class Captcha {
    var $chars	= 6;
    var $min_size	= 20;
    var $max_size	= 25;
    var $max_rotation = 30;
    var $noise = true;
    var $text = '';
    var $base_path;
    var $noise_factor = 10;  // this will multiplyed with number of chars
    var $websafe_colors = false;
    var $secret_key = '16084700-C444-4E94-915A-0A10CFBF8595-3C9F94AF-1EEB-43B6-B0E2-C92B40228957-4907E544-0AF6-4F67-AA50-E19B047F54FC';
    var $value = null;
    var $lx;			// ширина картинки
    var $ly;			// высота картинки
    var $jpegquality = 100;	// качество картинки
    var $nb_noise;		
    var $key;			
    var $gd_version;		
    var $r;
    var $g;
    var $b;

    var $fonts_folder;
    var $fonts = array('comic.ttf', 'arial.ttf', 'times.ttf');
    //
    function captcha() {
    
        $CI =& get_instance();
        $this->base_path = $CI->config->item('base_url');
        
        $this->fonts_folder = dirname($_SERVER['SCRIPT_FILENAME']).'/fonts/captcha/';
        // Test for GD-Library(-Version)
        $this->gd_version = $this->get_gd_version();
    }
    //
    function handler() {
        $this->set_custom_parameters();
        $this->render();
        if (get(CAPTCHA_PARAM_MODULE) == CAPTCHA_MODULE_NAME) {
            if (get(CAPTCHA_PARAM_RESET))
            $this->reset();
            if (get(CAPTCHA_PARAM_ACTION) == CAPTCHA_ACTION_GET)
            $this->draw();
        }      
    }
    //
    function guid() {
        return sprintf('%04x%04x-%04x-%03x4-%04x-%04x%04x%04x',
           mt_rand(0, 65535), mt_rand(0, 65535), // 32 bits for "time_low"
           mt_rand(0, 65535), // 16 bits for "time_mid"
           mt_rand(0, 4095),  // 12 bits before the 0100 of (version) 4 for "time_hi_and_version"
           bindec(substr_replace(sprintf('%016b', mt_rand(0, 65535)), '01', 6, 2)),
               // 8 bits, the last two of which (positions 6 and 7) are 01, for "clk_seq_hi_res"
               // (hence, the 2nd hex digit after the 3rd hyphen can only be 1, 5, 9 or d)
               // 8 bits for "clk_seq_low"
           mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535) // 48 bits for "node"  
        );  
    }
    //
    function reset() {
        $key = md5($this->guid());
        $this->value = substr(md5($key), 16 - $this->chars / 2, $this->chars);
        $_SESSION[CAPTCHA_SESSION_TAG] = $this->value;
    }
    //
    function render()  {
        if (!($this->value = session(CAPTCHA_SESSION_TAG)))
        $this->reset();
    }
    //
    function draw($submit = 0) {
        $this->reset();
        if (!$submit) {
            if ($this->gd_version == 0) 
            //die('There is no GD-Library-Support enabled. The Captcha-Class cannot be used!');
            // check TrueTypeFonts
            foreach ($this->fonts as $font_name) {
                if (!is_readable($this->fonts_folder.$font_name)) {
                    die('Font "'.$font_name.'" not found');
                }               
            }
            $this->nb_noise = $this->noise ? ($this->chars * $this->noise_factor) : 0;
            // set dimension of image
            if (!$this->lx)
            $this->lx = ($this->chars + 1) * (int)(($this->max_size + $this->min_size) / 1.5);
            if (!$this->ly)
            $this->ly = (int)(2.4 * $this->max_size);
            
            if($this->gd_version >= 2 && !$this->websafe_colors) {
                $func1 = 'imagecreatetruecolor';
                $func2 = 'imagecolorallocate';
            } else {
                $func1 = 'imageCreate';
                $func2 = 'imagecolorclosest';
            }
            $image = $func1(160, 50);
            $this->random_color(224, 255);
            $back = @imagecolorallocate($image, 225,226,228);
            @ImageFilledRectangle($image,0,0,$this->lx,$this->ly,$back);
            if (($this->gd_version < 2) or $this->websafe_colors) {
                $this->make_websafe_colors($image);    
            }
            if($this->nb_noise > 0) {
                $x = $this->lx - 200;
                $dx = 20;
                for($i=0; $i < $this->chars; $i++) {
                    srand((double)microtime()*1000000);
                    $size  = intval(rand((int)($this->min_size / 2.3), (int)($this->max_size / 1.7)) + 15);
                    srand((double)microtime()*1000000);
                    $angle  = intval(rand(0, 30));
                    $x    = $x + $dx;
                    $y    = intval(($this->ly - ($size / 5)) - 12);
                    $this->random_color(160, 224);
                    $color  = $func2($image, $this->r, $this->g, $this->b);
                    srand((double)microtime()*1000000);
                    $text  = $this->get_random_char();
                    $this->text .= $text;
                    @ImageTTFText($image, $size, $angle, $x, $y, $color, $this->random_font(), $text);
                }
            } else {
                for($i=0; $i < $this->lx; $i += (int)($this->min_size / 1.5)) {
                  $this->random_color(160, 224);
                  $color  = $func2($image, $this->r, $this->g, $this->b);
                  @imageline($image, $i, 0, $i, $this->ly, $color);
                }
                for($i=0 ; $i < $this->ly; $i += (int)($this->min_size / 1.8)) {
                  $this->random_color(160, 224);
                  $color  = $func2($image, $this->r, $this->g, $this->b);
                  @imageline($image, 0, $i, $this->lx, $i, $color);
                }
            }
            $r = intval(rand(0, 3));
            @ImageJPEG($image, dirname($_SERVER['SCRIPT_FILENAME']).'/temp/captcha'.$r.'.jpg', $this->jpegquality);
            @ImageDestroy($image);

            $this->set_value();
        }
        return '<img border="0" src="'.$this->base_path.'temp/captcha'.$r.'.jpg">';
    }
    //
    function set_value() {
        $_SESSION[CAPTCHA_SESSION_TAG] = $this->text;
    }
    //
    function get_value() {
        return $_SESSION[CAPTCHA_SESSION_TAG];
    }
    //
    function make_websafe_colors(&$image)	{
        for($r = 0; $r <= 255; $r += 51) {
            for($g = 0; $g <= 255; $g += 51) {
                for($b = 0; $b <= 255; $b += 51) {
                    $color = imagecolorallocate($image, $r, $g, $b);
                }
            }
        }
    }
    //
    function random_color($min,$max) {
        srand((double)microtime() * 1000000);
        $this->r = intval(rand($min,$max));
        srand((double)microtime() * 1000000);
        $this->g = intval(rand($min,$max));
        srand((double)microtime() * 1000000);
        $this->b = intval(rand($min,$max));
    }
    //
    function random_font() {
      srand((float)microtime() * 10000000);
      $key = array_rand($this->fonts);
      $font_name = $this->fonts[$key];
      return $this->fonts_folder.$font_name;
    }
    //
    function url() {
        return BASE_URL.'?'.CAPTCHA_PARAM_MODULE.'='.CAPTCHA_MODULE_NAME.'&'.CAPTCHA_PARAM_ACTION.'='.CAPTCHA_ACTION_GET;
    }
    //
    function get_gd_version() {
        static $gd_version_number = null;
        if ($gd_version_number === null) {
            ob_start();
            //phpinfo(8);
            $module_info = ob_get_contents();
            ob_end_clean();
            //var_dump($module_info); die();
            if (preg_match("/\bgd\s+version\b[^\d\n\r]+?([\d\.]+)/i", $module_info, $matches)) {
                $gd_version_number = $matches[1];
            } else {
                $gd_version_number = 0;
            }
        }
        return $gd_version_number;
    }
    //
    function init_custom_parameters($parameters = array()) {
      if (count($parameters)) {
        foreach ($parameters as $pname => $pvalue) {
          $_SESSION[CAPTCHA_SESSION_TAG_PARAMS][$pname] = $pvalue;
        } 
      }
    }
    //
    function unset_custom_parameters() {
      unset($_SESSION[CAPTCHA_SESSION_TAG_PARAMS]);
    }
    //
    function set_custom_parameters() {
      $parameters = session(CAPTCHA_SESSION_TAG_PARAMS);
      if (safe($parameters, 'chars')) {
        $this->chars = safe($parameters, 'chars');
      }
      if (safe($parameters, 'width')) {
        $this->lx = safe($parameters, 'width');
      }
      if (safe($parameters, 'height')) {
        $this->ly = safe($parameters, 'height');
      }
      if (safe($parameters, 'min_size')) {
        $this->min_size = safe($parameters, 'min_size');
      }
      if (safe($parameters, 'max_size')) {
        $this->max_size = safe($parameters, 'max_size');
      }
      if (safe($parameters, 'max_rotation')) {
        $this->max_rotation = safe($parameters, 'max_rotation');
      }
      if (safe($parameters, 'noise')) {
        $this->noise = safe($parameters, 'noise');
      }
      if (safe($parameters, 'noise_factor')) {
        $this->noise_factor = safe($parameters, 'noise_factor');
      }
    }
    //
    function get_random_char() {
        $char_periods = array(
            0 => rand(49, 57),
            1 => rand(65, 90),
            2 => rand(97, 122),
        );
        return chr(intval($char_periods[rand(0, 2)]));
    }
}