<?php
class Project_Twig_Extension extends Twig_Extension {
    public function getName(){
        return 'project';
    }
    public function getFunctions() {
        return array(
            'countModules' => new Twig_Function_Method($this, 'countModules'),
            'homePage' =>new Twig_Function_Method($this, 'homePage'),
            'getListBlogs' => new Twig_Function_Method($this, 'getListBlogs'),
            'getDecodeDate' => new Twig_Function_Method($this, 'getDecodeDate'),
            'writeBlogsCount' => new Twig_Function_Method($this, 'writeBlogsCount'),
            'writeUserLogin' => new Twig_Function_Method($this, 'writeUserLogin'),
            'getDate' => new Twig_Function_Method($this, 'getDate'),
            'getImagesForElement' => new Twig_Function_Method($this,'getImagesForElement'),
            'writeCuttingText' => new Twig_Function_Method($this,'writeCuttingText'),
            'getNameModule' => new Twig_Function_Method($this,'getNameModule'),
            'getNameModuleParent' => new Twig_Function_Method($this,'getNameModuleParent'),
            'getImageSize' => new Twig_Function_Method($this,'getImageSize'),
            'checkFileExists' => new Twig_Function_Method($this,'checkFileExists'),
            'getBaseUrl' => new Twig_Function_Method($this,'getBaseUrl'),
            'changeImageSize' => new Twig_Function_Method($this,'changeImageSize'),
            'getCaptionInput'  => new Twig_Function_Method($this,'getCaptionInput'),
            'getImageNews'  => new Twig_Function_Method($this,'getImageNews'),
            'callStrFunction' => new Twig_Function_Method($this,'callStrFunction'),
            'getCaptcha' => new Twig_Function_Method($this,'getCaptcha'),
            'getCaptchaValidate' => new Twig_Function_Method($this,'getCaptchaValidate'),
            'testOut' => new Twig_Function_Method($this,'testOut'),
            'initArray' => new Twig_Function_Method($this,'initArray'),
            'pushArray' => new Twig_Function_Method($this,'pushArray'),
            'pushArrayInKey' => new Twig_Function_Method($this,'pushArrayInKey'),
            'getVariable' => new Twig_Function_Method($this,'getVariable'),
            'getPicturesFromId' => new Twig_Function_Method($this,'getPicturesFromId'),
        );
    }
    public function testOut() {
        return "Test";
    }
    public function initArray() {
        return array();
    }
    public function pushArray($aArray,$aElement) {
        $aArray[] = $aElement;
        return $aArray;
    }
    public function pushArrayInKey($aArray,$aKey,$aElement) {
        $aArray[$aKey] = $aElement;
        return $aArray;
    }
    public function getVariable($aVariable) {
        return getVariable($aVariable);
    }
    function getPicturesFromId($aImageId=0) {
        return getPicturesFromId($aImageId);
    }
    public function getImagesForElement($aImages) {
        return getImagesForElement($aImages);
    }
    public function getCaptchaValidate() {
        return getCaptchaValidate();
    }
    public function getCaptcha() {
        $CI = &get_instance();
        $CI->load->library('Captcha');
        $captcha = $CI->captcha->draw();
        $captcha_value = $CI->captcha->get_value();
        $CI->session->set_userdata(array('captcha' => $captcha_value));
        return $captcha;
    }
    
    public function callStrFunction($aName,$aParam) {
        return call_user_func(array($this, $aName),$aParam);
    }
    public function getImageNews($aImageId) {
        $outRes = ($aImageId==0)?'':"<img class='' src='/{$this->getImagesForElement($aImageId)}' width='200px' height='200px' alt=''>";
        //$outRes = "<img class='' src='$inImg' width='200px' height='200px' alt=''>";
        return $outRes;
    }
    public function changeImageSize($aFile,$aWidth,$aHeight,$aQuality=75) {
        return CI_changeImageSize($aFile,$aWidth,$aHeight,$aQuality);
    }
    //
    public function getBaseUrl() {
        return base_url();
        
    }
    public function checkFileExists($aPath,$aFile) {
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$aPath.$aFile)) {
            return true;
        } else {
            return false;
        }
    }
    public function getImageSize($aImage) {
        $inImageSize = getimagesize ($aImage);
        return "width:{$inImageSize[0]}px; height:{$inImageSize[1]}px; ";
    }
    public function getNameModule($aSegment=null) {
        $CI = &get_instance();
        if (!$aSegment) {
            $inUri = $CI->uri->uri_string();
            $inUri = empty($inUri)?"home":$inUri;
        } else  {
            $inUri = $CI->uri->segment($aSegment);
        }
        return $inUri;
    }
    public function getNameModuleParent() {
        $CI = &get_instance();
        $inUri = $CI->uri->uri_string();
        $inUri = empty($inUri)?"home":$inUri;
        return $inUri;
    }
    public function writeCuttingText($aText,$aCount) {
        return (mb_strlen(strip_tags($aText))>$aCount?mb_substr(strip_tags($aText), 0, $aCount).'...':strip_tags($aText));
    }
    
    public function getDate() {
        return date('Y-m-d H:i:s',time());
    }
    public function countModules(){
        return false;
    }
    public function homePage(){
        return base_url();
    }
    public function getListBlogs(&$aData,&$CountToColumn) {
        $CI = &get_instance();
        $CI->load->model("Blogs_model");
        echo "<pre>";print_r($aData);
        return "<pre>";
    }
    public function writeUserLogin($aUserId) {
        $CI = &get_instance();
        $inUser = $CI->load->model('Users_model');
        return print_r($inUser->load($aUserId),true);
    }
    public function writeBlogsCount($aId) {
        return getBlogsCount($aId);
    }
    public function getDecodeDate($aDate,$aLan='ru') {
        return getDecodeDate($aDate,$aLan);
    }
    function getCaptionInput($aValue) {
        return getCaptionInput($aValue);
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

