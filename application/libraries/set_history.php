<?php if (!defined('BASEPATH')) exit('Нет доступа к скрипту');


class Set_history
{  

    /**
     * Изменить лог
     * @param type $data
     */
    public function change_log($data){
        $CI = &get_instance();
        $user = $CI->session->userdata('user');
        $CI->load->model('History_model'); 
        $array = array();
        if($user->id == '')
            return;        
        $array['modified_by'] = (int)$user->id;           
        $array['date_modify'] = date('Y-m-d H:i:s',time());
        $array['id'] = $data['id'];
        $table = $data['table'];
        $CI->History_model->update_history($table, $array); 
    }
    
     /**
     * Добавить лог
     * @param type $data
     */
    public function add_log($data){
        $CI = &get_instance();
        $user = $CI->session->userdata('user');
        $CI->load->model('History_model'); 
        $array = array();
        if($user->id == '')
            return;        
        $array['created_by'] = (int)$user->id;           
        $array['date_create'] = date('Y-m-d H:i:s',time());
        $array['id'] = $data['id'];
        $table = $data['table'];
        $CI->History_model->update_history($table, $array); 
    } 
    
    /**
     * Добавить событие
     * @param type $data
     */
    public function add_event($data){
        $CI = &get_instance();
        $user = $CI->session->userdata('user');
        $CI->load->model('Event_model'); 
        $array = array();
        if($user->id == '')
            return;        
        $array['user_id'] = (int)$data['user_id'];           
        $array['date'] = date('Y-m-d H:i:s',time());
        $array['event_id'] = $data['event_id'];
        $array['slug'] = $data['slug'];
        $CI->Event_model->insert($array); 
    } 
    /*
     * непонятно но используется в модулях
     */
    public function access_log($data) {
       
    }
}
?>