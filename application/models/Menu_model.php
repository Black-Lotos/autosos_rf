<?php
class Menu_model extends MY_Model {
    function __construct(){
        parent::__construct();
    }
    //
    protected function StartUp() {
        //parent::StartUp();
        $this->inTblName = 'tb_menu';
        $this->inTblCategoryName = 'tb_menu_category';
        $this->inTblUnion = 'v_menu_category_to_menu';
        $this->inCountRec = 0;
        $this->inOrderFields='owner_id, mi_weight';
        $this->inSelfId = 'menu_id';
        $this->inSelfName = 'mi_title_ru';
        $this->inStatus = 'mi_status';
        $this->inCategoryId = 'menu_category_id';
        $this->inCategoryStatus = 'mc_status';
        $this->inCategoryName = 'mc_name';
        
    }
    //
    public function getTabs($aPage=1,$aParam=null) {
        $outResult = $this->inTabs;
        $outMenu = !empty($aParam)?"/$aParam/":"";
        $outResult['property']= array('template'=>'administration/common/list-start-up-02.twig','title'=>'Список меню','include_js'=>'info.menu.js');
        if (empty($aParam)) {
            $outResult['data']['list'] = array('title' => 'Список меню','target'=>'pnl-list','url'=> "/administration/admin_menu/load{$outMenu}?page={$aPage}");
            $outResult['data']['add'] = array('title' => 'Добавить меню','target'=>'pnl-item','url'=> '/administration/admin_menu/add');
            
        } else  {
            //$this->Debug();
                $inCategory = $this->loadCategoryById($aParam, array('fields'=>array('mc_title as title')));
                //var_dump($inCategory); die();
                $outResult['property']['title'] = $inCategory['title'];
                $outResult['data']['list'] = array('title' => $inCategory['title'],'target'=>'pnl-list','url'=> "/administration/admin_menu/load{$outMenu}?page={$aPage}");
                $outResult['data']['add'] = array('title' => 'Добавить пункт','target'=>'pnl-item','url'=>"/administration/admin_menu/add/{$aParam}");
                }
        //var_dump("/administration/admin_menu/load{$outMenu}?page={$aPage}"); die();
        $outResult['data']['default'] = $outResult['data']['list']['url'];
        //loadContentUrl(base_url().$outResult['data']['list']['url']);    
        /*if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, base_url().$outResult['data']['list']['url']);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            $out = curl_exec($curl);
            $outResult['data']['default'] = $out;
            curl_close($curl);
        }*/

        return $outResult;
    }
    //
    public function loadParentMenuByLink($aFilter=array()) {
        $inParse = $this->load($aFilter,false);
        if($inParse) {
            $inParse = $this->load(array('mi_id'=>$inParse[0]['owner_id'],false));
        }
        //echo "<pre>"; var_dump($inParse); die();
    }
    //
    public function getMaxItem($aData, $aUnion=false) {
        $this->setObjectResult = true;

        if (is_array($aData)) {
            //$this->Debug();
            $aData['fields'] = array("max({$aData['fields'][0]}) as MaxRecNo");
            //$this->Debug();
            $inResult = $this->load($aData,false,false);
            $inResult = $inResult[0];
            //var_dump($inResult); die();
            return array('max_item'=>$inResult->MaxRecNo);
        } else return false;
    }
    // Перезагрузка родительского вызова, т.к. изменено количество параметров дерева категория - элементы в разрезе пейджинга при необходимости
    public function loadTree($aFilter=array(), $aUnion=false, $aPage=0, $aCountRec=12, $aRender=false) {
        //var_dump($aFilter); die();
        $inTreeFilter = (!empty($aFilter['tree']))?$aFilter['tree']:
            array('fields'=>array("{$this->inCategoryId} as value, {$this->inCategoryName} as title, {$this->inCategoryStatus}"));
        $inItemFilter = (!empty($aFilter['item']))?$aFilter['item']:
            array('fields'=>array("{$this->inSelfId} as value, {$this->inSelfName} as title, {$this->inStatus}"));    
        $inCategory = $this->loadCategory($inTreeFilter);
        if (is_array($inCategory)) {
            foreach ($inCategory as $keyCat=>$dataCat) {
                $inFilter = array_merge(
                    $inItemFilter,
                    array($this->inCategoryId=>$dataCat['value'])
                );
                //var_dump($inFilter);
                $this->setCountRecord(0);
                //$this->Debug();
                // перегрузили именно из за этого вызова
                $inSubTree = $this->load($inFilter, false, false, $aPage,$aCountRec);
                //echo "<pre>"; var_dump(count($inSubTree)); 
                if ($inSubTree) {
                    foreach ($inSubTree as $inKey => $inData) {
                        $inLevel3 = $this->load(array('owner_id'=>$inData['value'],'fields'=>array("{$this->Menu_model->getSelfId()} as value","{$this->Menu_model->getSelfName()} as title", "{$this->Menu_model->getStatus()} as status")), false, false, $aPage,$aCountRec);
                        if ($inLevel3) {
                            $inSubTree[$inKey]['sub_tree'] = $inLevel3;
                        };
                    }
                    $dataCat['sub_tree'] = $inSubTree;
                    $inCategory[$keyCat] = $dataCat;
                };
            }
        }
        if (!isset($inCategory[0]['sub_tree'])) {
            $inCategory[0]['sub_tree'] = '';
        }
        return $inCategory[0]['sub_tree'];
    }
    //
    
    public function load($aFilter=array(), $aUnion=true, $aRender=true, $aPage=0, $aCountRec=12) {
        $outWhere = ''; $outFields = '*'; 
        $inOrder=($aUnion)?"order by mc_weight, {$this->inOrderFields}":"order by $this->inOrderFields";
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }
        if (isset($aFilter['status'])) {
            $outWhere .= " and mi_status = '" . $aFilter['status'] . "'";
            unset($aFilter['status']);
        } 
        if (isset($aFilter['name'])) {
            $outWhere .= " and mc_name = '" . $aFilter['name'] . "'";
            unset($aFilter['name']);
        } 
        if (!empty($aFilter) &&  is_array($aFilter)) {
            foreach ($aFilter as $outKey=>$outData) {
                $outWhere .= " and  {$outKey} = '{$outData}'";
            }
        }
        $inTable = (($aUnion)?$this->inTblUnion:$this->inTblName);
        if ($this->inCountRec>0) {
            $inOrder = "order by {$this->inOrderFields} DESC limit {$this->inCountRec}";
        } 
        if ($aPage>0) {
            $inOrder = "order by {$this->inOrderFields} limit ".(($aPage-1)*$aCountRec).",{$aCountRec}";
        }
        $inSql = "SELECT {$outFields} FROM {$inTable} where 1 {$outWhere} {$inOrder}";
        $this->isDebug($inSql);
        $outData = (!$this->setObjectResult)?$this->db->query($inSql)->result_array():$this->db->query($inSql)->result();
        if (!empty($outData)) {
            $outData = ($aRender)?$this->renderMenu($outData):$outData;
            //echo "<pre>"; var_dump($outData); die("gggg");
            return $outData;
        }
        return false;
    }
    //
    public function getOutput($aData=array()) {
        $outResult = array (  
            'property'=>array('title'=>'Меню системы','isRun'=>true,'include_js'=>"info.menu.js"),
            'titles'=>array(),
            'data'=>array()
        );
        return  $outResult;
    }
    //
    public function getForm($aData=array(),$aProcess=null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frmMenu-add",'method'=>'post','action'=>"/administration/admin_menu/save",'include_js'=>'info.menu.js'),
            'form_data'=>array(
                array('caption'=>'Выбор меню','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'menu_category_id','set_value'=>$aData['menu_category_id'])),
                array('caption'=>'Выбор родителя','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'owner_id','set_value'=>$aData['owner_id'])),
                array('caption'=>'Выбор языка меню','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id','set_value'=>$aData['language_id'])),
                array('caption'=>'Алиас пункта','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_name','set_value'=>(!empty($aData['mi_name'])?$aData['mi_name']:''))),
                array('caption'=>'Ссылка вызова','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_url','set_value'=>(!empty($aData['mi_url'])?$aData['mi_url']:''))),
                array('caption'=>'Выводимый заголовок','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_title_ru','set_value'=>(!empty($aData['mi_title_ru'])?$aData['mi_title_ru']:''))),
                array('caption'=>'Title menu item','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_title_en','set_value'=>(!empty($aData['mi_title_en'])?$aData['mi_title_en']:''))),
                array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')), 
                array('caption'=>'Meta-Title','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                    'set_name'=>'meta_title','set_value'=>(!empty($aData['meta_title'])?$aData['meta_title']:''))),
                array('caption'=>'Meta-Keywords','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                    'set_name'=>'meta_keywords','set_value'=>(!empty($aData['meta_keywords'])?$aData['meta_keywords']:''))),
                array('caption'=>'Meta-Description','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                    'set_name'=>'meta_description','set_value'=>(!empty($aData['meta_description'])?$aData['meta_description']:''))),
                array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),                        
                array('caption'=>'Тип','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'mi_type','set_value'=>(!empty($aData['mi_type'])?$aData['mi_type']:'-1'))),
                array('caption'=>'Порядок вывода','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_weight','set_value'=>(!empty($aData['mi_weight'])?$aData['mi_weight']:'0'))),
                array('caption'=>'Примечание','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_require'=>'','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>'mi_description','set_value'=>(!empty($aData['mi_description'])?$aData['mi_description']:''))),
                array('caption'=>'Автоматическая коррекция','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_require'=>' ','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'mi_auto','set_value'=>(!empty($aData['mi_auto'])?$aData['mi_auto']:'-1'))),
                array('caption'=>'Каскадный стиль','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_class','set_value'=>(!empty($aData['mi_class'])?$aData['mi_class']:''))),
                array('caption'=>'Функция обратного вызова','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_function','set_value'=>(!empty($aData['mi_function'])?$aData['mi_function']:''))),
                array('caption'=>'Файл генерации','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mi_filename','set_value'=>(!empty($aData['mi_filename'])?$aData['mi_filename']:''))),
                array('caption'=>'Метка обновления','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'mi_updated','set_value'=>time())),
                array('caption'=>'Идентификатор пункта','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'menu_id','set_value'=>(!empty($aData['menu_id'])?$aData['menu_id']:''))),
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'menu_category_id','set_value'=> (!empty($aData['menu_category_id']))?$aData['menu_category_id']:'')),
                array('caption'=>'Активировать пункт','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>'mi_status','set_value'=>(!empty($aData['mi_status'])?$aData['mi_status']:'0'))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
            )
        );
        if ($aProcess==PROCESS_MENU_ADD_SECTION) {
            $inWeight = $this->loadCategory(array('fields'=>array('max(mc_weight) as max_weight')),true); $inWeight = $inWeight[0];
            $inWeight = (int)$inWeight->max_weight; $inWeight++;
            //var_dump($inWeight);die();
            $outResult['form_property']=array('name'=>"frmMenuAddSection",'method'=>'post','action'=>"/administration/admin_menu/save_section",'include_js'=>'info.menu.js');
            $outResult['form_data'] = array(
                array('caption'=>'Выбор языка меню','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id','set_value'=>$aData['language_id'])),
                array('caption'=>'Название меню','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mc_name','set_value'=>(!empty($aData['mc_name'])?$aData['mc_name']:''))),
                array('caption'=>'Выводимый заголовок','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mc_title','set_value'=>(!empty($aData['mc_title'])?$aData['mc_title']:''))),
                array('caption'=>'Примечание','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_require'=>'','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>'mc_description','set_value'=>(!empty($aData['mc_description'])?$aData['mc_description']:''))),
                array('caption'=>'Порядок вывода','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'mc_weight','set_value'=>(!empty($aData['mc_weight'])?$aData['mc_weight']:$inWeight))),
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_css'=>' ','set_name'=>'menu_category_id','set_value'=> (!empty($aData['menu_category_id']))?$aData['menu_category_id']:'')),
                array('caption'=>'Активировать меню','set_css'=>"text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>'mc_status','set_value'=>(!empty($aData['mc_status'])?$aData['mc_status']:'0'))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'')),        
            );    
        }
        return  $outResult;
    }
    //
    private function renderMenu($aMenu) {
        $outMenu = array();
        $inUriStr = "/".$this->uri->uri_string()."/";
        try {
            foreach ($aMenu as $keyMenu => $dataMenu) {
                if ($dataMenu['owner_id']==0) {
                    $oPattern = "/^" . str_replace('/', '\/', $dataMenu['mi_url']) . "/";
                    $oRes = preg_match($oPattern,$inUriStr);
                    $dataMenu['active'] = $this->inRecursive
                    ?
                    $dataMenu['active'] = ($oRes>0&&!empty($dataMenu['mi_url']))?'active':''
                    :
                    $dataMenu['active'] = ((!empty($dataMenu['mi_url']))&&($inUriStr==($dataMenu['mi_url']."/")))?'active':'';
                    $outMenu[$keyMenu] = $dataMenu;
                }
                else {
                    //
                    foreach ($outMenu as $keySmenu => $dataSmenu) {
                            if ($dataSmenu['menu_id']==$dataMenu['owner_id']) {
                                $oPattern = "/".str_replace('/', '\/', $dataMenu['mi_url'])."/";
                                $oRes = preg_match($oPattern,$dataSmenu['mi_url']);
                                //echo "=".$dataMenu['url']." ".uri_string()." ".$oRes."<br>";
                                $dataMenu['active'] = ($dataMenu['mi_url']==$inUriStr)?'active':'';
                                $outMenu[$keySmenu]['sub_tree'][] = $dataMenu;
                            }
                    }
                }
            }
        } 
        catch (Exception $e) {
            echo 'Выброшено исключение: ',  $e->getMessage(), "\n";
            var_dump($aMenu);
        }
        return $outMenu;
    }
}