<?php
    class Static_model extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            //parent::StartUp();
            $this->inTblName = 'tb_sp';
            $this->inTblCategoryName = 'tb_sp_category';
            $this->inTblUnion = 'v_spc_to_sp';
            $this->inCountRec = 0;
            $this->inOrderFields='spc_id, sp_id';
            $this->inSelfId = 'sp_id';
            $this->inStatus = 'sp_status';
            $this->inCategoryId = 'spc_id';
            $this->inCategoryName = 'spc_title';
            $this->inCategoryStatus = 'spc_id';
            $this->inAlias = 'sp_name';
        }
        //
        public function getTabs($aPage=1) {
            $outResult = $this->inTabs;
            $outResult['property'] = array('template' => 'administration/common/list-start-up-02.twig','title'=>'Статические страницы','include_js'=>'info.static-page.js');
            $outResult['data']['list'] = array('title' => 'Список страниц','target'=>'pnl-list','url'=> "/administration/admin_static/load?page={$aPage}");
            $outResult['data']['add'] = array('title' => 'Добавить страницу','target'=>'pnl-item','url'=> '/administration/admin_static/add');
            $outResult['data']['add_section'] = array('title' => 'Добавить раздел страниц','target'=>'pnl-collection','url'=> '/administration/admin_static/add_section');
            $outResult['data']['default'] = $outResult['data']['list']['url'];//file_get_contents(base_url().$outResult['data']['list']['url']);
                        
            return $outResult;
        }
        //
        public function loadTree($aFilter=array(),$aUnion=false,$aPage=0,$aCountRec=12, $aRender=false) {
            $inCategory = $this->loadCategory(array('fields'=>array('spc_title as title, spc_id as value, spc_status')));
            foreach ($inCategory as $outKey => $outData) {
                $inSubTree = $this->load(array('spc_id'=>$outData['value'],'fields'=>array('sp_title as title, sp_id as value, sp_status')),false);
                if ($inSubTree) {
                    $outData['sub_tree'] = $inSubTree;
                }
                $inCategory[$outKey] = $outData;
            }
            return $inCategory;
        }
        //
        public function getOutput($aData=array()) {
            $outResult = array (    
                'property'=>array('title'=>'Статические страницы','isRun'=>true,'include_js'=>"info.static-page.js"),
                'titles'=>array(),
                'data'=>array()
            );
            return  $outResult;
        }
        //
        public function getForm($aData=array(),$aProcess=null) {
            $outResult = array (    
                'form_property'=>array('name'=>"frmStaticPageAdd",'method'=>'post','action'=>"/administration/admin_static/save",'include_js'=>'info.static-page.js'),
                'form_data'=>array(
                    array('caption'=>'Выбор категории страниц','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'spc_id','set_value'=> (!empty($aData['spc_id']))?$aData['spc_id']:'')),
                    array('caption'=>'Выбор языка страницы','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id','set_value'=>$aData['language_id'])),
                    array('caption'=>'Выводимый заголовок','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'sp_title','set_value'=>(!empty($aData['sp_title'])?$aData['sp_title']:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')), 
                    array('caption'=>'Редирект страница с полным указанием html','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'redirect_url','set_value'=>(!empty($aData['redirect_url'])?$aData['redirect_url']:''))),
                    array('caption'=>'Meta-Title','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_title','set_value'=>(!empty($aData['meta_title'])?$aData['meta_title']:''))),
                    array('caption'=>'Meta-Keywords','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_keywords','set_value'=>(!empty($aData['meta_keywords'])?$aData['meta_keywords']:''))),
                    array('caption'=>'Meta-Description','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_description','set_value'=>(!empty($aData['meta_description'])?$aData['meta_description']:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Строка страницы для вызова','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'sp_name','set_value'=>(!empty($aData['sp_name'])?$aData['sp_name']:''))),
                    array('caption'=>'Текст страницы','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_require'=>'','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>'sp_text','set_value'=>(!empty($aData['sp_text'])?$aData['sp_text']:''))),
                    array('caption'=>'Активировать страницу','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>'sp_status','set_value'=>(!empty($aData['sp_status'])?$aData['sp_status']:'0'))),
                    array('caption'=>getCaptionInput('sp_date'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'sp_date','set_value'=>(!empty($aData['sp_date'])?$aData['sp_date']:date('Y-m-d H:i:s',time())))),
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'sp_id','set_value'=> (!empty($aData['sp_id']))?$aData['sp_id']:'')),
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'process','set_value'=>'item-save')),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                )
            );
            if ($aProcess==PROCESS_STATIC_ADD_SECTION) {
                /*$inWeight = $this->loadCategory(array('fields'=>array('max(mc_weight) as max_weight')),true); $inWeight = $inWeight[0];
                $inWeight = (int)$inWeight->max_weight; $inWeight++;*/
                //var_dump($inWeight);die();
                $outResult['form_property']=array('name'=>"frmStaticPageAddSection",'method'=>'post','action'=>"/administration/admin_static/save_section",'include_js'=>'info.static-page.js');
                $outResult['form_data'] = array(
                    array('caption'=>'Выбор языка страницы','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>
                                array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id','set_value'=>$aData['language_id'])),
                    array('caption'=>'Выводимый заголовок','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>
                                array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'spc_title','set_value'=>(!empty($aData['spc_title'])?$aData['spc_title']:''))),
                    array('caption'=>'Текст страницы','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>
                                array('set_require'=>'','set_require'=>'','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>'spc_text','set_value'=>(!empty($aData['spc_text'])?$aData['spc_text']:''))),
                    array('caption'=>'Идентификатор','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>
                                array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'spc_id','set_value'=> (!empty($aData['spc_id']))?$aData['spc_id']:'')),
                    array('caption'=>'Активировать страницу','set_css'=>"text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>'spc_status','set_value'=>(!empty($aData['spc_status'])?$aData['spc_status']:'0'))),
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'process','set_value'=>'section-save')),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                );    
            }
            return  $outResult;
        }
}