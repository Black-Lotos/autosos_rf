<?php
    class Auto_model extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            //parent::StartUp();
            $this->inTblName            = 'tb_avto';
            $this->inTblCategoryName    = 'tb_car';
            $this->inTblUnion           = 'v_car_to_avto';
            $this->inOrderFields        = 'car_id, avto_id';
            $this->inStatus             = 'avto_status'; 
            $this->inSelfId             = 'avto_id'; 
            $this->inSelfName           = 'avto_name';
            $this->inCategoryId         = 'car_id';
            $this->inCategoryName       = 'car_name';
            $this->inCategoryStatus     = 'car_status';
            $this->inSufix              = 'avto';
            $this->inPrefix             = 'avto';
            $this->inCategorySufix      = 'car';
            $this->inCategoryPrefix     = 'car';
        }
        //
        public function getTabs($aPage=1) {
            $outResult = $this->inTabs;
            $outResult['property']= array('template'=>'administration/common/list-start-up-02.twig','title'=>'Автомобильные ключи','include_js'=>'info.avto.js');
            $outResult['data']['list'] = array('title' => 'Список автомобилей','target'=>'pnl-list','url'=> "/administration/admin_avto/load?page={$aPage}");
            $outResult['data']['add_model'] = array('title' => 'Добавить модель','target'=>'pnl-list','url'=> "/administration/admin_avto/add_model");
            $outResult['data']['add'] = array('title' => 'Добавить автомобиль','target'=>'pnl-item','url'=> '/administration/admin_avto/add');
            $outResult['data']['add_section'] = array('title' => 'Добавить марку автомобилей','target'=>'pnl-collection','url'=> '/administration/admin_avto/add_section');
            $outResult['data']['default'] = $outResult['data']['list']['url'];
            return $outResult;
        }
        //
        public function getOutput($aData=array()) {
            $outResult = array (    
                //'property'=>array('title'=>'Автомобильные ключи','isRun'=>true,'include_js'=>"info.avto.js"),
                'titles'=>array(),
                'data'=>array()
            );
            return  $outResult;
        }
        //
        public function getForm($aData=array(),$aProcess=null) {
            $outResult = array (    
                'form_property'=>array('name'=>"frmStaticPageAdd",'method'=>'post','action'=>"/administration/admin_avto/save",'include_js'=>'info.avto.js'),
                'form_data'=>array(
                    array('caption'=>'Идентификатор машины','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->getSelfId()}",
                        'set_value'=> (!empty($aData[$this->getSelfId()]))?$aData[$this->getSelfId()]:'')),
                    array('caption'=>'Выбор марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"{$this->getCategoryId()}",
                        'set_value'=> (!empty($aData[$this->getCategoryId()]))?$aData[$this->getCategoryId()]:'')),
                    array('caption'=>'Выбор модели','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"model_id",
                        'set_value'=> (!empty($aData['model_id']))?$aData['model_id']:'')),            
                    array('caption'=>'Наименование автомобиля','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->getSelfName()}",
                        'set_value'=>(!empty($aData[$this->getSelfName()])?$aData[$this->getSelfName()]:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')), 
                    array('caption'=>'Meta-Title','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_title','set_value'=>(!empty($aData['meta_title'])?$aData['meta_title']:''))),
                    array('caption'=>'Meta-Keywords','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_keywords','set_value'=>(!empty($aData['meta_keywords'])?$aData['meta_keywords']:''))),
                    array('caption'=>'Meta-Description','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_description','set_value'=>(!empty($aData['meta_description'])?$aData['meta_description']:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),                        
                    array('caption'=>'','to_control'=>
                        array(  'set_type'=>'include_image',
                                'set_name'=>'btnClearImageAvto',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                //'set_for'=>"blogs_id_".(!empty($aData['blogs_id'])?$aData['blogs_id']:0)
                                )),
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"col-sm-12",'set_name'=>"{$this->inPrefix}_description",
                    'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),        
                    array('caption'=>'Год выпуска','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01", 'set_name'=>'avto_year',
                        'set_value'=>(!empty($aData['avto_year'])?$aData['avto_year']:''))),
                    array('caption'=>'Присоединить коллекцию','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>'cpicture_id',
                        'set_value'=>(!empty($aData['cpicture_id'])?$aData['cpicture_id']:''))),        
                    array('caption'=>'Активировать автомобиль','set_css'=>'text-cl-black',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'avto_status','set_value'=>(!empty($aData['avto_status'])?$aData['avto_status']:'0'))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                )
            );
            if ($aProcess==PROCESS_MODEL_ADD) {
                $outResult['form_property']=array('name'=>"frmStaticPageAddModel",'method'=>'post','action'=>"/administration/admin_avto/save_model",'include_js'=>'info.avto.js');
                $outResult['form_data'] = array(
                    array('caption'=>'Идентификатор модкли','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"model_id",
                        'set_value'=> (!empty($aData['model_id']))?$aData['model_id']:'')),
                    array('caption'=>'Выбор марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"{$this->getCategoryId()}",
                        'set_value'=> (!empty($aData[$this->getCategoryId()]))?$aData[$this->getCategoryId()]:'')),
                    array('caption'=>'Наименование модели','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>"input-skin-01",'set_name'=>'model_name',
                        'set_value'=>(!empty($aData['model_name'])?$aData['model_name']:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')), 
                    array('caption'=>'Meta-Title','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_title','set_value'=>(!empty($aData['meta_title'])?$aData['meta_title']:''))),
                    array('caption'=>'Meta-Keywords','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_keywords','set_value'=>(!empty($aData['meta_keywords'])?$aData['meta_keywords']:''))),
                    array('caption'=>'Meta-Description','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_description','set_value'=>(!empty($aData['meta_description'])?$aData['meta_description']:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),                        
                    array('caption'=>'Примечание для модели','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"col-sm-12",'set_name'=>"model_description",
                        'set_value'=>(!empty($aData["model_description"])?$aData["model_description"]:''))),
                    array('caption'=>'Активировать модель','set_css'=>"text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'model_status',
                        'set_value'=>(!empty($aData['model_status'])?$aData['model_status']:'0'))),
                    //array('caption'=>'','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'process','set_value'=>'section-save')),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                );    
            };     
            if ($aProcess==PROCESS_SECTION_ADD) {
                /*$inWeight = $this->loadCategory(array('fields'=>array('max(mc_weight) as max_weight')),true); $inWeight = $inWeight[0];
                $inWeight = (int)$inWeight->max_weight; $inWeight++;*/
                //var_dump($inWeight);die();
                $outResult['form_property']=array('name'=>"frmStaticPageAddSection",'method'=>'post','action'=>"/administration/admin_avto/save_section",'include_js'=>'info.avto.js');
                $outResult['form_data'] = array(
                    array('caption'=>'Идентификатор марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->getCategoryId()}",
                        'set_value'=> (!empty($aData[$this->getCategoryId()]))?$aData[$this->getCategoryId()]:'')),
                    array('caption'=>'Наименование марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>"input-skin-01",'set_name'=>$this->getCategoryName(),
                        'set_value'=>(!empty($aData[$this->getCategoryName()])?$aData[$this->getCategoryName()]:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')), 
                    array('caption'=>'Редирект страница с полным указанием html','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'redirect_url','set_value'=>(!empty($aData['redirect_url'])?$aData['redirect_url']:''))),
                    array('caption'=>'Meta-Title','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_title','set_value'=>(!empty($aData['meta_title'])?$aData['meta_title']:''))),
                    array('caption'=>'Meta-Keywords','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_keywords','set_value'=>(!empty($aData['meta_keywords'])?$aData['meta_keywords']:''))),
                    array('caption'=>'Meta-Description','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_description','set_value'=>(!empty($aData['meta_description'])?$aData['meta_description']:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),                        
                    array('caption'=>'','to_control'=>
                        array(  'set_type'=>'include_image',
                                'set_name'=>'btnClearImageCar',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                //'set_for'=>"blogs_id_".(!empty($aData['blogs_id'])?$aData['blogs_id']:0)
                                )),
                    array('caption'=>'Примечание для марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"col-sm-12",'set_name'=>"{$this->inCategoryPrefix}_description",
                        'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_description"])?$aData["{$this->inCategoryPrefix}_description"]:''))),
                    array('caption'=>'Активировать марку','set_css'=>"text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'car_status',
                        'set_value'=>(!empty($aData['car_status'])?$aData['car_status']:'0'))),
                    //array('caption'=>'','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'process','set_value'=>'section-save')),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                );    
            }
            return  $outResult;
        }
}