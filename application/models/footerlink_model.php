<?php
class Footerlink_model extends MY_Model {
    function __construct(){
        parent::__construct();
    }
    //
    protected function StartUp() {
        //parent::StartUp();
        $this->inTblName = 'tb_footerlink';
        $this->inStatus = 'footerlink_status';
        $this->inStatusMain='footerlink_main';
        $this->inTblUnion = 'v_category_to_footerlink';
        $this->inCountRec = 0;
        $this->inOrderFields='cfooterlink_id, footerlink_id';
        $this->inSelfId = 'footerlink_id';
        $this->inTblCategoryName = 'tb_footerlink_category';
        $this->inCategoryId = 'cfooterlink_id';
        $this->inCategoryStatus = 'language_status';
        $this->inCategoryName = 'language_name';
        $this->inSufix='footerlink';
        $this->inPrefix='footerlink';
    }
    //
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property'] = array('template'=>'administration/common/list-start-up-02.twig','title'=>'Ссылки сайта','include_js'=>"info-{$this->inSufix}.js");
        $outResult['data']['list'] = array('title' => 'Список ссылок','target'=>'pnl-list','url'=> "/administration/admin_footerlink/load?page={$aPage}");
        $outResult['data']['add'] = array('title' => 'Добавить ссылку','target'=>'pnl-item','url'=> '/administration/admin_footerlink/add');
        $outResult['data']['add_section'] = array('title' => 'Добавить раздел ссылок','target'=>'pnl-collection','url'=> '/administration/admin_footerlink/add_section');
        $outResult['data']['default'] = $outResult['data']['list']['url'];//file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    public function getForm($aData=array(), $aProcess=null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/administration/admin_footerlink/save",'include_js'=>"info-{$this->inSufix}.js"),
            'form_data'=>array(
                array('caption'=>'Идентификатор ссылки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_id",
                    'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                array('caption'=>'Категория ссылок','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'cfooterlink_id',
                    'set_value'=>(!empty($aData["cfooterlink_id"])?$aData["cfooterlink_id"]:''))),
                array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),            
                array('caption'=>'Заголовок ссылки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_title",
                    'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                array('caption'=>'Текст ссылки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_text",
                    'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                array('caption'=>'Url ссылки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_url",
                    'set_value'=>(!empty($aData["{$this->inSufix}_url"])?$aData["{$this->inSufix}_url"]:''))),            
                array('caption'=>'Активировать ссылку','set_css'=>"text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>'footerlink_status',
                    'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),
                /*array('caption'=>'Дата создания',
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'footerlink_creates','set_value'=>(!empty($aData['footerlink_creates'])?$aData['footerlink_creates']:date('Y-m-d H:i:s',time())))),*/
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        if ($aProcess==PROCESS_FOOTERLINK_SECTION) {
            $outResult['form_property']['action']="/administration/admin_footerlink/save_category";
            $outResult['form_data'] = array(
                array(  'caption'=>'Идентификатор блога','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"c{$this->inSufix}_id",
                                            'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                                                
                array(  'caption'=>'Название категории','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"c{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                array('caption'=>'Активировать ссылку','set_css'=>"text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>"c{$this->inSufix}_status",
                    'set_value'=>(!empty($aData["c{$this->inSufix}_status"])?$aData["c{$this->inSufix}_status"]:''))),                
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),                        
            );
        }
        return  $outResult;
    }
}