<?php
class Frequency_model extends MY_Model { 
    function StartUp() {
        $this->inTblName            = 'tb_keys_frequency';
        $this->inTblCategoryName    = '';
        $this->inTblUnion           = 'tb_keys_frequency';
        $this->inOrderFields        = 'mhz_name';
        $this->inStatus             = ''; 
        $this->inStatusMain         = ''; 
        $this->inStatusTop          = '';
        $this->inAlias              = '';
        $this->inSelfId             = 'mhz_id'; 
        $this->inSelfName           = 'mhz_name';
        $this->inCategoryId         = '';
        $this->inCategoryName       = '';
        $this->inCategoryStatus     = '';
        $this->inSufix              = 'mhz';
        $this->inPrefix             = 'mhz';
        $this->inCategorySufix      = '';
        $this->inCategoryPrefix     = '';
    }
    function __construct() {
        parent::__construct();
    }
    //
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property'] = array('template'=>'administration/common/list-start-up-02.twig','title'=>'Роли пользователя','include_js'=>"info.user.js");
        $outResult['data']['list'] = array('title' => 'Список ролей','target'=>'pnl-list','url'=> "/administration/users/load_roles?page={$aPage}");
        $outResult['data']['add'] = array('title' => 'Добавить роль','target'=>'pnl-item','url'=> '/administration/users/add_roles');
        $outResult['data']['default'] = $outResult['data']['list']['url']; //file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    function getForm($aData = array(), $aProcess = null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frmRoleAdd",'method'=>'post','action'=>"/administration/users/save_roles",'include_js'=>"info.user.js"),
            'form_data'=>array(
                array('caption'=>'Идентификатор роли','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"role_id",
                    'set_value'=>(!empty($aData["role_id"])?$aData["role_id"]:''))),
                array('caption'=>'Название роли','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"role_title",
                    'set_value'=>(!empty($aData["role_title"])?$aData["role_title"]:''))),
                //array('caption'=>'Дата создания','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'news_creates','set_value'=>(!empty($aData['news_creates'])?$aData['news_creates']:date('Y-m-d H:i:s',time())))),            
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        return $outResult;
    }
    
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

