<?php
class Images_model extends MY_Model {
    protected $inPicPath = "picture/";
    protected $inImgPath = "uploads/";
    protected $inVideoPath = "video/";
    function __construct(){
        parent::__construct();
    }
    //
    function StartUp() {
        $this->inTblName            = 'tb_images';
        $this->inTblCategoryName    = 'tb_images_category';
        $this->inTblUnion           = 'v_category_to_images';
        $this->inCountRec           = 0;
        $this->inOrderFields        = 'images_id';
        $this->inDebug              = false;
        $this->inStatus             = 'images_status'; 
        $this->inStatusMain         = 'images_main'; 
        $this->inStatusTop          = 'images_top';
        $this->inCategoryStatus     = '';
        $this->inAlias              = '';
        $this->inSelfId             = 'images_id'; 
        $this->inSelfName           = 'images_title'; 
        $this->inCategoryId         = '';
        $this->inCategoryName       = '';
        $this->inSufix              = 'images'; 
    }
    //
    
    //
    public function resizePicture($aImgId,$aWidth=null, $aHeight=null, $aAspectRatio=true) {
            $inImgSource = $this->loadById($aImgId, array($this->inSelfId=>$aImgId),false);
            //var_dump($inImgSource); die();
            //var_dump($aImgId,$aWidth,$aHeight, $aAspectRatio); die();
            if (empty($inImgSource)) {
                return false;
            }
            
            $inExt = mb_substr(strtolower(strrchr($inImgSource['images_name'], '.')),1,4);
            //Создаем изображение в зависимости от типа исходного файла
            switch ($inExt){
            case "jpg":
                $srcImage = @ImageCreateFromJPEG($this->inImgPath.$inImgSource['images_name']);
                break;
            case "jpeg":
                $srcImage = @ImageCreateFromJPEG($this->inImgPath.$inImgSource['images_name']);
                break;
            case "gif":
                $srcImage = @ImageCreateFromGIF($this->inImgPath.$inImgSource['images_name']);
                break;
            case "png":
                $srcImage = @ImageCreateFromPNG($this->inImgPath.$inImgSource['images_name']);
                break;
            default:
                return -1;
            }
            
            //var_dump($srcImage); die();
            $srcWidth = ImageSX($srcImage);
            $srcHeight = ImageSY($srcImage);
            //echo "Исходная картинка('$srcWidth, $srcHeight): <b>$this->inPicPath'</b><br><img src='{$srcImage}'/>";
            //header("Content-Type: image/".($inExt=='jpg')?'jpeg':$inExt);
            //imagejpeg($srcImage);
            if($aAspectRatio){
                $ratioWidth = $srcWidth/$aWidth;
                $ratioHeight = $srcHeight/$aHeight;
                $destHeight = intval($srcHeight/$ratioWidth);$aHeight;
                $destWidth = $aWidth;
            } else {
                
                $ratioWidth = $srcWidth/$aWidth;
                $ratioHeight = $srcHeight/$aHeight;
                $destHeight = $aHeight;
                $destWidth = intval($srcWidth/$ratioHeight);
            }
            $resImage = ImageCreateTrueColor($destWidth, $destHeight);
            $transparent = imagecolorallocatealpha($resImage, 0, 0, 0, 127); 
            imagefill($resImage, 0, 0, $transparent); 
            imagesavealpha($resImage, true);
            ImageCopyResampled($resImage, $srcImage, 0, 0, 0, 0, $destWidth, $destHeight, $srcWidth, $srcHeight);
            header("Content-Type: image/png");
            ImagePNG($resImage);
            ImageDestroy($srcImage);
            ImageDestroy($resImage);
        }
    //
    public function loadCategoryElemntByAlias($aAlias='alias_down_slider_video',$aFilter=array()) {
        $outWhere = "cimages_alias = '{$aAlias}' and "; $outFields = '*';
        if (isset($aFilter['fields']) &&  is_array($aFilter['fields'])) {
            $outFields = implode(',', $aFilter['fields']);
            unset($aFilter['fields']);
        }

        if (isset($aFilter['status'])) {
            $outWhere .= " and cimages_status = '" . $aFilter['status'] . "'";
        } else {
            if (!empty($aFilter) &&  is_array($aFilter)) {
                foreach ($aFilter as $outKey=>$outData) {
                    $outWhere .= " and  {$outKey} = '{$outData}'";
                }
            }
        }
        $inSql = "SELECT {$outFields} FROM {$this->inTblUnion} where 1 {$outWhere}";
        //var_dump($inSql); die();
        $outData = $this->db->query($inSql)->result_array();
        if (!empty($outData)) {
            return $outData[0];
        }
        return false;
    }
    //
    public function loadTree($aFilter=array(), $aUnion=false, $aPage=0, $aCountRec=12,$aRender=false) {
        //echo "<pre>"; var_dump($aFilter); die();
        $inFieldsCategory = $aFilter['category'];
        $inFieldsBlogs = $aFilter['images'];
        $outCategory = $this->loadCategory($inFieldsCategory);
        //echo "<pre>"; var_dump($outCategory); die();
        foreach ($outCategory as $outKey=>$outData) {
            $outData['sub_tree'] = $this->load(array('cimages_id'=>$outData['value'],'fields'=>$inFieldsBlogs['fields']));
            $outCategory[$outKey] = $outData;
        }
        return $outCategory;
    }
    //
    public function getForm($aData=array(), $aProcess=null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/administration/admin_images_video/save",'include_js'=>"info-{$this->inSufix}.js"),
            'form_data'=>array(
                array('caption'=>'Идентификатор видео','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"{$this->inSufix}_id",
                    'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                array('caption'=>'Категория видео','to_control'=>array('set_type'=>'select','set_name'=>'cimages_id','set_value'=>$aCategory)),
                array('caption'=>'Заголовок видео','to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inSufix}_title",
                    'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                array('caption'=>'Текст видео','to_control'=>array('set_require'=>'*','set_type'=>'textarea','set_name'=>"{$this->inSufix}_text",
                    'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                array('caption'=>'Видео файл','to_control'=>array('set_require'=>'*','set_type'=>'file','set_name'=>"{$this->inSufix}_file",
                    'set_value'=>(!empty($aData["{$this->inSufix}_file"])?$aData["{$this->inSufix}_file"]:''))),            
                array('caption'=>'Активировать видео','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'images_status',
                    'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),
                array('caption'=>'На главной','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_status_main",
                    'set_value'=>(!empty($aData["{$this->inSufix}_status_main"])?$aData["{$this->inSufix}_status_main"]:''))),
                array('caption'=>'В заголовке','to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inSufix}_status_top",
                    'set_value'=>(!empty($aData["{$this->inSufix}_status_top"])?$aData["{$this->inSufix}_status_top"]:''))),
                array('caption'=>'Дата создания',
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'images_creates','set_value'=>(!empty($aData['images_creates'])?$aData['images_creates']:date('Y-m-d H:i:s',time())))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSave{$this->inSufix}",'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        if ($aProcess==PROCESS_CONTENT_SECTION) {
            $outResult['form_property']['action']="/administration/admin_images_video/save_category";
            $outResult['form_data'] = array(
                array(  'caption'=>'Идентификатор видео',
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"c{$this->inSufix}_id",
                                            'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                array(  'caption'=>'Название категории',
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                array(  'caption'=>'Алиас категории',
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"c{$this->inSufix}_alias",
                        'set_value'=>(!empty($aData["c{$this->inSufix}_alias"])?$aData["c{$this->inSufix}_alias"]:''))),                
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'a','set_name'=>"btnSaveСContent",'set_css'=>'bottom-margin-1','set_url'=>'#')),                        
            );
        }
        return  $outResult;
    }
    //
    //
     public function getOutput($aProcess=null) {
        $outResult = array (    
            'property'=>array('title'=>'Контентное видео','isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'pieses/pieses-images-video.twig'),
            'titles'=>array(),
            'data'=>array()
        );
        switch ($aProcess) {
            case OUTPUT_LIST:
                $outResult['data']= $this->load(array(), true);
                $outResult['info']['count_record']=count($outResult['data']);
                foreach ($outResult['data'] as $inKey=>$inData) {
                    if (empty($inData['images_creates'])) {
                        unset($outResult['data'][$inKey]);
                    }
                    else {
                        $inData['images_creates'] = getDecodeDate($inData['images_creates']);
                        if (mb_strlen($inData['images_text'])>OUT_CUT_STRING) {
                            //die("yes");
                            $inData['cut_text'] =  mb_substr($inData['images_text'], 0, OUT_CUT_STRING).'...';
                            $inData['detail'] = true;
                        }
                        $outResult['data'][$inKey] = $inData;
                    }
                }
                break;
            default :
                $outResult['titles'] = array (    
                    array(
                        array('title'=>'ID','size'=>20),
                        array('title'=>'Заголовок блога','size'=>0),
                        array('title'=>"Статус",'size'=>80),
                        array('title'=>'Действия','size'=>80)
                    ),
                    'data'=>array($this->inSelfId,"{$this->inSufix}_title","{$this->inSufix}_status",'action')        
                );
        }
        return  $outResult;
    }
}