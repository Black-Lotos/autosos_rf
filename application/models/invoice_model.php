<?php
class Invoice_model extends MY_Model { 
    function StartUp() {
        $this->inTblName            = 'tb_invoice';
        $this->inTblCategoryName    = 'tb_invoice_section';
        $this->inTblUnion           = 'v_section_to_invoice';
        $this->inOrderFields        = '';
        $this->inStatus             = 'invoice_status'; 
        $this->inStatusMain         = ''; 
        $this->inStatusTop          = '';
        $this->inAlias              = '';
        $this->inSelfId             = 'invoice_id'; 
        $this->inSelfName           = 'invoice_title';
        $this->inCategoryId         = 'sinvoice_id';
        $this->inCategoryName       = 'sinvoice_title';
        $this->inCategoryStatus     = 'sinvoice_status';
        $this->inSufix              = 'invoice';
        $this->inPrefix             = 'invoice';
        $this->inCategorySufix      = 'sinvoice';
        $this->inCategoryPrefix     = 'sinvoice';
    }
    function __construct() {
        parent::__construct();
    }
    //
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property'] = array('template'=>'administration/common/list-start-up-02.twig','title'=>'Прайс лист','include_js'=>"info.price.js");
        $outResult['data']['list'] = array('title' => 'Список услуг','target'=>'pnl-list','url'=> "/administration/admin_price/load?page={$aPage}");
        $outResult['data']['add_section'] = array('title' => 'Добавить группу услуг','target'=>'pnl-item','url'=> '/administration/admin_price/add_section');
        $outResult['data']['add'] = array('title' => 'Добавить услугу','target'=>'pnl-item','url'=> '/administration/admin_price/add');
        $outResult['data']['default'] = $outResult['data']['list']['url']; //file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    function getForm($aData = array(), $aProcess = null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frmPriceAdd",'method'=>'post','action'=>"/administration/admin_price/save",'include_js'=>"info.price.js"),
            'form_data'=>array(
                array('caption'=>getVariable('caption_invoice_id'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_id",
                    'set_value'=>(!empty($aData["{$this->inPrefix}_id"])?$aData["{$this->inPrefix}_id"]:''))),
                array('caption'=>getVariable('caption_invoice_group_name'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"{$this->inCategoryPrefix}_id",
                    'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_id"])?$aData["{$this->inCategoryPrefix}_id"]:''))),            
                array('caption'=>getVariable('caption_invoice_name'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_title",
                    'set_value'=>(!empty($aData["{$this->inPrefix}_title"])?$aData["{$this->inPrefix}_title"]:''))),
                array('caption'=>getVariable('caption_invoice_price'),'set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_readonly'=>'','set_type'=>'numeric',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_price",
                        'set_value'=> (!empty($aData["{$this->inPrefix}_price"]))?$aData["{$this->inPrefix}_price"]:''),
                        ),                                                                    
                array('caption'=>getVariable('caption_invoice_description'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_description",
                    'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),                
                array('caption'=>getVariable('caption_status_record'),'set_css'=>'text-cl-black',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inPrefix}_status",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_status"])?$aData["{$this->inPrefix}_status"]:'0'))),            
                //array('caption'=>'Дата создания','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'news_creates','set_value'=>(!empty($aData['news_creates'])?$aData['news_creates']:date('Y-m-d H:i:s',time())))),            
                array('caption'=>getVariable('caption_button_save'),'to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        if ($aProcess==PROCESS_SECTION_ADD) {
            //var_dump($inWeight);die();
            $outResult['form_property']=array('name'=>"frmPriceAdd",'method'=>'post','action'=>"/administration/admin_price/save_section",'include_js'=>"info.price.js");
            $outResult['form_data'] = array(
                array('caption'=>getVariable('caption_invoice_group_id'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inCategoryPrefix}_id",
                    'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_id"])?$aData["{$this->inCategoryPrefix}_id"]:''))),
                array('caption'=>getVariable('caption_invoice_group_name'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inCategoryPrefix}_title",
                    'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_title"])?$aData["{$this->inCategoryPrefix}_title"]:''))),
                array('caption'=>getVariable('caption_invoice_group_description'),'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>"{$this->inCategoryPrefix}_description",
                    'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_description"])?$aData["{$this->inCategoryPrefix}_description"]:''))),                
                array('caption'=>getVariable('caption_status_record'),'set_css'=>'text-cl-black',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>"{$this->inCategoryPrefix}_status",
                        'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_status"])?$aData["{$this->inCategoryPrefix}_status"]:'0'))),            
                //array('caption'=>'Дата создания','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'news_creates','set_value'=>(!empty($aData['news_creates'])?$aData['news_creates']:date('Y-m-d H:i:s',time())))),            
                array('caption'=>getVariable('caption_button_save'),'to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                );
        }                
        return $outResult;
    }
    
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

