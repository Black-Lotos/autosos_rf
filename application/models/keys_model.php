<?php
    class Keys_model extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            //parent::StartUp();
            $this->inTblName            = 'tb_keys';
            $this->inTblCategoryName    = 'tb_avto';
            $this->inTblUnion           = 'tb_keys';
            $this->inOrderFields        = 'keys_id, avto_id';
            $this->inStatus             = 'keys_status'; 
            $this->inSelfId             = 'keys_id'; 
            $this->inSelfName           = 'keys_name';
            $this->inCategoryId         = 'avto_id';
            $this->inCategoryName       = 'avto_name';
            $this->inCategoryStatus     = 'avto_status';
            $this->inSufix              = 'keys';
            $this->inPrefix             = 'keys';
            $this->inCategorySufix      = 'avto';
            $this->inCategoryPrefix     = 'avto';
        }
        //
        public function getTabs($aParam,$aPage=1) {
            $outResult = $this->inTabs;
            $outResult['property']= array('template'=>'administration/common/list-start-up-02.twig','title'=>'Автомобильные ключи','include_js'=>'info.avto.js');
            $outResult['data']['list'] = array('title' => 'Список ключей','target'=>'pnl-list','url'=> "/administration/admin_avto/load_keys/{$aParam}/?page={$aPage}");
            $outResult['data']['add'] = array('title' => 'Добавить ключ','target'=>'pnl-item','url'=> "/administration/admin_avto/add_keys/{$aParam}");
            $outResult['data']['default'] = $outResult['data']['list']['url'];
            return $outResult;
        }
        //
        public function getOutput($aData=array()) {
            $outResult = array (    
                //'property'=>array('title'=>'Автомобильные ключи','isRun'=>true,'include_js'=>"info.avto.js"),
                'titles'=>array(),
                'data'=>array()
            );
            return  $outResult;
        }
        //
        public function getForm($aData=array(),$aProcess=null) {
            $outResult = array (    
                'form_property'=>array('name'=>"frmStaticPageAdd",'method'=>'post','action'=>"/administration/admin_avto/save_keys",'include_js'=>'info.avto.js'),
                'form_data'=>array(
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*', 'set_readonly' => '*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"keys_id",
                        'set_value'=> (!empty($aData['keys_id']))?$aData['keys_id']:'')),            
                    array('caption'=>'Выбор марки автомобиля','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*', 'set_readonly' => '*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"car_id",
                        'set_value'=> (!empty($aData['car_id']))?$aData['car_id']:'')),            
                    array('caption'=>'Выбор модели','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_readonly'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"model_id",
                        'set_value'=> (!empty($aData['model_id']))?$aData['model_id']:'')),            
                    array('caption'=>'Автомобиль','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"{$this->getCategoryId()}",
                        'set_value'=> (!empty($aData[$this->getCategoryId()]))?$aData[$this->getCategoryId()]:'')),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')), 
                    array('caption'=>'Meta-Title','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_title','set_value'=>(!empty($aData['meta_title'])?$aData['meta_title']:''))),
                    array('caption'=>'Meta-Keywords','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_keywords','set_value'=>(!empty($aData['meta_keywords'])?$aData['meta_keywords']:''))),
                    array('caption'=>'Meta-Description','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",
                        'set_name'=>'meta_description','set_value'=>(!empty($aData['meta_description'])?$aData['meta_description']:''))),
                    array('caption'=>'','to_control'=>array('set_require'=>'','set_type'=>'separator')),                        
                    array('caption'=>'Наименование ключа','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->getSelfName()}",
                        'set_value'=>(!empty($aData[$this->getSelfName()])?$aData[$this->getSelfName()]:''))),
                    array('caption'=>'Наложить водяной знак','set_css'=>'text-cl-black',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'keys_wm',
                        'set_value'=>(!empty($aData['keys_wm'])?$aData['keys_wm']:'1'))),                                
                    array('caption'=>'','to_control'=>
                        array(  'set_type'=>'include_image',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                //'set_for'=>"blogs_id_".(!empty($aData['blogs_id'])?$aData['blogs_id']:0)
                                )),     
                    array('caption'=>'Тип ключа','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_readonly'=>'','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"keys_type",
                        'set_extends'=>"control-to-element='#keys_mhz'",
                        'set_value'=> (!empty($aData['keys_type']))?$aData['keys_type']:'')),                        
                    array('caption'=>'Жало ключа','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_readonly'=>'','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"keys_sting",
                        'set_value'=> (!empty($aData['keys_sting']))?$aData['keys_sting']:'')),                                    
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"col-sm-12",'set_name'=>"{$this->inPrefix}_description",
                    'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),        
                    array('caption'=>'Частота работы','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01", 'set_name'=>'keys_mhz',
                        'set_value'=>(!empty($aData['keys_mhz'])?$aData['keys_mhz']:''))),
                    array('caption'=>'Цена ключа','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_readonly'=>'','set_type'=>'numeric',"set_css"=>"input-skin-01",'set_name'=>"keys_price",
                        'set_value'=> (!empty($aData['keys_price']))?$aData['keys_price']:''),
                        ),                                                        
                    array('caption'=>'В наличии','set_css'=>'text-cl-black',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'keys_instock',
                        'set_value'=>(!empty($aData['keys_instock'])?$aData['keys_instock']:'0'))),        
                    array('caption'=>'Активировать ключ','set_css'=>'text-cl-black',
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'keys_status',
                        'set_value'=>(!empty($aData['keys_status'])?$aData['keys_status']:'0'))),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                )
            );
            if ($aProcess==PROCESS_MODEL_ADD) {
                $outResult['form_property']=array('name'=>"frmStaticPageAddModel",'method'=>'post','action'=>"/administration/admin_avto/save_model",'include_js'=>'info.avto.js');
                $outResult['form_data'] = array(
                    array('caption'=>'Идентификатор модкли','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"model_id",
                        'set_value'=> (!empty($aData['model_id']))?$aData['model_id']:'')),
                    array('caption'=>'Выбор марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"{$this->getCategoryId()}",
                        'set_value'=> (!empty($aData[$this->getCategoryId()]))?$aData[$this->getCategoryId()]:'')),
                    array('caption'=>'Наименование модели','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>"input-skin-01",'set_name'=>'model_name',
                        'set_value'=>(!empty($aData['model_name'])?$aData['model_name']:''))),
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"col-sm-12",'set_name'=>"model_description",
                    'set_value'=>(!empty($aData["model_description"])?$aData["model_description"]:''))),
                    array('caption'=>'Активировать модель','set_css'=>"text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'model_status',
                        'set_value'=>(!empty($aData['model_status'])?$aData['model_status']:'0'))),
                    //array('caption'=>'','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'process','set_value'=>'section-save')),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                );    
            };     
            if ($aProcess==PROCESS_SECTION_ADD) {
                /*$inWeight = $this->loadCategory(array('fields'=>array('max(mc_weight) as max_weight')),true); $inWeight = $inWeight[0];
                $inWeight = (int)$inWeight->max_weight; $inWeight++;*/
                //var_dump($inWeight);die();
                $outResult['form_property']=array('name'=>"frmStaticPageAddSection",'method'=>'post','action'=>"/administration/admin_avto/save_section",'include_js'=>'info.avto.js');
                $outResult['form_data'] = array(
                    array('caption'=>'Идентификатор марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->getCategoryId()}",
                        'set_value'=> (!empty($aData[$this->getCategoryId()]))?$aData[$this->getCategoryId()]:'')),
                    array('caption'=>'Наименование марки','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>"input-skin-01",'set_name'=>$this->getCategoryName(),
                        'set_value'=>(!empty($aData[$this->getCategoryName()])?$aData[$this->getCategoryName()]:''))),
                    array('caption'=>'','to_control'=>
                        array(  'set_type'=>'include_image',
                                'set_css'=>'image_preview','set_value'=>(!empty($aData["images_id"])?$aData["images_id"]:0),
                                //'set_for'=>"blogs_id_".(!empty($aData['blogs_id'])?$aData['blogs_id']:0)
                                )),
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"col-sm-12",'set_name'=>"{$this->inCategoryPrefix}_description",
                    'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_description"])?$aData["{$this->inCategoryPrefix}_description"]:''))),
                    array('caption'=>'Активировать марку','set_css'=>"text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'car_status',
                        'set_value'=>(!empty($aData['car_status'])?$aData['car_status']:'0'))),
                    //array('caption'=>'','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'process','set_value'=>'section-save')),
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                );    
            }
            return  $outResult;
        }
}