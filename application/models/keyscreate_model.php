<?php
    class Keyscreate_model extends MY_Model {
        function __construct(){
            parent::__construct();
        }
        //
        protected function StartUp() {
            //parent::StartUp();
            $this->inTblName            = 'tb_keys_create';
            $this->inTblCategoryName    = 'tb_car';
            $this->inTblUnion           = 'v_avto_to_keyscreate';
            $this->inOrderFields        = 'keys_create, avto_id';
            $this->inStatus             = 'keys_status'; 
            $this->inSelfId             = 'keys_create_id'; 
            $this->inSelfName           = 'keys_create_name';
            $this->inCategoryId         = 'car_id';
            $this->inCategoryName       = 'car_name';
            $this->inCategoryStatus     = 'car_status';
            $this->inSufix              = 'keys_create';
            $this->inPrefix             = 'keys_create';
            $this->inCategorySufix      = 'avto';
            $this->inCategoryPrefix     = 'avto';
        }
        //
        public function getTabs($aParam,$aPage=1) {
            $outResult = $this->inTabs;
            $outResult['property']= array('template'=>'administration/common/list-start-up-02.twig','title'=>'Подбор ключа','include_js'=>'info.avto.js');
            $outResult['data']['list'] = array('title' => 'Список ключей','target'=>'pnl-list','url'=> "/administration/admin_keycreate/load/{$aParam}/?page={$aPage}");
            $outResult['data']['add'] = array('title' => 'Добавить ключ','target'=>'pnl-item','url'=> "/administration/admin_keycreate/add/{$aParam}");
            $outResult['data']['default'] = $outResult['data']['list']['url'];
            return $outResult;
        }
        //
        
  
        
  
        
        public function formEmail($aData,$aHeader) {
            
           
      
    
    $query = $this->db->query("SELECT car_name FROM tb_car where car_id='{$aData['car_id']}'");

foreach ($query->result_array() as $car_name)
{
  
    $queryCarmodel = $this->db->query("SELECT model_name FROM tb_model WHERE model_id='{$aData['model_id']}'  ");
      foreach ($queryCarmodel->result_array() as $car_model)
          {  
            $outResult  = "<h1>$aHeader</h1>";
            $outResult .= "<table width='406' border='1' cellpadding='5' cellspacing='0'>";
            $outResult .= "<tr><th colspan='2'>{$aData['keys_create_name']}</th></tr>";
            $outResult .= "<tr><td>Марка автомобиля</td><td>{$car_name['car_name']}</td></tr>"; //loadCategoryById
            $outResult .= "<tr><td>Модель</td><td>{$car_model['model_name']}</td></tr>";
            $outResult .= "<tr><td>VIN</td><td>{$aData['keys_create_vin']}</td></tr>";
            $outResult .= "<tr><td>Год</td><td>{$aData['keys_create_year']}</td></tr>";
            $outResult .= "<tr><td>Телефон для связи</td><td>{$aData['keys_create_phone']}&nbsp;</td></tr>";
            $outResult .= "</table>";
            return $outResult;
            }
           
}
          }
        //
        public function getOutput($aData=array()) {
            $outResult = array (    
                //'property'=>array('title'=>'Автомобильные ключи','isRun'=>true,'include_js'=>"info.avto.js"),
                'titles'=>array(),
                'data'=>array()

                
            );
            return  $outResult;
        }
        //
        public function getForm($aData=array(),$aProcess=null) {
            $outResult = array (    
                'form_property'=>array('name'=>"frmStaticPageAdd",'method'=>'post','action'=>"/page_keycreate/save",'include_js'=>'info.avto.js'),
                'form_data'=>array(
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*', 'set_readonly' => '*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>$this->inSelfId,
                        'set_value'=> (!empty($aData[$this->inSelfId]))?$aData[$this->inSelfId]:'')),            
                    array('caption'=>'Выбор марки автомобиля','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*', 'set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>"car_id",
                        'set_extends'=>"control-to-control='#model_id'",
                        'set_value'=> (!empty($aData['car_id']))?$aData['car_id']:'')),            
                    array(
                    'caption'=>'Выбор модели',
                    'set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*',
                    'set_type'=>'select',
                    "set_css"=>"input-skin-01",
                    'set_name'=>"model_id",
                    'set_value'=> (!empty($aData['model_id']))?$aData['model_id']:'')),   
                    array('caption'=>'Введите VIN','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_vin",
                        'set_value'=> (!empty($aData["{$this->inPrefix}_vin"]))?$aData["{$this->inPrefix}_vin"]:'')),            
                    array('caption'=>'Введите Год','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_year",
                        'set_extends'=>"input-chk-digits='true'",
                        'set_value'=> (!empty($aData["{$this->inPrefix}_year"]))?$aData["{$this->inPrefix}_year"]:'')),                        
                    array('caption'=>'Представтесь','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->getSelfName()}",
                        'set_value'=>(!empty($aData[$this->getSelfName()])?$aData[$this->getSelfName()]:''))),
                    array('caption'=>'Телефон для связи','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inPrefix}_phone",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_phone"])?$aData["{$this->inPrefix}_phone"]:''))),   
                    array('caption'=>'Опишите Вашу проблему','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"col-sm-8",'set_name'=>"{$this->inPrefix}_description",
                        'set_extends'=>"no-resize-textarea='true'",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),               
                    array('caption'=>'Я не бот','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'captcha',"set_css"=>"input-skin-01",'set_name'=>"edtCaptcha",
                        'set_value'=>"")),                                    
                    array('caption'=>'Подобрать ключ','to_control'=>array('set_type'=>'submit','set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                )
            );
            return  $outResult;
        }
}