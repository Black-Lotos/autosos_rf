<?php
class Language_model extends MY_Model {
    function __construct(){
        parent::__construct();
    }
    protected function StartUp() {
        //parent::StartUp();
        $this->inTblName = 'tb_language';
        $this->inTblUnion = 'tb_language';
        $this->inCountRec = 0;
        $this->inOrderFields='language_id';
        $this->inSelfId = 'language_id';
        $this->inCategoryId = 'language_id';
        $this->inCategoryStatus = 'language_status';
        $this->inCategoryName = 'language_name';
        $this->inSufix = 'language';
    }
    //
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property']=array('template'=>'administration/common/list-start-up-02.twig','title'=>'Языки системы','include_js'=>'info.language.js');
        $outResult['data']['list'] = array('title' => 'Список языков','target'=>'pnl-list','url'=> "/administration/admin_language/load?page={$aPage}");
        $outResult['data']['add'] = array('title' => 'Добавить язык','target'=>'pnl-item','url'=> '/administration/admin_language/add');
        $outResult['data']['default'] = $outResult['data']['list']['url'];//file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    public function getForm($aData=array(), $aProcess=null){
        $outResult = array (    
            'form_property'=>array('name'=>"frmlan-add{$this->inSufix}",'method'=>'post','action'=>"/administration/admin_language/save",'include_js'=>'info.language.js'),
            'form_data'=>array(
                array('caption'=>'Идентификатор языка','set_css'=>'text-pos-left text-cl-black',
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'language_id','set_value'=>(!empty($aData['language_id'])?$aData['language_id']:''))),
                array('caption'=>'Наименование языка (рус.)','set_css'=>'text-pos-left text-cl-black',
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'lan_title','set_value'=>(!empty($aData['lan_title'])?$aData['lan_title']:''))),
                array('caption'=>'Код языка','set_css'=>'text-pos-left text-cl-black',
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>'lan_code','set_value'=>(!empty($aData['lan_code'])?$aData['lan_code']:''))),
                array('caption'=>'Активировать язык','set_css'=>'text-cl-black',
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox','set_name'=>'lan_status','set_value'=>(!empty($aData['lan_status'])?$aData['lan_status']:''))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit',
                    'set_name'=>'','set_css'=>'bottom-margin-1','set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        return  $outResult;
    }
}