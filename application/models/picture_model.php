<?php
class Picture_model extends MY_Model {
    protected function StartUp() {
        $this->inTblName            = 'tb_pictures';
        $this->inStatus             = 'picture_status'; 
        $this->inStatusMain         = 'picture_main'; 
        $this->inStatusTop          = 'picture_top';
        $this->inSelfId             = 'picture_id'; 
        $this->inSelfName           = 'picture_title'; 
        $this->inCategoryId         = 'cpicture_id';
        $this->inCategoryName       = 'cpicture_title';
        $this->inCategoryStatus     = 'cpicture_status';
        $this->inSufix              = 'picture';
        $this->inTblCategoryName    = 'tb_category_pictures';
        $this->inTblUnion           = 'v_category_to_picture';
        $this->inCountRec           = 0;
        $this->inOrderFields        = 'cpicture_id, picture_id';
    }
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property'] = array('template' => 'administration/common/list-start-up-02.twig','title'=>'Коллекции изображений','include_js'=>'info.picture.js');
        $outResult['data']['list'] = array('title' => 'Список коллекций','target'=>'pnl-list','url'=> "/administration/admin_picture/load?page={$aPage}");
        $outResult['data']['add'] = array('title' => 'Добавить изображение в коллекцию','target'=>'pnl-item','url'=> '/administration/admin_picture/add');
        $outResult['data']['add_section'] = array('title' => 'Добавить коллекцию','target'=>'pnl-collection','url'=> '/administration/admin_picture/add_section');
        $outResult['data']['default'] = $outResult['data']['list']['url']; //file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    public function resizePicture($aImgId,$aWidth=null, $aHeight=null, $aAspectRatio=true) {
        $inImgSource = $this->loadById($aImgId, array($this->inSelfId=>$aImgId),false);
        if (empty($inImgSource)) {
            return false;
        }
        $inExt = mb_substr(strtolower(strrchr($inImgSource['picture_file'], '.')),1,4);
        switch ($inExt){
        case "jpg":
            $srcImage = @ImageCreateFromJPEG($this->inPicPath.$inImgSource['picture_file']);
            break;
        case "jpeg":
            $srcImage = @ImageCreateFromJPEG($this->inPicPath.$inImgSource['picture_file']);
            break;
        case "gif":
            $srcImage = @ImageCreateFromGIF($this->inPicPath.$inImgSource['picture_file']);
            break;
        case "png":
            $srcImage = @ImageCreateFromPNG($this->inPicPath.$inImgSource['picture_file']);
            break;
        default:
            return -1;
        }
        //var_dump($srcImage); die();
            $srcWidth = ImageSX($srcImage);
            $srcHeight = ImageSY($srcImage);
            //echo "Исходная картинка('$srcWidth, $srcHeight): <b>$this->inPicPath'</b><br><img src='{$srcImage}'/>";
            //header("Content-Type: image/".($inExt=='jpg')?'jpeg':$inExt);
            //imagejpeg($srcImage);
            if($aAspectRatio){
                $ratioWidth = $srcWidth/$aWidth;
                $ratioHeight = $srcHeight/$aHeight;
                $destHeight = intval($srcHeight/$ratioWidth);$aHeight;
                $destWidth = $aWidth;
            } else {
                
                $ratioWidth = $srcWidth/$aWidth;
                $ratioHeight = $srcHeight/$aHeight;
                $destHeight = $aHeight;
                $destWidth = intval($srcWidth/$ratioHeight);
            }
            $resImage = ImageCreateTrueColor($destWidth, $destHeight);
            $transparent = imagecolorallocatealpha($resImage, 0, 0, 0, 127); 
            imagefill($resImage, 0, 0, $transparent); 
            imagesavealpha($resImage, true);
            ImageCopyResampled($resImage, $srcImage, 0, 0, 0, 0, $destWidth, $destHeight, $srcWidth, $srcHeight);
            header("Content-Type: image/png");
            ImagePNG($resImage);
            ImageDestroy($srcImage);
            ImageDestroy($resImage);
    }
    //
    //
    public function resizeWM($aImgId,$aWidth=null, $aHeight=null, $aAspectRatio=true) {
        $inImgSource = $aImgId;
        if (empty($inImgSource)) {
            return false;
        }
        $inExt = mb_substr(strtolower(strrchr($inImgSource, '.')),1,4);
        switch ($inExt){
        case "jpg":
            $srcImage = @ImageCreateFromJPEG($this->inWmPath.$inImgSource);
            break;
        case "jpeg":
            $srcImage = @ImageCreateFromJPEG($this->inWmPath.$inImgSource);
            break;
        case "gif":
            $srcImage = @ImageCreateFromGIF($this->inWmPath.$inImgSource);
            break;
        case "png":
            $srcImage = @ImageCreateFromPNG($this->inWmPath.$inImgSource);
            break;
        default:
            return -1;
        }
        //var_dump($srcImage); die();
            $srcWidth = ImageSX($srcImage);
            $srcHeight = ImageSY($srcImage);
            //echo "Исходная картинка('$srcWidth, $srcHeight): <b>$this->inPicPath'</b><br><img src='{$srcImage}'/>";
            //header("Content-Type: image/".($inExt=='jpg')?'jpeg':$inExt);
            //imagejpeg($srcImage);
            if($aAspectRatio){
                $ratioWidth = $srcWidth/$aWidth;
                $ratioHeight = $srcHeight/$aHeight;
                $destHeight = intval($srcHeight/$ratioWidth);$aHeight;
                $destWidth = $aWidth;
            } else {
                
                $ratioWidth = $srcWidth/$aWidth;
                $ratioHeight = $srcHeight/$aHeight;
                $destHeight = $aHeight;
                $destWidth = intval($srcWidth/$ratioHeight);
            }
            $resImage = ImageCreateTrueColor($destWidth, $destHeight);
            $transparent = imagecolorallocatealpha($resImage, 0, 0, 0, 127); 
            imagefill($resImage, 0, 0, $transparent); 
            imagesavealpha($resImage, true);
            ImageCopyResampled($resImage, $srcImage, 0, 0, 0, 0, $destWidth, $destHeight, $srcWidth, $srcHeight);
            header("Content-Type: image/png");
            ImagePNG($resImage);
            ImageDestroy($srcImage);
            ImageDestroy($resImage);
    }
    //
    public function writeWM($aFile,$aIWM='watermark.png') {
        $inSrcData=getimagesize($aFile);
        switch($inSrcData[2]) {
            case 1: $inExt = "gif"; break;
            case 2: $inExt = "jpeg"; break;
            case 3: $inExt = "png"; break;
        }
        $inSrcWm = getimagesize("images/wm/$aIWM");
        switch($inSrcWm[2]) {
            case 1: $inExtWm = "gif"; break;
            case 2: $inExtWm = "jpeg"; break;
            case 3: $inExtWm = "png"; break;
        }
        $inFunCreateImg = "ImageCreateFrom$inExt";
        $inFunCreateWm = "ImageCreateFrom$inExtWm";
        $srcImage   = $inFunCreateImg($aFile);
        $wmImage    = $inFunCreateWm("images/wm/$aIWM");
        if($wmImage && $srcImage) {
            //var_dump($wmImage); die();
            $resultImage    = imagecreatetruecolor($inSrcData[0], $inSrcData[1]);
            $transparent = imagecolorallocatealpha($resultImage, 0, 0, 0, 127); 
            imagefill($resultImage, 0, 0, $transparent); 
            imagesavealpha($resultImage, true);
            // картинка
            imagecopyresampled($resultImage, $srcImage, 0, 0, 0, 0, $inSrcData[0], $inSrcData[1], $inSrcData[0], $inSrcData[1]);
            // расчет размеров
            $srcWidth = $inSrcData[0]; $srcHeight = $inSrcData[1];
            $srcWmWidth = $inSrcWm[0]; $srcWmHeight = $inSrcWm[1];
            $ratioWidth = $srcWidth/$srcWmWidth;
            $destHeight = intval($srcWmHeight*$ratioWidth);
            $destWidth = $srcWidth;
            $startY = ceil(($srcHeight/2)-($destHeight/2));
            // маска
            imagecopyresampled($resultImage, $wmImage, 0, $startY, 0, 0, $destWidth, $destHeight, $inSrcWm[0], $inSrcWm[1]);
            // окончательная обработка
            $inRun = "image$inExt"; 
            //header("Content-Type: image/$inExt");
            $inRun($resultImage,$aFile);
            imagedestroy($resultImage);
            imagedestroy($wmImage);
            imagedestroy($srcImage);
        }
    }
    //
    public function getForm($aData=array(), $aProcess=null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frm{$this->inSufix}-add",'method'=>'post','action'=>"/administration/admin_picture/save",'include_js'=>"info.picture.js"),
            'form_data'=>array(
                array('caption'=>'Идентификатор картинки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_id",
                    'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),
                array('caption'=>'Коллекция','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'select',"set_css"=>"input-skin-01",'set_name'=>'cpicture_id',
                    'set_value'=>!empty($aData['cpicture_id'])?$aData['cpicture_id']:'')),
                array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                        
                array('caption'=>'Заголовок картинки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_title",
                    'set_value'=>(!empty($aData["{$this->inSufix}_title"])?$aData["{$this->inSufix}_title"]:''))),
                 array('caption'=>'Текст картинки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'textarea',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_text",
                    'set_value'=>(!empty($aData["{$this->inSufix}_text"])?$aData["{$this->inSufix}_text"]:''))),
                /*array('caption'=>'<a class="button-base red" href="#" id="btnClearImagePreview" url="#'.'preview-'.
                        (!empty($aData["images_id"])?$aData["images_id"]:0).
                    '" for="preview-'.(!empty($aData["news_id"])?$aData["news_id"]:0).'">Удалить картинку</a>',
                    'to_control'=>array('set_type'=>'include_image','set_name'=>'preview-'.(!empty($aData["images_id"])?$aData["images_id"]:0),'set_css'=>'image_preview',
                        'set_callback'=>array('function'=>'getImageNews','param'=>(!empty($aData["images_id"])?$aData["images_id"]:'')))),*/
                array('caption'=>'Файл картинки','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'file',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_file",
                    'set_value'=>(!empty($aData["{$this->inSufix}_file"])?$aData["{$this->inSufix}_file"]:''))),            
                array('caption'=>'Код втраивания','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_readonly'=>'*','set_require'=>'','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_code",
                    'set_value'=>(!empty($aData["{$this->inSufix}_code"])?$aData["{$this->inSufix}_code"]:''))),                                    
                array('caption'=>'Порядок вывода','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_readonly'=>'*','set_require'=>'','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_weight",
                    'set_extends'=>"input-chk-digits='true'",        
                    'set_value'=>(!empty($aData["{$this->inSufix}_weight"])?$aData["{$this->inSufix}_weight"]:'0'))),                                                
                array('caption'=>'Активировать картинку','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'checkbox',"set_css"=>"input-skin-01",'set_name'=>'picture_status',
                    'set_value'=>(!empty($aData["{$this->inSufix}_status"])?$aData["{$this->inSufix}_status"]:''))),
                array('caption'=>'Дата создания','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'picture_creates','set_value'=>(!empty($aData['picture_creates'])?$aData['picture_creates']:date('Y-m-d H:i:s',time())))),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
        if ($aProcess==PROCESS_CONTENT_SECTION) {
            $outResult['form_property']['action']="/administration/admin_picture/save_category";
            $outResult['form_data'] = array(
                array(  'caption'=>'Идентификатор видео','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>"c{$this->inSufix}_id",
                                            'set_value'=>(!empty($aData["c{$this->inSufix}_id"])?$aData["c{$this->inSufix}_id"]:''))),
                array(  'caption'=>'Название коллеции','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"c{$this->inSufix}_title",
                        'set_value'=>(!empty($aData["c{$this->inSufix}_title"])?$aData["c{$this->inSufix}_title"]:''))),
                array('caption'=>'Язык','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>'language_id',
                    'set_value'=>(!empty($aData["language_id"])?$aData["language_id"]:''))),                            
                array(  'caption'=>'Алиас коллекции','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"c{$this->inSufix}_alias",
                        'set_value'=>(!empty($aData["c{$this->inSufix}_alias"])?$aData["c{$this->inSufix}_alias"]:''))),                
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),                        
            );
        }
        return  $outResult;
    }
    //
    //
    public function getOutput($aProcess=null) {
        $outResult = array (    
            'property'=>array('title'=>'Контентное видео','isRun'=>true,'include_js'=>"info-{$this->inSufix}.js",'template'=>'pieses/pieses-picture.twig'),
            'titles'=>array(),
            'data'=>array()
        );
    }
}