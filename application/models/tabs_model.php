<?php
    class Tabs_model extends CI_Model {
        private $inTemplate = 
            array(
                    "property"  =>  array(
                        //'name'=>''
                    ),
                    "data"      =>  array(
                        //array('title'=>'','url'=>''),
                    )
            );
        /*private $inTblCategoryName = 'tb_menu_category';
        private $inTblUnion = 'v_menu_category_to_menu';*/
        function __construct(){
            parent::__construct();
        }
        //
        public function loadFooterLink($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/footerlink/start-footer_link.twig';
            $outResult['data'][] = array('title' => 'Список ссылок','url'=> "/administration/admin_footerlink/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить ссылоку','url'=> '/administration/admin_footerlink/add');
            $outResult['data'][] = array('title' => 'Добавить раздел ссылок','url'=> '/administration/admin_footerlink/add_section');
            return $outResult;
        }
        //
        public function loadLanguages($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/language/start-language.twig';
            $outResult['data'][] = array('title' => 'Список языков','url'=> "/administration/admin_language/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить язык','url'=> '/administration/admin_language/add');
            return $outResult;
        }
        //
        public function loadNews($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/news/start-news.twig';
            $outResult['data'][] = array('title' => 'Список новостей','url'=> "/administration/admin_news/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить новость','url'=> '/administration/admin_news/add');
            $outResult['data'][] = array('title' => 'Добавить раздел новостей','url'=> '/administration/admin_news/add_section');
            return $outResult;
        }
        //
        public function loadBlogs($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/blogs/start-blogs.twig';
            $outResult['data'][] = array('title' => 'Список блогов','url'=> "/administration/admin_blogs/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить блог','url'=> '/administration/admin_blogs/add');
            $outResult['data'][] = array('title' => 'Добавить раздел блогов','url'=> '/administration/admin_blogs/add_section');
            return $outResult;
        }
        //
        public function loadContentVideo($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/admin_video/start-content-video.twig';
            $outResult['data'][] = array('title' => 'Список контент видео','url'=> "/administration/admin_content_video/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить контент видео','url'=> '/administration/admin_content_video/add');
            $outResult['data'][] = array('title' => 'Добавить раздел контент видео','url'=> '/administration/admin_content_video/add_section');
            return $outResult;
        }
        //
        public function loadContentPicture($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/admin-picture/start-content-picture.twig';
            $outResult['data'][] = array('title' => 'Список коллекций','url'=> "/administration/admin_picture/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить изображение в коллекцию','url'=> '/administration/admin_picture/add');
            $outResult['data'][] = array('title' => 'Добавить коллекцию','url'=> '/administration/admin_picture/add_section');
            return $outResult;
        }
        //
        public function loadProposal($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/proposal/start-proposal.twig';
            $outResult['data'][] = array('title' => 'Список заявок','url'=> "/administration/admin_proposal/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить заяыку','url'=> '/administration/admin_proposal/add');
            //$outResult['data'][] = array('title' => 'Добавить раздел ссылок','url'=> '/administration/footerlink/add_section');
            return $outResult;
        }
        //
        public function loadQuestion($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/question/start-question.twig';
            $outResult['data'][] = array('title' => 'Список вопросв','url'=> "/administration/admin_question/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить вопрос','url'=> '/administration/admin_question/add');
            //$outResult['data'][] = array('title' => 'Добавить раздел ссылок','url'=> '/administration/footerlink/add_section');
            return $outResult;
        }
        //
        public function loadAsktocontact($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/ask-to-contact/start-up.twig';
            $outResult['data'][] = array('title' => 'Список запросов связатся','url'=> "/administration/admin_asktocontact/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить','url'=> '/administration/admin_asktocontact/add');
            //$outResult['data'][] = array('title' => 'Добавить раздел ссылок','url'=> '/administration/footerlink/add_section');
            return $outResult;
        }
        //
        public function loadVideo($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/admin_video/start-video.twig';
            $outResult['data'][] = array('title' => 'Список контент видео','url'=> "/administration/admin_video/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить контент видео','url'=> '/administration/admin_video/add');
            return $outResult;
        }
        //
        
        public function loadSlider($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/admin_video/start-slider.twig';
            $outResult['data'][] = array('title' => 'Список картинок для слайдера','url'=> "/administration/admin_slider/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить картинку в сайдер','url'=> '/administration/admin_slider/add');
            return $outResult;
        }
        //
        public function loadUsers($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/users/start-users.twig';
            $outResult['data'][] = array('title' => 'Список пользователей','url'=> "/administration/users/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить пользователя','url'=> '/administration/users/add');
            return $outResult;
        }
        //
        public function loadSettings($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/settings/tabs-settings.twig';
            $outResult['data'][] = array('title' => 'Настройки','url'=> "/administration/settings/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Метаданные','url'=> '/administration/settings/load_metadata');
            $outResult['data'][] = array('title' => 'Переменные','url'=> '/administration/settings/load_variable');
            return $outResult;
        }
        //
        
        public function loadUsersRoles($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/users/start-user-roles.twig';
            $outResult['data'][] = array('title' => 'Список ролей','url'=> "/administration/users/load_roles?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить роль','url'=> '/administration/users/add_roles');
            return $outResult;
        }
        //
        public function loadMenuItem($aPage=1,$aMenu) {
            $outResult = $this->inTemplate;
            //var_dump(); die();
            $outResult['property']['template']  = 'administration/menu/start-menu.twig';
            $outResult['data'][] =  array('title'=>'Список пунктов','url'=>"/administration/admin_menu/load_menu_item?page={$aPage}".(($aMenu)?"&select_menu=".$aMenu:""));
            $outResult['data'][] =  array('title'=>'Добавить пункт','url'=>'/administration/admin_menu/add_menu_item'.(($aMenu)?"?select_menu=".$aMenu:""));            
            return $outResult;
        }
        
        //
        public function loadMenu($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/menu/start-menu.twig';
            $outResult['data'][] = array('title' => 'Список меню','url'=> "/administration/admin_menu/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить меню','url'=> '/administration/admin_menu/add');
            return $outResult;
        }
        //
        public function loadStaticPage($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/static_page/start-static-page.twig';
            $outResult['data'][] = array('title' => 'Список статических страниц','url'=> "/administration/admin_static/load?page={$aPage}");
            $outResult['data'][] = array('title' => 'Добавить статическую страницу','url'=> '/administration/admin_static/add');
            $outResult['data'][] = array('title' => 'Добавить раздел статических страниц','url'=> '/administration/admin_static/add_section');
            return $outResult;
        }
        //
        public function loadSearchPage($aPage=1,$aSearch='') {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/static_page/start-static-page.twig';
            $outResult['data'][] = array('title' => getCaptionInput("msg_search_news"),'url'=> "/news/goSearch?inSearch={$aSearch}&page={$aPage}");
            $outResult['data'][] = array('title' => getCaptionInput("msg_search_blogs"),'url'=> "/blogs/goSearch?inSearch={$aSearch}&page={$aPage}");
            $outResult['data'][] = array('title' => getCaptionInput("msg_search_video"),'url'=> "/cvideo_controller/goSearch?inSearch={$aSearch}&page={$aPage}");
            return $outResult;
        }
        //
        public function loadExpertHelp($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/expert-help/start-expert-help.twig';
            $outResult['data'][] = array('title' => "Список категорий помощников",'url'=> "/administration/admin_expert_help/load_section?page={$aPage}");
            $outResult['data'][] = array('title' => "Добавить помощника",'url'=> "/administration/admin_expert_help/add");
            $outResult['data'][] = array('title' => "Добавить категорию помощников",'url'=> "/administration/admin_expert_help/add_section");
            return $outResult;
        }
        public function loadSettingsSite($aPage=1) {
            $outResult = $this->inTemplate;
            $outResult['property']['template']  = 'administration/site-settings/start-site-settings.twig';
            $outResult['data'][] = array('title' => "Список переменных",'url'=> "/administration/admin_settings/load?page={$aPage}");
            $outResult['data'][] = array('title' => "Добавить переменную",'url'=> "/administration/admin_settings/add");
            $outResult['data'][] = array('title' => "Добавить мультиязычную переменную",'url'=> "/administration/admin_settings/add_multi_variable");
            return $outResult;
        }
    }