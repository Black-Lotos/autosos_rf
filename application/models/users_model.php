<?php

class Users_model extends MY_Model {
    static $table_logs = "tb_user_logs";
    static $table_role = 'tb_user_role';
    static $table_user_to_role='tb_user_roles_id';
    static $view_user_to_role = 'v_user_to_role';
    private $inRoleId = 'role_id';
    private $inRoleTitle = 'role_title';
    function StartUp() {
        $this->inTblName            = 'tb_user';
        $this->inTblCategoryName    = '';
        $this->inTblUnion           = 'tb_user';
        $this->inOrderFields        = 'user_reg_date';
        $this->inStatus             = 'user_active'; 
        $this->inStatusMain         = ''; 
        $this->inStatusTop          = '';
        $this->inAlias              = '';
        $this->inSelfId             = 'user_id'; 
        $this->inSelfName           = 'user_login';
        $this->inCategoryId         = '';
        $this->inCategoryName       = '';
        $this->inCategoryStatus     = '';
        $this->inSufix              = 'user';
        $this->inPrefix             = 'user';
        $this->inCategorySufix      = '';
        $this->inCategoryPrefix     = '';
        
    }
    function __construct() {
        parent::__construct();
    }
    //
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property'] = array('template'=>'administration/common/list-start-up-02.twig','title'=>'Пользователи','include_js'=>"info.user.js");
        $outResult['data']['list'] = array('title' => 'Список пользователей','target'=>'pnl-list','url'=> "/administration/users/load?page={$aPage}");
        $outResult['data']['add'] = array('title' => 'Добавить пользователя','target'=>'pnl-item','url'=> '/administration/users/add');
        $outResult['data']['default'] = $outResult['data']['list']['url'];//file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    function login($aName=null, $aPass=null) {
        $resOut = false;
        if (empty($aName)||empty($aPass)) return $resOut;
        $inPass = md5($aPass);
        $inData = $this->load(array($this->inSelfName=>$aName,'user_pass'=>$inPass,$this->inStatus=>1),false);
        if (!empty($inData)) {
            $inData = $inData[0];
            $toSes = (object)$inData;
            $this->session->set_userdata(array('user' => (object)$toSes));
            //echo "<pre>"; var_dump($toSes);die("Login");
            $inData['user_last_login'] = date('Y-m-d H:i:s',time());
            $this->save($inData);
            $resOut = true;
        }
        return $resOut;
    }
    //
    function isLogin() {
        $user = $this->session->userdata('user');
        $user = (object)$user;
        if (isset($user) && !empty($user->user_id)) {
            return true;
        }
        return false;
    }
    //
    function isCheck() {
        return array('error'=>false,'messages'=>array());
    }
    //
    function logout ($user_id) {
        
        $outLog = array('logs_date' => date('Y-m-d H:i:s',time()), 'logs_type' => '2', 'user_id' => $user_id);
        $this->db->insert(self::$table_logs, $outLog);
        //var_dump($this->db->insert(self::$table_logs, $outLog));
        $this->session->sess_destroy();
        return ;
    }
    //
    public function getRoleId($aRole) {
        //получение ид роли по имени  
        $inSql = "select {$this->inRoleId} from ".self::$table_role." where {$this->inRoleTitle}=?";
        $outRes = $this->db->query($inSql,array($aRole))->row();
        //var_dump(); die();
        if ($outRes) {
            return (int)$outRes->{$this->inRoleId};
        }
        return null;
    }
    public function getHasRole($aUser, $aRole) {
        // проверка имеет ли пользователь роль    
        $inRole_Id = $this->getRoleId($aRole);
        $inSql = "select {$this->inSelfId} from ".self::$table_user_to_role." where {$this->inSelfId}=? and {$this->inRoleId}=?";
        $outRes = $this->db->query($inSql,array($aUser->user_id,$inRole_Id))->row();
        if ($outRes) {
            return true;
        }
        return false;
    }
    public function getCheckRoles($aUser,$aRoles) {
        $outRes = true;
        foreach ($aRoles as $inRole) {
            if (!$this->getHasRole($aUser,$inRole)) {
                setMessage("Доступ запрещен", 'error');
                $outRes = false;
            }
        }
        return $outRes;
    }
    //
    public function getOutput($aProcess=PROCESS_USER,$aData=array()) {
        $outResult = array (    
            'property'=>array('include_js'=>"info.user.js",'template'=>'blogs/blogs-start-up.twig'),
        );
        switch ($aProcess) {
            case PROCESS_USER:
                $outResult['titles'] = array(
                    array('title'=>'ID','size'=>20),
                    array('title'=>'Логин пользователя','size'=>0),
                    array('title'=>'e-mail пользователя','size'=>0),
                    array('title'=>"Статус",'size'=>80),
                    array('title'=>'Действия','size'=>80)
                );
                $outResult['data']=array('user_id',"user_login","user_email","user_active",'action');
                break;
            case PROCESS_ROLES:
                $outResult['titles'] = array(
                    array('title'=>'ID','size'=>20),
                    array('title'=>'Роль пользователя','size'=>0),
                    array('title'=>'Действия','size'=>80)
                );
                $outResult['data']=array('role_id','role_title','&nbsp');
                break;
        }
        return  $outResult;
        }
    //
    public function getForm($aData=array(), $aProcess=null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frmUserAdd".(($aProcess==PROCESS_USER)?'Ajax':''),'method'=>'post','action'=>"/administration/users/save",'include_js'=>"info.user.js"),
            'form_data'=>array(
                array('caption'=>'Ідентификатор пользователя','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_id",
                    'set_value'=>(!empty($aData["{$this->inSufix}_id"])?$aData["{$this->inSufix}_id"]:''))),                        
                array('caption'=>'Введите Ф.И.О.','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_name",
                    'set_value'=>(!empty($aData["{$this->inSufix}_name"])?$aData["{$this->inSufix}_name"]:''))),                        
                array('caption'=>'Введите логин','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_login",
                    'set_value'=>(!empty($aData["{$this->inSufix}_login"])?$aData["{$this->inSufix}_login"]:''))),
                array('caption'=>'Введите пароль','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'password',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_pass",
                    'set_value'=>(!empty($aData["{$this->inSufix}_pass"])?$aData["{$this->inSufix}_pass"]:''))),            
                array('caption'=>'Повторите ввод пароля','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'password',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_pass_check",
                    'set_value'=>(!empty($aData["{$this->inSufix}_pass_check"])?$aData["{$this->inSufix}_pass_check"]:''))),                        
                array('caption'=>'Введите e-mail','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_email",
                    'set_value'=>(!empty($aData["{$this->inSufix}_email"])?$aData["{$this->inSufix}_email"]:''))),            
                array('caption'=>'Введите телефон','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_phone",
                    'set_value'=>(!empty($aData["{$this->inSufix}_phone"])?$aData["{$this->inSufix}_phone"]:''))),
                array('caption'=>'Активировать пользователя','set_css'=>"text-cl-black",
                    'to_control'=>array('set_require'=>'*','set_type'=>'hidden',"set_css"=>"input-skin-01",'set_name'=>"{$this->inSufix}_active",
                    'set_value'=>(!empty($aData["{$this->inSufix}_active"])?$aData["{$this->inSufix}_active"]:'0'))),
                array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                    'to_control'=>array('set_require'=>'','set_type'=>'separator')),
                array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"btnSaveUser".(($aProcess==PROCESS_USER)?'Ajax':''),
                    'set_css'=>(($aProcess==PROCESS_USER)?'bottom-margin-1':'bottom-margin-1'),'set_url'=>'#')),        
                //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
            )
        );
       switch ($aProcess) {
            case PROCESS_LOGIN:
                $outResult = array (    
                    'form_property'=>array('name'=>"frmUserLogin",'method'=>'post','action'=>"/administration/users/login",'include_js'=>""),
                    'form_data'=>array(
                        array('caption'=>'Войти','to_control'=>array('set_type'=>'h2','set_css'=>'form-signin-heading')),
                        array('caption'=>'Ваш логин','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>array('set_require'=>'*','set_type'=>'text',"set_css"=>"input-skin-01",'set_name'=>"user_login",
                            'set_value'=>(!empty($aData["user_login"])?$aData["user_login"]:''))),
                        array('caption'=>'Ваш пароль','set_css'=>"text-pos-left text-cl-black",
                            'to_control'=>array('set_require'=>'*','set_type'=>'password',"set_css"=>"input-skin-01",'set_name'=>"user_pass",
                            'set_value'=>(!empty($aData["user_pass"])?$aData["user_pass"]:''))),
                        //array('caption'=>'Дата создания','to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_name'=>'news_creates','set_value'=>(!empty($aData['news_creates'])?$aData['news_creates']:date('Y-m-d H:i:s',time())))),            
                        array('caption'=>'Войти в систему','to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1')),        
                        //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                    )
                );
            break;
        } 
        return  $outResult;
    }    
    //
    function get_permition($aUser=null) {
        $resOut = false;
        if (empty($aUser->user_id)||empty($aUser)) return $resOut;
        $this->db->select('role_id, role_title');
        $this->db->from('`'.self::$view_user_to_role.'`');
        $this->db->where('user_id', $aUser->user_id);
        $inData = $this->db->get()->result_array();
        if (empty($inData)) {
            $resOut = false; return $resOut;
        }
        $resOut = true; $outPermition=1;
        foreach ($inData as $outKey=>$outData) {
            $resOut = $resOut && ($outData['role_id']==4 || $outData['role_id']==5 || $outData['role_id']==3);
        }
        return $resOut;
    }
    //
    function save_profile_user($data, $id) {
        if (!empty($id)) {
            $this->db->where('id', $id);
            $this->db->update(self::$table, $data);
        }
        return $id;
    }
    function recovery($id = NULL) {
        if (empty($id) || $id == 1)
            return false;
        $this->db->query("UPDATE " . self::$table . " SET deleted = NULL WHERE id = ".$id."");
        return true;
    }
    
    function valid_email($aEmail = NULL) {
        if (empty($aEmail)) return false;
        $inPattern = "/^[^@]+@([a-zA-Z0-9\-]+\.)+([a-zA-Z0-9\-]{2}|net|com|gov|mil|org|edu|int)$/is";
        if (preg_match($inPattern, $aEmail)) {return true;}
        return false;
    }
    function valid_login($aLogin = NULL) {
        if (empty($aLogin)) return false;
        $inPattern = "/^[a-zA-Z-_\d]{1,}$/is";
        if (preg_match($inPattern, $aLogin)) {return true;}
        return false;
    }
    function valid_name($aName = NULL) {
        if (empty($aName)) return false;
        return true;
    }
    function valid_phone($aPhone = NULL) {
        if (empty($aPhone)) return false;
        $inPattern = "/^([0-9\+]{0,3})\s?\(([0-9]{1,6})\)\s?([0-9\-]{1,9})$/";
        if (preg_match($inPattern, $aPhone)) {return true;}
        return true;
    }
    function valid_login_is_exists($aLogin) {
        if (empty($aLogin)) return true;
        $outRes = $this->load(array($this->inSelfName=>$aLogin),false);
        //var_dump($outRes);
        if (!empty($outRes)) 
            return true;
        return false;
    }
}



