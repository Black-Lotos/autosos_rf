<?php
class Variables_model extends MY_Model {
    function __construct(){
        parent::__construct();
    }
    function StartUp() {
        $this->inTblName            = 'tb_variables';
        $this->inTblCategoryName    = 'tb_variable_category';
        $this->inTblUnion           = 'v_category_to_variable';
        $this->inCountRec           = 0;
        $this->inOrderFields        = 'variable_name';
        $this->inDebug              = false;
        $this->inStatus             = 'variable_active'; 
        $this->inStatusMain         = ''; 
        $this->inStatusTop          = '';
        $this->inAlias              = 'variable_name';
        $this->inSelfId             = 'variable_id'; 
        $this->inSelfName           = 'variable_name';
        $this->inCategoryId         = 'svariable_id';
        $this->inCategoryName       = 'svariable_title';
        $this->inCategoryStatus     = 'svariable_active';
        $this->inSufix              = '';
        $this->inPrefix             = 'variable';
        $this->inRecursive          = true;
        $this->setObjectResult      = false;
        $this->inCategorySufix      = '';
        $this->inCategoryPrefix     = 'svariable';
    }
    //
    public function getTabs($aPage=1) {
        $outResult = $this->inTabs;
        $outResult['property']= array('template'=>'administration/common/list-start-up-02.twig','title'=>'Переменные сайта','include_js'=>'info.variable.js');
        $outResult['data']['list'] = array('title' => "Группы переменных",'url'=> "/administration/admin_variable/load?page={$aPage}");
        $outResult['data']['add_section'] = array('title' => "Добавить группу",'url'=> "/administration/admin_variable/add_section");
        $outResult['data']['add'] = array('title' => "Добавить переменную",'url'=> "/administration/admin_variable/add");
        $outResult['data']['add_multi_variable'] = array('title' => "Добавить мультиязычную переменную",'url'=> "/administration/admin_variable/add_multi_variable");
        $outResult['data']['default'] = $outResult['data']['list']['url']; //file_get_contents(base_url().$outResult['data']['list']['url']);
        return $outResult;
    }
    //
    public function getOutput($aProcess=null){
        $outResult = array(
            
        );
        return $outResult;
    }
    public function getForm($aData = array(), $aProcess = null) {
        $outResult = array (    
            'form_property'=>array('name'=>"frmVariableAdd",'method'=>'post','action'=>"/administration/admin_variable/save_variable",'include_js'=>"info.variable.js"));
        switch ($aProcess) {
            case PROCESS_SECTION_ADD:
                $outResult['form_property']=array('name'=>"frmVariableAdd",'method'=>'post','action'=>"/administration/admin_variable/save_section",'include_js'=>'info.variable.js');
                $outResult['form_data']=array(
                    array('caption'=>'Идентификатор группы','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'hidden','set_name'=>"{$this->inCategoryId}",
                        'set_value'=>!empty($aData[$this->inCategoryId])?$aData[$this->inCategoryId]:'')),
                    array('caption'=>'Наименоввание группы','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_name'=>"{$this->inCategoryPrefix}_title",
                        'set_value'=>(!empty($aData["{$this->inCategoryPrefix}_title"])?$aData["{$this->inCategoryPrefix}_title"]:''))),          
                    array('caption'=>'Сохранить',
                        'to_control'=>array('set_type'=>'submit','set_name'=>"",'set_css'=>'bottom-margin-1','set_url'=>'#')),                    
                );
                break;
            case VARIABLE_STRING:
                $outResult['form_data']=array(
                    array('caption'=>'Сайт','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_css'=>'input-skin-01','set_name'=>"{$this->inCategoryId}",
                        'set_value'=>!empty($aData[$this->inCategoryId])?$aData[$this->inCategoryId]:'')),
                    array('caption'=>'Ідентификатор переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_id",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_id"])?$aData["{$this->inPrefix}_id"]:''))),                        
                    array('caption'=>'Наименоввание переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_name",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_name"])?$aData["{$this->inPrefix}_name"]:''))),                        
                    array('caption'=>'Значение переменной (Рус)','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_value",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value"])?$aData["{$this->inPrefix}_value"]:''))),
                    array('caption'=>'Значення змінної (Укр)','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_value_ua",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value_ua"])?$aData["{$this->inPrefix}_value_ua"]:''))),
                    array('caption'=>'Variable (Eng)','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_value_en",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value_en"])?$aData["{$this->inPrefix}_value_en"]:''))),            
                    array('caption'=>'Описание переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_description",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),            
                    array('caption'=>'Превикс группы переменных','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_prefix",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_prefix"])?$aData["{$this->inPrefix}_prefix"]:''))),            
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Функция обратного вызова для переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_callback",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_callback"])?$aData["{$this->inPrefix}_callback"]:''))),            
                    array('caption'=>'Тип переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_type",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_type"])?$aData["{$this->inPrefix}_type"]:''))),                        
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",
                        'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                );
                break;
            case VARIABLE_DIGIT:
                $outResult['form_data']=array(
                    array('caption'=>'Сайт','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_type'=>'select','set_name'=>"{$this->inCategoryId}",
                        'set_value'=>!empty($aData[$this->inCategoryId])?$aData[$this->inCategoryId]:'')),
                    array('caption'=>'Ідентификатор переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'hidden','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_id",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_id"])?$aData["{$this->inPrefix}_id"]:''))),                        
                    array('caption'=>'Наименоввание переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_name",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_name"])?$aData["{$this->inPrefix}_name"]:''))),                        
                    array('caption'=>'Значение переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'*','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_value",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_value"])?$aData["{$this->inPrefix}_value"]:''))),
                    array('caption'=>'Описание переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_description",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_description"])?$aData["{$this->inPrefix}_description"]:''))),                        
                    array('caption'=>'Превикс группы переменных','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_prefix",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_prefix"])?$aData["{$this->inPrefix}_prefix"]:''))),            
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Функция обратного вызова для переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'text','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_callback",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_callback"])?$aData["{$this->inPrefix}_callback"]:''))),            
                    array('caption'=>'Тип переменной','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'hidden','set_css'=>'input-skin-01','set_name'=>"{$this->inPrefix}_type",
                        'set_value'=>(!empty($aData["{$this->inPrefix}_type"])?$aData["{$this->inPrefix}_type"]:''))),                                    
                    array('caption'=>'','set_css'=>"text-pos-left text-cl-black",
                        'to_control'=>array('set_require'=>'','set_type'=>'separator')),            
                    array('caption'=>'Сохранить','to_control'=>array('set_type'=>'submit','set_name'=>"",
                        'set_css'=>'bottom-margin-1','set_url'=>'#')),        
                    //array('caption'=>'Идентификатор меню','to_control'=>array('set_require'=>'','set_type'=>'hidden','set_name'=>'form-name','set_value'=>$aMenu)),
                );
                break;
        }
        return $outResult;
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

