$(document).ready(function(){ 
    console.log("info-footerlink"); // string 1 rad baz tubular
    $("[id^='ref-footerlink-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-footerlink', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-footerlink-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-footerlink-delete-']").on({
        click:function() {
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/delete',
                //dataType: 'JSON',
                data:{process: 'delete-footerlink', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    //$(".divOutputBox").html(Responce);
                    location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-сfooterlink-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/edit_section',
                //dataType: 'JSON',
                data:{Process: 'edit-footerlink', cfooterlink_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-cfooterlink-delete-']").on({
        click:function() {
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/delete_section',
                //dataType: 'JSON',
                data:{process: 'delete-footerlink', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    //$(".divOutputBox").html(Responce);
                    location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-сfooterlink-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', cfooterlink_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    //
    $("[id^='ref-footerlink-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_footerlink/change_status',
                //dataType: 'JSON',
                data:{process: 'change-main', footerlink_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                }
            });     
        }
    });
    // footerlink_text
    if ($('#footerlink_text').length>0)             
        {$('#footerlink_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
})


