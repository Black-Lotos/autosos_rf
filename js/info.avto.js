$(document).ready(function(){ 
    if ($('#car_description').length>0)             
        {$('#car_description').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#avto_description').length>0)             
        {$('#avto_description').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#model_description').length>0)             
        {$('#model_description').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#keys_description').length>0)             
        {$('#keys_description').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    // проверяем где находимся и если есть выьор модели перечитываем его
    if ($("#model_id").length>0) {
        //alert("MODEL_OK");
        $("#car_id").on({
            change:function() {
                //alert("GUD");
                $.ajax({
                    type: 'post',
                    url: '/administration/admin_avto/model_change_item',
                    dataType: 'JSON',
                    data:{process: 'car-change', item:$(this).val()},
                    success: function(Responce) {
                        $("#model_id").empty();
                        $.each(Responce,function(inKey, inData){
                            console.log(inKey);
                            $('#model_id').append('<option value="' + inData.value + '">' + inData.title + '</option>');
                        });
                    }
                });
            }
        })
    }
    // проверяем где находимся и если есть выьор машины перечитываем его
    if ($("#avto_id").length>0) {
        $("#model_id").on({
            change:function() {
                //alert($(this).val());
                $.ajax({
                    type: 'post',
                    url: '/administration/admin_avto/avto_change_item',
                    dataType: 'JSON',
                    data:{process: 'model-change', item:$(this).val()},
                    success: function(Responce) {
                        $("#avto_id").empty();
                        $.each(Responce,function(inKey, inData){
                            console.log(inKey+" "+inData);
                            $('#avto_id').append('<option value="' + inData.value + '">' + inData.title + '</option>');
                        });
                    }
                });
            }
        })
    };
    
    $("#btnClearImageAvto").on({
        click:function() {
            $("#images_id").val('0');
            $("[id^='preview-image-']").attr('src',"/images/empty210x210.png");
            return false;
        }
    });
    
    $("#btnClearImageCar").on({
        click:function() {
            $("#images_id").val('0');
            $("[id^='preview-image-']").attr('src',"/images/empty210x210.png");
            return false;
        }
    });
    /**/
})


