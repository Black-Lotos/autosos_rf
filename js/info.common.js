$(document).ready(function(){ 
    /**/
    $("#list").on({
        click:function() {
            $("#form-ajax").load($(this).attr('url'));
            return false;
        } 
    });
    $("[id^='add']").on({
       click:function() {
            $("#form-ajax").load($(this).attr('url'));
            return false;
       } 
    });
    $("[id^='add_section']").on({
       click:function() {
            $("#form-ajax").load($(this).attr('url'));
            return false;
       } 
    });
    //alert('Common');
    /* сообщения системы sub_page_message*/
    if($('#output-system-success').length>0) {
        setTimeout(function(){$('#output-system-success').fadeOut()}, 5000);
    };
    $('[data-toggle="tabajax"]').click(function(e) {
        var $this = $(this),
            loadurl = $this.attr('href'),
            targ = $this.attr('data-target');

        $.get(loadurl, function(data) {$(targ).html(data);});

        $this.tab('show');
        return false;
    });
    /**/
    //
    /* НОВОСТИ */
    // редактировать
    $("[id^='ref-news-edit-']").each(function(){
        $(this).on({
            click:function() {
                $.ajax({
                type: 'POST',
                url: '/administration/admin_news/edit',
                //dataType: 'JSON',
                data:{process: 'edit-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
                });  
                return false;
            }
        })
    });
    // удалить
    $("[id^='ref-news-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена новость и коментарии к ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_news/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-news', news_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
            return false;
        }
    });
    // сменить статус
    $("[id^='ref-news-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', news_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
            return false;
        }
    });
    // 
    $("[id^='ref-snews-edit-']").on({
        click:function() {
            //alert('edit');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-snews', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            return false;
        }
    });
    $("[id^='ref-snews-delete-']").on({
        click:function() {
            if(confirm("Внимание! Будет удалена категория и все новости в ней. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_news/delete_category',
                    //dataType: 'JSON',
                    data:{process: 'delete-snews', nc_id:$(this).attr('id')},
                    success: function(Responce) {
                        //alert(Responce);
                        location.reload();
                    }
                });     
            }
            return false;
        }
    });
    $("[id^='ref-snews-check-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', nc_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
            return false;
        }
    });
    /**/
    /* КОНЕЦ РАБОТЫ С НОВОСТЯМИ*/
    /* МАРКИ, МОДЕЛИ, АВТОМОБИЛИ */
    $("[id^='ref-model-edit-']").on({
        click:function() {
            $.ajax({
                type: 'POST',
                url: '/administration/admin_avto/edit_model',
                //dataType: 'JSON',
                data:{process: 'edit-model', item:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    //
    $("[id^='ref-model-delete-']").on({
        click:function() {
            $.ajax({
                type: 'POST',
                url: '/administration/admin_avto/delete_model',
                //dataType: 'JSON',
                data:{process: 'delete-model', item:$(this).attr('id')},
                success: function(Responce) {
                    location.reload();
                }
            });
            return false;
        }
    });
    //
    $("[id^='ref-car-edit-']").on({
        click:function() {
            //alert("gfgfg");
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_avto/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-category', category_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    $("[id^='ref-car-delete-']").on({
        click:function() {
            //alert("gfgfg");
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_avto/delete_section',
                //dataType: 'JSON',
                data:{process: 'delete-category', category_id:$(this).attr('id')},
                success: function(Responce) {
                    location.reload();
                    //$("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    $("[id^='ref-avto-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_avto/edit',
                //dataType: 'JSON',
                data:{process: 'edit-item', item_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    //
    $("[id^='ref-avto-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            if (confirm('Звапрос на удаление автомобиля. Вы уверены?')) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_avto/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-item', item_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });
            }
            return false;
        }
    });
    $("[id^='ref-avto-checked-']").on({
        click:function() {
            var $inId = "#"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_avto/change_status',
                dataType: 'JSON',
                data:{process: 'change-status-avto', item_id:$(this).attr('id')},
                success: function(Responce) {
                    $.each(Responce,function(inKey, inData){
                        console.log(inKey+" "+inData);
                        if(inKey=='new-status') {
                            if (inData=='1') {
                                $($inId).removeClass('action-base action-un-checked');
                                $($inId).addClass('action-base action-checked');
                            }
                            else {
                                $($inId).removeClass('action-base action-checked');
                                $($inId).addClass('action-base action-un-checked');
                            }
                        }
                    });
                }
            });
            return false;
        }
    });
    if ($("#model_id").length>0) {
        //alert("MODEL_OK");
        $("#car_id").on({
            change:function() {
                //alert("GUD");
                $.ajax({
                    type: 'post',
                    url: '/administration/admin_avto/model_change_item',
                    dataType: 'JSON',
                    data:{process: 'car-change', item:$(this).val()},
                    success: function(Responce) {
                        $("#model_id").empty();
                        $.each(Responce,function(inKey, inData){
                            console.log(inKey);
                            $('#model_id').append('<option value="' + inData.value + '">' + inData.title + '</option>');
                        });
                    }
                });
            }
        })
    }
    //
    /* КОНЦ РАБОТЫ МАРКИ, МОДЕЛИ, АВТОМОБИЛИ */
    /**/
    $("[id^='ref-variable-edit-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_variable/edit',
                //dataType: 'JSON',
                data:{process: 'edit-variable', variable_id:$inId},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            console.log("info.site => edit => variable => "+$inId);
            return false;
        }
    });
    $("[id^='ref-variable-delete-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_variable/delete_variable',
                //dataType: 'JSON',
                data:{process: 'delete-variable', variable_id:$inId},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            console.log("info.site => edit => variable => "+$inId);
            return false;
        }
    });
    /**/
    /**/
    $("[id^='ref-keys-top-']").each(function(){
        $(this).on({
            click:function() {
                console.log($(this).attr('id'));
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_avto/top_keys_change',
                    //dataType: 'JSON',
                    data:{process: 'top-keys-change', item_id:$(this).attr('id')},
                    success: function(Responce) {
                        location.reload();
                    }
                });     
                return false;
            }
        })
    });
    $("[id^='ref-keys-edit-']").each(function(){
        $(this).on({
            click:function() {
                console.log($(this).attr('id'));
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_avto/edit_keys',
                    //dataType: 'JSON',
                    data:{process: 'edit-keys', item_id:$(this).attr('id')},
                    success: function(Responce) {
                        $("#form-ajax").html(Responce);
                        //location.reload();
                    }
                });     
                return false;
            }
        })
    });
    $("[id^='ref-keys-delete-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            if(confirm("Внимание! Будет удален ключ. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_avto/delete_keys',
                    //dataType: 'JSON',
                    data:{process: 'delete-keys', item_id:$inId},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
                console.log("info.site => delete => keys => "+$inId);
            }
            return false;
        }
    });
    /**/
    /**/
    $("[id^='ref-picture-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/edit',
                //dataType: 'JSON',
                data:{Process: 'edit-picture', picture_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-picture-delete-']").on({
        click:function() {
            if (confirm('Удалить картинку из коллекции?')) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_picture/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-picture', picture_id:$(this).attr('id')},
                    success: function(Responce) {
                        //$("#form-ajax").html(Responce);
                        location.reload();
                    }
                });     
            }
        }
    });
    //
    $("[id^='ref-picture-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', picture_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-spicture-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/delete_section',
                //dataType: 'JSON',
                data:{process: 'delete-category', cpicture_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-spicture-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-category', cpicture_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-spicture-check-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            //alert('change category'+$inId);
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-category', cpicture_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                }
            });     
        }
    });
    /**/
    /**/
    $("[id^='ref-invoice-edit-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_price/edit',
                //dataType: 'JSON',
                data:{process: 'edit-item', item_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    $("[id^='ref-invoice-delete-']").on({
        click:function() {
            //alert($(this).attr('id'));
            $.ajax({
                type: 'POST',
                url: '/administration/admin_price/delete',
                //dataType: 'JSON',
                data:{process: 'delete-item', item_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    /**/
    /*if ($('#keys_create_description').length>0){ 
        $('#keys_create_description').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })}*/
});

