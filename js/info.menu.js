$(document).ready(function(){ 
    $("[id^='ref-mc-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_menu/edit_section',
                //dataType: 'JSON',
                data:{process: 'menu-edit', menu_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    $("[id^='ref-mc-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_menu/delete_section',
                //dataType: 'JSON',
                data:{process: 'menu-delete', menu_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    $("[id^='ref-mc-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_menu/delete_section',
                //dataType: 'JSON',
                data:{process: 'menu-delete', menu_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    $("[id^='ref-mc-check-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_menu/ChangeStatus',
                //dataType: 'JSON',
                data:{process: 'menu-check', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    $("[id^='ref-menu-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_menu/edit',
                //dataType: 'JSON',
                data:{process: 'menu-item-edit', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    $("[id^='ref-menu-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_menu/delete',
                //dataType: 'JSON',
                data:{process: 'menu-item-delete', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    //
    $("[id^='ref-menu-check-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_menu/ChangeStatus',
                //dataType: 'JSON',
                data:{process: 'menu-item-check', menu_item_id:$(this).attr('id')},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            //alert($inDivId);
            return false;
        }
    });
    //
    if ($('#mi_description').length>0)             
        {$('#mi_description').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#mc_description').length>0) {
        $('#mc_description').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })
    };
})


