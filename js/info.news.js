$(document).ready(function(){ 
    console.log("info.news work"); // string 1 rad baz tubular
    if ($('#news_text').length>0)             
        {$('#news_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};   
    $("[id^='picture-to-div-']").on({
        click:function() {
            var $inImg = $(this).attr('id');
            var $regPattern = /(\d)+$/;
            var $regRes = $regPattern.exec($inImg);
            //alert($regRes[0]);
            $("#div-preview-img").html("<img src='/Images_module/getPicture/"+$regRes[0]+"/400/400'/>");
        }
    });
    
    /*
    //
    $("[id^='ref-news-edit-']").on({
        click:function() {
            alert('edit');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/edit',
                //dataType: 'JSON',
                data:{process: 'edit-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    
    
    //
    
    
    //
    
    $("[id^='ref-news-delete-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_news/delete',
                //dataType: 'JSON',
                data:{process: 'delete-news', news_id:$(this).attr('id')},
                success: function(Responce) {
                    
                    //$(".divOutputBox").html(Responce);
                    //location.reload();
                }
            });     
        }
    });
    //
    
    //
    */
})


