$(document).ready(function(){ 
    console.log("info-picture"); // string 1 rad baz tubular
    //
    $("[id^='ref-picture-main-']").on({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status-main', picture_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-main-un-checked');
                        $("#"+$inId).addClass('action-base action-main-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-main-checked');
                        $("#"+$inId).addClass('action-base action-main-un-checked');
                    }
                }
            });     
        }
    });
    
    //
    
    $("[id^='detail-']").on({
        click:function() {
            //alert('click');
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/admin_picture/detail',
                //dataType: 'JSON',
                data:{process: 'picture-detail', blogs_id:$inId},
                success: function(Responce) {
                    $('#PagingRecord').html(Responce);
                }
            });     
        }
    });
    //
   $("[id^='ref-picture-video-']").on({
        click:function() {
            
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_picture/picture_code',
                //dataType: 'JSON',
                data:{process: 'picture-code', picture_id:$inId},
                success: function(Responce) {
                    window.prompt ('Ваша ссылка',Responce);
                }
            });
        }
    });
    //
    if ($('#picture_text').length>0)             
        {$('#picture_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
})


