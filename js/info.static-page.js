$(document).ready(function(){ 
    $("[id^='ref-spc-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_static/edit_section',
                //dataType: 'JSON',
                data:{process: 'edit-category', spc_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    $("[id^='ref-sp-edit-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/admin_static/edit',
                //dataType: 'JSON',
                data:{process: 'edit-item', sp_id:$(this).attr('id')},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                }
            });
            return false;
        }
    });
    $("[id^='ref-sp-delete-']").on({
        click:function() {
            //var $inDivId = "#div-"+$(this).attr('id');
            if(confirm("Внимание! Будет удалена Статическая страница. Продолжить?")) {
                $.ajax({
                    type: 'POST',
                    url: '/administration/admin_static/delete',
                    //dataType: 'JSON',
                    data:{process: 'delete-item', sp_id:$(this).attr('id')},
                    success: function(Responce) {
                        location.reload();
                    }
                });
            }
            return false;
        }
    });
    $("[id^='ref-sp-check-']").each(function(){
        $(this).on('click',function(){
            
        });
    })
    
    if ($('#sp_text').length>0)             
        {$('#sp_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    if ($('#spc_text').length>0)             
        {$('#spc_text').tinymce({
            language:'ru',    
            selector: "textarea",
            plugins: [
                "autoresize advlist autolink autosave link image lists charmap preview hr anchor",
                "searchreplace code insertdatetime",
                "table contextmenu directionality textcolor paste textcolor colorpicker textpattern"
            ],

            toolbar1: "bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
            toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image code | insertdatetime preview | forecolor backcolor",
            toolbar3: "table | hr removeformat | subscript superscript |",

            menubar: false,
            toolbar_items_size: 'small',

            style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ],
        })};
    
})


