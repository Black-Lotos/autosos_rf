$(document).ready(function(){ 
    console.log("info.user"); // string 1 rad baz tubular
    /* roles */
    $("[id^='ref-role-edit-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/edit_roles',
                //dataType: 'JSON',
                data:{process: 'edit-role', role_id:$inId},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            console.log("info.user => edit => role => "+$inId);
            return false;
        }
    });
    $("[id^='ref-role-delete-']").on ({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/delete_roles',
                //dataType: 'JSON',
                data:{process: 'delete-role', role_id:$inId},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            console.log("info.user => delete => role => "+$inId);
            return false;
        }
    });
    /* end roles*/
    
    $("[id^='ref-user-edit-']").on({
        click : function(){
            var $inId = $(this).attr('id');
            console.log("info.user => edit => user => "+$inId);
            $.ajax({
                type: 'POST',
                url: '/administration/users/edit',
                //dataType: 'JSON',
                data:{process: 'edit-user', user_id:$inId},
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                    //location.reload();
                }
            });     
            return false;
        }
    });
    $("[id^='ref-user-delete-']").on ({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/delete',
                //dataType: 'JSON',
                data:{process: 'delete-user', user_id:$inId},
                success: function(Responce) {
                    //$("#form-ajax").html(Responce);
                    location.reload();
                }
            });     
            console.log("info.user => delete => role => "+$inId);
            return false;
        }
    });
    $("[id^='ref-user-check-']").on ({
        click:function() {
            var $inId = $(this).attr('id');
            $.ajax({
                type: 'POST',
                url: '/administration/users/change_status',
                //dataType: 'JSON',
                data:{process: 'change-status', user_id:$(this).attr('id')},
                success: function(Responce) {
                    if (Responce=='1') {
                        $("#"+$inId).removeClass('action-base action-un-checked');
                        $("#"+$inId).addClass('action-base action-checked');
                    }
                    else {
                        $("#"+$inId).removeClass('action-base action-checked');
                        $("#"+$inId).addClass('action-base action-un-checked');
                    }
                    location.reload();
                }
            });     
            return false;
        }
    });
    //
    $("#btnSaveUser").on ({
        click : function() {
            $(this).parent().parent().submit();
            return false;
        }
    });
    $('#frmUserAddAjax').submit(function(){
        var outData = $('#frmUserAddAjax').serialize();
        $.ajax({
            type: 'POST',
            url: '/administration/users/save',
            //dataType: 'JSON',
            data:outData,
            success: function(Responce) {
                $("#form-ajax").html(Responce);
            },
            error:  function(xhr, str){
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        })
        return false;
    });
    /*$("#btnSaveUserAjax").on ({
        click : function() {
            
            var outData = $('#frmUserAddAjax').serialize();
            $.ajax({
                type: 'POST',
                url: '/administration/users/save',
                //dataType: 'JSON',
                data:outData,
                success: function(Responce) {
                    $("#form-ajax").html(Responce);
                },
                error:  function(xhr, str){
                    alert('Возникла ошибка: ' + xhr.responseCode);
                }
            })
            //$("#frmUserAddAjax").submit();
            return false;
        }
    });*/
})


