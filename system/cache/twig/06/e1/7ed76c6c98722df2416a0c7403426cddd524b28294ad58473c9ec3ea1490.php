<?php

/* administration/common/list-start-up-02.twig */
class __TwigTemplate_06e17ed76c6c98722df2416a0c7403426cddd524b28294ad58473c9ec3ea1490 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "tabs", array())) {
            // line 2
            echo "    ";
            // line 3
            echo "    <h2 class='text-cl-black top-margin-01'>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "tabs", array()), "property", array()), "title", array()), "html", null, true);
            echo "</h2>
    <div class=\"btn-group btn-group-justified\">
        ";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "tabs", array()), "data", array()));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 6
                echo "            ";
                if (($context["key"] != "default")) {
                    // line 7
                    echo "                <div class=\"btn-group\">
                  <button id=\"";
                    // line 8
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "\" type=\"button\" class=\"btn btn-default btn-noborder\" href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true);
                    echo "\" url=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</button>
                </div>
            ";
                }
                // line 11
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "    </div>
    <div id='form-ajax'>
        
    </div>
    
    <nav>
        <ul id=\"paging_show\" class=\"pagination pull-left\" style=\"display:none;\">
            <li class=\"disabled\"><a href=\"#\" aria-label=\"Previous\"><span aria-hidden=\"true\">&laquo;</span></a></li>
            ";
            // line 20
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(0, ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "count", array()) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["oi"]) {
                // line 21
                echo "                <li class='";
                echo ((($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "active", array()) == ($context["oi"] + 1))) ? ("active") : (""));
                echo "'><a id='page_";
                echo twig_escape_filter($this->env, ($context["oi"] + 1), "html", null, true);
                echo "' href=\"/";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "link_run", array()), "html", null, true);
                echo "/?page=";
                echo twig_escape_filter($this->env, ($context["oi"] + 1), "html", null, true);
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "output_advanced", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, ($context["oi"] + 1), "html", null, true);
                echo "</a></li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "            <li class='disabled'>
                <a href=\"#\" aria-label=\"Next\">
                    <span aria-hidden=\"true\">&raquo;</span>
                </a>
            </li>
        </ul>
    </nav>
    
    ";
            // line 31
            echo (isset($context["sub_page_message"]) ? $context["sub_page_message"] : null);
            echo "
";
        }
        // line 33
        echo "<script type=\"text/javascript\">
    \$(document).ready(function(){
        ";
        // line 35
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "tabs", array()), "data", array()), "default", array())) {
            // line 36
            echo "            var inUrl =  '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "tabs", array()), "data", array()), "default", array()), "html", null, true);
            echo "';
            \$(\"#form-ajax\").load(inUrl,function(){
                \$(\"#paging_show\").show();
            });
        ";
        }
        // line 41
        echo "    })
</script>
";
        // line 43
        if ($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "tabs", array()), "property", array()), "include_js", array())) {
            // line 44
            echo "    <script type=\"text/javascript\" src=\"/js/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "tabs", array()), "property", array()), "include_js", array()), "html", null, true);
            echo "\"></script>
";
        }
    }

    public function getTemplateName()
    {
        return "administration/common/list-start-up-02.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 44,  123 => 43,  119 => 41,  110 => 36,  108 => 35,  104 => 33,  99 => 31,  89 => 23,  71 => 21,  67 => 20,  57 => 12,  51 => 11,  39 => 8,  36 => 7,  33 => 6,  29 => 5,  23 => 3,  21 => 2,  19 => 1,);
    }
}
