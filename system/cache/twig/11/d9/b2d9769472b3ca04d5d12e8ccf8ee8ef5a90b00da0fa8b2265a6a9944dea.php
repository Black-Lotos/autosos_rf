<?php

/* administration/common/list-system-img.twig */
class __TwigTemplate_11d9b2d9769472b3ca04d5d12e8ccf8ee8ef5a90b00da0fa8b2265a6a9944dea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "    <div class='tree-out'>
    ";
        // line 3
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "data", array())) {
            // line 4
            echo "        <ul>
        ";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "data", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 6
                echo "            <li class='root ";
                echo (($this->getAttribute($context["row"], "sub_tree", array())) ? ("open") : ("close"));
                echo "'>
                <div class='title'>";
                // line 7
                echo $this->getAttribute($context["row"], "title", array());
                echo "</div>
                <div class=\"action\">";
                // line 8
                echo (($this->getAttribute($context["row"], "action", array())) ? ($this->getAttribute($context["row"], "action", array())) : (""));
                echo "</div>
            </li>
            ";
                // line 10
                if ($this->getAttribute($context["row"], "sub_tree", array())) {
                    // line 11
                    echo "                <ul>
                    ";
                    // line 12
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["row"], "sub_tree", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["sub_row"]) {
                        // line 13
                        echo "                        <li class='level-img-01 ";
                        echo (((($context["key"] % 2) == 0)) ? ("text-cl-odd") : ("text-cl-even"));
                        echo " ";
                        echo (($this->getAttribute($context["sub_row"], "sub_tree", array())) ? ("open") : (""));
                        echo "'>
                                ";
                        // line 14
                        if ($this->getAttribute($context["sub_row"], "picture_id", array())) {
                            // line 15
                            echo "                                    <img src=\"/Images_module/getPicture/";
                            echo $this->getAttribute($context["sub_row"], "picture_id", array());
                            echo "/";
                            echo $this->env->getExtension('project')->getVariable("image_list_tree_width");
                            echo "/";
                            echo $this->env->getExtension('project')->getVariable("image_list_tree_width");
                            echo "/1\"/>
                                ";
                        }
                        // line 17
                        echo "                                <div class='title left-padding-px-1'>
                                    ";
                        // line 18
                        echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["sub_row"], "title", array()), 60);
                        echo "
                                </div>
                                <div class=\"action\">";
                        // line 20
                        echo (($this->getAttribute($context["sub_row"], "action", array())) ? ($this->getAttribute($context["sub_row"], "action", array())) : ("нет"));
                        echo "</div>
                        </li>
                        ";
                        // line 22
                        if ($this->getAttribute($context["sub_row"], "sub_tree", array())) {
                            // line 23
                            echo "                            <ul>
                                ";
                            // line 24
                            $context['_parent'] = (array) $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_row"], "sub_tree", array()));
                            foreach ($context['_seq'] as $context["sub_row_key"] => $context["sub_row_item"]) {
                                // line 25
                                echo "                                    <li class='level-02 ";
                                echo (((($context["sub_row_key"] % 2) == 0)) ? ("text-cl-odd") : ("text-cl-even"));
                                echo "'>
                                        ";
                                // line 26
                                if ($this->getAttribute($this->getAttribute($context["sub_row_item"], "sub_tree", array()), "child", array())) {
                                    // line 27
                                    echo "                                            <div class='title left-padding-2'><a href='";
                                    echo $this->getAttribute($this->getAttribute($context["sub_row_item"], "sub_tree", array()), "child", array());
                                    echo "' title='";
                                    echo $this->getAttribute($this->getAttribute($context["sub_row_item"], "sub_tree", array()), "caption", array());
                                    echo "'>";
                                    echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["sub_row_item"], "title", array()), 60);
                                    echo "</a></div>
                                        ";
                                } else {
                                    // line 29
                                    echo "                                            <div class='title left-padding-2'>";
                                    echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["sub_row_item"], "title", array()), 60);
                                    echo "</div>
                                        ";
                                }
                                // line 31
                                echo "                                        <div class=\"action\">";
                                echo (($this->getAttribute($context["sub_row_item"], "action", array())) ? ($this->getAttribute($context["sub_row_item"], "action", array())) : ("нет"));
                                echo "</div>
                                    </li>
                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['sub_row_key'], $context['sub_row_item'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 34
                            echo "                            </ul>
                        ";
                        }
                        // line 36
                        echo "                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['sub_row'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 37
                    echo "                </ul>    
            ";
                }
                // line 39
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "        </ul>
    ";
        } else {
            // line 41
            echo "    
        <ul>
            <li class='root'>Элементы отсутствуют. Необходимо провести добавление.<div class=\"action\">";
            // line 43
            echo (($this->getAttribute((isset($context["row"]) ? $context["row"] : null), "action", array())) ? ($this->getAttribute((isset($context["row"]) ? $context["row"] : null), "action", array())) : (""));
            echo "</div></li>
            ";
            // line 45
            echo "        </ul>
    ";
        }
        // line 47
        echo "    </div>
    ";
        // line 48
        if ($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "count", array())) {
            // line 49
            echo "        <nav class='pagination'>
            ";
            // line 50
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "count", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["oi"]) {
                // line 51
                echo "                ";
                // line 52
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "        </nav>
    ";
        }
        // line 55
        echo "
";
        // line 56
        echo (isset($context["sub_page_message"]) ? $context["sub_page_message"] : null);
        echo "    
";
        // line 58
        echo "<script type=\"text/javascript\" src=\"/js/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "setup_page", array()), "property", array()), "include_js", array()), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"/js/info.common.js\"></script>
";
        // line 98
        echo "
";
    }

    public function getTemplateName()
    {
        return "administration/common/list-system-img.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 98,  195 => 58,  191 => 56,  188 => 55,  184 => 53,  178 => 52,  176 => 51,  172 => 50,  169 => 49,  167 => 48,  164 => 47,  160 => 45,  156 => 43,  152 => 41,  148 => 40,  142 => 39,  138 => 37,  132 => 36,  128 => 34,  118 => 31,  112 => 29,  102 => 27,  100 => 26,  95 => 25,  91 => 24,  88 => 23,  86 => 22,  81 => 20,  76 => 18,  73 => 17,  63 => 15,  61 => 14,  54 => 13,  50 => 12,  47 => 11,  45 => 10,  40 => 8,  36 => 7,  31 => 6,  27 => 5,  24 => 4,  22 => 3,  19 => 2,);
    }
}
