<?php

/* administration/common/form-system.twig */
class __TwigTemplate_173dc3922fc662d83b6840d29e4a75ad247ce8c61fce619c1acb4a7944fa2b49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\" src=\"/js/tinymce-jquery/jquery.tinymce.min.js\"></script>
<script type=\"text/javascript\" src=\"/js/tinymce-jquery/tinymce.min.js\"></script>
<script type=\"text/javascript\" src=\"/js/moment-with-locales.min.js\"></script>
<script type=\"text/javascript\" src=\"/js/bootstrap-datetimepicker.min.js\"></script>
<link rel=\"stylesheet\" href=\"/css/bootstrap-datetimepicker.min.css\" />
<script type=\"text/javascript\">
    \$(document).ready(function(){
        if(\$(\"[no-resize-textarea='true']\").length>0) {
            \$(\"[no-resize-textarea='true']\").each(function(aKey,aData){
                var inElement = \$(this).attr(\"id\");
                \$('#'+inElement).addClass('no-resize-control');
            })
        }
        //
        if(\$(\"[input-chk-digits='true']\").length>0) {
            \$(\"[input-chk-digits='true']\").on({
                keydown:function(aEvent) {
                    if  (aEvent.keyCode == 46 || aEvent.keyCode == 8 || aEvent.keyCode == 9 || aEvent.keyCode == 27 || (aEvent.keyCode == 65 && aEvent.ctrlKey === true) ||
                            (aEvent.keyCode >= 35 && aEvent.keyCode <= 39)) { return; } 
                        else {
                            if  ((aEvent.keyCode < 48 || aEvent.keyCode > 57) && (aEvent.keyCode < 96 || aEvent.keyCode > 105 )) {
                                aEvent.preventDefault();
                            }
                        }
                }, 
            })
        }; 
        //
        if(\$(\"[control-to-element]\").length>0){
            \$(\"[control-to-element]\").each(function(aKey,aData){
                var inElement = \$(this).attr(\"control-to-element\");
                var inId = \$(this).attr('id');
                var inText = \$(\"#\"+inId+\" option:selected\").text();
                if (inText==\"С кнопками\") {
                    \$(inElement).attr('disabled', false);
                } else {
                    \$(inElement+' option:first').attr('selected', true);
                    \$(inElement).val('');
                    \$(inElement).attr('disabled', true);
                }
                console.log(inElement+\" \"+inId);
                \$(\"#\"+inId).on({
                    focus:function() {
                    },
                    change:function() {
                        var inText = \$(\"#\"+inId+\" option:selected\").text();
                        if (inText==\"С кнопками\") {
                            \$(inElement).attr('disabled', false);
                        } else {
                            \$(inElement+' option:first').attr('selected', true);
                            \$(inElement).val('');
                            \$(inElement).attr('disabled', true);
                        }
                    } 
                });
            });
        };
        if((\$(\"[input-date-time='true']\")).length>0) {
            \$(\"[input-date-time='true']\").datetimepicker({
                inline: true,
                sideBySide: true,
                language: 'ru',
                format: 'YYYY-MM-DD HH:MM'
                
            });
        }
        /**/
        \$(\"#picture_file\").fileinput({
            showUpload:true, 
            uploadTitle:'Загрузка', 
            maxFileCount: 10,
            allowedFileTypes: [\"image\", \"video\"],
            previewFileType:['jpg','png'],
            initialPreview: [
                \"<img src='/Images_module/getPicture/\"+\$(\"#picture_id\").val()+\"/180/150/1'>\",
            ],
        });
        /*загрузка изображений*/
        \$inPicture = '";
        // line 79
        echo twig_escape_filter($this->env, $this->env->getExtension('project')->testOut(), "html", null, true);
        echo "';
        \$(\"#UploadButton\").uploadFile({
            url:\"/Images_module/ajax_upload\",
            allowedTypes:\"png,gif,jpg,jpeg\",//типы файлов
            fileName:\"img_file\", //имя при аплоаде
            showProgress:false,
            showDone:false,
            ahowAbort:false,
            dragDrop:false,
            uploadButtonClass:'button-base-img btn-color-02', // класс кнопки загрузить из боотстрапа
            returnType:'json', // результат после записи
            onSuccess:function(files,data,xhr) { // все прошло нормально
                var outRes=\"\";
                \$(\".ajax-file-upload-statusbar\").hide(); // прячем процесс загрузки
                //\$('#divStatus').html(\"<img src='/\"+data+\"' width='100%' height='100%' />\");
                //разбираем джейсон по пунктам
                \$.each(data,function(inKey,inData){
                    outRes = outRes+inKey+ \" \"+inData+\"<br>\";
                    if(inKey=='rec-no') {
                       \$(\"#images_id\").val(inData);
                       // загружаем в превью с сохранением масштаба но ширине (0-ширина 1 высота) последгий параметр
                       \$(\"#div-preview\").css('background-image',\"url(/Images_module/getImages/\"+inData+\"/180/180/1)\");
                    };
                    if(inKey=='rec-name') { 
                    }
                })
                //\$('#divStatus').html(outRes);
            },
            onError: function (files, status, message) {
                \$('#divStatus').html(files+\"<br>\"+status+\"<br>\"+message);
            },
            onSubmit:function(files, xhr) {
                var outRes=\"<ul>\";
                \$.ajax({
                    type: 'POST',
                    url:\"/Images_module/ajax_upload_clean\",
                    //dataType: 'json',
                    data:{process: 'images-upload-clean', images_id:\$(\"#images_id\").val()},
                    success: function(Responce) {
                        //alert(Responce);
                        \$.each(Responce,function(inKey,inData){
                            outRes=outRes+\"<li>\"+inKey+\":\"+inData+\"</li>\";
                        });
                    }
                });
                outRes=outRes+\"</ul>\";
                //\$('#divStatus').html(outRes);
            }
        });
    if (\$(\"#keys_wm\").length>0) {
        \$(\"#keys_wm\").on('click',function(){
           if(\$(\"#keys_wm\").prop(\"checked\")) {
               \$(\"#preview-wm\").show();
           } else {
               \$(\"#preview-wm\").hide();
           }
        });
    }
// Место для моей функции начало 






// Место для моей функции конеч

       
// Завершающий тег главной функции        
    });
</script>
";
        // line 150
        if ($this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "form_property", array()), "include_js", array())) {
            // line 151
            echo "    ";
            // line 152
            echo "    <script type=\"text/javascript\" src=\"/js/";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "form_property", array()), "include_js", array()), "html", null, true);
            echo "\"></script>
";
        }
        // line 153
        echo " 
";
        // line 155
        echo "    <div id=\"input-form-ajax\" class=\"top-margin-0\"> 
        ";
        // line 156
        echo (isset($context["sub_page_message"]) ? $context["sub_page_message"] : null);
        echo " ";
        // line 157
        echo "        <form class='form-horizontal' enctype='multipart/form-data' id='";
        echo $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "form_property", array()), "name", array());
        echo "' name='";
        echo $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "form_property", array()), "name", array());
        echo "' method='";
        echo $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "form_property", array()), "method", array());
        echo "' action='";
        echo $this->getAttribute($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "form_property", array()), "action", array());
        echo "'>
            ";
        // line 159
        echo "            ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "form_data", array()));
        foreach ($context['_seq'] as $context["_key"] => $context["control"]) {
            // line 160
            echo "            ";
            // line 161
            echo "                ";
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "separator")) {
                // line 162
                echo "                    <hr class=\"";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\">
                ";
            }
            // line 164
            echo "                ";
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "submit")) {
                // line 165
                echo "                    <hr class=\"title\">
                    <input type=\"";
                // line 166
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array());
                echo "\" name=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" id=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" class=\"btn btn-success btn-lg ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" value='";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "'/>
                ";
            }
            // line 168
            echo "                ";
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "hidden")) {
                // line 169
                echo "                    <input type='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array());
                echo "' name='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' id='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' require=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array());
                echo "\" value='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "' /> 
                ";
            }
            // line 170
            echo " 
                ";
            // line 171
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "text")) {
                // line 172
                echo "                <div class='form-group'>
                    <label for=\"";
                // line 173
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" class=\"col-sm-4 control-label ";
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                        <div class=\"col-sm-8\">
                            <input type='";
                // line 175
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array());
                echo "' name='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' id='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' 
                                ";
                // line 176
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array())) ? ("required") : (""));
                echo " value='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "' 
                                autofocus class=\"form-control ";
                // line 177
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" 
                                placeholder=\"";
                // line 178
                echo $this->getAttribute($context["control"], "caption", array());
                echo " \"
                                ";
                // line 179
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_extends", array());
                echo "
                                require=\"";
                // line 180
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array());
                echo "\"/>
                        </div>
                </div>
                ";
            }
            // line 183
            echo " 
                ";
            // line 184
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "datepicker")) {
                // line 185
                echo "                    <div class=\"form-group\">
                        <label for=\"";
                // line 186
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" class=\"col-sm-4 control-label ";
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                            <div class=\"input-group date col-sm-8 ";
                // line 187
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" ";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_extended", array());
                echo ">
                                <input type=\"text\" class=\"form-control input-skin-01\" name='";
                // line 188
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' id='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array())) ? ("required") : (""));
                echo " value='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "' autofocus multiple=true class=\"file-loading form-control ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" placeholder=\"";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "\"/>
                                <span class=\"input-group-addon\">
                                <span class=\"glyphicon-calendar glyphicon\"></span>
                                </span>
                            </div>
                    </div>
                ";
            }
            // line 194
            echo "     
                ";
            // line 195
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "file")) {
                // line 196
                echo "                <div class='form-group'>
                    <label for=\"";
                // line 197
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" class=\"col-sm-4 control-label ";
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                        <div class=\"col-sm-8\">
                            <input type='";
                // line 199
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array());
                echo "' name='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "[]' id='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array())) ? ("required") : (""));
                echo " value='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "' autofocus multiple=true class=\"file-loading form-control ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" placeholder=\"";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "\"/>
                        </div>
                </div>
                ";
            }
            // line 202
            echo "     
                ";
            // line 203
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "password")) {
                // line 204
                echo "                <div class=\"form-group\">
                    <label for=\"";
                // line 205
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" class=\"col-sm-4 control-label ";
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                    <div class=\"col-sm-8\">
                        <input type='";
                // line 207
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array());
                echo "' name='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' id='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array())) ? ("required") : (""));
                echo " value='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "' autofocus class=\"form-control ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" placeholder=\"";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "\"/>
                    </div>
                </div>    
                ";
            }
            // line 210
            echo " 
                ";
            // line 211
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "textarea")) {
                // line 212
                echo "                <div class=\"form-group\">
                    <label for=\"";
                // line 213
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" class=\"col-sm-4 control-label ";
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                    <div class=\"";
                // line 214
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : ("col-sm-8"));
                echo "\">
                        <textarea name=\"";
                // line 215
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" id=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" 
                        class=\"form-control ";
                // line 216
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\"
                        ";
                // line 217
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_extends", array());
                echo " ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array())) ? ("required") : (""));
                echo " rows=\"5\" cols=\"10\">";
                echo trim($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array()));
                echo "</textarea>
                    </div>
                </div>    
                ";
            }
            // line 220
            echo " 
                ";
            // line 221
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "checkbox")) {
                echo "    
                
                <div class=\"form-group\">
                    <div class=\"col-sm-offset-3 col-sm-8\">
                        <div class=\"checkbox\">
                            <label class='";
                // line 226
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "'>
                              <input type=\"";
                // line 227
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array());
                echo "\" id=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" name=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" ";
                echo ((($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array()) > 0)) ? ("checked") : (""));
                echo ">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "
                            </label>
                        </div>
                    </div>            
                </div>    
                ";
            }
            // line 232
            echo "    
                ";
            // line 233
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "select")) {
                echo "    
                <div class=\"form-group\">
                    <label for=\"";
                // line 235
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" class=\"col-sm-4 control-label ";
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                    <div class=\"col-sm-8\">
                        ";
                // line 237
                $context["echoDisale"] = (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_readonly", array())) ? ("disabled") : (""));
                // line 238
                echo "                        <select ";
                echo (isset($context["echoDisale"]) ? $context["echoDisale"] : null);
                echo " id='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' name='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' 
                            autofocus class=\"form-control ";
                // line 239
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\"
                            ";
                // line 240
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_extends", array());
                echo ">
                            ";
                // line 241
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["items_select"]) {
                    // line 242
                    echo "                                ";
                    // line 243
                    echo "                                <option ";
                    echo (isset($context["echoDisale"]) ? $context["echoDisale"] : null);
                    echo " value='";
                    echo $this->getAttribute($context["items_select"], "value", array());
                    echo "' ";
                    echo $this->getAttribute($context["items_select"], "active", array());
                    echo ">";
                    echo $this->getAttribute($context["items_select"], "title", array());
                    echo "</option>
                                ";
                    // line 245
                    echo "                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['items_select'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 246
                echo "                        </select>
                    </div>
                </div>
                ";
            }
            // line 249
            echo "                
                ";
            // line 250
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "h2")) {
                // line 251
                echo "                    <h2 class=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array());
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</h2>
                ";
            }
            // line 253
            echo "                    ";
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "numeric")) {
                // line 254
                echo "                    <div class=\"form-group\">
                    <label class=\"col-sm-4 control-label ";
                // line 255
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\" for=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                        <div class=\"input-group num-skin-01\">
                          <div class=\"input-group-addon\">руб.</div>
                          <input type=\"text\" class=\"form-control ";
                // line 258
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" 
                                name=\"";
                // line 259
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" id=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\" placeholder=\"";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "\"
                                require=\"";
                // line 260
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array());
                echo "\" 
                                input-chk-digits=\"true\" input-chk-digits-message=\"Ошибка ввода. ()\"
                                value='";
                // line 262
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "'
                                />
                          <div class=\"input-group-addon\">.00</div>
                        </div>
                    </div>      
                    ";
            }
            // line 268
            echo "                ";
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "include_image")) {
                // line 269
                echo "                    <input type='hidden' id='images_id' name='images_id' value='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "'>
                    <div class=\"image-upload\">
                        <div class='col-sm-4 preview' id=\"divStatus\">
                            ";
                // line 272
                if ((($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array())) : (""))) {
                    // line 273
                    echo "                                <div id='div-preview' class='div-prv-img'>
                                    <img id='preview-wm' src='/Images_module/getWM/watermark.png/190/190/1'/>
                                </div>
                                <script type=\"text/javascript\">
                                    \$(document).ready(function(){
                                        \$(\"#div-preview\").css('background-image',\"url(/Images_module/getImages/";
                    // line 278
                    echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                    echo "/190/190/1)\");
                                    })
                                </script>
                                ";
                    // line 283
                    echo "                            ";
                } else {
                    // line 284
                    echo "                                <div id='div-preview' class='div-prv-img'>
                                    ";
                    // line 286
                    echo "                                    <img id='preview-wm' src='/Images_module/getWM/watermark.png/190/190/1'/>
                                </div>
                            ";
                }
                // line 289
                echo "                        </div>
                        <div class=\"col-sm-8 input-skin-04\">
                            <a href=\"#\" id=\"UploadButton\"></a>
                            ";
                // line 292
                if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array()) != 0)) {
                    // line 293
                    echo "                                <a href=\"#\" id=\"";
                    echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array())) : ("btnClearImageElement"));
                    echo "\" class='button-base-img btn-color-01' for='";
                    echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_for", array());
                    echo "'></a>
                            ";
                }
                // line 295
                echo "                        </div>
                    </div>
                    <div style='clear:both;'></div> 
                ";
            }
            // line 299
            echo "                ";
            if (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_type", array()) == "captcha")) {
                echo "    
                <div class=\"form-group\">
                    <label class=\"col-sm-4 control-label ";
                // line 301
                echo (($this->getAttribute($context["control"], "set_css", array())) ? ($this->getAttribute($context["control"], "set_css", array())) : (""));
                echo "\" for=\"";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "\">";
                echo $this->getAttribute($context["control"], "caption", array());
                echo "</label>
                    <div class=\"input-skin-03\">
                        <center>";
                // line 303
                echo $this->env->getExtension('project')->getCaptcha();
                echo "</center>
                        <input type=\"text\" class=\"form-control input-skin-01\" name='";
                // line 304
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' id='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_name", array());
                echo "' ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_require", array())) ? ("required") : (""));
                echo " value='";
                echo $this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_value", array());
                echo "' autofocus multiple=true class=\"file-loading form-control ";
                echo (($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) ? ($this->getAttribute($this->getAttribute($context["control"], "to_control", array()), "set_css", array())) : (""));
                echo "\" placeholder=\"Введите код с картинки\"/>
                    </div>
                </div>            
                ";
            }
            // line 308
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['control'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 309
        echo "        </form>
    </div>
";
        // line 311
        echo " ";
    }

    public function getTemplateName()
    {
        return "administration/common/form-system.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  713 => 311,  709 => 309,  703 => 308,  688 => 304,  684 => 303,  675 => 301,  669 => 299,  663 => 295,  655 => 293,  653 => 292,  648 => 289,  643 => 286,  640 => 284,  637 => 283,  631 => 278,  624 => 273,  622 => 272,  615 => 269,  612 => 268,  603 => 262,  598 => 260,  590 => 259,  586 => 258,  576 => 255,  573 => 254,  570 => 253,  562 => 251,  560 => 250,  557 => 249,  551 => 246,  545 => 245,  534 => 243,  532 => 242,  528 => 241,  524 => 240,  520 => 239,  511 => 238,  509 => 237,  500 => 235,  495 => 233,  492 => 232,  475 => 227,  471 => 226,  463 => 221,  460 => 220,  449 => 217,  445 => 216,  439 => 215,  435 => 214,  427 => 213,  424 => 212,  422 => 211,  419 => 210,  400 => 207,  391 => 205,  388 => 204,  386 => 203,  383 => 202,  364 => 199,  355 => 197,  352 => 196,  350 => 195,  347 => 194,  327 => 188,  321 => 187,  313 => 186,  310 => 185,  308 => 184,  305 => 183,  298 => 180,  294 => 179,  290 => 178,  286 => 177,  280 => 176,  272 => 175,  263 => 173,  260 => 172,  258 => 171,  255 => 170,  241 => 169,  238 => 168,  225 => 166,  222 => 165,  219 => 164,  213 => 162,  210 => 161,  208 => 160,  203 => 159,  192 => 157,  189 => 156,  186 => 155,  183 => 153,  177 => 152,  175 => 151,  173 => 150,  99 => 79,  19 => 1,);
    }
}
