<?php

/* blocks/block-carusel.twig */
class __TwigTemplate_2251de31ec82b67fe685b223c9304aee4b4205e25cdc6514c74944df313af9f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 2
            echo "    <div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
        <!-- Индикатор карусели -->
        ";
            // line 10
            echo "        <!-- Контент карусели -->
        <div class=\"carousel-inner\">
            ";
            // line 12
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 13
                echo "                <!-- если индекс равен 0 ставим активной первую картинку в карусели -->
                <div class=\"item ";
                // line 14
                echo ((($context["key"] == 0)) ? ("active") : (""));
                echo "\" data-slide-number=\"";
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\">
                    <!-- Заголовок -->
                    <!--<h2>";
                // line 16
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                echo "</h2>-->
                    <!-- Картинка -->
                    ";
                // line 19
                echo "                    <div style='height:300px; vertical-align: central;'>
                    <center>
                        <img alt='' src='/Images_module/getPicture/";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                echo "/560/460/1' >
                    </center>
                    </div>
                    <!-- дополнительные части для каручели отключены пока за ненадобностью -->
                    ";
                // line 29
                echo "                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "        </div>
        <!-- навигация карусели право-лево -->
        <a class=\"carousel-control left\" href=\"#myCarousel\" data-slide=\"prev\">
            <span class=\"glyphicon glyphicon-chevron-left\"></span>
        </a>
        <a class=\"carousel-control right\" href=\"#myCarousel\" data-slide=\"next\">
            <span class=\"glyphicon glyphicon-chevron-right\"></span>
        </a>
    </div>
    <div class=\"clearfix\"></div>
    <center>
        <div class=\"col-md-12 hidden-sm hidden-xs\" id=\"slider-thumbs\">
                <!-- thumb navigation carousel items -->
              <ul class=\"list-inline\">
                  ";
            // line 45
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 46
                echo "                        <li> 
                            <a id=\"carousel-selector-";
                // line 47
                echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                echo "\" class=\"";
                echo ((($context["key"] == 0)) ? ("selected") : (""));
                echo "\">
                                <img src=\"/Images_module/getPicture/";
                // line 48
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                echo "/80/60/0\" class=\"img-responsive\">
                            </a>
                        </li>
                  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "                </ul>
        </div>
    </center>
    <div class=\"clearfix\"></div>                  
";
        }
    }

    public function getTemplateName()
    {
        return "blocks/block-carusel.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 52,  95 => 48,  89 => 47,  86 => 46,  82 => 45,  66 => 31,  59 => 29,  52 => 21,  48 => 19,  43 => 16,  36 => 14,  33 => 13,  29 => 12,  25 => 10,  21 => 2,  19 => 1,);
    }
}
