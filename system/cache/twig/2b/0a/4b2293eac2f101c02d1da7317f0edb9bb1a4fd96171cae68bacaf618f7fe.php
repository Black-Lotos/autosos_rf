<?php

/* news/news-list-all.twig */
class __TwigTemplate_2b0a4b2293eac2f101c02d1da7317f0edb9bb1a4fd96171cae68bacaf618f7fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class='news-block'>
    ";
        // line 2
        if ((isset($context["title"]) ? $context["title"] : null)) {
            // line 3
            echo "        <h2>";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</h2>
    ";
        }
        // line 4
        echo "  
    ";
        // line 5
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 6
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 7
                echo "            <div>
            ";
                // line 8
                if ($this->getAttribute($context["item"], "value", array())) {
                    // line 9
                    echo "                <span class=\"title\"><a id='detail-news-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "' href=\"/news/detail/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</a></span>
                ";
                    // line 10
                    if ($this->getAttribute($context["item"], "images_id", array())) {
                        // line 11
                        echo "                    <img class='xs' src=\"/Images_module/getImages/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                        echo "/215/150/1\" alt=\"\"/>
                ";
                    }
                    // line 13
                    echo "                <span class=\"about\">";
                    echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["item"], "note", array()), 400);
                    echo "</span>
                ";
                    // line 15
                    echo "            ";
                } else {
                    echo "    
                <h2>";
                    // line 16
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</h2>
            ";
                }
                // line 18
                echo "            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "    ";
        }
        // line 21
        echo "</div>

";
    }

    public function getTemplateName()
    {
        return "news/news-list-all.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 21,  84 => 20,  77 => 18,  72 => 16,  67 => 15,  62 => 13,  56 => 11,  54 => 10,  45 => 9,  43 => 8,  40 => 7,  35 => 6,  33 => 5,  30 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
