<?php

/* avto/avto-detail.twig */
class __TwigTemplate_2b3fbec36bbaaa5b998ab2d143578b1ec8d9c52dd809547524182dfaa5a78db5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 13
        echo "
<div class=\"avto\">
    <h2>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "title", array()), "html", null, true);
        echo "</h2>
    ";
        // line 16
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "property", array()), "keys_type", array())) {
            // line 17
            echo "        <hr class=\"title\">
        <center>
            <div class=\"btn-group div-center\" role=\"group\" aria-label=\"...\">
                <a class=\"btn btn-info\" href=\"/page_car/detail_top_keys/";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array()), "html", null, true);
            echo "/1\" role=\"button\">Ключи с кнопками</a>
                <a class=\"btn btn-info\" href=\"/page_car/detail_top_keys/";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array()), "html", null, true);
            echo "/3\" role=\"button\">Корпус</a>
                <a class=\"btn btn-info\" href=\"/page_car/detail_top_keys/";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array()), "html", null, true);
            echo "/2\" role=\"button\">Без кнопок</a>
                ";
            // line 26
            echo "            </div> 
        </center>
    ";
        }
        // line 29
        echo "    
    ";
        // line 30
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "collection", array())) {
            // line 31
            echo "        <hr class=\"title\">
        ";
            // line 32
            $this->env->loadTemplate("blocks/block-carusel.twig")->display(array_merge($context, array("data" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "collection", array()))));
            // line 33
            echo "    ";
        }
        // line 34
        echo "    ";
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "images_id", array())) {
            // line 35
            echo "        ";
            // line 36
            echo "    ";
        }
        // line 37
        echo "    <div class=\"row left-padding-1 right-padding-1\">
    ";
        // line 38
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "sub_tree", array())) {
            // line 39
            echo "        <hr class=\"title-child\">
        ";
            // line 40
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "sub_tree", array()));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 41
                echo "            <div class=\"col-lg-5 right-margin-3 left-margin-2\">
                <h2><a class='link-03' href=\"";
                // line 42
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_url", array()), "html", null, true);
                echo "detail/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_name", array()), "html", null, true);
                echo "</a></h2>
                ";
                // line 43
                if ($this->getAttribute($context["item"], "images_id", array())) {
                    // line 44
                    echo "                <center>
                    <a href=\"";
                    // line 45
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_url", array()), "html", null, true);
                    echo "detail/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_id", array()), "html", null, true);
                    echo "\"><img class='img-thumbnail' id='block-image-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                    echo "' src='/Images_module/getImages/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                    echo "/100/100/0'/></a>
                </center>    
                ";
                }
                // line 48
                echo "                <p class=\"";
                echo (($this->getAttribute($context["item"], "keys_instock", array())) ? ("text-cl-06") : ("text-cl-07"));
                echo " text-bold para-center\">";
                echo (($this->getAttribute($context["item"], "keys_instock", array())) ? ("В наличии") : ("Нет в наличии"));
                echo "</p>
                <a class='btn-detail' href=\"";
                // line 49
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_url", array()), "html", null, true);
                echo "detail/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_id", array()), "html", null, true);
                echo "\"></a>
                ";
                // line 55
                echo "                
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "    ";
        }
        echo "    

    </div>
    <hr class=\"title\">    
    ";
        // line 62
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "text", array());
        echo "
</div>
";
        // line 65
        echo "<div class=\"clearfix bottom-margin-0\"></div>";
    }

    public function getTemplateName()
    {
        return "avto/avto-detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 65,  141 => 62,  133 => 58,  125 => 55,  119 => 49,  112 => 48,  100 => 45,  97 => 44,  95 => 43,  87 => 42,  84 => 41,  80 => 40,  77 => 39,  75 => 38,  72 => 37,  69 => 36,  67 => 35,  64 => 34,  61 => 33,  59 => 32,  56 => 31,  54 => 30,  51 => 29,  46 => 26,  42 => 22,  38 => 21,  34 => 20,  29 => 17,  27 => 16,  23 => 15,  19 => 13,);
    }
}
