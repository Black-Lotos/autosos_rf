<?php

/* news/news-start-up.twig */
class __TwigTemplate_2ee495798264e9f2049c37145d786d3ca6ea9c8ab4d0effc55c7b8b14520dec3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class='block-01'>
    ";
        // line 2
        if ((isset($context["title"]) ? $context["title"] : null)) {
            // line 3
            echo "        <h2>";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</h2>
    ";
        }
        // line 4
        echo "  
    ";
        // line 5
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 6
            echo "        <div class=\"block-01\">
            <ul class=\"sidenews\">
            ";
            // line 8
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 9
                echo "                ";
                if ($this->getAttribute($context["item"], "value", array())) {
                    // line 10
                    echo "                    ";
                    if ($this->getAttribute($context["item"], "images_id", array())) {
                        // line 11
                        echo "                        ";
                        // line 12
                        echo "                    ";
                    }
                    // line 13
                    echo "                    <li class=\"bottom-margin-no \"><a id='detail-news-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "' href=\"/news/detail/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</a></li>
                    ";
                    // line 16
                    echo "                ";
                } else {
                    echo "    
                    <h2>";
                    // line 17
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</h2>
                ";
                }
                // line 19
                echo "                
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "            </ul>
        </div>
    ";
        }
        // line 24
        echo "    <hr/>
    <a class=\"link-01 bottom-margin-1 top-margin-1\" id='news-start-up-all' href=\"/news/\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('project')->getVariable("caption_all_news"), "html", null, true);
        echo "</a>
</div>
";
    }

    public function getTemplateName()
    {
        return "news/news-start-up.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 25,  85 => 24,  80 => 21,  73 => 19,  68 => 17,  63 => 16,  54 => 13,  51 => 12,  49 => 11,  46 => 10,  43 => 9,  39 => 8,  35 => 6,  33 => 5,  30 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
