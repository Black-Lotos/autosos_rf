<?php

/* blocks/block-car-start-up.twig */
class __TwigTemplate_34a16943024fb9ad5bfbc5af459cb7e57780537c929732b5c6ccfca8924711ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "        <div class=\"cl-right-content\">
            <div>";
        // line 4
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "text", array());
        echo "</div>
            <a class=\"btn btn-info\" href=\"/page_car/detail_top_keys/";
        // line 5
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array());
        echo "\" role=\"button\">Топ ключи ";
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "title", array());
        echo "</a>
            <img class='position-right' id='block-image-";
        // line 6
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array());
        echo "' src='/Images_module/getImages/";
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "images_id", array());
        echo "/100/100/1'/>
        </div>

";
        // line 10
        echo "

";
    }

    public function getTemplateName()
    {
        return "blocks/block-car-start-up.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 10,  32 => 6,  26 => 5,  22 => 4,  19 => 3,);
    }
}
