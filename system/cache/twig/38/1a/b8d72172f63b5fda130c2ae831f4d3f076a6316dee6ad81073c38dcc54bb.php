<?php

/* blocks/block-search.twig */
class __TwigTemplate_381ab8d72172f63b5fda130c2ae831f4d3f076a6316dee6ad81073c38dcc54bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<div class='block-01'>
    <hr/>
    <div class=\"search-form\"> <!-- search-form start -->
        <form class=\"form-inline\" method='get' id='frmSearch' action='/";
        // line 6
        echo twig_escape_filter($this->env, (($this->env->getExtension('project')->getNameModule(1)) ? ($this->env->getExtension('project')->getNameModule(1)) : ("home")), "html", null, true);
        echo "/goSearch'>
            <div class=\"form-group\">
                <input type=\"text\" name='inSearch' class='form-control input-skin-01' placeholder=\"Поиск\" onfocus=\"this.placeholder = ''\" onblur=\"this.placeholder = 'Поиск'\" />
            </div>
            <div class=\"form-group top-margin-0\">
                <input type=\"submit\" class='btn btn-success btn-sm' value=\"Провести поиск\" id='btnGoSearch'/>
            </div>
        </form>
    </div> <!-- search-form end -->
    <hr/>
</div>

";
    }

    public function getTemplateName()
    {
        return "blocks/block-search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 6,  19 => 2,);
    }
}
