<?php

/* avto/avto-detail.twig */
class __TwigTemplate_40448ca477004a233d36352af5d3794ce5b75f422e101e1eb55e5ec51454dfbb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 13
        echo "
<div class=\"avto\">
    <h2>";
        // line 15
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "title", array()), "html", null, true);
        echo "</h2>
    ";
        // line 16
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "property", array()), "keys_type", array())) {
            // line 17
            echo "        <hr class=\"title\">
        <center>
            <div class=\"btn-group div-center\" role=\"group\" aria-label=\"...\">
                <a class=\"btn btn-info ";
            // line 20
            echo ((($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "property", array()), "keys_type", array()) == 1)) ? ("active") : (""));
            echo "\" href=\"/page_car/detail_top_keys/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array()), "html", null, true);
            echo "/1\" role=\"button\">Ключи с кнопками</a>
                <a class=\"btn btn-info ";
            // line 21
            echo ((($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "property", array()), "keys_type", array()) == 3)) ? ("active") : (""));
            echo "\" href=\"/page_car/detail_top_keys/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array()), "html", null, true);
            echo "/3\" role=\"button\">Корпус</a>
                <a class=\"btn btn-info ";
            // line 22
            echo ((($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "property", array()), "keys_type", array()) == 2)) ? ("active") : (""));
            echo "\" href=\"/page_car/detail_top_keys/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "value", array()), "html", null, true);
            echo "/2\" role=\"button\">Без кнопок</a>
            </div> 
        </center>
    ";
        }
        // line 26
        echo "    
    ";
        // line 27
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "collection", array())) {
            // line 28
            echo "        <hr class=\"title\">
        ";
            // line 29
            $this->env->loadTemplate("blocks/block-carusel.twig")->display(array_merge($context, array("data" => $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "collection", array()))));
            // line 30
            echo "    ";
        }
        // line 31
        echo "    <div class=\"row left-padding-1 right-padding-1\">
    ";
        // line 32
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "sub_tree", array())) {
            // line 33
            echo "        <hr class=\"title-child\"/>
        ";
            // line 34
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "sub_tree", array()));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 35
                echo "            <div class=\"col-lg-5 right-margin-3 left-margin-2 height-05\">
                <h2><a class='link-03' href=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_url", array()), "html", null, true);
                echo "detail/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_id", array()), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_name", array()), "html", null, true);
                echo "</a></h2>
                ";
                // line 37
                if ($this->getAttribute($context["item"], "images_id", array())) {
                    // line 38
                    echo "                <center>
                    <a href=\"";
                    // line 39
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_url", array()), "html", null, true);
                    echo "detail/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_id", array()), "html", null, true);
                    echo "\"><img class='img-thumbnail' id='block-image-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                    echo "' src='/Images_module/getImages/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                    echo "/100/100/0'/></a>
                </center>    
                ";
                }
                // line 41
                echo " 
                <p class=\"";
                // line 42
                echo (($this->getAttribute($context["item"], "keys_instock", array())) ? ("text-cl-06") : ("text-cl-07"));
                echo " text-bold para-center\">";
                echo (($this->getAttribute($context["item"], "keys_instock", array())) ? ("В наличии") : ("Нет в наличии"));
                echo "</p>
                <a class='btn-detail' href=\"";
                // line 43
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_url", array()), "html", null, true);
                echo "detail/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "keys_id", array()), "html", null, true);
                echo "\"></a>
            </div>
            ";
                // line 55
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "    ";
        }
        echo "    

    </div>
    <hr class=\"title\"/>    
    ";
        // line 60
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "text", array());
        echo "
</div>
";
        // line 63
        echo "<div class=\"clearfix bottom-margin-0\"></div>";
    }

    public function getTemplateName()
    {
        return "avto/avto-detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  143 => 63,  138 => 60,  130 => 56,  124 => 55,  117 => 43,  111 => 42,  108 => 41,  96 => 39,  93 => 38,  91 => 37,  83 => 36,  80 => 35,  76 => 34,  73 => 33,  71 => 32,  68 => 31,  65 => 30,  63 => 29,  60 => 28,  58 => 27,  55 => 26,  46 => 22,  40 => 21,  34 => 20,  29 => 17,  27 => 16,  23 => 15,  19 => 13,);
    }
}
