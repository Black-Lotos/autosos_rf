<?php

/* site-master-page.twig */
class __TwigTemplate_5d6752d0b2fd17f0b1bb8c10801e707469d627eb414c38da58b41ad5caf42aba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "title", array()), "html", null, true);
        echo "</title>
        ";
        // line 8
        if ($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "keywords", array())) {
            // line 9
            echo "            <meta name=\"keywords\" content=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "keywords", array()), "html", null, true);
            echo "\" />
        ";
        }
        // line 11
        echo "        ";
        if ($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "description", array())) {
            // line 12
            echo "            <meta name=\"description\" content=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "description", array()), "html", null, true);
            echo "\" />
        ";
        }
        // line 13
        echo "    
        
        
        <!-- Bootstrap -->
        <link href=\"/bootstrap/css/bootstrap.css\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"/css/default.css\">
        <link rel=\"stylesheet\" href=\"/css/output-system.css\">
        <link rel=\"stylesheet\" href=\"/css/old-style.css\">
        <link rel=\"stylesheet\" href=\"/css/carusel.css\">
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link href=\"/css/default.css\" rel=\"stylesheet\">
        <script type=\"text/javascript\" src=\"/js/jquery-2.1.3.js\"></script>
        <script type=\"text/javascript\" src=\"/js/ajax_upload/jquery.uploadfile.js\"></script>
        <script type=\"text/javascript\" src=\"/bootstrap/js/bootstrap.js\"></script>
        <script type=\"text/javascript\" src=\"/js/info.common.js\"></script>
        <script type=\"text/javascript\" src=\"/js/info.carusel.js\"></script>
    </head>
    ";
        // line 34
        echo "    <body class=\"bg-black\">
        <div class=\"container work-size\"> <!-- container start -->
            <div class=\"row bg-all-page\">
                ";
        // line 37
        if ((isset($context["sub_page_message"]) ? $context["sub_page_message"] : null)) {
            // line 38
            echo "                    ";
            echo (isset($context["sub_page_message"]) ? $context["sub_page_message"] : null);
            echo "
                ";
        }
        // line 39
        echo "    
                ";
        // line 40
        if ((isset($context["sub_page"]) ? $context["sub_page"] : null)) {
            // line 41
            echo "                    <div class=\"col-sm-3 col-sm-offset-5 top-margin-4\">
                        ";
            // line 42
            echo (isset($context["sub_page"]) ? $context["sub_page"] : null);
            echo "
                    </div>
                ";
        } else {
            // line 45
            echo "                ";
            $context["miSubTree"] = "";
            echo "    
                <div class=\"bg-logo\"><a href='";
            // line 46
            echo $this->env->getExtension('project')->getBaseUrl();
            echo "' class='logo'></a></div>
                <div class=\"col-sm-9 div-header div-left topmenu\">
                <ul>
                    
                    ";
            // line 50
            if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "menu", array())) {
                // line 51
                echo "                        ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "menu", array()));
                foreach ($context['_seq'] as $context["key"] => $context["mi"]) {
                    // line 52
                    echo "                            <li class=\"";
                    echo ((($context["key"] == 0)) ? ("left-no") : (""));
                    echo "\"><a class=\"";
                    echo (($this->getAttribute($context["mi"], "mi_class", array())) ? ($this->getAttribute($context["mi"], "mi_class", array())) : ("button-base orange"));
                    echo " ";
                    echo (($this->getAttribute($context["mi"], "active", array())) ? ("active") : (""));
                    echo "\" href=\"";
                    echo $this->getAttribute($context["mi"], "mi_url", array());
                    echo "\" url=\"";
                    echo $this->getAttribute($context["mi"], "mi_url", array());
                    echo "\">";
                    echo $this->getAttribute($context["mi"], "mi_title_ru", array());
                    echo "</a></li>
                            ";
                    // line 53
                    if (($this->getAttribute($context["mi"], "sub_tree", array()) && ($this->getAttribute($context["mi"], "active", array()) == "active"))) {
                        // line 54
                        echo "                                ";
                        $context["miSubTree"] = $this->getAttribute($context["mi"], "sub_tree", array());
                        // line 55
                        echo "                            ";
                    }
                    // line 56
                    echo "                            
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['mi'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 58
                echo "                    ";
            }
            // line 59
            echo "                    </ul>
                </div>
                <div class=\"col-sm-9 div-left div-top-work\">
                    <div class='row col-left-work'>
                        <div class=\"col-xs-3 cl-left-content\"> 
                            ";
            // line 64
            if ((isset($context["miSubTree"]) ? $context["miSubTree"] : null)) {
                // line 65
                echo "                                <ul class='sub-menu'>
                                ";
                // line 66
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["miSubTree"]) ? $context["miSubTree"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["submi"]) {
                    // line 67
                    echo "                                    <li class=\"";
                    echo $this->getAttribute($context["submi"], "active", array());
                    echo "\"><a href=\"";
                    echo $this->getAttribute($context["submi"], "mi_url", array());
                    echo "\" url=\"";
                    echo $this->getAttribute($context["submi"], "mi_url", array());
                    echo "\" class=\"";
                    echo $this->getAttribute($context["submi"], "active", array());
                    echo "\">";
                    echo $this->getAttribute($context["submi"], "mi_title_ru", array());
                    echo "</a></li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submi'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 69
                echo "                                </ul>
                            ";
            }
            // line 71
            echo "                            ";
            echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "left", array());
            echo "
                        </div>
                        <div class=\"col-xs-9 cl-right-content\">
                            ";
            // line 74
            echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "right", array());
            echo "
                        </div>
                    </div>    
                </div>
                ";
        }
        // line 79
        echo "            </div>
            <div class=\"row\"> <!-- row start -->
                <div class=\"col-sm-12 div-footer-bottom\">
                    ";
        // line 82
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "copyright", array());
        echo "
                </div> <!-- footer-bottom end -->
            </div> <!-- row end -->
        </div>
\t\t
    </body>
    ";
        // line 89
        echo "</html>";
    }

    public function getTemplateName()
    {
        return "site-master-page.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 89,  206 => 82,  201 => 79,  193 => 74,  186 => 71,  182 => 69,  165 => 67,  161 => 66,  158 => 65,  156 => 64,  149 => 59,  146 => 58,  139 => 56,  136 => 55,  133 => 54,  131 => 53,  116 => 52,  111 => 51,  109 => 50,  102 => 46,  97 => 45,  91 => 42,  88 => 41,  86 => 40,  83 => 39,  77 => 38,  75 => 37,  70 => 34,  48 => 13,  42 => 12,  39 => 11,  33 => 9,  31 => 8,  27 => 7,  19 => 1,);
    }
}
