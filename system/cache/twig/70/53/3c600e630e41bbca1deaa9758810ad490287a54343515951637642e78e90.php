<?php

/* blocks/block-02.twig */
class __TwigTemplate_70533c600e630e41bbca1deaa9758810ad490287a54343515951637642e78e90 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 3
            echo "    <div class='block-01'>
        ";
            // line 4
            if ($this->getAttribute((isset($context["property"]) ? $context["property"] : null), "title", array())) {
                // line 5
                echo "            <h2>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["property"]) ? $context["property"] : null), "title", array()), "html", null, true);
                echo "</h2>
        ";
            }
            // line 6
            echo "    
        ";
            // line 7
            if ((isset($context["data"]) ? $context["data"] : null)) {
                // line 8
                echo "            <ul>
                ";
                // line 9
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 10
                    echo "                    <li class=\"text-lg text-height-01 ";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "active", array()), "html", null, true);
                    echo "\">
                        <a href='";
                    // line 11
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true);
                    echo "load/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "' class='ctrl-dec-3'>
                            <img src='/Images_module/getImages/";
                    // line 12
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                    echo "/40/40/1'/>
                        </a>
                        <span class='text-lg  left-padding-px-1'><a href='";
                    // line 14
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true);
                    echo "load/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "'>";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</a></span>
                    </li>
                    ";
                    // line 16
                    if ($this->getAttribute($context["item"], "sub_tree", array())) {
                        // line 17
                        echo "                        <ul>
                        ";
                        // line 18
                        $context['_parent'] = (array) $context;
                        $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "sub_tree", array()));
                        foreach ($context['_seq'] as $context["_key"] => $context["subitem"]) {
                            // line 19
                            echo "                            <li>
                                <a href='";
                            // line 20
                            echo twig_escape_filter($this->env, $this->getAttribute($context["subitem"], "url", array()), "html", null, true);
                            echo "'>";
                            echo twig_escape_filter($this->env, $this->getAttribute($context["subitem"], "title", array()), "html", null, true);
                            echo "</a>
                            </li>
                        ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subitem'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 23
                        echo "                        </ul>
                    ";
                    }
                    // line 25
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 26
                echo "            </ul>
        ";
            }
            // line 28
            echo "    </div>
";
        } else {
            // line 30
            echo "    
";
        }
    }

    public function getTemplateName()
    {
        return "blocks/block-02.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 30,  106 => 28,  102 => 26,  96 => 25,  92 => 23,  81 => 20,  78 => 19,  74 => 18,  71 => 17,  69 => 16,  60 => 14,  55 => 12,  49 => 11,  44 => 10,  40 => 9,  37 => 8,  35 => 7,  32 => 6,  26 => 5,  24 => 4,  21 => 3,  19 => 2,);
    }
}
