<?php

/* blocks/block-build.twig */
class __TwigTemplate_71a8259bcb473fe88e0a9c8bf5a8d5abbd44bcb2f398a72175dcc888ef93e87d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "    ";
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 4
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 5
                echo "            ";
                echo $context["item"];
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "    ";
        }
        // line 9
        echo "<div class=\"clearfix bottom-margin-0\"></div>
";
    }

    public function getTemplateName()
    {
        return "blocks/block-build.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  36 => 7,  27 => 5,  22 => 4,  19 => 3,);
    }
}
