<?php

/* avto/keys-detail.twig */
class __TwigTemplate_724d52047415a4234ef36a0095b0385089b8ed860bc5a760f4a4a7275d9271c9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 13
        echo "<div class=\"avto\">
    <h2>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_name", array()), "html", null, true);
        echo "</h2>
    <hr class=\"title\">
    ";
        // line 16
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "images_id", array())) {
            // line 17
            echo "        <img class='img-thumbnail' id='block-image-";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "images_id", array()), "html", null, true);
            echo "' src='/Images_module/getImages/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "images_id", array()), "html", null, true);
            echo "/680/400/1'/>
    ";
        }
        // line 19
        echo "    <div>
        <h3>Характеристики:</h3>
        <div>
            <table class=\"key-specification\">
                <tr>
                    ";
        // line 24
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_mhz", array()), "title", array())) {
            // line 25
            echo "                        <th>Частота</th>
                    ";
        }
        // line 27
        echo "                    ";
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_sting", array()), "title", array())) {
            // line 28
            echo "                        <th>Жало ключа</th>
                    ";
        }
        // line 30
        echo "                    ";
        if ($this->env->getExtension('project')->getVariable("keys_show_price")) {
            // line 31
            echo "                        <th>Цена ключа</th>
                    ";
        }
        // line 32
        echo "    
                    <th>Наличие</th>
                </tr>
                <tr>
                    ";
        // line 36
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_mhz", array()), "title", array())) {
            // line 37
            echo "                        <td class=\"centers\">";
            echo $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_mhz", array()), "title", array());
            echo "&nbsp;Mhz</td>
                    ";
        }
        // line 39
        echo "                    ";
        if ($this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_sting", array()), "title", array())) {
            // line 40
            echo "                        <td class=\"centers\">";
            echo $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_sting", array()), "title", array());
            echo "</td>
                    ";
        }
        // line 41
        echo "    
                    ";
        // line 42
        if ($this->env->getExtension('project')->getVariable("keys_show_price")) {
            // line 43
            echo "                        <td class=\"centers\">";
            echo twig_escape_filter($this->env, ((($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_price", array()) > 0)) ? ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_price", array())) : ("Уточнять по телефону")), "html", null, true);
            echo "</td>
                    ";
        }
        // line 45
        echo "                    <td class=\"";
        echo (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_instock", array())) ? ("text-cl-06") : ("text-cl-07"));
        echo " text-bold centers\">";
        echo (($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_instock", array())) ? ("В наличии") : ("Нет в наличии"));
        echo "</td>
                </tr>
            </table>
        </div>
    </div>
    <div class=\"clearfix\"></div>
    <div>
        <h3>Описание:</h3>
        ";
        // line 53
        echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "keys_description", array());
        echo "
    </div>
</div>
";
        // line 57
        echo "<div class=\"clearfix\"></div>
";
    }

    public function getTemplateName()
    {
        return "avto/keys-detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 57,  112 => 53,  98 => 45,  92 => 43,  90 => 42,  87 => 41,  81 => 40,  78 => 39,  72 => 37,  70 => 36,  64 => 32,  60 => 31,  57 => 30,  53 => 28,  50 => 27,  46 => 25,  44 => 24,  37 => 19,  29 => 17,  27 => 16,  22 => 14,  19 => 13,);
    }
}
