<?php

/* administration/admin-master-page.twig */
class __TwigTemplate_7d67730cadefd5bc4994a6a0df53ae34fc95ebc2ac7f462232c4f1e0df4a575a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "title", array()), "html", null, true);
        echo "</title>
        <!-- Bootstrap -->
        <link href=\"/bootstrap/css/bootstrap.css\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"/css/default.css\">
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link href=\"/css/default.css\" rel=\"stylesheet\">
        <script type=\"text/javascript\" src=\"/js/jquery-2.1.3.js\"></script>
        <script type=\"text/javascript\" src=\"/bootstrap/js/bootstrap.js\"></script>
    </head>
    ";
        // line 20
        echo "    <body class=\"bg-white\">
        <div class=\"container work-size\"> <!-- container start -->
            <div class=\"row bg-all-page-admin\">
                ";
        // line 23
        if ((isset($context["sub_page_message"]) ? $context["sub_page_message"] : null)) {
            // line 24
            echo "                    ";
            echo (isset($context["sub_page_message"]) ? $context["sub_page_message"] : null);
            echo "
                ";
        }
        // line 25
        echo "    
                ";
        // line 26
        if ((isset($context["sub_page"]) ? $context["sub_page"] : null)) {
            // line 27
            echo "                    <div class=\"col-sm-5 col-sm-offset-4 top-margin-4\">
                        ";
            // line 28
            echo (isset($context["sub_page"]) ? $context["sub_page"] : null);
            echo "
                    </div>
                ";
        } else {
            // line 31
            echo "                <div class=\"col-sm-9 div-header div-left\">
                    ";
            // line 32
            if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "menu", array())) {
                // line 33
                echo "                        ";
                $context['_parent'] = (array) $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "menu", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["mi"]) {
                    // line 34
                    echo "                            <a class=\"";
                    echo (($this->getAttribute($context["mi"], "mi_class", array())) ? ($this->getAttribute($context["mi"], "mi_class", array())) : ("button-base orange"));
                    echo " ";
                    echo (($this->getAttribute($context["mi"], "active", array())) ? ("active") : (""));
                    echo "\" href=\"";
                    echo $this->getAttribute($context["mi"], "mi_url", array());
                    echo "\" url=\"";
                    echo $this->getAttribute($context["mi"], "mi_url", array());
                    echo "\">";
                    echo $this->getAttribute($context["mi"], "mi_title_ru", array());
                    echo "</a>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mi'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 36
                echo "                    ";
            }
            // line 37
            echo "                </div>
                <div class=\"col-sm-9 div-left div-top-work\">
                    <div class='row'>
                        <div class=\"col-xs-3 cl-left-content\">
                            ";
            // line 41
            echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "left", array());
            echo "
                        </div>
                        <div class=\"col-xs-9\">
                            ";
            // line 44
            echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "right", array());
            echo "
                        </div>
                    </div>    
                </div>
                ";
        }
        // line 49
        echo "            </div>
            <div class=\"row\"> <!-- row start -->
                <div class=\"col-sm-12 div-footer-bottom\">
                    ";
        // line 52
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "copyright", array());
        echo "
                </div> <!-- footer-bottom end -->
            </div> <!-- row end -->
        </div>
    </body>
    ";
        // line 58
        echo "</html>";
    }

    public function getTemplateName()
    {
        return "administration/admin-master-page.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 58,  124 => 52,  119 => 49,  111 => 44,  105 => 41,  99 => 37,  96 => 36,  79 => 34,  74 => 33,  72 => 32,  69 => 31,  63 => 28,  60 => 27,  58 => 26,  55 => 25,  49 => 24,  47 => 23,  42 => 20,  27 => 7,  19 => 1,);
    }
}
