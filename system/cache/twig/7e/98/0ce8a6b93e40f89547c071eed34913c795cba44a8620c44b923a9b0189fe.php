<?php

/* blocks/block-01.twig */
class __TwigTemplate_7e980ce8a6b93e40f89547c071eed34913c795cba44a8620c44b923a9b0189fe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 3
            echo "    <div class='block-01'>
        ";
            // line 4
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 5
                echo "            <h2>";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                echo "</h2>
            ";
                // line 6
                if ($this->getAttribute($context["item"], "sub_tree", array())) {
                    // line 7
                    echo "                <ul>
                ";
                    // line 8
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "sub_tree", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["subitem"]) {
                        // line 9
                        echo "                    <li><a href='";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["subitem"], "url", array()), "html", null, true);
                        echo "'>";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["subitem"], "title", array()), "html", null, true);
                        echo "</a></li>
                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subitem'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 11
                    echo "                </ul>
            ";
                }
                // line 13
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 14
            echo "    </div>
";
        } else {
            // line 16
            echo "    
";
        }
    }

    public function getTemplateName()
    {
        return "blocks/block-01.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 16,  63 => 14,  57 => 13,  53 => 11,  42 => 9,  38 => 8,  35 => 7,  33 => 6,  28 => 5,  24 => 4,  21 => 3,  19 => 2,);
    }
}
