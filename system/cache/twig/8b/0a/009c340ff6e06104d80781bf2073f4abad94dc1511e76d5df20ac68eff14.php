<?php

/* common/search/keys-list-search.twig */
class __TwigTemplate_8b0a009c340ff6e06104d80781bf2073f4abad94dc1511e76d5df20ac68eff14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class='block-search'>
    ";
        // line 2
        if ((isset($context["title"]) ? $context["title"] : null)) {
            // line 3
            echo "        <h2>";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</h2>
    ";
        }
        // line 4
        echo "  
    ";
        // line 5
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 6
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 7
                echo "            <div class=\"ResultLeft\">
            <center>    
            ";
                // line 9
                if ($this->getAttribute($context["item"], "value", array())) {
                    // line 10
                    echo "                
                ";
                    // line 12
                    echo "                <span class=\"title\"><a id='detail-news-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "' href=\"/page_keys/detail/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</a></span>
                <center>
                ";
                    // line 14
                    if ($this->getAttribute($context["item"], "images_id", array())) {
                        // line 15
                        echo "                    <img class='xs' src=\"/Images_module/getImages/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                        echo "/215/150/1\" alt=\"\"/>
                ";
                    }
                    // line 17
                    echo "                </center>
                ";
                    // line 19
                    echo "                ";
                    // line 20
                    echo "            ";
                } else {
                    echo "    
                <h2>";
                    // line 21
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</h2>
            ";
                }
                // line 23
                echo "            </center>

            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "    ";
        }
        // line 28
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "common/search/keys-list-search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 28,  91 => 27,  82 => 23,  77 => 21,  72 => 20,  70 => 19,  67 => 17,  61 => 15,  59 => 14,  49 => 12,  46 => 10,  44 => 9,  40 => 7,  35 => 6,  33 => 5,  30 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
