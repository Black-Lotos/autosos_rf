<?php

/* blocks/block-model-start-up.twig */
class __TwigTemplate_912f2ef78bcd62c3df6d27c5b40edf883d9cec59b35ed1f3187db9ccbcf5f981 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "    ";
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 4
            echo "        <div class=\"row padding-position-01\">
        ";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 6
                echo "            <div class='avto-model'>
                <h2>";
                // line 7
                echo $this->getAttribute($context["item"], "title", array());
                echo "</h2>
\t\t\t\t<hr class='title' />
                ";
                // line 9
                if ($this->getAttribute($context["item"], "sub_tree", array())) {
                    // line 10
                    echo "                    ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["item"], "sub_tree", array()));
                    foreach ($context['_seq'] as $context["_key"] => $context["sub_item"]) {
                        // line 11
                        echo "                    <div class='element-info'>
                        ";
                        // line 12
                        if ($this->getAttribute($context["sub_item"], "images_id", array())) {
                            // line 13
                            echo "                            <a href=\"/page_car/detail_avto/";
                            echo $this->getAttribute($context["sub_item"], "value", array());
                            echo "\">
                                <img src='/Images_module/getImages/";
                            // line 14
                            echo $this->getAttribute($context["sub_item"], "images_id", array());
                            echo "/200/200/1'/>
                            </a>
                        ";
                        }
                        // line 16
                        echo "    
                        <span class='title'>
                            <a id=\"detail-avto-";
                        // line 18
                        echo $this->getAttribute($context["sub_item"], "value", array());
                        echo "\" class=\"link-02\" href=\"/page_car/detail_avto/";
                        echo $this->getAttribute($context["sub_item"], "value", array());
                        echo "\">";
                        echo $this->getAttribute($context["sub_item"], "title", array());
                        echo "</a>
                        </span>
                        
                        <span>";
                        // line 21
                        echo $this->getAttribute($context["sub_item"], "year", array());
                        echo "</span>
                    </div>    
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 24
                    echo "                ";
                }
                // line 25
                echo "            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "    
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "blocks/block-model-start-up.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 26,  86 => 25,  83 => 24,  74 => 21,  64 => 18,  60 => 16,  54 => 14,  49 => 13,  47 => 12,  44 => 11,  39 => 10,  37 => 9,  32 => 7,  29 => 6,  25 => 5,  22 => 4,  19 => 3,);
    }
}
