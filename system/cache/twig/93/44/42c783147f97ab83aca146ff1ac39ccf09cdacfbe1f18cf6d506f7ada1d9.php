<?php

/* administration/common/list-system-tree.twig */
class __TwigTemplate_934442c783147f97ab83aca146ff1ac39ccf09cdacfbe1f18cf6d506f7ada1d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "    <div class='tree-out'>
    ";
        // line 3
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "data", array())) {
            // line 4
            echo "        <ul>
        ";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "data", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 6
                echo "            <li class='root ";
                echo (($this->getAttribute($context["row"], "sub_tree", array())) ? ("open") : ("close"));
                echo "'>
                <div class='title'>";
                // line 7
                echo $this->getAttribute($context["row"], "title", array());
                echo "</div>
                <div class=\"action\">";
                // line 8
                echo (($this->getAttribute($context["row"], "action", array())) ? ($this->getAttribute($context["row"], "action", array())) : (""));
                echo "</div>
            </li>
            ";
                // line 10
                if ($this->getAttribute($context["row"], "sub_tree", array())) {
                    // line 11
                    echo "                <ul>
                    ";
                    // line 12
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["row"], "sub_tree", array()));
                    foreach ($context['_seq'] as $context["key"] => $context["sub_row"]) {
                        // line 13
                        echo "                        <li class='level-01 ";
                        echo (((($context["key"] % 2) == 0)) ? ("text-cl-odd") : ("text-cl-even"));
                        echo " ";
                        echo (($this->getAttribute($context["sub_row"], "sub_tree", array())) ? ("") : (""));
                        echo "'>
                            <div class='title left-padding-px-6'>";
                        // line 14
                        echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["sub_row"], "title", array()), 60);
                        echo "</div>
                            <div class=\"action\">";
                        // line 15
                        echo (($this->getAttribute($context["sub_row"], "action", array())) ? ($this->getAttribute($context["sub_row"], "action", array())) : ("нет"));
                        echo "</div>
                        </li>
                        ";
                        // line 17
                        if ($this->getAttribute($context["sub_row"], "sub_tree", array())) {
                            // line 18
                            echo "                            <ul>
                                ";
                            // line 19
                            $context['_parent'] = (array) $context;
                            $context['_seq'] = twig_ensure_traversable($this->getAttribute($context["sub_row"], "sub_tree", array()));
                            foreach ($context['_seq'] as $context["sub_row_key"] => $context["sub_row_item"]) {
                                // line 20
                                echo "                                    <li class='level-02 ";
                                echo (((($context["sub_row_key"] % 2) == 0)) ? ("text-cl-odd") : ("text-cl-even"));
                                echo "'>
                                            ";
                                // line 21
                                if ($this->getAttribute($this->getAttribute($context["sub_row_item"], "sub_tree", array()), "child", array())) {
                                    // line 22
                                    echo "                                                <div class='title left-padding-px-9'><a href='";
                                    echo $this->getAttribute($this->getAttribute($context["sub_row_item"], "sub_tree", array()), "child", array());
                                    echo "' class='link-04' title='";
                                    echo $this->getAttribute($this->getAttribute($context["sub_row_item"], "sub_tree", array()), "caption", array());
                                    echo "'>";
                                    echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["sub_row_item"], "title", array()), 60);
                                    echo "</a></div>
                                            ";
                                } else {
                                    // line 24
                                    echo "                                                <div class='title left-padding-px-9'>";
                                    echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["sub_row_item"], "title", array()), 60);
                                    echo "</div>
                                            ";
                                }
                                // line 26
                                echo "                                            <div class=\"action\">";
                                echo (($this->getAttribute($context["sub_row_item"], "action", array())) ? ($this->getAttribute($context["sub_row_item"], "action", array())) : ("нет"));
                                echo "</div>
                                    </li>
                                ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['sub_row_key'], $context['sub_row_item'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 29
                            echo "                            </ul>
                        ";
                        }
                        // line 31
                        echo "                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['key'], $context['sub_row'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 32
                    echo "                </ul>    
            ";
                }
                // line 34
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "        </ul>
    ";
        } else {
            // line 36
            echo "    
        <ul>
            <li class=''>Элементы отсутствуют. Необходимо провести добавление.<div class=\"\">";
            // line 38
            echo (($this->getAttribute((isset($context["row"]) ? $context["row"] : null), "action", array())) ? ($this->getAttribute((isset($context["row"]) ? $context["row"] : null), "action", array())) : (""));
            echo "</div></li>
            ";
            // line 40
            echo "        </ul>
    ";
        }
        // line 42
        echo "    </div>
    ";
        // line 43
        if ($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "count", array())) {
            // line 44
            echo "        <nav class='pagination'>
            ";
            // line 45
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "count", array())));
            foreach ($context['_seq'] as $context["_key"] => $context["oi"]) {
                // line 46
                echo "                ";
                // line 47
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "        </nav>
    ";
        }
        // line 50
        echo "
";
        // line 51
        echo (isset($context["sub_page_message"]) ? $context["sub_page_message"] : null);
        echo "    
";
        // line 53
        echo "<script type=\"text/javascript\" src=\"/js/";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "setup_page", array()), "property", array()), "include_js", array()), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\" src=\"/js/info.common.js\"></script>";
    }

    public function getTemplateName()
    {
        return "administration/common/list-system-tree.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  179 => 53,  175 => 51,  172 => 50,  168 => 48,  162 => 47,  160 => 46,  156 => 45,  153 => 44,  151 => 43,  148 => 42,  144 => 40,  140 => 38,  136 => 36,  132 => 35,  126 => 34,  122 => 32,  116 => 31,  112 => 29,  102 => 26,  96 => 24,  86 => 22,  84 => 21,  79 => 20,  75 => 19,  72 => 18,  70 => 17,  65 => 15,  61 => 14,  54 => 13,  50 => 12,  47 => 11,  45 => 10,  40 => 8,  36 => 7,  31 => 6,  27 => 5,  24 => 4,  22 => 3,  19 => 2,);
    }
}
