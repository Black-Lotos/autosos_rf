<?php

/* common/search/keys-list-search.twig */
class __TwigTemplate_cdc96ffea32b64196c10ae7df016e9e4738a22d6cc76e446cfa3027b82379e19 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class='block-search'>
    ";
        // line 2
        if ((isset($context["title"]) ? $context["title"] : null)) {
            // line 3
            echo "        <h2>";
            echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
            echo "</h2>
    ";
        }
        // line 4
        echo "  
    ";
        // line 5
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 6
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 7
                echo "            <div class=\"ResultLeft\">
            <center>    
            ";
                // line 9
                if ($this->getAttribute($context["item"], "value", array())) {
                    // line 10
                    echo "                
                ";
                    // line 12
                    echo "                <span class=\"title\"><a id='detail-news-";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "' href=\"/page_keys/detail/";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</a></span>
                ";
                    // line 13
                    if ($this->getAttribute($context["item"], "images_id", array())) {
                        // line 14
                        echo "                    <img class='xs' src=\"/Images_module/getImages/";
                        echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                        echo "/215/150/1\" alt=\"\"/>
                ";
                    }
                    // line 16
                    echo "                <span class=\"about\">";
                    echo $this->env->getExtension('project')->writeCuttingText($this->getAttribute($context["item"], "note", array()), 400);
                    echo "</span>
                ";
                    // line 18
                    echo "            ";
                } else {
                    echo "    
                <h2>";
                    // line 19
                    echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                    echo "</h2>
            ";
                }
                // line 21
                echo "            </center>

            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "    ";
        }
        // line 26
        echo "    <center>
        <ul class=\"pagination\">
            <li><a href=\"&page=1\">1</a></li>
            <li><a href=\"&page=2\">2</a></li>
            <li><a href=\"&page=3\">3</a></li>
            <li><a href=\"&page=4\">4</a></li>
            <li><a href=\"&page=5\">5</a></li>
        </ul>
    </center>    
</div>
";
    }

    public function getTemplateName()
    {
        return "common/search/keys-list-search.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 26,  90 => 25,  81 => 21,  76 => 19,  71 => 18,  66 => 16,  60 => 14,  58 => 13,  49 => 12,  46 => 10,  44 => 9,  40 => 7,  35 => 6,  33 => 5,  30 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
