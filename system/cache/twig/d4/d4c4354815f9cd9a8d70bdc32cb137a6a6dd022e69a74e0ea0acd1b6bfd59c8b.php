<?php

/* welcome_message.twig */
class __TwigTemplate_3a5142f6a3f3a8a27e32dce1e26405f50c4bd5614a6c9d10b5088f069de984c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang=\"en\">
<head>
\t<meta charset=\"utf-8\">
\t<title>Welcome to CodeIgniter</title>

\t<style type=\"text/css\">

\t::selection { background-color: #E13300; color: white; }
\t::-moz-selection { background-color: #E13300; color: white; }

\tbody {
\t\tbackground-color: #fff;
\t\tmargin: 40px;
\t\tfont: 13px/20px normal Helvetica, Arial, sans-serif;
\t\tcolor: #4F5155;
\t}

\ta {
\t\tcolor: #003399;
\t\tbackground-color: transparent;
\t\tfont-weight: normal;
\t}

\th1 {
\t\tcolor: #444;
\t\tbackground-color: transparent;
\t\tborder-bottom: 1px solid #D0D0D0;
\t\tfont-size: 19px;
\t\tfont-weight: normal;
\t\tmargin: 0 0 14px 0;
\t\tpadding: 14px 15px 10px 15px;
\t}

\tcode {
\t\tfont-family: Consolas, Monaco, Courier New, Courier, monospace;
\t\tfont-size: 12px;
\t\tbackground-color: #f9f9f9;
\t\tborder: 1px solid #D0D0D0;
\t\tcolor: #002166;
\t\tdisplay: block;
\t\tmargin: 14px 0 14px 0;
\t\tpadding: 12px 10px 12px 10px;
\t}

\t#body {
\t\tmargin: 0 15px 0 15px;
\t}

\tp.footer {
\t\ttext-align: right;
\t\tfont-size: 11px;
\t\tborder-top: 1px solid #D0D0D0;
\t\tline-height: 32px;
\t\tpadding: 0 10px 0 10px;
\t\tmargin: 20px 0 0 0;
\t}

\t#container {
\t\tmargin: 10px;
\t\tborder: 1px solid #D0D0D0;
\t\tbox-shadow: 0 0 8px #D0D0D0;
\t}
\t</style>
</head>
<body>

<div id=\"container\">
\t<h1>Welcome to CodeIgniter!</h1>

\t<div id=\"body\">
\t\t<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>

\t\t<p>If you would like to edit this page you'll find it located at:</p>
\t\t<code>application/views/welcome_message.php</code>

\t\t<p>The corresponding controller for this page is found at:</p>
\t\t<code>application/controllers/Welcome.php</code>

\t\t<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href=\"user_guide/\">User Guide</a>.</p>
\t</div>

\t<p class=\"footer\">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
        
</div>

</body>
</html>";
    }

    public function getTemplateName()
    {
        return "welcome_message.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <?php*/
/* defined('BASEPATH') OR exit('No direct script access allowed');*/
/* ?><!DOCTYPE html>*/
/* <html lang="en">*/
/* <head>*/
/* 	<meta charset="utf-8">*/
/* 	<title>Welcome to CodeIgniter</title>*/
/* */
/* 	<style type="text/css">*/
/* */
/* 	::selection { background-color: #E13300; color: white; }*/
/* 	::-moz-selection { background-color: #E13300; color: white; }*/
/* */
/* 	body {*/
/* 		background-color: #fff;*/
/* 		margin: 40px;*/
/* 		font: 13px/20px normal Helvetica, Arial, sans-serif;*/
/* 		color: #4F5155;*/
/* 	}*/
/* */
/* 	a {*/
/* 		color: #003399;*/
/* 		background-color: transparent;*/
/* 		font-weight: normal;*/
/* 	}*/
/* */
/* 	h1 {*/
/* 		color: #444;*/
/* 		background-color: transparent;*/
/* 		border-bottom: 1px solid #D0D0D0;*/
/* 		font-size: 19px;*/
/* 		font-weight: normal;*/
/* 		margin: 0 0 14px 0;*/
/* 		padding: 14px 15px 10px 15px;*/
/* 	}*/
/* */
/* 	code {*/
/* 		font-family: Consolas, Monaco, Courier New, Courier, monospace;*/
/* 		font-size: 12px;*/
/* 		background-color: #f9f9f9;*/
/* 		border: 1px solid #D0D0D0;*/
/* 		color: #002166;*/
/* 		display: block;*/
/* 		margin: 14px 0 14px 0;*/
/* 		padding: 12px 10px 12px 10px;*/
/* 	}*/
/* */
/* 	#body {*/
/* 		margin: 0 15px 0 15px;*/
/* 	}*/
/* */
/* 	p.footer {*/
/* 		text-align: right;*/
/* 		font-size: 11px;*/
/* 		border-top: 1px solid #D0D0D0;*/
/* 		line-height: 32px;*/
/* 		padding: 0 10px 0 10px;*/
/* 		margin: 20px 0 0 0;*/
/* 	}*/
/* */
/* 	#container {*/
/* 		margin: 10px;*/
/* 		border: 1px solid #D0D0D0;*/
/* 		box-shadow: 0 0 8px #D0D0D0;*/
/* 	}*/
/* 	</style>*/
/* </head>*/
/* <body>*/
/* */
/* <div id="container">*/
/* 	<h1>Welcome to CodeIgniter!</h1>*/
/* */
/* 	<div id="body">*/
/* 		<p>The page you are looking at is being generated dynamically by CodeIgniter.</p>*/
/* */
/* 		<p>If you would like to edit this page you'll find it located at:</p>*/
/* 		<code>application/views/welcome_message.php</code>*/
/* */
/* 		<p>The corresponding controller for this page is found at:</p>*/
/* 		<code>application/controllers/Welcome.php</code>*/
/* */
/* 		<p>If you are exploring CodeIgniter for the very first time, you should start by reading the <a href="user_guide/">User Guide</a>.</p>*/
/* 	</div>*/
/* */
/* 	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>*/
/*         */
/* </div>*/
/* */
/* </body>*/
/* </html>*/
