<?php

/* avto/list-car-start-up.twig */
class __TwigTemplate_e8531dd3cfb62e4388825eb1cb616874a8d5870acc0aac19cee2aea4ae813dd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 3
            echo "    <center>
    <div class=\"media\">
    ";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["key"] => $context["item"]) {
                // line 6
                echo "        <div class=\"media-left media-middle img-thumbnail\">
            <a href=\"";
                // line 7
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "url", array()), "html", null, true);
                echo "load/";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "value", array()), "html", null, true);
                echo "\">
                <img class=\"media-object\" src=\"/Images_module/getImages/";
                // line 8
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "images_id", array()), "html", null, true);
                echo "/64/64/1\" alt=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, $this->getAttribute($context["item"], "title", array()), "html", null, true);
                echo "\">
            </a>
        </div>
        ";
                // line 11
                if (((($context["key"] + 1) % 8) == 0)) {
                    // line 12
                    echo "            </div>
            <div class=\"media\">
        ";
                }
                // line 14
                echo "    
        ";
                // line 19
                echo "        
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "    </div>
    </center>
";
        }
    }

    public function getTemplateName()
    {
        return "avto/list-car-start-up.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 21,  58 => 19,  55 => 14,  50 => 12,  48 => 11,  38 => 8,  32 => 7,  29 => 6,  25 => 5,  21 => 3,  19 => 2,);
    }
}
