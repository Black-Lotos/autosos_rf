<?php

/* blocks/block-build.twig */
class __TwigTemplate_e95640f961c3e9fea5b48a85f83dcecc09bb7dec171e7daac2f6272f30537c91 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 3
        echo "    ";
        if ((isset($context["data"]) ? $context["data"] : null)) {
            // line 4
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 5
                echo "            ";
                echo $context["item"];
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "    ";
        }
        // line 8
        echo "    ";
        if (($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "per_to_page", array()) > 0)) {
            // line 9
            echo "    <center>
        <ul class=\"pagination\">
            ";
            // line 11
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable(range(1, intval(floor(($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "count", array()) / $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "per_to_page", array()))))));
            foreach ($context['_seq'] as $context["_key"] => $context["oi"]) {
                // line 12
                echo "                <li ";
                echo ((($context["oi"] == $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "current", array()))) ? ("class=active") : (""));
                echo "><a href=";
                echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "url_link", array());
                echo "&page=";
                echo $context["oi"];
                echo ">";
                echo $context["oi"];
                echo "</a></li>
                ";
                // line 14
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['oi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "        </ul>
    </center>        
    ";
        }
        // line 19
        echo "<div class=\"clearfix bottom-margin-0\"></div>
";
    }

    public function getTemplateName()
    {
        return "blocks/block-build.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 19,  67 => 15,  61 => 14,  50 => 12,  46 => 11,  42 => 9,  39 => 8,  36 => 7,  27 => 5,  22 => 4,  19 => 3,);
    }
}
