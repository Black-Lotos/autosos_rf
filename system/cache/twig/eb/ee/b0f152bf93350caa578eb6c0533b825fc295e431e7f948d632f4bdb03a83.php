<?php

/* admin-master-page.twig */
class __TwigTemplate_ebeeb0f152bf93350caa578eb6c0533b825fc295e431e7f948d632f4bdb03a83 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "property", array()), "title", array()), "html", null, true);
        echo "</title>
        <!-- Bootstrap -->
        <link href=\"/bootstrap/css/bootstrap.css\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"/css/default.css\">
        <link rel=\"stylesheet\" href=\"/css/output-system.css\">
        <link rel=\"stylesheet\" href=\"/bootstrap-fileinput-master/css/fileinput.css\">
        <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
        <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
        <![endif]-->
        <link href=\"/css/default.css\" rel=\"stylesheet\">
        <script type=\"text/javascript\" src=\"/js/jquery-2.1.3.js\"></script>
        <script type=\"text/javascript\" src=\"/js/ajax_upload/jquery.uploadfile.js\"></script>
        <script type=\"text/javascript\" src=\"/bootstrap/js/bootstrap.js\"></script>
        <script type=\"text/javascript\" src=\"/js/info.common.js\"></script>
        <script type=\"text/javascript\" src=\"/bootstrap-fileinput-master/js/fileinput.js\"></script>
        <script type=\"text/javascript\" src=\"/bootstrap-fileinput-master/js/fileinput_locale_ru.js\"></script>
        
    </head>
    ";
        // line 27
        echo "    ";
        $context["miSubTree"] = $this->env->getExtension('project')->initArray();
        // line 28
        echo "    <body class=\"bg-white\">
        <div class='div-header-admin'>
            ";
        // line 30
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "menu", array())) {
            // line 31
            echo "                <div class=\"bs-example bs-navbar-top-example\">
                    <nav class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">
                        <!-- We use the fluid option here to avoid overriding the fixed width of a normal container within the narrow content columns. -->
                        <div class=\"container-fluid\">
                            <div class=\"navbar-header\">
                              <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-6\">
                                <span class=\"sr-only\">Toggle navigation</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                              </button>
                              <a class=\"navbar-brand\" href=\"#\"></a>
                            </div>
                        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-6\">
                            <ul class=\"nav navbar-nav\">
                                ";
            // line 46
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "menu", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["mi"]) {
                // line 47
                echo "                                    <li class=\"";
                echo (($this->getAttribute($context["mi"], "mi_class", array())) ? ($this->getAttribute($context["mi"], "mi_class", array())) : ("button-base orange"));
                echo " ";
                echo (($this->getAttribute($context["mi"], "active", array())) ? ("active") : (""));
                echo "\"><a class=\"\" href=\"";
                echo $this->getAttribute($context["mi"], "mi_url", array());
                echo "\" url=\"";
                echo $this->getAttribute($context["mi"], "mi_url", array());
                echo "\">";
                echo $this->getAttribute($context["mi"], "mi_title_ru", array());
                echo "</a></li>
                                    ";
                // line 48
                if (($this->getAttribute($context["mi"], "sub_tree", array()) && ($this->getAttribute($context["mi"], "active", array()) == "active"))) {
                    // line 49
                    echo "                                        ";
                    $context["miSubTree"] = $this->getAttribute($context["mi"], "sub_tree", array());
                    // line 50
                    echo "                                    ";
                }
                // line 51
                echo "                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['mi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 52
            echo "                            </ul>
                        </div><!-- /.navbar-collapse -->
                      </div>
                    </nav>
                  </div>
            ";
        }
        // line 58
        echo "        </div>
        <div class='row top-margin-px-2 width-size-10 div-center'>
            <div class=\"col-xs-3\"> 
                <div class='top-margin-px-8'>
                ";
        // line 62
        if ((isset($context["miSubTree"]) ? $context["miSubTree"] : null)) {
            // line 64
            echo "                    ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["miSubTree"]) ? $context["miSubTree"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["submi"]) {
                // line 65
                echo "                        ";
                // line 66
                echo "                        <a class='btn btn-sm btn-primary btn-block btn-text-color-01 ";
                echo $this->getAttribute($context["submi"], "active", array());
                echo "' href=\"";
                echo $this->getAttribute($context["submi"], "mi_url", array());
                echo "\" url=\"";
                echo $this->getAttribute($context["submi"], "mi_url", array());
                echo "\" class=\"";
                echo $this->getAttribute($context["submi"], "active", array());
                echo "\">";
                echo $this->getAttribute($context["submi"], "mi_title_ru", array());
                echo "</a>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['submi'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "                    ";
            // line 69
            echo "                ";
        }
        // line 70
        echo "                ";
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "left", array());
        echo "
                </div>
            </div>
            <div class=\"col-xs-9\">
                <div class='top-margin-px-1'>
                    ";
        // line 75
        echo $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "right", array());
        echo "
                </div>
            </div>
        </div>
    </body>
    ";
        // line 81
        echo "</html>";
    }

    public function getTemplateName()
    {
        return "admin-master-page.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 81,  160 => 75,  151 => 70,  148 => 69,  146 => 68,  129 => 66,  127 => 65,  122 => 64,  120 => 62,  114 => 58,  106 => 52,  100 => 51,  97 => 50,  94 => 49,  92 => 48,  79 => 47,  75 => 46,  58 => 31,  56 => 30,  52 => 28,  49 => 27,  27 => 7,  19 => 1,);
    }
}
