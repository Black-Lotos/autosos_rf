<?php

/* blocks/blocks-build-page.twig */
class __TwigTemplate_ed092aa7f947bf0c1e42393784bca8b3d1da615d41f893d011c6d0c6ff14a53c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "<div class='row panel-containter-content'>
    ";
        // line 4
        echo "    <h1>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "header_page", array()), "html", null, true);
        echo "</h1>
    ";
        // line 6
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "top_message_page", array()), "html", null, true);
        echo "</h2>
    <hr/>
    ";
        // line 9
        echo "    ";
        if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content", array())) {
            // line 10
            echo "        ";
            echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content", array());
            echo "
    ";
        }
        // line 12
        echo "    <h3>";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "footer_page", array()), "html", null, true);
        echo "</h3>
</div>
";
    }

    public function getTemplateName()
    {
        return "blocks/blocks-build-page.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 12,  36 => 10,  33 => 9,  27 => 6,  22 => 4,  19 => 2,);
    }
}
